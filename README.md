# RODOS

## How to Begin

For setup and building RODOS see [the wiki](https://movewiki.lrt.lrg.tum.de/index.php/DOSIS/firmware/doc/guides/01_getting_started). Here the DOSIS examples are build, which contain pure RODOS applications.

Use the Virtual Machine (Install VirtualBox) for easy access (Your WARR username and password is needed): [VirtualBox Image](https://data.warr.de/01_Allgemein/Rodos/VirtualBoxVM/)

The directory structure of the current rodos distribution ist divided in following dirs:

* **api**   api of rodos
* **src**   implementations for many differnt hardware platforms
* **doc**   documentation
* **support**   programs and libs often used in space applications
* **test-suite**  Tests for automated execution
* **tutorials**   Examples, for you

To begin you will need first the api and the tutorials

## Releases

Releases of RODOS follow semver versioning with an additional first digit representing the current upstream RODOS version.
Rules for releasing:

Each merge on master is a new release
The merge commit must be created by hand (e.g from the console), not in gitlab
The merge commit message must contain the release version, breaking changes, new features and bug fixes
If the release is not a pre-release, bump major if breaking changes exist (R.X+1.Y.Z), bump minor for new features (R.X.Y+1.Z) and patch for fixes only (R.X.Y.Z+1)
The merge commit message must follow the following template (empty sections shall be deleted):

R.X.Y.Z: <Small release message>

breaking changes:
* some change

features:
* some feature

bug fixes:
* some fix

For pre-release versions, prefix minor by 9, e.g. 202.0.91.0

tests, clang-tidy and clang-format have to run without errors

## Information from Upstream

Any questions, suggestions or complaints?
please contact

Sergio Montenegro
sergio.montenegro@uni-wuerzburg.de

We want to improve it. We hear your suggestions!

### Our Users

<span>
<img align="left" src="https://gitlab.com/rodos/rodos/-/wikis/uploads/d15c43bc579fbc20139b74743426256f/Kamaro_Logo.png" width="128">
<p>&emsp;&emsp;<a href="https://kamaro-engineering.de/">Kamaro Engineering e.V. - Modular Field Robot Beteigeuze NOVA</a></p>
</span>
<br/><br/>

<span>
<img align="left" src="https://gitlab.com/rodos/rodos/-/wikis/uploads/a5cd452ed15dc470a136c9c379c1e31a/wuespace.png" width="128">
<p>&emsp;&emsp;<a href="https://www.wuespace.de/">WüSpace e.V. - Aerospace association Würzburg</a></p>
</span>
<br/><br/>
<br/><br/>

### How to cite RODOS?
If you intend to cite RODOS in your work, please cite the following references:
- `@article{montenegro2009rodos,
  title={RODOS - real time kernel design for dependability},
  author={Montenegro, Sergio and Dannemann, Frank},
  journal={DASIA 2009 - DAta Systems in Aerospace},
  volume={669},
  pages={66},
  year={2009}
}`
