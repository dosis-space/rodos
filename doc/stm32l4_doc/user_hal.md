# Notes regarding usage of HAL with the STM32L4 port

## GPIO

- In order to get the RODOS pinIdx for a pin, take the letter times 16(0 for PAx, 16 for PBx, 32 for PCx, etc) plus the number(e.g. GPIO_010 for PA10, GPIO_34 for PC2)

## UART

- LPUART1 is UART_IDX0, the remaining ones are counting upwards from there

## CAN

- CAN_IDX0 is CAN1, CAN_IDX1 is CAN2
- Each CAN controller may only be initialized once
- A maximum of 3 HAL_CANs per CAN controller is currently possible

## I2C

- I2C slave is not supported
- I2C_IDX0 is I2C1, then counting upwards
- On MCU versions which only have I2C1 and I2C3, I2C3 is I2C_IDX1
- Currently only works with APB frequencies of 16MHz or 80 MHz and baudrates of 10khz, 100khz or 400khz

## SPI

- SPI slave is not supported
- SPI_IDX0 is SPI1, then counting upwards
- on MCU versions which only have SPI1 and SPI3, SPI3 is SPI_IDX1
- Baud rate must be APB frequency divided by a power of two between 2 and 256