# Guide to flashing the VA41620 from Linux

NOTE: The script sometimes works only every second time

## Required software
- The gnu arm-none-eabi toolchain. This is covered in the [DOSIS getting started guide](https://gitlab.lrz.de/move/dosis/firmware/-/blob/master/doc/guides/01_getting_started.md)
- Download and install the J-Link Software and Documentation pack: https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack
- install the pygdbmi library: `pip3 install --user -U pygdbmi`

## building a binary
- run `meson buildva --cross-file meson/va41620/cross.ini` to create the build directory
- run `ninja -Cbuildva build-led_test` to build the firmware

## flashing a binary
- run `arm-none-eabi-objcopy -Obinary buildva/meson/tutorials/led_test.elf firmware.bin`
- run `scripts/va41620/vaflash.py firmware.bin`
- alternatively, you can also use the `ninja run-led_test` target
- you might get an error that your gdb version doesn't support the armv3m architecture. In this case, try `scripts/va41620/vaflash.py firmware.bin --gdb_path=arm-none-eabi-gdb`


## Recovering from a bad firmware
The VA41620 has the unfortunate hardware bug that the debug interface isn't working if the current firmware is invalid. In this case, it might be necessary to flash the FRAM directly from another microcontroller.

- take a NUCLEO-L496 board
- run the `flash_va41620_fram` binary on it: `meson buildn496 --cross-file meson/stm32l4/cross.ini;ninja -Cbuildn496 run:flash_va41620_fram`
- make sure that the VA41620 board is not plugged in
- remove the 3V3 and 1V5 jumpers on JP4 on the VA41620 board
- connect the SPI_B pins on the CN7 header on the nucleo board to the J15 pins on the VA41620 board. Also connect a ground pin.
    - NOTE: the order of the SPI_B pins from top to bottom is: MOSI, SCK, NSS, MISO
- plug in the VA41620 board
- reset the nucleo board in order to run the code from the start
- debug output of the nucleo board should say `Flashing succeeded`
- plug out the VA41620 board
- remove the SPI wires between the boards
- add the 3V3 and 1V5 jumpers back
- plug the VA41620 board back in
- it should now be possible to flash the VA41620 board normally
