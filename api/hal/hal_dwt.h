#pragma once

#include "thread.h"

namespace RODOS {

class DwtManager {

  private:
    static void DwtNotImplemented();
    static void resetAllThreadCycleCounts();

  public:
    static void     init();
    static uint32_t getCycleCount();
    static void     startCycleCount();
    static void     stopCycleCount();
    static void     reset();

    /*
     * Get pointers to and number of all available threads.
     * 
     * @param[out] threads Array with pointers to all threads.
     * @param[in] size Size of passed threads array.
     * @return Number of available threads.
     */
    static uint8_t  getThreadPointers(Thread* threads[], uint8_t size);
};

} // namespace RODOS
