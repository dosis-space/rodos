#pragma once

#include "bit_field.h"

#include <stdint.h>

namespace RODOS {

namespace RESET_REASON {
    using LOWPOWER = UInt32SubValue<0, 1>;
    using WWDG = UInt32SubValue<1, 1>;
    using IWDG = UInt32SubValue<2, 1>;
    using SOFTWARE = UInt32SubValue<3, 1>;
    using BROWNOUT = UInt32SubValue<4, 1>;
    using EXTERNAL = UInt32SubValue<5, 1>;
    using OBL = UInt32SubValue<6, 1>;
    using FIREWALL = UInt32SubValue<7, 1>;
}

class ResetReason {
public:
    static void init(uint32_t lastResetReason);

    static UInt32BitField getReason();

private:
    static UInt32BitField reason;
};

}
