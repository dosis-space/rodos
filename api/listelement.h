
/**
* @file listelement.h
* @date 2008/04/28 16:13
* @author Sergio Montenegro, Lutz Dittrich
*
*
* @brief linked list management (header)
*
*/

#pragma once

#include "list-legacy.h"
#include "list-protected.h"

#include <stdint.h>
#include <atomic>

namespace RODOS {


/**
 * @class ListElement
 * @brief node of a single linked list
 *
 * A node of a single linked list.
 *
 * @warning Not thread safe,
 * usually only for static elements which will never be deleted,
 * eg. never on stacks. Can be safely appended to / removed from
 * ProtectedLists at runtime though.
 */
class ListElement {
    friend class List;
    friend class ProtectedList;

  private:
    static int32_t listElementCounter;

    std::atomic<bool> m_isDestructable{ true };
    ListIterInfo      m_nextIterInfo{};

    ListIterInfo* findIterInfoToThis(ProtectedList& list) const;
    void          appendHelper(std::atomic<ListElement*>& head);

  public:
    const char* name;          ///< name of the element (for debugging)
    int32_t     listElementID; ///< set from listElementCounter
    void*       owner;         ///< free for user, rodos does not use it

    /**
     * Creates an element and adds it to the beginning of the list.
     * @param[in,out] list The list the element shall be added to.
     * @param[in] name Clear text name.
     */
    ListElement(List& list, const char* name = "ListElement", void* owner = 0);
    ListElement(const char* name = "ListElement", void* owner = 0);

    /** destructor */
    virtual ~ListElement();

    /** appends this ListElement to a LegacyList at system startup time */
    void append(List& list);

    /** appends this ListElement to a ProtectedList at system startup time */
    void append(ProtectedList& list);

    /**
     * @brief Safely appends this ListElement to a ProtectedList at runtime.
     *
     * This operation will wait until all old iterations on the
     * list's head element have finished.
     *
     * @warning
     * - this function cannot be called at system startup time
     * - blocks (do not call from non-thread),
     * but will return after a finite amount of time
     * - trying to destruct this ListElement simultaneously
     * will lead to undefined behaviour
     */
    void appendAtRuntime(ProtectedList& list);

    /**
     * @brief Safely removes this ListElement from a ProtectedList at runtime.
     *
     * The operation guarantees that destruction of this ListElement is
     * safe after the function returns. 'Safe' in this context means that
     * there aren't (and won't be in the future) any threads iterating
     * over the ListElement.
     * This operation will wait until all old iterations on the
     * removed element have finished.
     *
     * @warning
     * - this function cannot be called at system startup time
     * - blocks (do not call from non-thread),
     * but will return after a finite amount of time
     * - trying to destruct this ListElement simultaneously
     * will lead to undefined behaviour.
     */
    void removeAtRuntime(ProtectedList& list);

    /**
     * Returns the name of the object as string.
     * @return pointer to name of the object
     */
    const char* getName() const {
        return name;
    }
};


} // namespace


