#pragma once

#include <atomic>

#include "list-iter-info.h"

namespace RODOS {

/**
 * @def ITERATE_LIST(__type,__list)
 * @param[in] __type Base type of list nodes.
 * @param[in] __list The LegacyList.
 *
 * a handy macro: for_each_do (for iterating LegacyLists)
 */
#define ITERATE_LIST(__type, __list)                                     \
    for(auto __it = __list.begin(); __it != __list.end(); ++__it)        \
        for(__type* iter = static_cast<__type*>(&(*__it)), *_ = nullptr; \
            _ == nullptr; _ = reinterpret_cast<__type*>(1))

class ListElement;

/**
 * @brief Simple Legacy List type
 *
 * Aimed to provide compatability with legacy code and should emulate:
 * `typedef ListElement* List;`
 *
 * @note can be iterated over via a range-based for loop
 */
class List {
    friend class ListElement;

  public:
    constexpr List() : m_head{ nullptr } {}
    constexpr List(ListElement* head) : m_head{ head } {}

    List& operator=(List&& list) {
        m_head = list.m_head.load();
        return *this;
    }
    bool operator==(const List& rhs) const {
        return (this->m_head == rhs.m_head);
    }
    bool operator!=(const List& rhs) const { return !(*this == rhs); }


    class Iterator;
    class ListEnd {
      public:
        bool operator==(const Iterator& rhs) const {
            return (rhs.m_currentPtrCopy == nullptr);
        }
        bool operator!=(const Iterator& rhs) const { return !(*this == rhs); }
    };
    Iterator begin();
    ListEnd  end();

    /**
     * @brief Iterator to iterate over LegacyLists
     */
    class Iterator {
        friend class ListEnd;

      public:
        explicit Iterator(List& list);

        Iterator& operator++();
        Iterator  operator++(int);

        ListElement& operator*() const;
        ListElement* operator->() const;

        bool operator==(const Iterator& rhs) const;
        bool operator!=(const Iterator& rhs) const { return !(*this == rhs); }

      private:
        ListElement* m_currentPtrCopy;
    };

  private:
    std::atomic<ListElement*> m_head;
};

} // namespace RODOS
