#pragma once

#include "list-protected.h"

#include <cstdint>

namespace RODOS {

/**
 * @brief Enum indexing all global ProtectedLists in the system
 * @note Used to access lists in the GlobalLists Singleton via GlobalLists::get()
 */
enum class GlobalListsEnum : uint8_t {
    THREAD_LIST,

    NUM
};

/**
 * @brief Singleton object containing all global ProtectedLists in the system
 *
 * @note Singleton pattern (object construction/copy/move forbidden):
 *  - constructor is private
 *  - copy constructor / copy assignment explicitly deleted
 *  - move constructor / move assignment not declared (implicitly or explicitly)
 */
class GlobalLists {
  public:
    /**
     * @brief Get a ProtectedList from the Singleton (guaranteed to be already initialized)
     * @param list GlobalListsEnum indexing the requested list in the internal array
     */
    static ProtectedList& get(GlobalListsEnum list) {
        static GlobalLists instance{};
        return instance.m_lists[static_cast<uint8_t>(list)];
    }

    GlobalLists(const GlobalLists&) = delete;
    GlobalLists& operator=(const GlobalLists&) = delete;

  private:
    GlobalLists() = default;

    ProtectedList m_lists[static_cast<uint8_t>(GlobalListsEnum::NUM)]{};
};

} // namespace RODOS
