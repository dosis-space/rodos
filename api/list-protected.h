#pragma once

#include <atomic>

#include "rodos-semaphore.h"
#include "list-iter-info.h"

namespace RODOS {

class ListElement;

/**
 * @brief Simple List Wrapper class with synchronization protection
 *
 * This stores a pointer to the first element of the list and
 * a Semaphore for synchronization of possible runtime modifications.
 *
 * @note can be iterated over via a range-based for loop
 */
class ProtectedList {
    friend class ListElement;

  public:
    class Iterator;
    class ListEnd {
      public:
        bool operator==(const Iterator& rhs) const {
            return (rhs.m_currentPtrCopy == nullptr);
        }
        bool operator!=(const Iterator& rhs) const { return !(*this == rhs); }
    };
    Iterator begin();
    ListEnd  end();

    /**
     * @brief Iterator to iterate over ProtectedLists
     *
     * Iterating over a ListElement with this Iterator is guaranteed to be safe,
     * meaning a ListElement that is iterated over cannot be destructed validly.
     *
     * @note doesn't block though (can be used in scheduler / interrupts)
     * due to use of atomic operations and the CAS instruction
     */
    class Iterator {
        friend class ListEnd;

      public:
        explicit Iterator(ProtectedList& list);
        ~Iterator();
        Iterator(const Iterator& other);

        Iterator& operator++();
        Iterator  operator++(int);

        ListElement& operator*() const;
        ListElement* operator->() const;

        bool operator==(const Iterator& rhs) const;
        bool operator!=(const Iterator& rhs) const { return !(*this == rhs); }

      private:
        static ListElement* occupyElement(ListIterInfo& iterInfo);

        static void leaveElement(
          ListIterInfo&      iterInfo,
          const ListElement* elemPtrCopy);

        std::atomic<ListIterInfo*> m_currentIter;
        std::atomic<ListElement*>  m_currentPtrCopy;
    };

    Semaphore sema{};

  private:
    ListIterInfo m_head{};
};

} // namespace RODOS
