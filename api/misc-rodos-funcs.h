
/**
* @file misc-rodos-funcs.h
* @date 2008/06/17 10:46
* @author Sergio MOntenegro
*
* @brief simple misc functions
*/

#pragma once


#include <stdint.h>
#include <stddef.h>


namespace RODOS {

extern bool isShuttingDown; // set by hwResetAndReboot(), read by destructors

/**
 * if a network and a gateway are present, my node number
 */
int32_t getNodeNumber();

int64_t getNumberOfReceivedMsgsFromNetwork();

/**
 * allocation of memory. No free of memory exists.
 * allocates static memory.
 * it shall not be unset after thread:run() begin
 */
extern void* xmalloc(size_t len);

extern void* xmallocAndTrapOnFailure(size_t len);

template<typename T>
inline T* xmalloc(){
	return (T*) xmalloc(sizeof(T));
}

template<typename T>
inline T* xmallocAndTrapOnFailure(){
	return (T*) xmallocAndTrapOnFailure(sizeof(T));
	
}

extern void hwResetAndReboot();        ///<  End of Program -> reboot Hw dependent
extern void hwInitWatchdog(int32_t intervalMilliseconds);
extern void hwTriggerWatchdog();        ///<  for CPUS which provide a hardware watchdog
extern void hwDisableInterrupts();      // global interrupt disable - use carefully
extern void hwEnableInterrupts();       // global interrupt enable - use carefully
extern void deepSleepUntil(int64_t until); //< cpu core and periphery off until external interrupt or time "until"
extern bool isSchedulerRunning();       //< implemented in the platform dependent scheduler
extern int64_t hwGetRTCTime();
extern void hwSetRTCTime(int64_t time);
extern void hwChargeRTCBackupBattery();
extern void hwDisableChargeRTCBackupBattery();
extern int hwLoadRTCBackupRegister(size_t index, uint32_t& reg);
extern int hwStoreRTCBackupRegister(size_t index, uint32_t val);
extern int32_t hwFlashPageSize();
extern int32_t hwFlashUnlock(uint32_t key1, uint32_t key2);
extern int32_t hwFlashLock();
extern int32_t hwFlashRead(size_t offset, size_t length, uint8_t *buffer);
extern int32_t hwFlashWrite(size_t offset, size_t length, const uint8_t *buffer);
extern int32_t hwFlashErasePage(size_t offset);

/** Nop... no operation ... do nothing ***/
void nop( ... );

extern float getCpuLoad(); ///< value from 0 to 1, average from the last call


inline bool getbit(uint32_t bitmap, uint8_t bitIndex) { return (bitmap >> bitIndex) & 0x01; }

inline void setbit(uint32_t* bitmap, bool value, uint8_t bitIndex) {
    if(value) *bitmap |=  (0x01u  << bitIndex);
    else      *bitmap &= ~(0x01u  << bitIndex);
}

template <typename T>
constexpr T min(const T a, const T b) {
    return (a < b) ? a : b;
}

template <typename T>
constexpr T max(const T a, const T b) {
    return (a > b) ? a : b;
}

template <typename T>
constexpr T abs(const T a) {
    return (a >= 0) ? a : -a;
}

constexpr uint32_t uint32_tOnes(uint8_t n) {
    return (static_cast<uint32_t>(1)  << n) - static_cast<uint32_t>(1);
}

template <typename T>
constexpr bool inClosedInterval(const T& a, const T& min, const T& max) {
    return (min <= a) && (a <= max);
}

template <typename T>
constexpr bool inOpenInterval(const T& a, const T& min, const T& max) {
    return (min < a) && (a < max);
}

constexpr uint16_t chars2short(const char a, const char b) {
    return static_cast<uint16_t>((a << 8) + b);
}

constexpr uint8_t not0(const void* a) { return (a) ? 1 : 0; }

}  // namespace


/**
 * MAIN() will be executed after initialisation and before
 * threads are started. RODOS provides an empty MAIN function.
 * The user may write its own MAIN function which will be
 * linked instead of the RODOS empty one.
 * outside of namespace RODOS!
 **/
void MAIN();

