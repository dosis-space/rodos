#pragma once

#include <cstdint>
#include <atomic>

#include "bit_field.h"

namespace RODOS {

class ListElement;

/**
 * @brief Structure holding all information relevant to lock-free List iteration
 */
struct ListIterInfo {
    /**
     * @brief Structure encoding all information regarding the next ListElement
     * (index of next pointer + iteration counters).
     * Consists only of a 32Bit word and thus can be updated atomically.
     */
    class InfoFields {
      public:
        static constexpr uint32_t NUM_BITS_FIELD = 8;

        template <uint32_t offset>
        using Field = UInt32BitField::SubValue<offset, NUM_BITS_FIELD>;

        /**
         * @name All Byte-Field types
         * @note can be injected as template parameters into methods below
         * @{
         */
        using NextPtrInd = Field<NUM_BITS_FIELD * 3>;
        using MainCtr    = Field<NUM_BITS_FIELD * 1>;
        using BranchCtr  = Field<NUM_BITS_FIELD * 0>;
        /** @} */

        static constexpr uint8_t NEXT_PTR_INDEX_DEFAULT = 0;
        static constexpr uint8_t MAIN_CTR_DEFAULT       = 0;
        static constexpr uint8_t BRANCH_CTR_DEFAULT     = 0;

        constexpr InfoFields(
          uint8_t nextPtrInd = NEXT_PTR_INDEX_DEFAULT,
          uint8_t mainCtr    = MAIN_CTR_DEFAULT,
          uint8_t branchCtr  = BRANCH_CTR_DEFAULT) {
            m_infoFields.write(
              NextPtrInd(nextPtrInd),
              MainCtr(mainCtr),
              BranchCtr(branchCtr));
        }

        /**
         * @brief extract a byte field determined by \a F
         */
        template <typename F>
        constexpr uint8_t getField() const {
            return static_cast<uint8_t>(m_infoFields.read<F>());
        }

        /**
         * @brief add a signed value to a byte field determined by \a F
         * @warning no sanity checks (aimed for increments/decrements)
         */
        template <typename F>
        constexpr void modifyField(int value) {
            m_infoFields.set<F>(static_cast<uint32_t>(getField<F>() + value));
        }

        constexpr uint32_t read() const { return m_infoFields.read(); }

      private:
        UInt32BitField m_infoFields{};
    };

    /**
     * @brief atomically extracts the index of the currently non-active pointer in #nextPtrs
     */
    uint8_t getNonActiveNextPtrIndex() const;

    /**
     * @brief extracts the currently active pointer in #nextPtrs
     * @warning not atomic - should only be called when this ListIterInfo is occupied!
     */
    ListElement* getNextPtr() const;

    /**
     * @brief atomically rewires the currently active next pointer in a
     * to a generic \a destPtr via index swap
     *
     * @note Resets all counters to 0. This will temporarily set the List
     * in an invalid state. This can be resolved by waiting for the counters
     * to manifest themselves again via waitForBranchCtrEnd().
     */
    InfoFields rewirePtrTo(ListElement* destPtr);

    /**
     * @brief Waits for BranchCtr of a generic \a iterInfo to reach
     * the value of MainCtr in \a infoOld.
     *
     * Used to wait until the old iterations that went over a now rewired
     * ListIterInfo are finished.
     */
    void waitForBranchCtrEnd(const InfoFields& infoOld);

    /**
     * Waits for MainCtr of a generic \a iterInfo to reach 0.
     *
     * Used to wait until a ListIterInfo in a standard state
     * (isn't getting rewired) isn't being iterated over anymore.
     */
    void waitForMainCtrEnd();

    /**
     * @brief The actual atomic InfoFields variable holding all
     * iteration counters and the next pointer index.
     */
    std::atomic<InfoFields> infoFields{};

    /**
     * @brief Array holding the atomic pointer to the next element in the List.
     *
     * Only one pointer is valid (determined by a field in #infoFields).
     * This allows atomically propagating a new pointer via an index swap.
     *
     * @note This construct can only be used because the next pointer isn't
     * modified by iterations and Append / Remove have to wait until the
     * iterations notice a swap (iterations can only 'miss' one pointer swap).
     */
    std::atomic<ListElement*> nextPtrs[2]{ { nullptr }, { nullptr } };
};


} // namespace RODOS
