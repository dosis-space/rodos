#include "migration/migration-types.h"
#include "migration/type-list.h"
#include "migration/implementation/info/migration-info-request-subscriber.h"

#include "rodos-debug.h"
#include "rodos-semaphore.h"

#include "utilities/matchers.h"
#include "storage_test_classes.h"

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationInfoRequestSubscriberTestSuite : public ::testing::Test {
  protected:
    static constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{
        .num_slots       = 2,
        .max_thread_size = 500,
        .stack_size      = 100
    };
    using TestTypeList = TypeList<StorageTestThreadClassInt>;

    using ConcreteMigInfo = MigrationInfo<MIG_STORAGE_PARAMS.num_slots>;

    using ConcreteMigStorage =
      implementation::storage::MigrationStorage<TestTypeList, MIG_STORAGE_PARAMS>;

    using ConcreteMigInfoRequestSub =
      implementation::info::MigrationInfoRequestSubscriber<TestTypeList, MIG_STORAGE_PARAMS>;

    static constexpr uint16_t SYS_HASH         = 0x1234;
    static constexpr uint16_t INVALID_SYS_HASH = 0xFFFF;

    static constexpr uint16_t INSTANCE_ID = 0xAA;

    static constexpr uint16_t DUMMY_SENDER_SYS_HASH = 0x5678;
    static constexpr uint16_t DUMMY_SRC_INSTANCE_ID = INSTANCE_ID;

    static constexpr uint16_t INVALID_DEST_INSTANCE_ID = 0xFFFF;

    static constexpr uint32_t DUMMY_TICKET = 0x1000;
    static constexpr int      DUMMY_X_VAL  = 42;

    void SetUp() override {
        auto* slot_ptr = m_mig_storage.reserveEmptySlotUnprotected();
        ASSERT_TRUE(slot_ptr != nullptr);
        slot_ptr->createThread<StorageTestThreadClassInt>(DUMMY_TICKET, DUMMY_X_VAL);
    }

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Semaphore m_mig_sema{};

    ConcreteMigStorage          m_mig_storage{};
    Topic<MigrationInfoRequest> m_mig_info_request_topic{};

    StrictMock<Topic<MaxMigInfo>> m_mig_info_topic{};

    ConcreteMigInfoRequestSub m_mig_info_request_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_sema,
        m_mig_info_request_topic,
        m_mig_info_topic,
        m_mig_storage
    };

    MigrationInfoRequest m_incoming_mig_info_request{
        .sender_sys_hash     = DUMMY_SENDER_SYS_HASH,
        .sender_instance_id  = DUMMY_SRC_INSTANCE_ID,
        .request_sys_hash    = SYS_HASH,
        .request_instance_id = INSTANCE_ID
    };
    ConcreteMigInfo m_expected_mig_info{
        .requester_sys_hash    = DUMMY_SENDER_SYS_HASH,
        .requester_instance_id = DUMMY_SRC_INSTANCE_ID,
        .mig_storage_info{ { true, DUMMY_TICKET }, { false, 0 } },
    };
};


TEST_F(MigrationInfoRequestSubscriberTestSuite, Test_put_valid) {
    EXPECT_CALL(m_mig_info_topic,
                topicInterfacePublishMsgPartMock(EqMigInfoVoidPtr(m_expected_mig_info),
                                                 sizeof(ConcreteMigInfo),
                                                 true,
                                                 nullptr));

    m_mig_info_request_sub.put(m_incoming_mig_info_request);
}

TEST_F(MigrationInfoRequestSubscriberTestSuite, Test_put_invalid_request_sys_hash) {
    m_incoming_mig_info_request.request_sys_hash = INVALID_SYS_HASH;
    m_mig_info_request_sub.put(m_incoming_mig_info_request);
    m_incoming_mig_info_request.request_sys_hash = SYS_HASH;
}

TEST_F(MigrationInfoRequestSubscriberTestSuite, Test_put_invalid_request_destination) {
    m_incoming_mig_info_request.request_instance_id = INVALID_DEST_INSTANCE_ID;
    m_mig_info_request_sub.put(m_incoming_mig_info_request);
    m_incoming_mig_info_request.request_instance_id = INSTANCE_ID;
}

} // namespace
