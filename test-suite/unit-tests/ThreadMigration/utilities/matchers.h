#pragma once

#include <gmock/gmock-matchers.h>

#include <cstring>

MATCHER_P(EqMigCmd, mig_cmd, "") {
    return (arg.sender_sys_hash == mig_cmd.sender_sys_hash) &&
           (arg.sender_instance_id == mig_cmd.sender_instance_id) &&
           (arg.target_sys_hash == mig_cmd.target_sys_hash) &&
           (arg.migrate_from_id == mig_cmd.migrate_from_id) &&
           (arg.migrate_to_id == mig_cmd.migrate_to_id) &&
           (arg.seq_num == mig_cmd.seq_num) &&
           (arg.ticket == mig_cmd.ticket);
}

MATCHER_P(EqCmdBackChannelData, cmd_back_channel_data, "") {
    return (arg.slot_index == cmd_back_channel_data.slot_index) &&
           (arg.sender_sys_hash == cmd_back_channel_data.sender_sys_hash) &&
           (arg.sender_instance_id == cmd_back_channel_data.sender_instance_id) &&
           (arg.migrate_to_id == cmd_back_channel_data.migrate_to_id) &&
           (arg.seq_num == cmd_back_channel_data.seq_num) &&
           (arg.ticket == cmd_back_channel_data.ticket);
}

MATCHER_P(EqMigData, mig_data, "") {
    if((arg.sys_hash != mig_data.sys_hash) ||
       (arg.src_instance_id != mig_data.src_instance_id) ||
       (arg.dest_instance_id != mig_data.dest_instance_id) ||
       (arg.type_index != mig_data.type_index) ||
       (arg.ticket != mig_data.ticket) ||
       (arg.len != mig_data.len) ||
       (sizeof(arg.bytes) != sizeof(mig_data.bytes))) {
        return false;
    }
    return (std::memcmp(arg.bytes, mig_data.bytes, sizeof(mig_data.len)) == 0);
}

MATCHER_P(EqMigAck, mig_ack, "") {
    return (arg.dest_sys_hash == mig_ack.dest_sys_hash) &&
           (arg.dest_instance_id == mig_ack.dest_instance_id) &&
           (arg.ack_type == mig_ack.ack_type) &&
           (arg.mig_error == mig_ack.mig_error) &&
           (arg.seq_num == mig_ack.seq_num);
}

MATCHER_P(EqMigInfoRequest, mig_info_request, "") {
    return (arg.sender_sys_hash == mig_info_request.sender_sys_hash) &&
           (arg.sender_instance_id == mig_info_request.sender_instance_id) &&
           (arg.request_sys_hash == mig_info_request.request_sys_hash) &&
           (arg.request_instance_id == mig_info_request.request_instance_id);
}

MATCHER_P(EqMigInfoVoidPtr, mig_info, "") {
    auto& arg_mig_info = *static_cast<decltype(mig_info)*>(arg);
    if((arg_mig_info.requester_sys_hash != mig_info.requester_sys_hash) ||
       (arg_mig_info.requester_instance_id != mig_info.requester_instance_id) ||
       (arg_mig_info.num_slots != mig_info.num_slots) ||
       (sizeof(arg_mig_info.mig_storage_info) != sizeof(mig_info.mig_storage_info))) {
        return false;
    }
    for(unsigned i = 0; i < mig_info.num_slots; i++) {
        if((arg_mig_info.mig_storage_info[i].is_valid != mig_info.mig_storage_info[i].is_valid) ||
           (arg_mig_info.mig_storage_info[i].ticket != mig_info.mig_storage_info[i].ticket)) {
            return false;
        }
    }
    return true;
}
