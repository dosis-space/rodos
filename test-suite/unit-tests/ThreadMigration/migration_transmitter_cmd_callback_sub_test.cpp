#include "migration/migration-types.h"
#include "migration/implementation/transmitter/migration-transmitter-cmd-callback-subscriber.h"

#include "rodos-debug.h"

#include "timemodel.h"
#include "utilities/matchers.h"

#include <gmock/gmock-spec-builders.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationTransmitterCmdCallbackSubscriberTestSuite : public ::testing::Test {
  protected:
    static constexpr meta::MigrationQueueParams MIG_QUEUE_PARAMS{
        .num_elems_mig_cmd_queue = 2
    };
    using ConcreteMigTransmitterCmdCallbackSub =
      implementation::transmitter::
        MigrationTransmitterCmdCallbackSubscriber<MIG_QUEUE_PARAMS.num_elems_mig_cmd_queue>;

    using MigCmdBackChannelFifo =
      typename ConcreteMigTransmitterCmdCallbackSub::MigCmdBackChannelFifo;

    static constexpr uint8_t  DUMMY_SLOT_INDEX    = 5;
    static constexpr uint16_t DUMMY_MIGRATE_TO_ID = 0xAA;
    static constexpr uint32_t DUMMY_TICKET        = 0x1000;

    void SetUp() override {}

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<CmdBackChannelData> m_mig_cmd_callback_topic{};

    StrictMock<MigCmdBackChannelFifo> m_mig_cmd_back_channel{};

    ConcreteMigTransmitterCmdCallbackSub m_mig_transmitter_cmd_callback_sub{
        m_mig_cmd_callback_topic,
        m_mig_cmd_back_channel
    };

    CmdBackChannelData m_incomding_cmd_back_channel_data{
        .slot_index    = DUMMY_SLOT_INDEX,
        .migrate_to_id = DUMMY_MIGRATE_TO_ID,
        .ticket        = DUMMY_TICKET
    };
};


TEST_F(MigrationTransmitterCmdCallbackSubscriberTestSuite, Test_put_valid) {
    EXPECT_CALL(m_mig_cmd_back_channel,
                syncPutMock(EqCmdBackChannelData(m_incomding_cmd_back_channel_data),
                            RODOS::END_OF_TIME));

    m_mig_transmitter_cmd_callback_sub.put(m_incomding_cmd_back_channel_data);
}

} // namespace
