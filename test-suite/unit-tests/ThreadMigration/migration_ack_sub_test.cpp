#include "migration/migration-types.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"
#include "migration/implementation/ack/scoped_ack_acceptance_session.h"

#include "synccommbuffer.h"
#include "rodos-debug.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationAckSubscriberTestSuite : public ::testing::Test {
  protected:
    static constexpr unsigned STANDARD_TIMES_CLEAR_DATA_STATE = 2;

    static constexpr uint16_t SYS_HASH    = 0x1234;
    static constexpr uint16_t INSTANCE_ID = 0xAA;

    static constexpr uint16_t INVALID_SYS_HASH      = 0xFF'FF;
    static constexpr uint16_t UNRELATED_INSTANCE_ID = 0xFF'FF;

    static constexpr uint16_t DUMMY_SEQ_NUM   = 13;
    static constexpr uint16_t INVALID_SEQ_NUM = DUMMY_SEQ_NUM - 1;

    void SetUp() override {}

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<MigrationAck> m_mig_ack_topic{};

    StrictMock<SyncCommBuffer<MigrationError>> m_mig_error_forwarder{};

    implementation::ack::MigrationAckSubscriber m_mig_response_ack_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_ack_topic,
        AckType::RESPONSE_ACK,
        m_mig_error_forwarder
    };
    implementation::ack::MigrationAckSubscriber m_mig_command_ack_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_ack_topic,
        AckType::COMMAND_ACK,
        m_mig_error_forwarder
    };

    MigrationAck m_incoming_mig_ack{
        .dest_sys_hash    = SYS_HASH,
        .dest_instance_id = INSTANCE_ID,
        .ack_type         = AckType::RESPONSE_ACK,
        .mig_error        = MigrationError::OK,
        .seq_num          = DUMMY_SEQ_NUM
    };
};


TEST_F(MigrationAckSubscriberTestSuite, Test_put_response_ack_sub_valid) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_error_forwarder, getOnlyIfNewData(_)).WillOnce(Return(false));
        EXPECT_CALL(m_mig_error_forwarder, put(m_incoming_mig_ack.mig_error));
    }
    EXPECT_CALL(m_mig_error_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);
    implementation::ack::ScopedAckAcceptanceSession session{ m_mig_response_ack_sub, DUMMY_SEQ_NUM };

    m_mig_response_ack_sub.put(m_incoming_mig_ack);
}

TEST_F(MigrationAckSubscriberTestSuite, Test_put_response_ack_sub_forwarder_full) {
    EXPECT_CALL(m_mig_error_forwarder, getOnlyIfNewData(_)).WillOnce(Return(true));
    EXPECT_CALL(m_mig_error_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);
    implementation::ack::ScopedAckAcceptanceSession session{ m_mig_response_ack_sub, DUMMY_SEQ_NUM };

    m_mig_response_ack_sub.put(m_incoming_mig_ack);
}

TEST_F(MigrationAckSubscriberTestSuite, Test_put_response_ack_sub_invalid_ack) {
    EXPECT_CALL(m_mig_error_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);
    implementation::ack::ScopedAckAcceptanceSession session{ m_mig_response_ack_sub, DUMMY_SEQ_NUM };

    // test invalid sys_hash
    m_incoming_mig_ack.dest_sys_hash = INVALID_SYS_HASH;
    m_mig_response_ack_sub.put(m_incoming_mig_ack);
    m_incoming_mig_ack.dest_sys_hash = SYS_HASH;

    // test other AckType received
    m_incoming_mig_ack.ack_type = AckType::COMMAND_ACK;
    m_mig_response_ack_sub.put(m_incoming_mig_ack);
    m_incoming_mig_ack.ack_type = AckType::RESPONSE_ACK;

    // test Ack going to other node
    m_incoming_mig_ack.dest_instance_id = UNRELATED_INSTANCE_ID;
    m_mig_response_ack_sub.put(m_incoming_mig_ack);
    m_incoming_mig_ack.dest_instance_id = INSTANCE_ID;

    // test Ack of invalid seq_num
    m_incoming_mig_ack.seq_num = INVALID_SEQ_NUM;
    m_mig_response_ack_sub.put(m_incoming_mig_ack);
    m_incoming_mig_ack.seq_num = DUMMY_SEQ_NUM;
}

TEST_F(MigrationAckSubscriberTestSuite, Test_response_ack_sub_sessions) {
    constexpr unsigned NUM_STARTS = 1;
    constexpr unsigned NUM_STOPS  = 1;
    EXPECT_CALL(m_mig_error_forwarder, clearDataState()).Times(NUM_STARTS + NUM_STOPS);

    // test no valid session pending
    m_mig_response_ack_sub.put(m_incoming_mig_ack);

    // test starting sessions with parameters not matching the incoming Ack
    m_mig_response_ack_sub.startAcceptingAcks(INVALID_SEQ_NUM);
    m_mig_response_ack_sub.put(m_incoming_mig_ack);
    m_mig_response_ack_sub.stopAcceptingAcks();

    // test no valid session pending
    m_mig_response_ack_sub.put(m_incoming_mig_ack);
}

TEST_F(MigrationAckSubscriberTestSuite, Test_command_ack_sub_valid) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_error_forwarder, getOnlyIfNewData(_)).WillOnce(Return(false));
        EXPECT_CALL(m_mig_error_forwarder, put(m_incoming_mig_ack.mig_error));
    }
    EXPECT_CALL(m_mig_error_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);
    implementation::ack::ScopedAckAcceptanceSession session{ m_mig_command_ack_sub, DUMMY_SEQ_NUM };

    m_incoming_mig_ack.ack_type = AckType::COMMAND_ACK;
    m_mig_command_ack_sub.put(m_incoming_mig_ack);
}

TEST_F(MigrationAckSubscriberTestSuite, Test_command_ack_sub_invalid_ack) {
    EXPECT_CALL(m_mig_error_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);
    implementation::ack::ScopedAckAcceptanceSession session{ m_mig_command_ack_sub, DUMMY_SEQ_NUM };

    m_incoming_mig_ack.ack_type = AckType::RESPONSE_ACK;
    m_mig_command_ack_sub.put(m_incoming_mig_ack);
}

} // namespace
