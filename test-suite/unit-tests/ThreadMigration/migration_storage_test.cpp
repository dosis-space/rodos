#include "migration/type-list.h"
#include "migration/implementation/storage/migration-storage.h"

#include "rodos-debug.h"

#include "storage_test_classes.h"

#include <gtest/gtest.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;

class MigrationStorageTestSuite : public ::testing::Test {
  protected:
    static constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{
        .num_slots       = 2,
        .max_thread_size = 500,
        .stack_size      = 100
    };
    using TestTypeList =
      TypeList<StorageTestThreadClassInt, StorageTestThreadClassChar>;

    using ConcreteMigStorage =
      implementation::storage::MigrationStorage<TestTypeList, MIG_STORAGE_PARAMS>;
    using ConcreteMigStorageSlot = typename ConcreteMigStorage::ConcreteMigStorageSlot;

    static constexpr int32_t CLASS_INT_TYPE_IND  = TestTypeList::index_of<StorageTestThreadClassInt>();
    static constexpr int32_t CLASS_CHAR_TYPE_IND = TestTypeList::index_of<StorageTestThreadClassChar>();

    static constexpr uint32_t TICKET_INT_0 = 0x5000;
    static constexpr int      VAL_INT_0    = 0x5566;

    static constexpr uint32_t TICKET_CHAR_1 = 0x6000;
    static constexpr char     VAL_CHAR_1    = 'A';

    static constexpr uint32_t INVALID_TICKET = 0xFFFF;

    void SetUp() override {
        StorageTestThreadClassInt::s_destructor_call_count  = 0;
        StorageTestThreadClassChar::s_destructor_call_count = 0;
    }

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    void fillStorageValidly() {
        ConcreteMigStorageSlot* slot_ptr0 = m_storage.reserveEmptySlotUnprotected();
        ASSERT_TRUE(slot_ptr0 != nullptr);
        slot_ptr0->createThread<StorageTestThreadClassInt>(TICKET_INT_0, VAL_INT_0);
        ASSERT_EQ(CLASS_INT_TYPE_IND, slot_ptr0->getTypeIndex());
        ASSERT_TRUE(slot_ptr0->isValidlyOccupied());

        ConcreteMigStorageSlot* slot_ptr1 = m_storage.reserveEmptySlotUnprotected();
        ASSERT_TRUE(slot_ptr1 != nullptr);
        slot_ptr1->createThread<StorageTestThreadClassChar>(TICKET_CHAR_1, VAL_CHAR_1);
        ASSERT_EQ(CLASS_CHAR_TYPE_IND, slot_ptr1->getTypeIndex());
        ASSERT_TRUE(slot_ptr1->isValidlyOccupied());
    }

    ConcreteMigStorage m_storage{};
};


TEST_F(MigrationStorageTestSuite, Test_StorageSlot) {
    ConcreteMigStorageSlot slot{};
    ASSERT_EQ(ConcreteMigStorageSlot::Status::EMPTY, slot.getStatus());
    ASSERT_TRUE(slot.getTypeIndex() < 0);
    ASSERT_EQ(nullptr, slot.getMigThreadPtr());

    // test occupy
    slot.occupy();
    ASSERT_FALSE(slot.isValidlyOccupied());
    ASSERT_EQ(ConcreteMigStorageSlot::Status::OCCUPIED, slot.getStatus());

    // test createThread
    constexpr uint32_t DUMMY_INT_TICKET = 0x2000;
    constexpr int      DUMMY_INT_VAL    = 0x1234;
    slot.createThread<StorageTestThreadClassInt>(DUMMY_INT_TICKET, DUMMY_INT_VAL);
    ASSERT_EQ(CLASS_INT_TYPE_IND, slot.getTypeIndex());
    ASSERT_TRUE(slot.isValidlyOccupied());

    // test free
    slot.free();
    ASSERT_FALSE(slot.isValidlyOccupied());
    ASSERT_EQ(ConcreteMigStorageSlot::Status::EMPTY, slot.getStatus());
    ASSERT_TRUE(slot.getTypeIndex() < 0);
    ASSERT_EQ(nullptr, slot.getMigThreadPtr());
}

TEST_F(MigrationStorageTestSuite, Test_finding_slots) {
    fillStorageValidly();

    // test valid find
    int slot_index_char1 = m_storage.findSlotIndexByTicketUnprotected(TICKET_CHAR_1);
    ASSERT_TRUE(slot_index_char1 == 1);
    auto* slot_ptr_char1 = m_storage.getSlotPtrByIndex(slot_index_char1);
    ASSERT_TRUE(slot_ptr_char1 != nullptr);
    auto* test_thread_char_ptr1 =
      reinterpret_cast<StorageTestThreadClassChar*>(slot_ptr_char1->getMigThreadPtr());
    ASSERT_EQ(TICKET_CHAR_1, test_thread_char_ptr1->getTicket());
    ASSERT_EQ(VAL_CHAR_1, test_thread_char_ptr1->m_c);

    // test invalid find
    int slot_index_invalid = m_storage.findSlotIndexByTicketUnprotected(INVALID_TICKET);
    ASSERT_TRUE(slot_index_invalid == -1);
    auto* slot_ptr_invalid = m_storage.getSlotPtrByIndex(slot_index_invalid);
    ASSERT_TRUE(slot_ptr_invalid == nullptr);
}

TEST_F(MigrationStorageTestSuite, Test_valid_free_reserve_sequence) {
    fillStorageValidly();

    // find and remove thread
    int   slot_index_char1 = m_storage.findSlotIndexByTicketUnprotected(TICKET_CHAR_1);
    auto* slot_ptr_char1   = m_storage.getSlotPtrByIndex(slot_index_char1);
    ASSERT_TRUE(slot_ptr_char1 != nullptr);
    auto* test_thread_char_ptr1 =
      reinterpret_cast<StorageTestThreadClassChar*>(slot_ptr_char1->getMigThreadPtr());

    test_thread_char_ptr1->~StorageTestThreadClassChar();
    slot_ptr_char1->free();

    // test valid reserve
    constexpr uint32_t      TICKET_CHAR_3 = 0x8000;
    constexpr char          VAL_CHAR_3    = 'B';
    ConcreteMigStorageSlot* slot_ptr3     = m_storage.reserveEmptySlotUnprotected();
    ASSERT_TRUE(slot_ptr3 != nullptr);
    slot_ptr3->createThread<StorageTestThreadClassChar>(TICKET_CHAR_3, VAL_CHAR_3);
    ASSERT_EQ(CLASS_CHAR_TYPE_IND, slot_ptr3->getTypeIndex());
    ASSERT_TRUE(slot_ptr3->isValidlyOccupied());
}

TEST_F(MigrationStorageTestSuite, Test_invalid_free_reserve_sequence) {
    fillStorageValidly();

    // check invalid reserve
    constexpr uint32_t      TICKET_INT = 0x7000;
    ConcreteMigStorageSlot* slot_ptr   = m_storage.reserveEmptySlotUnprotected();
    ASSERT_TRUE(slot_ptr == nullptr);

    // test invalid find
    int slot_index_int = m_storage.findSlotIndexByTicketUnprotected(TICKET_INT);
    ASSERT_TRUE(slot_index_int < 0);
    auto* slot_ptr_int = m_storage.getSlotPtrByIndex(slot_index_int);
    ASSERT_EQ(nullptr, slot_ptr_int);
}

} // namespace
