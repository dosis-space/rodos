#include "migration/migration-types.h"
#include "migration/type-list.h"
#include "migration/implementation/storage/migration-storage.h"
#include "migration/implementation/transmitter/migration-transmitter-helper.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"

#include "timemodel.h"
#include "rodos-debug.h"

#include "transmitter_test_class.h"
#include "utilities/matchers.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationTransmitterHelperTestSuite : public ::testing::Test {
  protected:
    static constexpr unsigned STANDARD_TIMES_CLEAR_DATA_STATE = 2;

    static constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{
        .num_slots       = 1,
        .max_thread_size = 500,
        .stack_size      = 100
    };
    static constexpr int64_t                    MIG_ACK_TIMEOUT = 0 * SECONDS;
    static constexpr meta::MigrationQueueParams MIG_QUEUE_PARAMS{
        .num_elems_mig_cmd_queue = 2
    };
    using TestTypeList = TypeList<TransmitterTestClass>;

    using ConcreteMigStorage =
      implementation::storage::MigrationStorage<TestTypeList, MIG_STORAGE_PARAMS>;
    using ConcreteMigStorageSlot = typename ConcreteMigStorage::ConcreteMigStorageSlot;

    using ConcreteMigTransmitterHelper =
      implementation::transmitter::
        MigrationTransmitterHelper<TestTypeList, MIG_STORAGE_PARAMS, MIG_ACK_TIMEOUT, MIG_QUEUE_PARAMS>;

    using ConcreteMigData = typename ConcreteMigTransmitterHelper::ConcreteMigData;

    static constexpr int TRANSMITTER_CLASS_IND = TestTypeList::index_of<TransmitterTestClass>();

    static constexpr uint16_t SYS_HASH    = 0x1234;
    static constexpr uint16_t INSTANCE_ID = 0xAA;

    static constexpr uint16_t DUMMY_SENDER_SYS_HASH    = 0xABCD;
    static constexpr uint16_t DUMMY_SENDER_INSTANCE_ID = INSTANCE_ID;

    static constexpr uint16_t DUMMY_OTHER_INSTANCE_ID = 0xBB;
    static constexpr uint32_t DUMMY_TICKET            = 0x1000;
    static constexpr int      DUMMY_INT_VAL           = 42;

    static constexpr uint16_t DUMMY_SEQ_NUM = 13;

    static constexpr uint8_t  INVALID_SLOT_INDEX = 0xFF;

    void SetUp() override {
        TransmitterTestClass::s_destruct_count = 0;

        // create thread in storage
        m_slot_ptr = m_mig_storage.reserveEmptySlotUnprotected();
        ASSERT_TRUE(m_slot_ptr != nullptr);
        m_slot_ptr->createThread<TransmitterTestClass>(DUMMY_TICKET, DUMMY_INT_VAL);
        m_thread_ptr = static_cast<TransmitterTestClass*>(m_slot_ptr->getMigThreadPtr());

        m_slot_index =
          static_cast<uint8_t>(
            m_mig_storage.findSlotIndexByTicketUnprotected(m_thread_ptr->getTicket()));
        m_incoming_cmd_back_channel_data.slot_index = m_slot_index;
        m_incoming_cmd_back_channel_data.ticket     = m_thread_ptr->getTicket();

        std::memcpy(m_expected_mig_data.bytes,
                    static_cast<void*>(m_thread_ptr),
                    sizeof(TransmitterTestClass));
    }

    void TearDown() override {
        EXPECT_TRUE(TransmitterTestClass::s_destruct_count <= 1);
        if(TransmitterTestClass::s_destruct_count == 0) {
            m_thread_ptr->~TransmitterTestClass();
            m_slot_ptr->free();
        }

        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<MigrationCmd>    m_mig_cmd_topic{};
    Topic<ConcreteMigData> m_mig_data_topic{};
    Topic<MigrationAck>    m_mig_ack_topic{};

    ConcreteMigStorage m_mig_storage{};

    Semaphore                      m_dummy_mig_sema{};
    SyncCommBuffer<MigrationError> m_mig_receiver_error_forwarder{};

    implementation::ack::MigrationAckSubscriber m_mig_transmitter_ack_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_ack_topic,
        AckType::RESPONSE_ACK,
        m_mig_receiver_error_forwarder
    };
    ConcreteMigTransmitterHelper m_mig_transmitter_helper{
        SYS_HASH,
        INSTANCE_ID,
        m_dummy_mig_sema,
        m_mig_data_topic,
        m_mig_ack_topic,
        m_mig_storage,
        m_mig_receiver_error_forwarder,
        m_mig_transmitter_ack_sub
    };

    ConcreteMigStorageSlot* m_slot_ptr{ nullptr };
    TransmitterTestClass*   m_thread_ptr{ nullptr };
    uint8_t                 m_slot_index{};
    CmdBackChannelData      m_incoming_cmd_back_channel_data{
             .slot_index{},
             .sender_sys_hash    = DUMMY_SENDER_SYS_HASH,
             .sender_instance_id = DUMMY_SENDER_INSTANCE_ID,
             .migrate_to_id      = DUMMY_OTHER_INSTANCE_ID,
             .seq_num            = DUMMY_SEQ_NUM,
             .ticket{}
    };

    ConcreteMigData m_expected_mig_data{
        .sys_hash         = SYS_HASH,
        .src_instance_id  = INSTANCE_ID,
        .dest_instance_id = DUMMY_OTHER_INSTANCE_ID,
        .type_index       = TRANSMITTER_CLASS_IND,
        .ticket           = DUMMY_TICKET,
        .len              = sizeof(TransmitterTestClass),
        .bytes{}
    };
    size_t m_expected_actual_size_mig_data =
      MIG_DATA_SIZE_WITHOUT_BYTE_BUFFER + m_expected_mig_data.len;

    MigrationAck m_expected_command_ack{
        .dest_sys_hash    = DUMMY_SENDER_SYS_HASH,
        .dest_instance_id = DUMMY_SENDER_INSTANCE_ID,
        .ack_type         = AckType::COMMAND_ACK,
        .mig_error        = MigrationError::OK,
        .seq_num          = DUMMY_SEQ_NUM
    };
};


TEST_F(MigrationTransmitterHelperTestSuite, Test_migrateThread_valid) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_data_topic,
                    publishMsgPartMock(EqMigData(m_expected_mig_data),
                                       m_expected_actual_size_mig_data,
                                       true));
        EXPECT_CALL(m_mig_receiver_error_forwarder, syncGet(_, MIG_ACK_TIMEOUT))
          .WillOnce(DoAll(SetArgReferee<0>(MigrationError::OK), Return(true)));
        EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));
    }
    EXPECT_CALL(m_mig_receiver_error_forwarder, clearDataState())
      .Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    unsigned manual_destruct_count = 0;
    ASSERT_EQ(manual_destruct_count, TransmitterTestClass::s_destruct_count);
    ASSERT_TRUE(m_mig_transmitter_helper.migrateThread(m_incoming_cmd_back_channel_data));
    ASSERT_EQ(++manual_destruct_count, TransmitterTestClass::s_destruct_count);
}

TEST_F(MigrationTransmitterHelperTestSuite, Test_migrateThread_invalid_slot_index) {
    m_expected_command_ack.mig_error = MigrationError::INVALID_SLOT_ON_SRC_NODE;
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));

    m_incoming_cmd_back_channel_data.slot_index = INVALID_SLOT_INDEX;
    ASSERT_FALSE(m_mig_transmitter_helper.migrateThread(m_incoming_cmd_back_channel_data));
    ASSERT_TRUE(RODOS::error);
    RODOS::error = false;
}

TEST_F(MigrationTransmitterHelperTestSuite, Test_migrateThread_invalid_ticket) {
    m_expected_command_ack.mig_error = MigrationError::INVALID_SLOT_ON_SRC_NODE;
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));

    m_thread_ptr->~TransmitterTestClass();
    m_slot_ptr->free();

    ASSERT_FALSE(m_mig_transmitter_helper.migrateThread(m_incoming_cmd_back_channel_data));
    ASSERT_TRUE(RODOS::error);
    RODOS::error = false;
}

TEST_F(MigrationTransmitterHelperTestSuite, Test_migrateThread_Timeout) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_data_topic,
                    publishMsgPartMock(EqMigData(m_expected_mig_data),
                                       m_expected_actual_size_mig_data,
                                       true));
        EXPECT_CALL(m_mig_receiver_error_forwarder, syncGet(_, MIG_ACK_TIMEOUT))
          .WillOnce(Return(false));
        m_expected_command_ack.mig_error = MigrationError::TRANSMITTER_ACK_TIMEOUT;
        EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));
        EXPECT_CALL(*m_slot_ptr->getMigThreadPtr(), resume());
    }
    EXPECT_CALL(m_mig_receiver_error_forwarder, clearDataState())
      .Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    ASSERT_FALSE(m_mig_transmitter_helper.migrateThread(m_incoming_cmd_back_channel_data));
    ASSERT_EQ(0, TransmitterTestClass::s_destruct_count);
}

TEST_F(MigrationTransmitterHelperTestSuite, Test_migrateThread_ReceiverError) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_data_topic,
                    publishMsgPartMock(EqMigData(m_expected_mig_data),
                                       m_expected_actual_size_mig_data,
                                       true));
        EXPECT_CALL(m_mig_receiver_error_forwarder, syncGet(_, MIG_ACK_TIMEOUT))
          .WillOnce(DoAll(SetArgReferee<0>(MigrationError::NO_EMPTY_SPACE_ON_DEST_NODE),
                          Return(true)));
        m_expected_command_ack.mig_error = MigrationError::NO_EMPTY_SPACE_ON_DEST_NODE;
        EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));
        EXPECT_CALL(*m_slot_ptr->getMigThreadPtr(), resume());
    }
    EXPECT_CALL(m_mig_receiver_error_forwarder, clearDataState())
      .Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    ASSERT_FALSE(m_mig_transmitter_helper.migrateThread(m_incoming_cmd_back_channel_data));
    ASSERT_EQ(0, TransmitterTestClass::s_destruct_count);
}

} // namespace
