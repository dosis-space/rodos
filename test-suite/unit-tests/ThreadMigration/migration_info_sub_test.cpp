#include "migration/migration-types.h"
#include "migration/implementation/info/migration-info-subscriber.h"
#include "migration/implementation/info/scoped_mig_info_acceptance_session.h"

#include "rodos-debug.h"
#include "synccommbuffer.h"
#include "timemodel.h"

#include "utilities/matchers.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationInfoSubscriberTestSuite : public ::testing::Test {
  protected:
    static constexpr uint32_t PUT_RET_SUCCESS = 1;
    static constexpr uint32_t PUT_RET_DROPPED = 0;

    static constexpr unsigned STANDARD_TIMES_CLEAR_DATA_STATE = 2;

    static constexpr uint8_t INCOMING_NUM_SLOTS = 3;

    using ConcreteMigInfo = MigrationInfo<INCOMING_NUM_SLOTS>;

    static constexpr int32_t SIZE_INCOMING_MIG_INFO = sizeof(ConcreteMigInfo::mig_storage_info);

    static constexpr uint16_t SYS_HASH         = 0x1234;
    static constexpr uint16_t INVALID_SYS_HASH = 0xFFFF;

    static constexpr uint16_t INSTANCE_ID              = 0xAA;
    static constexpr uint16_t INVALID_DEST_INSTANCE_ID = 0xFFFF;


    static constexpr uint32_t DUMMY_TICKET_0 = 0x1000;
    static constexpr uint32_t DUMMY_TICKET_1 = 0x2000;

    void SetUp() override {}

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<MaxMigInfo> m_mig_info_topic{};

    StrictMock<SyncCommBuffer<int32_t>> m_mig_commander_forwarder{};

    implementation::info::MigrationInfoSubscriber m_mig_info_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_info_topic,
        m_mig_commander_forwarder
    };

    ConcreteMigInfo m_incoming_mig_info{
        .requester_sys_hash    = SYS_HASH,
        .requester_instance_id = INSTANCE_ID,
        .mig_storage_info{
          { true, DUMMY_TICKET_0 },
          { false, 0 },
          { true, DUMMY_TICKET_1 },
        },
    };
    NetMsgInfo m_dummy_net_msg_info{};

    int32_t m_expected_diff_to_expected_size = 0;
};


TEST_F(MigrationInfoSubscriberTestSuite, Test_put_valid) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_commander_forwarder, getOnlyIfNewData(_)).WillOnce(Return(false));
        EXPECT_CALL(m_mig_commander_forwarder, put(m_expected_diff_to_expected_size));
    }
    EXPECT_CALL(m_mig_commander_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    MigrationSlotInfo m_output_mig_storage_info[INCOMING_NUM_SLOTS]{};

    implementation::info::ScopedMigInfoAcceptanceSession session{
        m_mig_info_sub,
        m_output_mig_storage_info,
        INCOMING_NUM_SLOTS
    };
    ASSERT_EQ(PUT_RET_SUCCESS, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
    ASSERT_EQ(0, std::memcmp(m_output_mig_storage_info,
                             m_incoming_mig_info.mig_storage_info,
                             SIZE_INCOMING_MIG_INFO));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_sanity_check_invalid_topicId) {
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId + 1,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_sanity_check_invalid_length) {
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  0,
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_sanity_check_nullptr) {
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  nullptr,
                                                  m_dummy_net_msg_info));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_invalid_mig_info_sys_hash) {
    m_incoming_mig_info.requester_sys_hash = INVALID_SYS_HASH;
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
    m_incoming_mig_info.requester_sys_hash = SYS_HASH;
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_invalid_mig_info_destination) {
    m_incoming_mig_info.requester_instance_id = INVALID_DEST_INSTANCE_ID;
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
    m_incoming_mig_info.requester_instance_id = INSTANCE_ID;
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_no_valid_Session_pending) {
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_Session_nullptr) {
    constexpr unsigned NUM_STARTS = 1;
    constexpr unsigned NUM_STOPS  = 1;
    EXPECT_CALL(m_mig_commander_forwarder, clearDataState()).Times(NUM_STARTS + NUM_STOPS);

    m_mig_info_sub.startAcceptingMigInfos(nullptr, INCOMING_NUM_SLOTS);
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
    m_mig_info_sub.stopAcceptingMigInfos();
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_forwarder_full) {
    EXPECT_CALL(m_mig_commander_forwarder, getOnlyIfNewData(_)).WillOnce(Return(true));
    EXPECT_CALL(m_mig_commander_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    MigrationSlotInfo m_output_mig_storage_info[INCOMING_NUM_SLOTS]{};

    implementation::info::ScopedMigInfoAcceptanceSession session{
        m_mig_info_sub,
        m_output_mig_storage_info,
        INCOMING_NUM_SLOTS
    };
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_smaller_input_array) {
    constexpr uint32_t SMALLER_ARRAY_NUM_SLOTS = 1;
    constexpr uint32_t SIZE_SMALLER_MIG_STORAGE_INFO =
      SMALLER_ARRAY_NUM_SLOTS * sizeof(MigrationSlotInfo);
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_commander_forwarder, getOnlyIfNewData(_)).WillOnce(Return(false));
        m_expected_diff_to_expected_size = SIZE_INCOMING_MIG_INFO - SIZE_SMALLER_MIG_STORAGE_INFO;
        EXPECT_CALL(m_mig_commander_forwarder, put(m_expected_diff_to_expected_size));
    }
    EXPECT_CALL(m_mig_commander_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    MigrationSlotInfo m_output_mig_storage_info[SMALLER_ARRAY_NUM_SLOTS]{};

    implementation::info::ScopedMigInfoAcceptanceSession session{
        m_mig_info_sub,
        m_output_mig_storage_info,
        SMALLER_ARRAY_NUM_SLOTS
    };
    ASSERT_EQ(PUT_RET_SUCCESS, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
    ASSERT_EQ(0, std::memcmp(m_output_mig_storage_info,
                             m_incoming_mig_info.mig_storage_info,
                             SIZE_SMALLER_MIG_STORAGE_INFO));
}

TEST_F(MigrationInfoSubscriberTestSuite, Test_put_larger_input_array) {
    constexpr int32_t LARGER_ARRAY_NUM_SLOTS = 10;
    constexpr int32_t SIZE_LARGER_MIG_STORAGE_INFO =
      LARGER_ARRAY_NUM_SLOTS * sizeof(MigrationSlotInfo);
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_commander_forwarder, getOnlyIfNewData(_)).WillOnce(Return(false));
        m_expected_diff_to_expected_size = SIZE_INCOMING_MIG_INFO - SIZE_LARGER_MIG_STORAGE_INFO;
        EXPECT_CALL(m_mig_commander_forwarder, put(m_expected_diff_to_expected_size));
    }
    EXPECT_CALL(m_mig_commander_forwarder, clearDataState()).Times(STANDARD_TIMES_CLEAR_DATA_STATE);

    MigrationSlotInfo  m_output_mig_storage_info[LARGER_ARRAY_NUM_SLOTS]{};
    constexpr uint32_t DUMMY_FILL_TICKET = 0xFF'FF'FF'FF;
    for(auto& elem : m_output_mig_storage_info) {
        elem.is_valid = true;
        elem.ticket   = DUMMY_FILL_TICKET;
    }

    implementation::info::ScopedMigInfoAcceptanceSession session{
        m_mig_info_sub,
        m_output_mig_storage_info,
        LARGER_ARRAY_NUM_SLOTS
    };
    ASSERT_EQ(PUT_RET_SUCCESS, m_mig_info_sub.put(m_mig_info_topic.topicId,
                                                  sizeof(m_incoming_mig_info),
                                                  &m_incoming_mig_info,
                                                  m_dummy_net_msg_info));
    ASSERT_EQ(0, std::memcmp(m_output_mig_storage_info,
                             m_incoming_mig_info.mig_storage_info,
                             SIZE_INCOMING_MIG_INFO));
    for(unsigned i = INCOMING_NUM_SLOTS; i < LARGER_ARRAY_NUM_SLOTS; i++) {
        ASSERT_TRUE(m_output_mig_storage_info[i].is_valid);
        ASSERT_EQ(DUMMY_FILL_TICKET, m_output_mig_storage_info[i].ticket);
    }
}

} // namespace
