#pragma once

#include "migration/migratable-thread.h"

class ReceiverTestClass : public RODOS::migration::MigratableThread {
  public:
    ReceiverTestClass(uint32_t ticket, int x)
      : RODOS::migration::MigratableThread(ticket, "ReceiverTestClass"),
        m_x{ x }, m_is_copy_constructed{ false } {}

    ReceiverTestClass(const ReceiverTestClass& rhs)
      : RODOS::migration::MigratableThread(rhs.getTicket(), "ReceiverTestClass"),
        m_x{ rhs.m_x }, m_is_copy_constructed{ true } {}

    void loop() final {}

    int  m_x{};
    bool m_is_copy_constructed;
};
