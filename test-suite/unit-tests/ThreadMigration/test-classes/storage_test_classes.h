#pragma once

#include "migration/migratable-thread.h"

#include "rodos-assert.h"
#include "rodos-debug.h"

class StorageTestThreadClassInt : public RODOS::migration::MigratableThread {
  public:
    StorageTestThreadClassInt(uint32_t ticket, int x)
      : RODOS::migration::MigratableThread(ticket, "StorageTestClassInt"), m_x{ x } {}

    ~StorageTestThreadClassInt() {
        s_destructor_call_count++;
    }

    void loop() final {}

    static inline uint32_t s_destructor_call_count = 0;

    int m_x;
};

class StorageTestThreadClassChar : public RODOS::migration::MigratableThread {
  public:
    StorageTestThreadClassChar(uint32_t ticket, char c)
      : RODOS::migration::MigratableThread(ticket, "StorageTestClassChar"), m_c{ c } {}

    ~StorageTestThreadClassChar() {
        s_destructor_call_count++;
    }

    void loop() final {}

    static inline uint32_t s_destructor_call_count = 0;

    char m_c;
};
