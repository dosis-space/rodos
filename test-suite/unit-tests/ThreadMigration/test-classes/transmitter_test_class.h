#pragma once

#include "migration/migratable-thread.h"

#include "rodos-assert.h"
#include "rodos-debug.h"

class TransmitterTestClass : public RODOS::migration::MigratableThread {
  public:
    TransmitterTestClass(uint32_t ticket, int x)
      : RODOS::migration::MigratableThread(ticket, "TransmitterTestClass"),
        m_x{ x } {}

    ~TransmitterTestClass() override {
        s_destruct_count++;
    }

    void loop() final {}

    inline static int s_destruct_count = 0;

    int m_x;
};
