#pragma once

#include "migration/migratable-thread.h"

#include "rodos-assert.h"
#include "rodos-debug.h"

struct GetSizeOfTypeClass : public RODOS::migration::MigratableThread {
    static constexpr auto NUM_BYTES_DUMMY_ARRAY = 27;

    GetSizeOfTypeClass(uint32_t ticket, const char* name)
      : RODOS::migration::MigratableThread(ticket, name) {}

    void loop() override {}

    uint8_t m_dummyByteArray[NUM_BYTES_DUMMY_ARRAY]{};
};

struct ReconstructClass : public RODOS::migration::MigratableThread {
    ReconstructClass(uint32_t ticket, const char* name)
      : RODOS::migration::MigratableThread(ticket, name) {}

    void loop() override {}
};

struct CopyClass : public RODOS::migration::MigratableThread {
    CopyClass(uint32_t ticket, const char* name, uint16_t x)
      : RODOS::migration::MigratableThread(ticket, name), m_x{ x } {}

    void loop() override {}

    uint16_t m_x;
};

struct DeconstructClass : public RODOS::migration::MigratableThread {
    DeconstructClass(uint32_t ticket, const char* name)
      : RODOS::migration::MigratableThread(ticket, name) {}
    ~DeconstructClass() { s_destruct_count++; }

    void loop() override {}

    inline static int s_destruct_count = 0;
};
