#include "migration/migration-types.h"
#include "migration/type-list.h"
#include "migration/implementation/storage/migration-storage.h"
#include "migration/implementation/transmitter/migration-transmitter-cmd-subscriber.h"

#include "rodos-semaphore.h"
#include "timemodel.h"
#include "rodos-debug.h"

#include "transmitter_test_class.h"
#include "utilities/matchers.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationTransmitterCmdSubscriberTestSuite : public ::testing::Test {
  protected:
    static constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{
        .num_slots       = 1,
        .max_thread_size = 500,
        .stack_size      = 100
    };
    static constexpr meta::MigrationQueueParams MIG_QUEUE_PARAMS{
        .num_elems_mig_cmd_queue = 2
    };
    using TestTypeList = TypeList<TransmitterTestClass>;

    using ConcreteMigStorage =
      implementation::storage::MigrationStorage<TestTypeList, MIG_STORAGE_PARAMS>;
    using ConcreteStorageSlot = typename ConcreteMigStorage::ConcreteMigStorageSlot;

    using ConcreteMigTransmitterCmdSub =
      implementation::transmitter::
        MigrationTransmitterCmdSubscriber<TestTypeList, MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;

    static constexpr uint16_t SYS_HASH    = 0x1234;
    static constexpr uint16_t INSTANCE_ID = 0xAA;

    static constexpr uint16_t DUMMY_SENDER_SYS_HASH    = 0xABCD;
    static constexpr uint16_t DUMMY_SENDER_INSTANCE_ID = INSTANCE_ID;

    static constexpr uint16_t DUMMY_OTHER_INSTANCE_ID = 0xBB;
    static constexpr uint32_t DUMMY_TICKET            = 0x1000;
    static constexpr int      DUMMY_INT_VAL           = 42;

    static constexpr uint16_t DUMMY_SEQ_NUM = 13;

    static constexpr uint32_t INVALID_TICKET = 0xFF;

    void SetUp() override {
        m_slot_ptr = m_mig_storage.reserveEmptySlotUnprotected();
        ASSERT_TRUE(m_slot_ptr != nullptr);

        m_slot_ptr->createThread<TransmitterTestClass>(DUMMY_TICKET, DUMMY_INT_VAL);

        int slot_index = m_mig_storage.findSlotIndexByTicketUnprotected(DUMMY_TICKET);
        ASSERT_TRUE(slot_index >= 0);
        m_expected_cmd_back_channel_data.slot_index = static_cast<uint8_t>(slot_index);
    }

    void TearDown() override {
        auto* thread_ptr = reinterpret_cast<TransmitterTestClass*>(m_slot_ptr->getMigThreadPtr());
        thread_ptr->~TransmitterTestClass();
        m_slot_ptr->free();

        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<MigrationCmd>       m_mig_cmd_topic{};
    Topic<CmdBackChannelData> m_mig_cmd_callback_topic{};
    Topic<MigrationAck>       m_mig_ack_topic{};

    ConcreteMigStorage   m_mig_storage{};
    ConcreteStorageSlot* m_slot_ptr{ nullptr };

    Semaphore                    m_dummy_mig_sema{};
    ConcreteMigTransmitterCmdSub m_mig_transmitter_cmd_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_dummy_mig_sema,
        m_mig_cmd_topic,
        m_mig_cmd_callback_topic,
        m_mig_ack_topic,
        m_mig_storage
    };

    MigrationCmd m_incoming_mig_cmd{
        .sender_sys_hash    = DUMMY_SENDER_SYS_HASH,
        .sender_instance_id = DUMMY_SENDER_INSTANCE_ID,
        .target_sys_hash    = SYS_HASH,
        .migrate_from_id    = INSTANCE_ID,
        .migrate_to_id      = DUMMY_OTHER_INSTANCE_ID,
        .seq_num            = DUMMY_SEQ_NUM,
        .ticket             = DUMMY_TICKET
    };

    CmdBackChannelData m_expected_cmd_back_channel_data{
        .slot_index{},
        .sender_sys_hash    = DUMMY_SENDER_SYS_HASH,
        .sender_instance_id = DUMMY_SENDER_INSTANCE_ID,
        .migrate_to_id      = m_incoming_mig_cmd.migrate_to_id,
        .seq_num            = DUMMY_SEQ_NUM,
        .ticket             = m_incoming_mig_cmd.ticket
    };

    MigrationAck m_expected_command_ack{
        .dest_sys_hash    = DUMMY_SENDER_SYS_HASH,
        .dest_instance_id = DUMMY_SENDER_INSTANCE_ID,
        .ack_type         = AckType::COMMAND_ACK,
        .mig_error{},
        .seq_num = DUMMY_SEQ_NUM
    };
};


TEST_F(MigrationTransmitterCmdSubscriberTestSuite, Test_put_valid) {
    EXPECT_CALL(*m_slot_ptr->getMigThreadPtr(),
                markShouldMigrate(EqCmdBackChannelData(m_expected_cmd_back_channel_data),
                                  Ref(m_mig_cmd_callback_topic)))
      .WillOnce(Return(true));

    m_mig_transmitter_cmd_sub.put(m_incoming_mig_cmd);
}

TEST_F(MigrationTransmitterCmdSubscriberTestSuite, Test_put_unrelated_cmd) {
    EXPECT_CALL(*m_slot_ptr->getMigThreadPtr(), markShouldMigrate(_, _)).Times(0);

    // test other sys_hash
    constexpr uint16_t OTHER_SYS_HASH  = 0xFF;
    m_incoming_mig_cmd.target_sys_hash = OTHER_SYS_HASH;
    m_mig_transmitter_cmd_sub.put(m_incoming_mig_cmd);
    m_incoming_mig_cmd.target_sys_hash = SYS_HASH;

    // test other from_id
    constexpr uint16_t OTHER_FROM_ID   = 0xFF;
    m_incoming_mig_cmd.migrate_from_id = OTHER_FROM_ID;
    m_mig_transmitter_cmd_sub.put(m_incoming_mig_cmd);
    m_incoming_mig_cmd.migrate_from_id = INSTANCE_ID;
}

TEST_F(MigrationTransmitterCmdSubscriberTestSuite, Test_put_ticket_not_found) {
    m_expected_command_ack.mig_error = MigrationError::TICKET_NOT_FOUND_ON_SRC_NODE;
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));
    EXPECT_CALL(*m_slot_ptr->getMigThreadPtr(), markShouldMigrate(_, _)).Times(0);

    m_incoming_mig_cmd.ticket = INVALID_TICKET;
    m_mig_transmitter_cmd_sub.put(m_incoming_mig_cmd);
}

TEST_F(MigrationTransmitterCmdSubscriberTestSuite, Test_put_already_migrating) {
    {
        InSequence seq{};
        EXPECT_CALL(*m_slot_ptr->getMigThreadPtr(),
                    markShouldMigrate(EqCmdBackChannelData(m_expected_cmd_back_channel_data),
                                      Ref(m_mig_cmd_callback_topic)))
          .WillOnce(Return(false));

        m_expected_command_ack.mig_error = MigrationError::THREAD_ALREADY_MIGRATING_ON_SRC_NODE;
        EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_command_ack), true));
    }

    m_mig_transmitter_cmd_sub.put(m_incoming_mig_cmd);
}

} // namespace
