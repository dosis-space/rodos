#include "migration/migration-types.h"
#include "migration/implementation/commander/migration-commander-helper.h"

#include "synccommbuffer.h"
#include "rodos-debug.h"
#include "timemodel.h"

#include "utilities/matchers.h"

#include <gmock/gmock-actions.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationCommanderHelperTestSuite : public ::testing::Test {
  protected:
    static constexpr uint16_t SYS_HASH    = 0x1234;
    static constexpr uint16_t INSTANCE_ID = 0xAA;

    static constexpr uint16_t DUMMY_OTHER_SYS_HASH      = 0xABCD;
    static constexpr uint16_t DUMMY_OTHER_INSTANCE_ID_0 = INSTANCE_ID;
    static constexpr uint16_t DUMMY_OTHER_INSTANCE_ID_1 = 0xBB;
    static constexpr uint16_t DUMMY_TICKET              = 0x1000;

    static constexpr uint16_t EXPECTED_STARTING_SEQ_NUM = 1;

    static constexpr MigrationError DUMMY_INCOMING_MIG_ERROR =
      MigrationError::INVALID_SLOT_ON_SRC_NODE;

    static constexpr int64_t DUMMY_TIMEOUT = 10 * RODOS::SECONDS;

    void SetUp() override {}

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<MigrationAck>         m_mig_ack_topic{};
    Topic<MigrationCmd>         m_mig_cmd_topic{};
    Topic<MigrationInfoRequest> m_mig_info_request_topic{};

    StrictMock<SyncCommBuffer<MigrationError>>              m_mig_error_forwarder{};
    StrictMock<implementation::ack::MigrationAckSubscriber> m_mig_command_ack_sub{};

    StrictMock<SyncCommBuffer<int32_t>>                       m_mig_commander_notifier{};
    StrictMock<implementation::info::MigrationInfoSubscriber> m_mig_info_sub{};

    implementation::commander::MigrationCommanderHelper m_mig_commander_helper{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_cmd_topic,
        m_mig_info_request_topic,
        m_mig_error_forwarder,
        m_mig_command_ack_sub,
        m_mig_commander_notifier,
        m_mig_info_sub
    };

    MigrationCmd m_expected_mig_cmd{
        .sender_sys_hash    = SYS_HASH,
        .sender_instance_id = INSTANCE_ID,
        .target_sys_hash    = DUMMY_OTHER_SYS_HASH,
        .migrate_from_id    = DUMMY_OTHER_INSTANCE_ID_0,
        .migrate_to_id      = DUMMY_OTHER_INSTANCE_ID_1,
        .seq_num            = EXPECTED_STARTING_SEQ_NUM,
        .ticket             = DUMMY_TICKET
    };

    MigrationInfoRequest m_expected_info_request{
        .sender_sys_hash     = SYS_HASH,
        .sender_instance_id  = INSTANCE_ID,
        .request_sys_hash    = DUMMY_OTHER_SYS_HASH,
        .request_instance_id = DUMMY_OTHER_INSTANCE_ID_0
    };
};


TEST_F(MigrationCommanderHelperTestSuite, Test_requestMigration) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_command_ack_sub, startAcceptingAcks(EXPECTED_STARTING_SEQ_NUM));
        EXPECT_CALL(m_mig_cmd_topic, publishMock(EqMigCmd(m_expected_mig_cmd), true));
    }
    m_mig_commander_helper.requestMigration(DUMMY_OTHER_SYS_HASH,
                                            DUMMY_OTHER_INSTANCE_ID_0,
                                            DUMMY_OTHER_INSTANCE_ID_1,
                                            DUMMY_TICKET);
}

TEST_F(MigrationCommanderHelperTestSuite, Test_waitForMigrationResponse) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_error_forwarder, syncGet(_, DUMMY_TIMEOUT))
          .WillOnce(DoAll(SetArgReferee<0>(DUMMY_INCOMING_MIG_ERROR),
                          Return(true)));
        EXPECT_CALL(m_mig_command_ack_sub, stopAcceptingAcks());
    }
    MigrationError mig_error{};
    ASSERT_TRUE(
      m_mig_commander_helper.waitForMigrationResponse(mig_error, DUMMY_TIMEOUT));
    ASSERT_EQ(DUMMY_INCOMING_MIG_ERROR, mig_error);
}


TEST_F(MigrationCommanderHelperTestSuite, Test_syncGetMigrationInfo) {
    constexpr uint8_t NUM_SLOTS = 10;
    MigrationSlotInfo mig_storage_info[NUM_SLOTS]{};
    constexpr int32_t DIFF_TO_EXPECTED_SIZE_OUT = -13;
    int32_t           diff_to_expected_size{};
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_info_sub, startAcceptingMigInfos(mig_storage_info, NUM_SLOTS));
        EXPECT_CALL(m_mig_info_request_topic,
                    publishMock(EqMigInfoRequest(m_expected_info_request), true));
        EXPECT_CALL(m_mig_commander_notifier, syncGet(_, DUMMY_TIMEOUT))
          .WillOnce(DoAll(SetArgReferee<0>(DIFF_TO_EXPECTED_SIZE_OUT),
                          Return(true)));
        EXPECT_CALL(m_mig_info_sub, stopAcceptingMigInfos());
    }

    m_mig_commander_helper.syncGetMigrationInfo(DUMMY_OTHER_SYS_HASH,
                                                DUMMY_OTHER_INSTANCE_ID_0,
                                                mig_storage_info,
                                                diff_to_expected_size,
                                                DUMMY_TIMEOUT);
}

} // namespace
