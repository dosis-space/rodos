#include "migration/migration-types.h"
#include "migration/implementation/storage/migration-storage.h"
#include "migration/implementation/receiver/migration-receiver-helper.h"

#include "timemodel.h"
#include "synccommbuffer.h"

#include "receiver_test_class.h"
#include "utilities/matchers.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <cstring>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationReceiverHelperTestSuite : public ::testing::Test {
  protected:
    static constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{
        .num_slots       = 2,
        .max_thread_size = 500,
        .stack_size      = 100
    };
    static constexpr meta::MigrationQueueParams MIG_QUEUE_PARAMS{
        .num_elems_mig_recv_queue = 2,
    };
    using TestTypeList = TypeList<ReceiverTestClass>;

    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;

    using ConcreteMigStorage =
      implementation::storage::MigrationStorage<TestTypeList, MIG_STORAGE_PARAMS>;
    using ConcreteMigStorageSlot =
      typename ConcreteMigStorage::ConcreteMigStorageSlot;

    using ConcreteMigReceiverHelper =
      implementation::receiver::
        MigrationReceiverHelper<TestTypeList, MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;

    static constexpr uint32_t RECEIVER_CLASS_IND = TestTypeList::index_of<ReceiverTestClass>();

    static constexpr uint16_t SYS_HASH    = 0x1234;
    static constexpr uint16_t INSTANCE_ID = 0xAA;

    static constexpr uint16_t DUMMY_TRANSMITTER_ID = 0xBB;

    static constexpr uint32_t DUMMY_TICKET       = 0x1000;
    static constexpr int      DUMMY_X_VAL        = 0x3344;
    static constexpr uint32_t DUMMY_OTHER_TICKET = 0x2000;
    static constexpr int      DUMMY_OTHER_X_VAL  = 0x5566;

    void SetUp() override {
        m_thread_ptr_in_dummy_mig_data = new(m_dummy_incoming_mig_data.bytes) ReceiverTestClass{
            DUMMY_TICKET,
            DUMMY_X_VAL
        };
    }

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Semaphore           m_mig_sema{};
    Topic<MigrationAck> m_mig_ack_topic{};

    ConcreteMigStorage m_mig_storage{};

    ConcreteMigReceiverHelper m_mig_receiver_helper{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_sema,
        m_mig_ack_topic,
        m_mig_storage
    };

    ConcreteMigStorageSlot& m_first_slot{ *m_mig_storage.getSlotPtrByIndex(0) };
    ConcreteMigStorageSlot& m_second_slot{ *m_mig_storage.getSlotPtrByIndex(1) };

    ReceiverTestClass m_dummy_other_test_obj{ DUMMY_OTHER_TICKET, DUMMY_OTHER_X_VAL };
    ConcreteMigData   m_dummy_incoming_mig_data{
          .sys_hash         = SYS_HASH,
          .src_instance_id  = DUMMY_TRANSMITTER_ID,
          .dest_instance_id = INSTANCE_ID,
          .type_index       = RECEIVER_CLASS_IND,
          .ticket           = DUMMY_TICKET,
          .len              = sizeof(ReceiverTestClass),
          .bytes{},
    };
    ReceiverTestClass* m_thread_ptr_in_dummy_mig_data{ nullptr };

    MigrationAck m_expected_mig_response_ack{
        .dest_sys_hash    = SYS_HASH,
        .dest_instance_id = DUMMY_TRANSMITTER_ID,
        .ack_type         = AckType::RESPONSE_ACK,
        .mig_error        = MigrationError::OK,
        .seq_num          = EXPECTED_SEQ_NUM_FOR_RESPONSE_ACK
    };
};


TEST_F(MigrationReceiverHelperTestSuite, Test_receiveThread_valid) {
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_mig_response_ack), true));

    ASSERT_TRUE(m_mig_receiver_helper.receiveThread(m_dummy_incoming_mig_data));

    ASSERT_TRUE(m_first_slot.isValidlyOccupied());
    auto* slot_thread_ptr = static_cast<ReceiverTestClass*>(m_first_slot.getMigThreadPtr());
    ASSERT_EQ(m_thread_ptr_in_dummy_mig_data->getTicket(), slot_thread_ptr->getTicket());
    ASSERT_EQ(m_thread_ptr_in_dummy_mig_data->m_x, slot_thread_ptr->m_x);
    ASSERT_TRUE(slot_thread_ptr->m_is_copy_constructed);
}

TEST_F(MigrationReceiverHelperTestSuite, Test_receiveThread_invalid_type_index) {
    m_expected_mig_response_ack.mig_error = MigrationError::INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE;
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_mig_response_ack), true));

    constexpr int32_t INVALID_TYPE_INDEX = TestTypeList::size;
    m_dummy_incoming_mig_data.type_index = INVALID_TYPE_INDEX;
    ASSERT_FALSE(m_mig_receiver_helper.receiveThread(m_dummy_incoming_mig_data));

    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() != nullptr);
    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() != nullptr);
    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() == nullptr);
}

TEST_F(MigrationReceiverHelperTestSuite, Test_receiveThread_invalid_size) {
    m_expected_mig_response_ack.mig_error = MigrationError::INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE;
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_mig_response_ack), true));

    m_dummy_incoming_mig_data.len = 0;
    ASSERT_FALSE(m_mig_receiver_helper.receiveThread(m_dummy_incoming_mig_data));

    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() != nullptr);
    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() != nullptr);
    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() == nullptr);
}


TEST_F(MigrationReceiverHelperTestSuite, Test_receiveThread_storage_replay) {

    m_expected_mig_response_ack.mig_error = MigrationError::THREAD_ALREADY_EXISTENT_ON_DEST_NODE;
    EXPECT_CALL(m_mig_ack_topic,
                publishMock(EqMigAck(m_expected_mig_response_ack),
                            true));

    auto& first_slot = *m_mig_storage.reserveEmptySlotUnprotected();
    ASSERT_EQ(&m_first_slot, &first_slot);
    first_slot.createThread<ReceiverTestClass>(*m_thread_ptr_in_dummy_mig_data);

    ASSERT_FALSE(m_mig_receiver_helper.receiveThread(m_dummy_incoming_mig_data));

    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() != nullptr);
    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() == nullptr);
}

TEST_F(MigrationReceiverHelperTestSuite, Test_receiveThread_storage_full) {
    m_expected_mig_response_ack.mig_error = MigrationError::NO_EMPTY_SPACE_ON_DEST_NODE;
    EXPECT_CALL(m_mig_ack_topic, publishMock(EqMigAck(m_expected_mig_response_ack), true));

    auto& first_slot = *m_mig_storage.reserveEmptySlotUnprotected();
    ASSERT_EQ(&m_first_slot, &first_slot);
    first_slot.createThread<ReceiverTestClass>(m_dummy_other_test_obj);
    auto& second_slot = *m_mig_storage.reserveEmptySlotUnprotected();
    ASSERT_EQ(&m_second_slot, &second_slot);
    second_slot.createThread<ReceiverTestClass>(m_dummy_other_test_obj);

    ASSERT_FALSE(m_mig_receiver_helper.receiveThread(m_dummy_incoming_mig_data));

    ASSERT_TRUE(m_mig_storage.reserveEmptySlotUnprotected() == nullptr);
    ASSERT_EQ(DUMMY_OTHER_TICKET, m_first_slot.getMigThreadPtr()->getTicket());
    ASSERT_EQ(DUMMY_OTHER_TICKET, m_second_slot.getMigThreadPtr()->getTicket());
}

} // namespace
