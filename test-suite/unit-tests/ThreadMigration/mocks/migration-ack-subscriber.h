#pragma once

#include "migration/migration-types.h"

#include "synccommbuffer.h"

#include <gmock/gmock-function-mocker.h>

namespace RODOS::migration::implementation::ack {

class MigrationAckSubscriber {
  public:
    MigrationAckSubscriber()          = default;
    virtual ~MigrationAckSubscriber() = default;

    MOCK_METHOD(void, startAcceptingAcks, (uint16_t expected_seq_num));
    MOCK_METHOD(void, stopAcceptingAcks, ());

    MOCK_METHOD(void, put, (MigrationAck& mig_ack));
};

} // namespace RODOS::migration::implementation::ack
