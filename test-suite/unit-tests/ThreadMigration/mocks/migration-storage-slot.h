#pragma once

#include <gmock/gmock-function-mocker.h>

namespace RODOS::migration::implementation::storage {

template <typename TList, uint32_t MAX_THREAD_SIZE, uint32_t STACK_SIZE>
class MigrationStorageSlot {
  public:
    MigrationStorageSlot()          = default;
    virtual ~MigrationStorageSlot() = default;

    MOCK_METHOD(void, createThreadMock, ());

    template <typename T>
    void createThread(const T& rhs) {
        createThreadMock();
    }
};

} // namespace RODOS::migration::implementation::storage
