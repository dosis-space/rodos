#pragma once

#include "migration/migration-types.h"

#include "subscriber.h"
#include "synccommbuffer.h"

#include <gmock/gmock-function-mocker.h>

namespace RODOS::migration::implementation::info {

class MigrationInfoSubscriber {
  public:
    MigrationInfoSubscriber()          = default;
    virtual ~MigrationInfoSubscriber() = default;

    MOCK_METHOD(void, startAcceptingMigInfos, (void* mig_slot_info_array, uint8_t num_slots));
    MOCK_METHOD(void, stopAcceptingMigInfos, ());

    MOCK_METHOD(uint32_t, put,
                (const uint32_t topic_id, const size_t len, void* data, const NetMsgInfo&));
};

} // namespace RODOS::migration::implementation::info
