#pragma once

#include "migration/migration-types.h"

#include "thread.h"
#include "topic.h"

#include <gmock/gmock-function-mocker.h>

#include <cstdint>

namespace RODOS::migration {

class MigratableThread : public Thread {
  public:
    MigratableThread(uint32_t ticket, const char*) : m_ticket{ ticket } {}
    MigratableThread(const MigratableThread& rhs) {}
    virtual ~MigratableThread() = default;

    virtual void loop() = 0;

    uint32_t getTicket() const { return m_ticket; }

    void initializeMigratableThread(uint8_t* stack_begin, uint32_t stack_size) {}

    MOCK_METHOD(bool, markShouldMigrate,
                (const CmdBackChannelData&  throw_back_data,
                 Topic<CmdBackChannelData>& mig_cmd_callback_topic));

    MOCK_METHOD(void, resume, ());

  private:
    uint32_t m_ticket;
};

} // namespace RODOS::migration
