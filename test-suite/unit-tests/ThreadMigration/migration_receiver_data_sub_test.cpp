#include "migration/migration-types.h"
#include "migration/implementation/receiver/migration-receiver-data-subscriber.h"
#include "migration/implementation/receiver/scoped_mig_data_read_session.h"

#include "fifo.h"
#include "timemodel.h"
#include "rodos-debug.h"

#include "utilities/matchers.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;
using namespace ::testing;

class MigrationReceiverDataSubscriberTestSuite : public ::testing::Test {
  protected:
    static constexpr uint32_t PUT_RET_SUCCESS = 1;
    static constexpr uint32_t PUT_RET_DROPPED = 0;

    static constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{
        .num_slots       = 1,
        .max_thread_size = 500,
        .stack_size      = 100
    };
    static constexpr meta::MigrationQueueParams MIG_QUEUE_PARAMS{
        .num_elems_mig_recv_queue = 2
    };
    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;

    using ConcreteMigReceiverDataSub =
      implementation::receiver::MigrationReceiverDataSubscriber<MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;
    using ConcreteScopedMigDataReadSession =
      implementation::receiver::ScopedMigDataReadSession<MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;

    using MigRecvQueue = BlockFifo<ConcreteMigData, MIG_QUEUE_PARAMS.num_elems_mig_recv_queue>;

    static constexpr uint16_t SYS_HASH          = 0x1234;
    static constexpr uint16_t INVALID_SYS_HASH  = 0xFFFF;
    static constexpr uint16_t INSTANCE_ID       = 0xAA;
    static constexpr uint16_t OTHER_INSTANCE_ID = 0xFFFF;

    static constexpr uint16_t DUMMY_SRC_INSTANCE_ID = 0xBB;

    static constexpr int32_t  DUMMY_TYPE_INDEX = 7;
    static constexpr uint32_t DUMMY_TICKET     = 0x3000;
    static constexpr uint8_t  DUMMY_ARRAY[]{ 1, 2, 3, 4, 5 };

    static constexpr uint8_t DUMMY_FILL_CHARACTER = 0xFF;

    void SetUp() override {
        std::memcpy(m_incoming_mig_data.bytes, DUMMY_ARRAY, sizeof(DUMMY_ARRAY));
        std::memset(&m_dummy_mig_data_out, DUMMY_FILL_CHARACTER, sizeof(m_dummy_mig_data_out));
    }

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }

    Topic<ConcreteMigData> m_mig_data_topic{};

    Semaphore                m_mig_recv_queue_sema{};
    StrictMock<MigRecvQueue> m_mig_recv_queue{};

    Thread                     m_dummy_thread{};
    ConcreteMigReceiverDataSub m_mig_receiver_data_sub{
        SYS_HASH,
        INSTANCE_ID,
        m_mig_data_topic,
        m_mig_recv_queue_sema,
        m_mig_recv_queue,
        m_dummy_thread
    };

    ConcreteMigData m_incoming_mig_data{
        .sys_hash         = SYS_HASH,
        .src_instance_id  = DUMMY_SRC_INSTANCE_ID,
        .dest_instance_id = INSTANCE_ID,
        .type_index       = DUMMY_TYPE_INDEX,
        .ticket           = DUMMY_TICKET,
        .len              = sizeof(DUMMY_ARRAY),
        .bytes{}
    };
    uint32_t m_expected_actual_size_mig_data =
      MIG_DATA_SIZE_WITHOUT_BYTE_BUFFER + m_incoming_mig_data.len;

    NetMsgInfo m_dummy_net_msg_info{};

    ConcreteMigData m_dummy_mig_data_out{};
};


TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_valid) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_recv_queue, getBufferToWrite(_)).WillOnce(Return(&m_dummy_mig_data_out));
        EXPECT_CALL(m_mig_recv_queue, writeConcluded(1));
        EXPECT_CALL(m_dummy_thread, resume());
    }

    ASSERT_EQ(PUT_RET_SUCCESS, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId,
                                                           m_expected_actual_size_mig_data,
                                                           &m_incoming_mig_data,
                                                           m_dummy_net_msg_info));
    ASSERT_EQ(0, std::memcmp(&m_dummy_mig_data_out,
                             &m_incoming_mig_data,
                             m_expected_actual_size_mig_data));
    for(unsigned i = m_dummy_mig_data_out.len; i < sizeof(m_dummy_mig_data_out.bytes); i++) {
        ASSERT_EQ(DUMMY_FILL_CHARACTER, m_dummy_mig_data_out.bytes[i]);
    }
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_sanity_check_invalid_topicId) {
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId + 1,
                                                           m_expected_actual_size_mig_data,
                                                           &m_incoming_mig_data,
                                                           m_dummy_net_msg_info));
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_sanity_check_too_small_message) {
    constexpr uint32_t TOO_SMALL_SIZE = ConcreteMigReceiverDataSub::MIN_ALLOWED_MIG_DATA_SIZE - 1;
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId,
                                                           TOO_SMALL_SIZE,
                                                           &m_incoming_mig_data,
                                                           m_dummy_net_msg_info));
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_sanity_check_too_large_message) {
    constexpr uint32_t TOO_LARGE_SIZE = ConcreteMigReceiverDataSub::MAX_ALLOWED_MIG_DATA_SIZE + 1;
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId,
                                                           TOO_LARGE_SIZE,
                                                           &m_incoming_mig_data,
                                                           m_dummy_net_msg_info));
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_sanity_check_nullptr) {
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId,
                                                           m_expected_actual_size_mig_data,
                                                           nullptr,
                                                           m_dummy_net_msg_info));
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_invalid_mig_data_sys_hash) {
    m_incoming_mig_data.sys_hash = INVALID_SYS_HASH;
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId,
                                                           m_expected_actual_size_mig_data,
                                                           &m_incoming_mig_data,
                                                           m_dummy_net_msg_info));
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_put_invalid_mig_data_destination) {
    m_incoming_mig_data.dest_instance_id = OTHER_INSTANCE_ID;
    ASSERT_EQ(PUT_RET_DROPPED, m_mig_receiver_data_sub.put(m_mig_data_topic.topicId,
                                                           m_expected_actual_size_mig_data,
                                                           &m_incoming_mig_data,
                                                           m_dummy_net_msg_info));
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_Session_valid) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_recv_queue, getBufferToRead(_)).WillOnce(Return(&m_incoming_mig_data));
        EXPECT_CALL(m_mig_recv_queue, readConcluded(1));
    }
    ConcreteScopedMigDataReadSession session{
        m_mig_recv_queue_sema,
        m_mig_recv_queue
    };
    ASSERT_EQ(&m_incoming_mig_data, &session.accessMigData());
}

TEST_F(MigrationReceiverDataSubscriberTestSuite, Test_Session_nullptr) {
    {
        InSequence seq{};
        EXPECT_CALL(m_mig_recv_queue, getBufferToRead(_))
          .WillOnce(Return(nullptr))
          .WillOnce(Return(&m_incoming_mig_data));
        EXPECT_CALL(m_mig_recv_queue, readConcluded(1));
    }
    ConcreteScopedMigDataReadSession session{
        m_mig_recv_queue_sema,
        m_mig_recv_queue
    };
    ASSERT_EQ(&m_incoming_mig_data, &session.accessMigData());
}

} // namespace
