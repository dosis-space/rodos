#include "migration/type-list.h"

#include <gtest/gtest.h>

namespace {
using namespace RODOS;
using namespace RODOS::migration;

struct Foo {
    void setX(int x) { m_x = 1 * x; }

    static constexpr int DEFAULT_VAL = -1;
    int                  m_x{ DEFAULT_VAL };
};

struct Bar {
    void setX(int x) { m_x = 2 * x; }

    static constexpr int DEFAULT_VAL = -2;
    int                  m_x{ DEFAULT_VAL };
};

struct TestFunctor {
    template <typename T>
    static void func(void* addr, int x) {
        auto* ptr = static_cast<T*>(addr);
        ptr->setX(x);
    }
};


class TypeListTestSuite : public ::testing::Test {
  protected:
    static constexpr int32_t SIZE_LIST0 = 0;
    static constexpr int32_t SIZE_LIST1 = 4;

    using MyTypeList0 = TypeList<>;
    using MyTypeList1 = TypeList<int, double, Foo, Bar>;
    using MyTypeList2 = TypeList<Foo, Bar>;

    void SetUp() override {}

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }
};


TEST_F(TypeListTestSuite, Test_TypeList_size) {
    ASSERT_EQ(SIZE_LIST0, MyTypeList0::size);
    ASSERT_EQ(SIZE_LIST1, MyTypeList1::size);
}

TEST_F(TypeListTestSuite, Test_TypeList_index_of) {
    static constexpr int32_t INDEX0 = 0;
    ASSERT_EQ(INDEX0, MyTypeList1::index_of<int>());
    static constexpr int32_t INDEX3 = 3;
    ASSERT_EQ(INDEX3, MyTypeList1::index_of<Bar>());
}

TEST_F(TypeListTestSuite, Test_TypeList_callFuncOnIndexedType) {
    constexpr static int X_NEW = 42;
    Bar                  bar{};

    // invalid index with empty list
    constexpr int32_t INVALID_IND_LIST0 = 0;
    ASSERT_EQ(IndexedTypeFound::FALSE,
              MyTypeList0::callFuncOnIndexedType<TestFunctor>(
                INVALID_IND_LIST0, &bar, X_NEW));
    ASSERT_EQ(Bar::DEFAULT_VAL, bar.m_x);

    // too big index with non-empty list
    constexpr int32_t OVERFLOWING_IND_LIST2 = 3;
    ASSERT_EQ(IndexedTypeFound::FALSE,
              MyTypeList2::callFuncOnIndexedType<TestFunctor>(
                OVERFLOWING_IND_LIST2, &bar, X_NEW));
    ASSERT_EQ(Bar::DEFAULT_VAL, bar.m_x);

    // negative index with non-empty list
    constexpr int32_t UNDERFLOWING_IND_LIST2 = -1;
    ASSERT_EQ(IndexedTypeFound::FALSE,
              MyTypeList2::callFuncOnIndexedType<TestFunctor>(
                UNDERFLOWING_IND_LIST2, &bar, X_NEW));
    ASSERT_EQ(Bar::DEFAULT_VAL, bar.m_x);

    // valid call
    constexpr int32_t BAR_IND = MyTypeList2::index_of<Bar>();
    ASSERT_EQ(IndexedTypeFound::TRUE,
              MyTypeList2::callFuncOnIndexedType<TestFunctor>(
                BAR_IND, &bar, X_NEW));
    ASSERT_EQ(2 * X_NEW, bar.m_x);
}

} // namespace
