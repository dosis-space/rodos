#include "migration/type-list.h"
#include "migration/implementation/migration-functors.h"

#include "rodos-debug.h"

#include "functor_test_classes.h"
#include "thread.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cstring>

namespace {
using namespace RODOS::migration;
using namespace ::testing;

class MigrationFunctorTestSuite : public ::testing::Test {
  protected:
    using TestGetSizeOfTypeList = TypeList<GetSizeOfTypeClass>;
    using TestReconstructList   = TypeList<ReconstructClass>;
    using TestCopyList          = TypeList<CopyClass>;
    using TestDeconstructList   = TypeList<DeconstructClass>;

    static constexpr uint32_t MAX_THREAD_SIZE = 200;
    static constexpr uint32_t STACK_SIZE      = 100;

    using ConcreteMigStorageSlot =
      implementation::storage::MigrationStorageSlot<TestReconstructList, MAX_THREAD_SIZE, STACK_SIZE>;

    using ConcreteReconstructFunctor =
      implementation::ReconstructFunctor<TestReconstructList, MAX_THREAD_SIZE, STACK_SIZE>;

    static constexpr int GET_SIZE_OF_TYPE_CLASS_IND = TestGetSizeOfTypeList::index_of<GetSizeOfTypeClass>();
    static constexpr int RECONSTRUCT_CLASS_IND      = TestReconstructList::index_of<ReconstructClass>();
    static constexpr int COPY_CLASS_IND             = TestCopyList::index_of<CopyClass>();
    static constexpr int DECONSTRUCT_CLASS_IND      = TestDeconstructList::index_of<DeconstructClass>();

    static constexpr uint32_t    DUMMY_THREAD_TICKET = 0x66778899;
    static constexpr const char* DUMMY_THREAD_NAME   = "Dummy Thread Name";
    static constexpr uint16_t    DUMMY_X_VAL         = 0x1234;

    void SetUp() override {
        DeconstructClass::s_destruct_count = 0;
    }

    void TearDown() override {
        EXPECT_FALSE(RODOS::error) << RODOS::errorMessage;
        RODOS::error = false;
    }
};

TEST_F(MigrationFunctorTestSuite, Test_GetSizeOfTypeFunctor_Valid) {
    uint32_t size{};

    auto ret_valid =
      TestGetSizeOfTypeList::callFuncOnIndexedType<implementation::GetSizeOfTypeFunctor>(
        GET_SIZE_OF_TYPE_CLASS_IND,
        size);
    ASSERT_EQ(IndexedTypeFound::TRUE, ret_valid);
    ASSERT_EQ(size, sizeof(GetSizeOfTypeClass));
}

TEST_F(MigrationFunctorTestSuite, Test_ReconstructFunctor_Valid) {
    ConcreteMigStorageSlot mock_slot{};
    uint8_t                dummyBuffer[sizeof(ReconstructClass)]{};

    EXPECT_CALL(mock_slot, createThreadMock());

    auto ret_valid =
      TestReconstructList::callFuncOnIndexedType<ConcreteReconstructFunctor>(
        RECONSTRUCT_CLASS_IND,
        mock_slot,
        dummyBuffer[0]);
    ASSERT_EQ(IndexedTypeFound::TRUE, ret_valid);
}

TEST_F(MigrationFunctorTestSuite, Test_CopyFunctor_Valid) {
    uint8_t  original_object_buffer[sizeof(CopyClass)]{};
    uint8_t  reference_copy_buffer[sizeof(CopyClass)]{};
    uint8_t  copy_buffer[sizeof(CopyClass)]{};
    uint32_t bytes_written{};

    {
        auto* object_ptr = new(original_object_buffer) CopyClass{
            DUMMY_THREAD_TICKET,
            DUMMY_THREAD_NAME,
            DUMMY_X_VAL,
        };
        std::memcpy(reference_copy_buffer, original_object_buffer, sizeof(reference_copy_buffer));

        auto ret_valid =
          TestCopyList::callFuncOnIndexedType<implementation::CopyFunctor>(
            COPY_CLASS_IND,
            *object_ptr,
            copy_buffer[0],
            bytes_written);
        ASSERT_EQ(IndexedTypeFound::TRUE, ret_valid);
    }
    ASSERT_EQ(sizeof(copy_buffer), bytes_written);
    ASSERT_EQ(0, std::memcmp(reference_copy_buffer, copy_buffer, sizeof(copy_buffer)));

    auto* reference_copy_object = reinterpret_cast<CopyClass*>(reference_copy_buffer);
    auto* copied_object         = reinterpret_cast<CopyClass*>(copy_buffer);
    ASSERT_EQ(reference_copy_object->m_x, copied_object->m_x);
    ASSERT_EQ(reference_copy_object->getTicket(), copied_object->getTicket());
}

TEST_F(MigrationFunctorTestSuite, Test_DeconstructFunctor_Valid) {
    uint8_t original_object_buffer[sizeof(DeconstructClass)]{};
    int     manual_destruct_count = 0;
    ASSERT_EQ(manual_destruct_count, DeconstructClass::s_destruct_count);

    {
        auto* object_ptr = new(original_object_buffer) DeconstructClass{
            DUMMY_THREAD_TICKET,
            DUMMY_THREAD_NAME,
        };

        auto ret_valid =
          TestDeconstructList::callFuncOnIndexedType<implementation::DestructFunctor>(
            DECONSTRUCT_CLASS_IND,
            *object_ptr);
        ASSERT_EQ(IndexedTypeFound::TRUE, ret_valid);
        ASSERT_EQ(++manual_destruct_count, DeconstructClass::s_destruct_count);
    }
}

} // namespace
