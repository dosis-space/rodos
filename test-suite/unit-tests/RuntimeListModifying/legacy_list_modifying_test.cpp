#include <gtest/gtest.h>

#include "list-legacy.h"
#include "listelement.h"

#include "thread.h"
#include "misc-rodos-funcs.h"
#include "rodos-debug.h"

namespace {

constexpr int      NUM_ELEMS_GLOB = 3;
RODOS::ListElement globDefaultElems[NUM_ELEMS_GLOB]{
    { "elem0" },
    { "elem1" },
    { "elem2" }
};

class LegacyListModifyingTestSuite : public ::testing::Test {
  protected:
    void SetUp() override {
        RODOS::isShuttingDown   = false;
        RODOS::schedulerRunning = false;

        for(int i = 0; i < NUM_ELEMS_GLOB; i++) {
            globDefaultElems[i].append(m_legacyList);
        }
        ASSERT_EQ(0, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
    }

    void TearDown() override {
        ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
        RODOS::rodosErrorCounter = 0;
    }

    unsigned long m_expected_error_counter{ 0 };

    RODOS::List m_legacyList{ nullptr };
    RODOS::List m_otherList{ nullptr };
};

TEST_F(LegacyListModifyingTestSuite, Test_iterator_operations) {
    // test prefix / deref / member of pointer
    {
        auto iter = m_legacyList.begin();
        ASSERT_EQ(&globDefaultElems[NUM_ELEMS_GLOB - 1], &*iter);
        for(int i = NUM_ELEMS_GLOB - 1; i-- > 0;) {
            ASSERT_EQ(&globDefaultElems[i], &*++iter);
            ASSERT_EQ(globDefaultElems[i].listElementID, iter->listElementID);
        }
        ASSERT_TRUE(m_legacyList.end() == ++iter);
    }

    // test postfix / deref / member of pointer
    {
        auto iter = m_legacyList.begin();
        for(int i = NUM_ELEMS_GLOB; i-- > 0;) {
            ASSERT_EQ(globDefaultElems[i].listElementID, iter->listElementID);
            ASSERT_EQ(&globDefaultElems[i], &*iter++);
        }
        ASSERT_TRUE(m_legacyList.end() == iter);
    }

    // test comparison
    {
        auto iter0 = m_legacyList.begin();
        auto iter1 = m_legacyList.begin();
        for(int i = NUM_ELEMS_GLOB; i-- > 0;) {
            ASSERT_TRUE(iter0++ == iter1++);
        }
        ASSERT_TRUE(iter0 == m_legacyList.end());
        ASSERT_TRUE(iter1 == m_legacyList.end());
    }

    RODOS::isShuttingDown = true;
}

TEST_F(LegacyListModifyingTestSuite, Test_iterator_sequences) {
    // iterator over empty list
    auto iterEmptyList = m_otherList.begin();
    ASSERT_TRUE(iterEmptyList == m_otherList.end());

    // iterator over currently one-element list
    RODOS::ListElement elem0{ m_otherList, "elem0" };
    auto               iterOneElemList = m_otherList.begin();

    // iterator over two-element list
    RODOS::ListElement elem1{ m_otherList, "elem1" };
    auto               iterTwoElemList = m_otherList.begin();

    ASSERT_TRUE(iterTwoElemList != m_otherList.end());
    ASSERT_EQ(&elem1, &*iterTwoElemList);
    iterTwoElemList++;
    ASSERT_TRUE(iterTwoElemList != m_otherList.end());
    ASSERT_EQ(&elem0, &*iterTwoElemList);
    iterTwoElemList++;
    ASSERT_TRUE(iterTwoElemList == m_otherList.end());

    // test if old iterator still only iterates over one element
    ASSERT_TRUE(iterOneElemList != m_otherList.end());
    ASSERT_EQ(&elem0, &*iterOneElemList);
    iterOneElemList++;
    ASSERT_TRUE(iterOneElemList == m_otherList.end());

    RODOS::isShuttingDown = true;
}


TEST_F(LegacyListModifyingTestSuite, Test_destruction) {
    // test destruction non-appended ListElement (valid)
    {
        RODOS::ListElement elem0{ "elem0" };
    }
    ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;

    // test destruction appended ListElement (invalid)
    {
        RODOS::ListElement elem1{ m_legacyList, "elem1" };
    }
    ASSERT_EQ(++m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
    RODOS::isShuttingDown = true;
}

TEST_F(LegacyListModifyingTestSuite, Test_range_based_loop) {
    // test standard loop
    {
        unsigned index = NUM_ELEMS_GLOB;
        for(auto& elem : m_legacyList) {
            index--;

            ASSERT_EQ(&globDefaultElems[index], &elem);
        }
    }

    // test nested iterator loop
    {
        unsigned index_outer = NUM_ELEMS_GLOB;
        for(auto& elem_outer : m_legacyList) {
            index_outer--;

            unsigned index_inner = NUM_ELEMS_GLOB;
            for(auto& elem_inner : m_legacyList) {
                index_inner--;

                ASSERT_EQ(&globDefaultElems[index_inner], &elem_inner);
                ASSERT_EQ(&globDefaultElems[index_outer], &elem_outer);
            }
        }
    }

    RODOS::isShuttingDown = true;
}

TEST_F(LegacyListModifyingTestSuite, Test_shovel_over) {
    // test re-appending all elements of one list to another (shoveling over)
    // -> done e.g. in 'main.cpp' to distribute Subscribers to Topics
    // IMPORTANT: after shoveling over, original list is in undefined state
    for(auto iter = m_legacyList.begin(); iter != m_legacyList.end();) {
        auto* elem_ptr = &(*iter);
        iter++;

        elem_ptr->append(m_otherList);
        ASSERT_EQ(0, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
    }

    int index = 0;
    for(auto& elem : m_otherList) {
        ASSERT_EQ(&globDefaultElems[index++], &elem);
    }

    RODOS::isShuttingDown = true;
}

TEST_F(LegacyListModifyingTestSuite, Test_macro) {
    // test macro simple
    unsigned index = NUM_ELEMS_GLOB;
    ITERATE_LIST(RODOS::ListElement, m_legacyList) {
        ASSERT_EQ(&globDefaultElems[--index], iter);
    }

    // test macro loop breakable
    constexpr int       ARBITRARY_BREAK_INDEX = 1;
    RODOS::ListElement* elem1Ptr{ nullptr };
    ITERATE_LIST(RODOS::ListElement, m_legacyList) {
        if(iter == &globDefaultElems[ARBITRARY_BREAK_INDEX]) {
            elem1Ptr = iter;
            break;
        }
    }
    ASSERT_EQ(&globDefaultElems[ARBITRARY_BREAK_INDEX], elem1Ptr);

    RODOS::isShuttingDown = true;
}

} // namespace
