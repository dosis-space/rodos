#include "misc-rodos-funcs.h"

#include "thread.h"

namespace RODOS {

bool isShuttingDown = false;

bool isSchedulerRunning() {
    return schedulerRunning;
}

} // namespace RODOS
