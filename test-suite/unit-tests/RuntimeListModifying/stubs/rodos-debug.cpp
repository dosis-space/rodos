#include "rodos-debug.h"

#include <cstdio>

namespace RODOS {

void PRINTF(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}

unsigned long rodosErrorCounter = 0;
const char* rodosErrorMsg = "";
void RODOS_ERROR(const char* text) {
    rodosErrorCounter++;
    rodosErrorMsg = text;
}

} // namespace RODOS
