#include "thread.h"

namespace RODOS {

std::atomic<bool> schedulerRunning{ false };

void Thread::yield() {}

} // namespace RODOS
