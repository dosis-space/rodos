#include <gtest/gtest.h>

#include "list-iter-info.h"
#include "listelement.h"

#include "rodos-debug.h"

namespace {

using namespace RODOS;

class ListIterInfoTestSuite : public ::testing::Test {
  protected:
    void TearDown() override {
        EXPECT_EQ(0, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
        RODOS::rodosErrorCounter = 0;
    }
};

TEST_F(ListIterInfoTestSuite, Test_build_get_info_fields) {
    constexpr uint8_t  TEST_NEXT_PTR_IND = 0x01;
    constexpr uint8_t  TEST_MAIN_CTR     = 0x7B;
    constexpr uint8_t  TEST_BRANCH_CTR   = 0x63;
    constexpr uint32_t TEST_INFO_FIELDS  = 0x01007B63;

    auto result = ListIterInfo::InfoFields{
        TEST_NEXT_PTR_IND,
        TEST_MAIN_CTR,
        TEST_BRANCH_CTR
    };
    ASSERT_EQ(TEST_INFO_FIELDS, result.read());

    uint8_t nextPtrIndField = result.getField<ListIterInfo::InfoFields::NextPtrInd>();
    ASSERT_EQ(TEST_NEXT_PTR_IND, nextPtrIndField);

    uint8_t mainCtrField = result.getField<ListIterInfo::InfoFields::MainCtr>();
    ASSERT_EQ(TEST_MAIN_CTR, mainCtrField);

    uint8_t branchCtrField = result.getField<ListIterInfo::InfoFields::BranchCtr>();
    ASSERT_EQ(TEST_BRANCH_CTR, branchCtrField);
}

TEST_F(ListIterInfoTestSuite, Test_getNextPtr) {
    ListIterInfo iterInfo{};
    ListElement  nextElem{ "nextElem" };

    iterInfo.nextPtrs[ListIterInfo::InfoFields::NEXT_PTR_INDEX_DEFAULT] = &nextElem;
    ASSERT_EQ(&nextElem, iterInfo.getNextPtr());
}

TEST_F(ListIterInfoTestSuite, Test_rewirePtr) {
    ListIterInfo             iterInfo{};
    ListElement              elem0{ "elem0" }, elem1{ "elem1" };
    ListIterInfo::InfoFields infoOldExpected{}, infoOldActual{};

    infoOldExpected   = iterInfo.infoFields;
    infoOldActual     = iterInfo.rewirePtrTo(&elem0);
    ASSERT_EQ(&elem0, iterInfo.getNextPtr());
    ASSERT_EQ(infoOldExpected.read(), infoOldActual.read());

    infoOldExpected   = iterInfo.infoFields;
    infoOldActual     = iterInfo.rewirePtrTo(&elem1);
    ASSERT_EQ(&elem1, iterInfo.getNextPtr());
    ASSERT_EQ(infoOldExpected.read(), infoOldActual.read());
}

TEST_F(ListIterInfoTestSuite, Test_waitForBranchCtrEnd_terminates) {
    constexpr uint8_t TEST_OLD_MAIN_CTR_VAL = 123;
    ListIterInfo      iterInfo{};

    ListIterInfo::InfoFields infoOld{ 0, TEST_OLD_MAIN_CTR_VAL, 0 };
    iterInfo.infoFields = { 0, 0, TEST_OLD_MAIN_CTR_VAL };

    printf("check: shouldn't spinlock forever:\n");
    iterInfo.waitForBranchCtrEnd(infoOld);
    printf("check: passed!\n");
}

TEST_F(ListIterInfoTestSuite, Test_waitForMainCtrEnd_terminates) {
    constexpr uint8_t TEST_MAIN_CTR_BREAK_COND_VAL = 0;
    ListIterInfo      iterInfo{};

    iterInfo.infoFields =
      ListIterInfo::InfoFields{ 0, TEST_MAIN_CTR_BREAK_COND_VAL, 0 };

    printf("check: shouldn't spinlock forever:\n");
    iterInfo.waitForMainCtrEnd();
    printf("check: passed!\n");
}


} // namespace
