#include <gtest/gtest.h>

#include "list-protected.h"
#include "listelement.h"

#include "thread.h"
#include "misc-rodos-funcs.h"
#include "rodos-debug.h"

namespace {

static constexpr int NUM_ELEMS_GLOB = 3;
RODOS::ListElement   globDefaultElems[NUM_ELEMS_GLOB]{
    { "defaultElem0" },
    { "defaultElem1" },
    { "defaultElem2" }
};

class ProtectedListModifyingTestSuite : public ::testing::Test {
  protected:
    void SetUp() override {
        RODOS::isShuttingDown   = false;
        RODOS::schedulerRunning = false;

        for(int i = 0; i < NUM_ELEMS_GLOB; i++) {
            globDefaultElems[i].append(m_protectedList);
        }
        ASSERT_EQ(0, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;

        RODOS::schedulerRunning = true;
    }

    void TearDown() override {
        ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
        RODOS::rodosErrorCounter = 0;
        RODOS::isShuttingDown    = true;
    }

    unsigned long m_expected_error_counter{ 0 };

    RODOS::ProtectedList m_protectedList{};
};


TEST_F(ProtectedListModifyingTestSuite, Test_iterator_operations) {
    // test prefix / deref / member of pointer
    {
        auto iter = m_protectedList.begin();
        ASSERT_EQ(&globDefaultElems[NUM_ELEMS_GLOB - 1], &*iter);
        for(int i = NUM_ELEMS_GLOB - 1; i-- > 0;) {
            ASSERT_EQ(&globDefaultElems[i], &*++iter);
            ASSERT_EQ(globDefaultElems[i].listElementID, iter->listElementID);
        }
        ASSERT_TRUE(m_protectedList.end() == ++iter);
    }

    // test postfix / deref / member of pointer
    {
        auto iter = m_protectedList.begin();
        for(int i = NUM_ELEMS_GLOB; i-- > 0;) {
            ASSERT_EQ(globDefaultElems[i].listElementID, iter->listElementID);
            ASSERT_EQ(&globDefaultElems[i], &*iter++);
        }
        ASSERT_TRUE(m_protectedList.end() == iter);
    }

    // test comparison
    {
        auto iter0 = m_protectedList.begin();
        auto iter1 = m_protectedList.begin();
        for(int i = NUM_ELEMS_GLOB; i-- > 0;) {
            ASSERT_TRUE(++iter0 != iter1++);
            ASSERT_TRUE(iter0 == iter1);
        }
        ASSERT_TRUE(iter0 == m_protectedList.end());
        ASSERT_TRUE(iter1 == m_protectedList.end());
    }

    RODOS::isShuttingDown = true;
}

TEST_F(ProtectedListModifyingTestSuite, Test_runtime_append) {
    // valid append
    RODOS::ListElement elem0{ "elem0" };
    elem0.appendAtRuntime(m_protectedList);
    ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
    {
        auto iter = m_protectedList.begin();
        ASSERT_TRUE(iter != m_protectedList.end());
        ASSERT_EQ(&elem0, &*iter);
        iter++;

        for(int i = NUM_ELEMS_GLOB; i-- > 0;) {
            ASSERT_EQ(&globDefaultElems[i], &*iter++);
        }
    }

    // invalid append
    elem0.appendAtRuntime(m_protectedList);
    ASSERT_EQ(++m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;

    RODOS::isShuttingDown = true;
}

TEST_F(ProtectedListModifyingTestSuite, Test_runtime_remove) {
    // valid remove
    constexpr int ARBITRARY_REMOVE_INDEX = 0;
    globDefaultElems[ARBITRARY_REMOVE_INDEX].removeAtRuntime(m_protectedList);
    ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
    {
        auto iter = m_protectedList.begin();
        for(int i = NUM_ELEMS_GLOB; i-- > 0;) {
            if(i != ARBITRARY_REMOVE_INDEX) {
                ASSERT_EQ(&globDefaultElems[i], &*iter++);
            }
        }
        ASSERT_TRUE(m_protectedList.end() == iter);
    }

    // invalid remove
    globDefaultElems[ARBITRARY_REMOVE_INDEX].removeAtRuntime(m_protectedList);
    ASSERT_EQ(++m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
}

TEST_F(ProtectedListModifyingTestSuite, Test_sequence_appends_removes) {
    // append-and-remove-in-middle sequence
    RODOS::ListElement elem0{ "elem0" };
    elem0.appendAtRuntime(m_protectedList);
    globDefaultElems[1].removeAtRuntime(m_protectedList);
    {
        auto iter = m_protectedList.begin();
        ASSERT_TRUE(iter != m_protectedList.end());
        ASSERT_EQ(&elem0, &*iter++);
        ASSERT_TRUE(iter != m_protectedList.end());
        ASSERT_EQ(&globDefaultElems[2], &*iter++);
        ASSERT_TRUE(iter != m_protectedList.end());
        ASSERT_EQ(&globDefaultElems[0], &*iter++);
        ASSERT_TRUE(iter == m_protectedList.end());
    }

    // remove until empty list
    elem0.removeAtRuntime(m_protectedList);
    globDefaultElems[0].removeAtRuntime(m_protectedList);
    globDefaultElems[2].removeAtRuntime(m_protectedList);
    {
        auto iter = m_protectedList.begin();
        ASSERT_TRUE(iter == m_protectedList.end());
    }

    // append removed element again
    elem0.appendAtRuntime(m_protectedList);
    {
        auto iter = m_protectedList.begin();
        ASSERT_TRUE(iter != m_protectedList.end());
        ASSERT_EQ(&elem0, &*iter++);
        ASSERT_TRUE(iter == m_protectedList.end());
    }

    // remove element again directly
    elem0.removeAtRuntime(m_protectedList);
    {
        auto iter = m_protectedList.begin();
        ASSERT_TRUE(iter == m_protectedList.end());
    }

    RODOS::isShuttingDown = true;
}

TEST_F(ProtectedListModifyingTestSuite, Test_destruction) {
    {
        RODOS::ListElement elem{ "elem" };
    }
    ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;

    {
        RODOS::ListElement elem{ "elem" };
        elem.appendAtRuntime(m_protectedList);
    }
    ASSERT_EQ(++m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;

    {
        RODOS::ListElement elem{ "elem" };
        elem.appendAtRuntime(m_protectedList);
        elem.removeAtRuntime(m_protectedList);
    }
    ASSERT_EQ(m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;

    {
        RODOS::ListElement elem{ "elem" };
        elem.appendAtRuntime(m_protectedList);
        elem.removeAtRuntime(m_protectedList);
        elem.appendAtRuntime(m_protectedList);
    }
    ASSERT_EQ(++m_expected_error_counter, RODOS::rodosErrorCounter) << RODOS::rodosErrorMsg;
}

TEST_F(ProtectedListModifyingTestSuite, Test_iteration_loop) {
    for(int i = 0; i < NUM_ELEMS_GLOB; i++) {
        globDefaultElems[i].removeAtRuntime(m_protectedList);
    }
    constexpr int      NUM_ELEMS_LOCAL = 3;
    RODOS::ListElement elems[NUM_ELEMS_LOCAL]{
        { "elem0" },
        { "elem1" },
        { "elem2" }
    };
    for(int i = 0; i < NUM_ELEMS_LOCAL; i++) {
        elems[i].appendAtRuntime(m_protectedList);
    }

    // test range based loop simple
    int index = NUM_ELEMS_LOCAL;
    for(auto& elem : m_protectedList) {
        ASSERT_EQ(&elems[--index], &elem);
    }

    // test nested iterator loop
    int indexOuter = NUM_ELEMS_LOCAL;
    for(auto& elemOuter : m_protectedList) {
        indexOuter--;

        int indexInner = NUM_ELEMS_LOCAL;
        for(auto& elemInner : m_protectedList) {
            indexInner--;

            ASSERT_EQ(&elems[indexInner], &elemInner);
            ASSERT_EQ(&elems[indexOuter], &elemOuter);
        }
    }

    RODOS::isShuttingDown = true;
}

} // namespace
