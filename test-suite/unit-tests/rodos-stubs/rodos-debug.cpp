#include "rodos-debug.h"

namespace RODOS {

bool        error = false;
std::string errorMessage;

void RODOS_ERROR(const char* text) {
    error        = true;
    errorMessage = text;
    if(RodosErrorMock::s_rodosErrorMock != nullptr) {
        RodosErrorMock::s_rodosErrorMock->rodosError(text);
    }
}

RodosErrorMock* RodosErrorMock::s_rodosErrorMock{ nullptr };
RodosErrorMock::RodosErrorMock() {
    s_rodosErrorMock = this;
}
RodosErrorMock::~RodosErrorMock() {
    s_rodosErrorMock = nullptr;
}

bool        message = false;
std::string printfMessage;

RodosPrintfMock* RodosPrintfMock::s_rodosPrintfMock{ nullptr };

RodosPrintfMock::RodosPrintfMock() {
    s_rodosPrintfMock = this;
}
RodosPrintfMock::~RodosPrintfMock() {
    s_rodosPrintfMock = nullptr;
}

void PRINTF(const char* fmt, ...) {
    message       = true;
    printfMessage = fmt;
    if(RodosPrintfMock::s_rodosPrintfMock != nullptr) {
        RodosPrintfMock::s_rodosPrintfMock->rodosPrintf(fmt);
    }
}

} // namespace RODOS
