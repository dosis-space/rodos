#include "rodos-semaphore.h"

#include <mutex>
#include <vector>

namespace RODOS {

Semaphore::Semaphore()
  : owner{ nullptr },
    ownerEnterCnt{ 0 },
    ownerPriority{ 0 },
    context{ nullptr } {}

void Semaphore::enter() {
}

void Semaphore::leave() {
}

} // namespace RODOS
