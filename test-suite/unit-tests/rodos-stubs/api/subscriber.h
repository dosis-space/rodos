#pragma once

#include "topic.h"

#include <cstddef>

namespace RODOS {

class Putter;

class NetMsgInfo {
  public:
    int32_t  senderNode; ///< Node ID of sending instance of RODOS
    int64_t  sentTime;   ///< Time in localTime units
    uint32_t senderThreadId;
    uint32_t linkId;
};

class Subscriber {
  public:
    Subscriber(TopicInterface&, const char* = "anonymSubscriber") {}
    Subscriber(TopicInterface&, Putter&, const char* = "anonymSubscriber", bool = false) {}
    virtual ~Subscriber() = default;

    virtual uint32_t put(const uint32_t, const size_t, void*, const NetMsgInfo&) = 0;
};

template <typename Type>
class SubscriberReceiver : public Subscriber {
  public:
    SubscriberReceiver(TopicInterface& topic, const char* name = "anonymSubscriber")
      : Subscriber(topic, name) {}

    virtual void put(Type& msg) = 0;

    virtual uint32_t put(const uint32_t, const size_t, void*, const NetMsgInfo&) final { return 1; }
};

} // namespace RODOS
