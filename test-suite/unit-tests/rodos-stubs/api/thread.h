#pragma once

#include "listelement.h"
#include "timemodel.h"

#include <atomic>
#include <cstddef>

#include <gmock/gmock-function-mocker.h>

namespace RODOS {

inline std::atomic<int32_t> taskRunning{ false };

class Thread : public ListElement {
  public:
    Thread()                   = default;
    virtual ~Thread() override = default;

    static long* getFirstValidStackAddr(char*, size_t) { return nullptr; }
    static void  yield() {}
    static void  suspendCallerUntil(int64_t time = END_OF_TIME) {}

    MOCK_METHOD(void, resume, ());

    void initializeStack() {}
    void create() {}

    char*  stackBegin{ nullptr };
    size_t stackSize{};
    long*  stack{ nullptr };
};

template <size_t STACK_SIZE = 0>
class StaticThread : public Thread {
  public:
    virtual ~StaticThread() override = default;

    virtual void init() {}
    virtual void run() {}
};

inline void AT([[maybe_unused]] int64_t _time) {}

} // namespace RODOS
