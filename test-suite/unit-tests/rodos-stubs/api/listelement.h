#pragma once

#include "list-protected.h"

namespace RODOS {

class ListElement {
  public:
    ListElement() = default;
    virtual ~ListElement() = default;

    void append(ProtectedList&) {}

    void appendAtRuntime(ProtectedList&) {}
    void removeAtRuntime(ProtectedList&) {}
};

} // namespace RODOS
