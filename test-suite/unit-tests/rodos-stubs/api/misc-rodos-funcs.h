#pragma once

#include <algorithm>

namespace RODOS {

inline bool isShuttingDown{ false };
inline bool schedulerRunning{ false };

inline bool isSchedulerRunning() {
    return schedulerRunning;
}

using std::min;
using std::max;
using std::abs;

} // namespace RODOS
