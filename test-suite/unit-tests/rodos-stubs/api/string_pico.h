#pragma once

#include <cstring>

namespace RODOS {

using std::memcpy;
using std::memset;
using std::memcmp;

} // namespace RODOS
