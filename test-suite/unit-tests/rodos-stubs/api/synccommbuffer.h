#pragma once

#include <gmock/gmock-function-mocker.h>

namespace RODOS {

template <class Type>
class SyncCommBuffer {
  public:
    SyncCommBuffer()          = default;
    virtual ~SyncCommBuffer() = default;

    MOCK_METHOD(void, get, (Type& data));
    MOCK_METHOD(bool, getOnlyIfNewData, (Type& data));

    MOCK_METHOD(void, put, (const Type& data));
    MOCK_METHOD(void, putZero, ());
    MOCK_METHOD(bool, syncGet, (Type& data, const int64_t timeout));
    MOCK_METHOD(void, clearDataState, ());
};


} // namespace RODOS
