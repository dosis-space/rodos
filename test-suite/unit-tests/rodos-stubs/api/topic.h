#pragma once

#include <gmock/gmock-function-mocker.h>

namespace RODOS {

class NetMsgInfo;

class TopicInterface {
  public:
    virtual ~TopicInterface() = default;

    MOCK_METHOD(uint32_t, topicInterfacePublishMock,
                (void* msg, bool shallSendToNetwork, NetMsgInfo* netMsgInfo));
    MOCK_METHOD(uint32_t, topicInterfacePublishMsgPartMock,
                (void* msg, size_t lenToSend, bool shallSendToNetwork, NetMsgInfo* netMsgInfo));

    uint32_t publish(void* msg, bool shallSendToNetwork = true, NetMsgInfo* netMsgInfo = 0) {
        return topicInterfacePublishMock(msg, shallSendToNetwork, netMsgInfo);
    }
    uint32_t publishMsgPart(void*       msg,
                            size_t      lenToSend,
                            bool        shallSendToNetwork = true,
                            NetMsgInfo* netMsgInfo         = 0) {
        return topicInterfacePublishMsgPartMock(msg, lenToSend, shallSendToNetwork, netMsgInfo);
    }
};

template <typename Type>
class Topic : public TopicInterface {
  public:
    Topic() = default;
    Topic(int64_t id, const char* name, bool _onlyLocal = false) {}
    virtual ~Topic() override = default;

    MOCK_METHOD(uint32_t, publishMock, (Type& msg, bool shallSendToNetwork));
    MOCK_METHOD(uint32_t, publishMsgPartMock,
                (Type& msg, size_t lenToSend, bool shallSendToNetwork));

    uint32_t publish(Type& msg, bool shallSendToNetwork = true) {
        return publishMock(msg, shallSendToNetwork);
    }

    uint32_t publishMsgPart(Type& msg, size_t lenToSend, bool shallSendToNetwork = true) {
        return publishMsgPartMock(msg, lenToSend, shallSendToNetwork);
    }

    uint32_t topicId{};
};

} // namespace RODOS
