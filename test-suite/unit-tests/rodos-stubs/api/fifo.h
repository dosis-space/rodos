#pragma once

#include "timemodel.h"

#include <gmock/gmock-function-mocker.h>

namespace RODOS {

template <class Type, size_t len>
class SyncFifo {
  public:
    virtual ~SyncFifo() = default;

    MOCK_METHOD(bool, syncPutMock, (const Type& val, const int64_t timeout));
    MOCK_METHOD(bool, syncGetMock, (Type& val, const int64_t timeout));

    bool syncPut(const Type& val, const int64_t timeout = RODOS::END_OF_TIME) {
        return syncPutMock(val, timeout);
    }
    bool syncGet(Type& val, const int64_t timeout = RODOS::END_OF_TIME) {
        return syncGetMock(val, timeout);
    }

    MOCK_METHOD(bool, isFull, (), (const));
    MOCK_METHOD(bool, isEmpty, (), (const));
    MOCK_METHOD(void, clear, ());
};

template <class Type, size_t len>
class BlockFifo {
  public:
    virtual ~BlockFifo() = default;

    MOCK_METHOD(Type*, getBufferToWrite, (size_t& maxLen));
    MOCK_METHOD(Type*, getBufferToRead, (size_t& maxLen));

    MOCK_METHOD(void, writeConcluded, (size_t numOfWrittenElements));
    MOCK_METHOD(void, readConcluded, (size_t sizeRead));

    MOCK_METHOD(bool, isFull, (), (const));
    MOCK_METHOD(bool, isEmpty, (), (const));
    MOCK_METHOD(void, clear, ());
};


} // namespace RODOS
