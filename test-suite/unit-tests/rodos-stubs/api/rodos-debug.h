#pragma once

#include <string>
#include <cstdio>
#include <cstdarg>

#include <gmock/gmock-function-mocker.h>

#define SCREEN_CLEAR "\x1B[2J"
#define SCREEN_CLEAR_LINE "\x1B[K"
#define SCREEN_MOVE_CURSOR_TO "\x1B[%d;%dH"
#define SCREEN_SCROLL_REGION "\x1B[%d;%dr"
#define SCREEN_RESET "\x1B[0m"

#define SCREEN_BLACK "\x1B[30m"
#define SCREEN_RED "\x1B[31m"
#define SCREEN_GREEN "\x1B[32m"
#define SCREEN_YELLOW "\x1B[33m"
#define SCREEN_BLUE "\x1B[34m"
#define SCREEN_WHITE "\x1B[37m"
#define SCREEN_MAGENTA "\x1b[35m"
#define SCREEN_CYAN "\x1b[36m"
#define SCREEN_BRIGHT_RED "\x1b[91m"
#define SCREEN_BRIGHT_GREEN "\x1b[92m"
#define SCREEH_UNDERLINE "\x1b[4m"

#define STRINGIZE(x) #x
#define NUMBER_TO_STRING(x) STRINGIZE(x)
#define LOCATION __FILE__ " : " NUMBER_TO_STRING(__LINE__)

namespace RODOS {

extern bool        error;
extern std::string errorMessage;

extern bool        message;
extern std::string printfMessage;

void RODOS_ERROR(const char* text);
void PRINTF([[maybe_unused]] const char* fmt, ...);

/**
 * Mock object for global function RODOS_ERROR
 *
 * To use this mock simply create an instance and place all expectations
 * on RODOS_ERROR instead on the rodosError method.
 *
 * @warning it is not intended to have multiple instance of RodosErrorMock at
 * the same time. This will most likely lead to failed expectations!
 */
class RodosErrorMock {
  public:
    static RodosErrorMock* s_rodosErrorMock;
    RodosErrorMock();
    virtual ~RodosErrorMock();

    MOCK_METHOD(void, rodosError, (const char* text));
};

class RodosPrintfMock {
  public:
    static RodosPrintfMock* s_rodosPrintfMock;
    RodosPrintfMock();
    virtual ~RodosPrintfMock();

    MOCK_METHOD(void, rodosPrintf, (const char* text));
};

} // namespace RODOS
