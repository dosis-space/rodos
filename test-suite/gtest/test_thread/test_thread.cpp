#include <gtest/gtest.h>
#include "rodos.h"

#ifdef GTEST_R
namespace TEST {

class TestThread : public StaticThread<> {
  public:
    using StaticThread<>::StaticThread;

    void init() {
        PRINTF("Hey, i am there for testing!");
    }

    void run() {
        char  THREAD_NAME[] = "";
        char* argv[]        = { THREAD_NAME };
        int   argc          = sizeof(argv) / sizeof(char*);
        ::testing::InitGoogleTest(&argc, argv);
        int testResult           = RUN_ALL_TESTS();
        RODOS::printErrorReports = 0;
        exit(testResult);
    }
};

TestThread testThread{ "Test Thread" };

} // namespace TEST
#endif
