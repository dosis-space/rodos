target_dir_rel = '..' / target_dir
test_dir = target_dir_rel / 'tests'

generic_test_dep = declare_dependency(
    sources: [
              test_dir / 'mocks' / 'independent' / 'genericIO.cpp',
              test_dir / 'mocks' / 'independent' / 'misc-rodos-funcs.cpp',
              test_dir / 'mocks' / 'independent' / 'rodos-debug.cpp',
              test_dir / 'mocks' / 'cortex_m' / 'nvic.cpp',
              test_dir / 'mocks' / 'cortex_m' / 'nvic_mock.cpp',
              test_dir / 'mocks' / 'peripheral_ctrl' / 'peripheral_defs.cpp',
              test_dir / 'mocks' / 'peripheral_ctrl' / 'pin_config' / 'pin_config_mock.cpp',
              test_dir / 'mocks' / 'peripheral_ctrl' / 'aborting_error.cpp',
              test_dir / 'peripheral_test_fixture.cpp' ],
    include_directories: [test_dir / 'mocks']
)

empty_dep = declare_dependency()

pin_config_mock_src = files(
              test_dir / 'mocks' / 'peripheral_ctrl' / 'pin_config' / 'pin_config.cpp',
)

gpio_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hal/hal_gpio.cpp',
              target_dir_rel / 'hw_hal/hw_hal_gpio.cpp',
    ]
)

clkgen_test_dep = declare_dependency(
    sources: [
              test_dir / 'mocks' / 'peripheral_ctrl' / 'clock_management' / 'busywait_hbo.cpp',
              target_dir_rel / 'peripheral_ctrl/clock_management/clock_initializers.cpp',
    ]
)

pin_config_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'peripheral_ctrl/pin_config/pin_config.cpp',
    ]
)

uart_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hal/hal_uart.cpp',
              target_dir_rel / 'hw_hal/hw_hal_uart.cpp',
              pin_config_mock_src
    ]
)

adc_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hal/hal_adc.cpp',
              target_dir_rel / 'hw_hal/hw_hal_adc.cpp',
              pin_config_mock_src
    ]
)
time_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hw/hw_time.cpp'
    ]
)

can_filter_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hw_hal/can/can_filter.cpp',
    ]
)

can_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hal/hal_can.cpp',
              target_dir_rel / 'hw_hal/can/can_filter.cpp',
              target_dir_rel / 'hw_hal/can/can_controller.cpp',
              target_dir_rel / 'hw_hal/can/hw_hal_can.cpp',
    ]
)

spi_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hal/hal_spi.cpp',
              target_dir_rel / 'hw_hal/hw_hal_spi.cpp',
              pin_config_mock_src
    ]
)

i2c_test_dep = declare_dependency(
    sources: [
              target_dir_rel / 'hal/hal_i2c.cpp',
              target_dir_rel / 'hw_hal/hw_hal_i2c.cpp',
    ]
)

unit_tests = {
    'GPIO_Test/gpio_test.cpp': gpio_test_dep,
    'uart_test/uart_test.cpp': uart_test_dep,
    'register_test/register_test.cpp' : empty_dep,
    'clkgen_test/clkgen_test.cpp' : clkgen_test_dep,
    'pin_config_test/pin_config_test.cpp' : pin_config_test_dep,
    'adc_test/adc_test.cpp' : adc_test_dep,
    'time_test/time_test.cpp' : time_test_dep,
    'can_test/can_filter_test.cpp' : can_filter_test_dep,
    'can_test/can_frame_metadata_test.cpp' : empty_dep,
    'can_test/can_test.cpp' : can_test_dep,
    'spi_test/spi_test.cpp' : spi_test_dep,
    'i2c_test/i2c_test.cpp' : i2c_test_dep,
}


rodos_stubable_dep = declare_dependency(include_directories: [api_inc, target_inc])

native_tests = []
native_test_paths = []
foreach tester_src, test_dep : unit_tests
    name = tester_src.split('/')[1].split('.')[0]
    exe = executable(name,
        sources: [test_dir / tester_src],
        dependencies: [
            gtest_all_dep,
            generic_test_dep,
            test_dep,
            rodos_stubable_dep, # order important! (rodos_stubable_dep after test_dep)
        ],
        build_by_default: false,
        override_options : [
            'cpp_std=' + get_option('cpp_std'),
            'b_sanitize=undefined'
        ],
        cpp_args : ['-fno-sanitize-recover=all'],
        link_args : ['-fno-sanitize-recover=all'],
        native: true,
    )
  native_tests += exe
  native_test_paths += exe.full_path()
endforeach

run_target('check-native-tests', command : [ meson.source_root() + '/scripts/run-tests.sh' ] + native_test_paths, depends : native_tests)
