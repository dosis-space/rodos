How to build RODOS with meson
=============================

Meson is a meta build-system suitable for large projects where build steps are
configured declaratively in meson.build. Meson can automatically generate a
build.ninja from this build configuration. Afterwards, ninja is used to compile the
project. The following text explains the required steps to build rodos projects
with meson.

RODOS supports different target platforms, which require different compilation configuration.
The configuration for all the ports is encapsulated in separate meson directories at `meson/`.
There, port configuration directories such as `posix`, `linux-x86`, and `stm32f4` exist.
In order to enable a port, we make use of a special feature of meson: the [`cross-file/native-file`](https://mesonbuild.com/Cross-compilation.html).
If this file is set on the command line when `meson` is called, the content of the supplied file is evaluated before the main `meson.build` is processed.

Generate Make Files
-------------------

```shell script
# if on x86:
$ meson build

# to force 32bit:
$ meson --native-file meson/linux-x86/cross.ini build-x86

# other linux based platforms:
$ meson --native-file meson/posix/cross.ini build-posix

# For embedded use --cross-file instead of --native-file
& meson --cross-file meson/stm32f4/cross.ini build-stm32f4
```

When calling meson, the appropriate port `cross.ini` file must be passed via the argument `--cross-file` or `--native-file`. Use `--cross-file` if building for embedded platforms and `--native-file` if building for your own computer.

The targets `linux-makecontext` (default), `linux-x86`, `posix`, `posixmac` (if on MacOS), `stm32f4`, `sf2` and `raspberrypi3` are all supported by the meson build files.

The last argument `build` declares which directory to use for building.
If building for multiple ports it is required to use different build directories here, e.g.: `meson build-linux`.

Compile RODOS
-------------
```shell script
$ ninja -Cbuild
```

This commands builds the RODOS library, which is then placed at `build/meson/librodos.a`.
Use the argument `-C` to choose a build directory corresponding to the earlier `meson` command.

Compile RODOS' Support Lib
--------------------------
The support-libs are included automatically when compiling rodos with meson.

Compile Tutorials
-----------------
For most of the tutorials, there are ready compilation targets in meson.
They are available as targets for building `build:<tutorial-name>` and running `run:<tutorial-name>`, where the tutorial name is usually the name of the cpp file.

Optionally, to build all tutorials enable them using the meson option `-Dbuild_all_turorials=true`:

```shell script
# assuming build exists already
$ meson configure build -Dbuild_all_tutorials=true
```

All tutorial targets are available regardless of the above option:

```shell script
$ ninja -Cbuild build:timewaitbeats

or

$ ninja -Cbuild run:timewaitbeats
```

The compiled executables are then available under `build/tutorials`.

Create Own Application Using meson
----------------------------------

Instead of installing RODOS globally to the computer, we encourage users to add it as a subproject to the user project. Meson requires subprojects to be placed inside the `subprojects` directory.
If you are using git, you can add it directly as submodule just like this:
```shell script
mkdir subprojects
git submodule add https://gitlab.com/rodos/rodos.git subprojects/rodos
```
Otherwise, if you do not use git, you can simply clone (or manually copy) it into a subfolder:
```shell script
mkdir subprojects
cd subprojects
git clone https://gitlab.com/rodos/rodos.git
```
In each case, you need to make RODOS available to your application's meson.build by adding it as a subproject and extracting the `rodos` dependency variable:
```meson
rodos = subproject('rodos').get_variable('rodos')
```

Now simply adding rodos to your executable as dependency enables building your own RODOS applications:

```meson
executable('myApp', 'myApp.cpp', dependencies: rodos)
```

Applications can then be built from the directory where your meson.build is with:

```shell script
meson build
ninja -Cbuild

# for cross-builds
meson --cross-file=subprojects/rodos/meson/stm32f4/cross.ini build-stm32f4
ninja -Cbuild-stm32f4
```

Your executable should now be placed in `build`. If it is called `myApp` as in the example it can be run with `build/myApp`.

Some Tips and Tricks
---------------------

### STM32F4 Boards

Seperate `STM32F4` boards can be chosen as target using the meson option `-Df4_board`. See `meson-options.txt` for all available targets.

```shell script
meson --cross-file=meson/stm32f4/cross.ini -Df4_board=disovery build-stm32f4
```

### Testing Using the Gtest Framework

Gtest can be natively integrated into RODOS. The gtest tests are then executed from a RODOS Thread after RODOS boot-up. This means all RODOS features are available in this test.

To make use of that [write a gtest test](https://github.com/google/googletest/blob/master/googletest/docs/primer.md) and use the `rodos_testing` variable as dependency in meson:

```meson
rodos_testing = subproject('rodos').get_variable('rodos_testing')

test('myTest', executable('myTestExecutable', 'test.cpp', dependencies: rodos_testing))
```

All tests are executed using the test target.

```shell script
ninja -Cbuild test
```

### Using Support Programs

The support-programs of RODOS are also supported in meson. See `meson/support-programs/meson.build` for the available programs (all can be extracted using `get_variable` as above).

Also see `meson/support-libs/meson.build` for some usage examples (The ccsds header generation at the bottom, the interesting part is `gen['bitFieldsSerializer'].process(ccsds_def_files)`).
