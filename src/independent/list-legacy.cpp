#include "list-legacy.h"

#include "listelement.h"

namespace RODOS {

List::Iterator List::begin() {
    return Iterator{ *this };
}
List::ListEnd List::end() {
    return ListEnd{};
}


List::Iterator::Iterator(List& list)
  : m_currentPtrCopy{ list.m_head } {}


bool List::Iterator::operator==(const Iterator& rhs) const {
    return (this->m_currentPtrCopy == rhs.m_currentPtrCopy);
}

ListElement& List::Iterator::operator*() const {
    return *m_currentPtrCopy;
}
ListElement* List::Iterator::operator->() const {
    return m_currentPtrCopy;
}


List::Iterator& List::Iterator::operator++() {
    m_currentPtrCopy = m_currentPtrCopy->m_nextIterInfo.getNextPtr();
    return *this;
}
List::Iterator List::Iterator::operator++(int) {
    auto tmp = *this;
    ++(*this);
    return tmp;
}

} // namespace RODOS
