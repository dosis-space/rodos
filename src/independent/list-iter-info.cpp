#include "list-iter-info.h"

#include "rodos-assert.h"
#include "thread.h"
#include "rodos-debug.h"

namespace RODOS {

uint8_t ListIterInfo::getNonActiveNextPtrIndex() const {
    uint8_t nextPtrInd = infoFields.load().getField<InfoFields::NextPtrInd>();
    return (nextPtrInd == 0) ? 1 : 0;
}

ListElement* ListIterInfo::getNextPtr() const {
    uint8_t nextPtrInd = infoFields.load().getField<InfoFields::NextPtrInd>();

    RODOS_ASSERT_IFNOT_RETURN(nextPtrInd == 0 || nextPtrInd == 1, nullptr);
    return this->nextPtrs[nextPtrInd];
}

ListIterInfo::InfoFields ListIterInfo::rewirePtrTo(ListElement* destPtr) {
    uint8_t nonActiveNextPtrIndex =
      this->getNonActiveNextPtrIndex();
    this->nextPtrs[nonActiveNextPtrIndex] = destPtr;

    InfoFields infoNew{ nonActiveNextPtrIndex };
    return std::atomic_exchange(&this->infoFields, infoNew);
}

void ListIterInfo::waitForBranchCtrEnd(const InfoFields& infoOld) {
    uint8_t oldMainCtr = infoOld.getField<InfoFields::MainCtr>();

    while(true) {
        uint8_t currentBranchCtr =
          this->infoFields.load().getField<InfoFields::BranchCtr>();
        if(currentBranchCtr >= oldMainCtr) {
            break;
        }
        Thread::yield();
    }
}

void ListIterInfo::waitForMainCtrEnd() {
    while(true) {
        uint8_t currentMainCtr =
          this->infoFields.load().getField<InfoFields::MainCtr>();
        if(currentMainCtr == 0) {
            break;
        }
        Thread::yield();
    }
}

} // namespace RODOS
