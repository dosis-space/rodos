#include "list-protected.h"

#include "list-iter-info.h"
#include "listelement.h"

namespace RODOS {

ProtectedList::Iterator ProtectedList::begin() {
    return Iterator{ *this };
}
ProtectedList::ListEnd ProtectedList::end() {
    return ListEnd{};
}


ProtectedList::Iterator::Iterator(ProtectedList& list)
  : m_currentIter{ &list.m_head },
    m_currentPtrCopy{ occupyElement(*m_currentIter) } {}

ProtectedList::Iterator::~Iterator() {
    leaveElement(*m_currentIter, m_currentPtrCopy);
}

ProtectedList::Iterator::Iterator(const Iterator& other)
  : m_currentIter{ other.m_currentIter.load() },
    m_currentPtrCopy{ occupyElement(*m_currentIter) } {}


bool ProtectedList::Iterator::operator==(const Iterator& rhs) const {
    return (this->m_currentIter == rhs.m_currentIter) &&
           (this->m_currentPtrCopy == rhs.m_currentPtrCopy);
}


ListElement& ProtectedList::Iterator::operator*() const {
    return *m_currentPtrCopy;
}
ListElement* ProtectedList::Iterator::operator->() const {
    return m_currentPtrCopy;
}


ProtectedList::Iterator& ProtectedList::Iterator::operator++() {
    ListElement* nextElemPtrCopy =
      occupyElement(m_currentPtrCopy.load()->m_nextIterInfo);
    leaveElement(*m_currentIter, m_currentPtrCopy);

    m_currentIter    = &m_currentPtrCopy.load()->m_nextIterInfo;
    m_currentPtrCopy = nextElemPtrCopy;

    return *this;
}
ProtectedList::Iterator ProtectedList::Iterator::operator++(int) {
    auto tmp = *this;
    ++(*this);
    return tmp;
}


ListElement* ProtectedList::Iterator::occupyElement(ListIterInfo& iterInfo) {
    using InfoFields = ListIterInfo::InfoFields;

    InfoFields infoFieldsCopy{}, infoFieldsNew{};
    do {
        infoFieldsNew = infoFieldsCopy = iterInfo.infoFields.load();
        infoFieldsNew.modifyField<InfoFields::MainCtr>(+1);
    } while(!std::atomic_compare_exchange_weak(&iterInfo.infoFields,
                                               &infoFieldsCopy,
                                               infoFieldsNew));

    uint8_t index = infoFieldsCopy.getField<InfoFields::NextPtrInd>();
    return iterInfo.nextPtrs[index];
}

void ProtectedList::Iterator::leaveElement(
  ListIterInfo& iterInfo, const ListElement* elemPtrCopy) {

    using InfoFields = ListIterInfo::InfoFields;

    InfoFields infoFieldsCopy{}, infoFieldsNew{};
    do {
        infoFieldsNew = infoFieldsCopy = iterInfo.infoFields.load();

        uint8_t nextPtrInd = infoFieldsCopy.getField<InfoFields::NextPtrInd>();

        if(iterInfo.nextPtrs[nextPtrInd] == elemPtrCopy) {
            infoFieldsNew.modifyField<InfoFields::MainCtr>(-1);
        } else {
            infoFieldsNew.modifyField<InfoFields::BranchCtr>(+1);
        }
    } while(!std::atomic_compare_exchange_weak(&iterInfo.infoFields,
                                               &infoFieldsCopy,
                                               infoFieldsNew));
}

} // namespace RODOS
