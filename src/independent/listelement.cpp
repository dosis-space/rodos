



/**
* @file listelement.cc
* @date 2008/04/26 17:12
* @author Sergio Montenegro, Lutz Dittrich
*
*
* Listelement: basic object for operating system objects
*
* @brief linked list management
*/

#include "listelement.h"
#include "rodos-assert.h"
#include "rodos-debug.h"
#include "thread.h"
#include "misc-rodos-funcs.h"

namespace RODOS {

int32_t ListElement::listElementCounter = 0;

ListElement::ListElement(List& list, const char* name, void* owner)
  : name{ name }, listElementID{ listElementCounter++ }, owner{ owner } {
    append(list);
}

ListElement::ListElement(const char* name, void* owner)
  : name{ name }, listElementID{ listElementCounter++ }, owner{ owner } {}

ListElement::~ListElement() {
    if(isShuttingDown || m_isDestructable) {
        return;
    }
    PRINTF("error in ~ListElement(): "
           "\"%s\" (id: %d) isn't marked destructable!\n",
           this->getName(), (int)this->listElementID);
    RODOS_ERROR("destructing ListElement which is still in a List!");
}


void ListElement::append(List& list) {
    appendHelper(list.m_head);
}

void ListElement::append(ProtectedList& list) {
    constexpr uint8_t INDEX = ListIterInfo::InfoFields::NEXT_PTR_INDEX_DEFAULT;
    appendHelper(list.m_head.nextPtrs[INDEX]);
}

void ListElement::appendHelper(std::atomic<ListElement*>& head) {
    RODOS_ASSERT_IFNOT_RETURN_VOID(!isSchedulerRunning());
    m_isDestructable = false;

    uint8_t nextPtrInd =
      m_nextIterInfo.infoFields.load()
        .getField<ListIterInfo::InfoFields::NextPtrInd>();
    m_nextIterInfo.nextPtrs[nextPtrInd] = head.load();

    head = this;
}


void ListElement::appendAtRuntime(ProtectedList& list) {
    RODOS_ASSERT_IFNOT_RETURN_VOID(isSchedulerRunning() && m_isDestructable);
    PROTECT_IN_SCOPE(list.sema);
    m_isDestructable = false;

    constexpr uint8_t INDEX = ListIterInfo::InfoFields::NEXT_PTR_INDEX_DEFAULT;
    m_nextIterInfo.infoFields.store({});
    m_nextIterInfo.nextPtrs[INDEX] = list.m_head.getNextPtr();

    auto infoOld = list.m_head.rewirePtrTo(this);

    list.m_head.waitForBranchCtrEnd(infoOld);
}

void ListElement::removeAtRuntime(ProtectedList& list) {
    RODOS_ASSERT_IFNOT_RETURN_VOID(isSchedulerRunning() && !m_isDestructable);
    PROTECT_IN_SCOPE(list.sema);

    ListIterInfo* ptrIterInfoPrev = findIterInfoToThis(list);
    RODOS_ASSERT_IFNOT_RETURN_VOID(ptrIterInfoPrev != nullptr);

    auto infoOld = ptrIterInfoPrev->rewirePtrTo(m_nextIterInfo.getNextPtr());

    ptrIterInfoPrev->waitForBranchCtrEnd(infoOld);
    m_nextIterInfo.waitForMainCtrEnd();

    m_isDestructable = true;
}


ListIterInfo* ListElement::findIterInfoToThis(ProtectedList& list) const {
    for(ListIterInfo* iter = &list.m_head;
        iter->getNextPtr() != nullptr;
        iter = &iter->getNextPtr()->m_nextIterInfo) {

        if(iter->getNextPtr() == this) {
            return iter;
        }
    }
    return nullptr;
}

} // namespace RODOS
