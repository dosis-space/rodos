#include "hw_specific.h"
#include "misc-rodos-funcs.h"
#include "thread.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <new>

namespace RODOS {

ucontext_t* volatile contextT;

constexpr size_t STACKSIZE = 4096;     /* stack size  WARNING/TODO: That is this? */
ucontext_t signal_context; /* the interrupt context */
extern void* signal_stack;

uintptr_t Thread::getCurrentStackAddr(){
    volatile ucontext_t* c = reinterpret_cast<volatile ucontext_t*>(context);
#ifdef __x86_64__
    return static_cast<uintptr_t>(c->uc_mcontext.gregs[REG_RSP]);
#else
    return static_cast<uintptr_t>(c->uc_mcontext.gregs[REG_ESP]);
#endif
}

/**
 *create context on stack and return a pointer to it
 */
long* hwInitContext(long* stack, void* object) {
    // create context on top of stack (don't overwrite current 'contextT')
    auto*       byte_stack_top = reinterpret_cast<uint8_t*>(stack);
    ucontext_t* newContext     = new(byte_stack_top - sizeof(ucontext_t) + 1) ucontext_t{};

    // initialize new context with current context
    getcontext(newContext);

    // fill context stack fields
    // stack top at ss_sp + ss_size (therefore ss_size = 0)
    newContext->uc_stack.ss_sp    = byte_stack_top - sizeof(ucontext_t);
    newContext->uc_stack.ss_size  = 0;
    newContext->uc_stack.ss_flags = 0;

    // fill context signal mask
    if(sigemptyset(&newContext->uc_sigmask) < 0) {
        perror("sigemptyset");
        isShuttingDown = true;
        exit(1);
    }
    // make new thread context and return it (stored in thread object)
    makecontext(newContext, (void (*)())threadStartupWrapper, 1, object);
    return reinterpret_cast<long*>(newContext);
}

void startIdleThread() { }

}

//___________________________________________________________________________
/*
 *  In other bare metal implementations this has to be done in assembler 
 */
extern "C" void __asmSaveContextAndCallScheduler();
void __asmSaveContextAndCallScheduler() {

    /* Create new scheduler context */
    getcontext(&RODOS::signal_context);
    RODOS::signal_context.uc_stack.ss_sp    = RODOS::signal_stack;
    RODOS::signal_context.uc_stack.ss_size  = RODOS::STACKSIZE;
    RODOS::signal_context.uc_stack.ss_flags = 0;
    sigemptyset(&RODOS::signal_context.uc_sigmask);
    makecontext(&RODOS::signal_context, (void (*)())RODOS::schedulerWrapper, 1, RODOS::contextT);

    /* save running thread, jump to scheduler */
    swapcontext(RODOS::contextT, &RODOS::signal_context);
}

extern "C" void __asmSwitchToContext(long* context);
void __asmSwitchToContext(long* context) {
    RODOS::contextT = (ucontext_t*)context;
    setcontext(RODOS::contextT); /* go */
}
