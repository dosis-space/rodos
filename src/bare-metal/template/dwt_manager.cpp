#include "rodos-debug.h"
#include "hal/hal_dwt.h"

namespace RODOS {

void DwtManager::DwtNotImplemented() { RODOS_ERROR("Data Watchpoint and Trace Unit not implemented for this port"); }

void DwtManager::resetAllThreadCycleCounts() {}

void DwtManager::init() {}

void DwtManager::startCycleCount() {}

void DwtManager::stopCycleCount() {}

uint32_t DwtManager::getCycleCount() { return 0; }

void DwtManager::reset() {}

uint8_t DwtManager::getThreadPointers(Thread* threads[], uint8_t size) { return 0; }

} // namespace RODOS
