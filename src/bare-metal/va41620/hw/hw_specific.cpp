#include "hw_context.h"

#include "cortex_m/nvic.h"
#include "clkgen_config.h"
#include "peripheral_ctrl/pin_config/pin_config.h"
#include "peripheral_ctrl/peripheral_defs.h"
#include "hw_uart_dbg.h"
#include "clkgen_config.h"


namespace RODOS {

void hwDisableInterrupts()
{
    asm volatile("cpsid i");
}

void hwEnableInterrupts()
{
    asm volatile("cpsie i");
}

void hwInit()
{
    HWContext::init();
}

void enableIrqRouterPeripheral(){
    sysconfigPeripheralBitband->IRQ_ROUTER_ENABLE.write(1);
}

void HWContext::init()
{
    enableIrqRouterPeripheral();
    pin_config::enableIOConfigPeripheral();
    NVIC::setAllIrqsToDefaultPriority();
    uartStdout.init(115200);
    //extern int32_t myNodeNr; // independent/globaldata.cpp
    PRINTF("CPU: %lu MHz\n", globalClockSetup.getSysclkFrequency() / 1'000'000);
}

void hwResetAndReboot()
{
    NVIC::systemReset();
}

void hwSetGPSTime(int64_t time) {}
int64_t hwGetGPSTime() { return 0; }

/**
 *  RTC dummies
 */
int64_t hwGetRTCTime()
{
    return 0;
}

void hwSetRTCTime([[gnu::unused]] int64_t time)
{
    return;
}

int hwLoadRTCBackupRegister([[gnu::unused]] size_t index, [[gnu::unused]] uint32_t& reg)
{
    return -1;
}

int hwStoreRTCBackupRegister([[gnu::unused]] size_t index, [[gnu::unused]] uint32_t val)
{
    return -1;
}

int32_t hwFlashPageSize() { return -1; }
int32_t hwFlashUnlock(uint32_t, uint32_t) { return -1; }
int32_t hwFlashLock() { return -1; }
int32_t hwFlashRead(size_t, size_t, uint8_t*) { return -1; }
int32_t hwFlashWrite(size_t, size_t, const uint8_t*) { return -1; }
int32_t hwFlashErasePage(size_t) { return -1; }

void enterSleepMode()
{
    asm volatile("wfi");
}

void hwInitWatchdog(int32_t interval) { }
void hwTriggerWatchdog() { }

}
