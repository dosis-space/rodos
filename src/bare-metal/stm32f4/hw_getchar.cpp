#include <cstdint>

#include "rodos.h"

#ifndef NO_RODOS_NAMESPACE
namespace RODOS {
#endif

extern HAL_UART uartStdout;

int8_t getcharNoWait() {
  int16_t c = uartStdout.getcharNoWait();
  if(c<0) {
    return -1;
  } else {
    return static_cast<int8_t>(c);
  }
}

bool isCharReady() { 
  return uartStdout.isDataReady();
}

char* getsNoWait() {
	static char inBuffer[122];
	static int inBufferIndex = 0;

	while(1) { // terminated with return, No Busy Waiting!

		int8_t c = getcharNoWait();
		if(c < 0) return 0;

		if(c == '\r' || c == '\n') { // end of string -> return
			inBuffer[inBufferIndex] = 0;
			inBufferIndex = 0;
			return inBuffer;
		}

		inBuffer[inBufferIndex] = static_cast<char>(c);
		if(inBufferIndex < 120) inBufferIndex++;
	}
}

void activateTopicCharInput() { }

#ifndef NO_RODOS_NAMESPACE
}
#endif

