#pragma once
#include "peripheral_ctrl/clock_management/clock_manager.h"
#include "platform-parameter.h"

namespace RODOS {

class SysTickManager{
private:
    static constexpr uint32_t freq = HCLK::freq/8;

    static constinit uint32_t ticks;

    static inline void setReloadValue(uint32_t ticks);
    static inline void resetCurrentVal();
    static inline void enable();
public:
    static void start();
    static void setInterval(const int64_t microseconds);
    static void stop();

    static constexpr uint32_t maxTicks = 0x00FF'FFFF;
    static constexpr int64_t calculateTicks(int64_t microseconds){
        const int64_t ticks = (freq * microseconds) / 1'000'000;

        /* According to ARM DUI 0553A: Cortex™-M4 Devices, 4.4.2 SysTick Reload Value Register:
         * The interrupt is triggered when counting from 1 to 0, this means that
         * * A value of 0 effectively disables the timer
         * * To get an interrupt every N cycles, the reload value must be N-1
         * Also avoid unexpected behavior when specifing microseconds>0 would result in no interrupts at all.
         */
        static_assert(((freq * 1) / 1'000'000) > 1, "processor frequency too low");


        return (ticks == 0) ? 0 : (ticks - 1);
    }

};

}
