#include "hal/hal_resetreason.h"
#include "peripheral_ctrl/register_maps/rcc_registers.h"

namespace RODOS {

UInt32BitField ResetReason::reason;

void ResetReason::init(uint32_t lastResetReason){
    reason.write(0);

    if (lastResetReason & RCC_CSR::LPWRRSTF(1)){
        reason.set(RESET_REASON::LOWPOWER(1));
    }

    if (lastResetReason & RCC_CSR::WWDGRSTF(1)){
        reason.set(RESET_REASON::WWDG(1));
    }

    if (lastResetReason & RCC_CSR::IWWGRSTF(1)){
        reason.set(RESET_REASON::IWDG(1));
    }

    if (lastResetReason & RCC_CSR::SFTRSTF(1)){
        reason.set(RESET_REASON::SOFTWARE(1));
    }

    if (lastResetReason & RCC_CSR::BORRSTF(1)){
        reason.set(RESET_REASON::BROWNOUT(1));
    }

    if (lastResetReason & RCC_CSR::PINRSTF(1)){
        reason.set(RESET_REASON::EXTERNAL(1));
    }

    if (lastResetReason & RCC_CSR::OBLRSTF(1)){
        reason.set(RESET_REASON::OBL(1));
    }

    if(lastResetReason & RCC_CSR::FWRSTF(1)){
        reason.set(RESET_REASON::FIREWALL(1));
    }
}

UInt32BitField ResetReason::getReason() {
    return reason;
}

}
