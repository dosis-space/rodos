#include "rtc.h"

#include "timemodel.h"

#include "peripheral_ctrl/peripheral_defs.h"

namespace RODOS::RTC {

/**
 * Check if the RTC Calendar has been initialized
 */
static bool isRTCInitialized() {
    return !!rtc.ISR.readBits(RTC_ISR::INITS(1));
}

/**
 * 46.3.7 RTC initialization and configuration: RTC register write protection
 */
static void disableWriteProtection() {
    rtc.WPR.set(RTC_WPR::KEY(0xCA));
    rtc.WPR.set(RTC_WPR::KEY(0x53));
}
static void enableWriteProtection() {
    rtc.WPR.set(RTC_WPR::KEY(0xFF));
}

static void writeTime(uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec) {
    disableWriteProtection();

    // 1. enter initialization mode
    rtc.ISR.set(RTC_ISR::INIT(1));

    // 2. wait for initialization mode to begin
    // this shouldn't take long, so it's ok to busy wait
    while(!rtc.ISR.readBits(RTC_ISR::INITF(1))) {}

    // 3. prescalars are already setup to generate a 1Hz clk_spre

    // 4. Configure time format to 24h and load initial values for date and time.
    rtc.CR.set(RTC_CR::FMT(0));
    {
        using namespace RTC_DR;

        rtc.DR.write(DU(day % 10), DT(day / 10), MU(month % 10), MT(month / 10), YU(year % 10), YT(year / 10));
    }
    {
        using namespace RTC_TR;

        rtc.TR.write(SU(sec % 10), ST(sec / 10), MNU(min % 10), MNT(min / 10), HU(hour % 10), HT(hour / 10));
    }

    // 5. exit the initialization mode
    rtc.ISR.set(RTC_ISR::INIT(0));

    enableWriteProtection();
}

void init()
{
    /* Refer to RM0432 Rev 9, Section 5.1.5 Battery backup domain
     * 1. Enable write access to the PWR control registers
     */
    rcc.APB1ENR1.set(APB1ENR1::PWREN(1));
    rcc.APB1ENR1.read(); // delay ?

    /* 2. After system reset the backup domain registers are protected from writes, disable this protection */
    pwr.CR1.set(PWR_CR1::DBP(1));

    /* 3. Set the clock source for the RTC
     *    If recovering from Vbat brown-out, these values will have already been set
     */
    rcc.BDCR.set(RCC_BDCR::RTCSEL(static_cast<uint8_t>(RTC_SOURCE::LSE)));

    /* 4. Enable the RTC peripheral */
    rcc.BDCR.set(RCC_BDCR::RTCEN(1));

    /* Set the RTCAPBEN bit: Enable the RTC APB clock, allowing register access to the RTC;
     * This clock is used by the RTC register interface, not the RTC itself.
     * See `6.4.19 APB1 peripheral clock enable register 1 (RCC_APB1ENR1)` in the manual.
     */
    rcc.APB1ENR1.set(APB1ENR1::RTCAPBEN(1));

    // if the clock has not been initialised, then Vbat has lost power, to allow some form of time keeping
    // we set the clock to 0 and let it run
    if (!isRTCInitialized()) {
        // Reset to 2000-01-01 00:00:00
        writeTime(1, 1, 1, 0, 0, 0);
    }
}

void setTime(int64_t utcTime)
{
    int32_t year;
    int32_t month;
    int32_t day;

    int32_t hour;
    int32_t min;
    double sec;

    TimeModel::localTime2Calendar(utcTime, year, month, day, hour, min, sec);

    year -= 1999;

    writeTime(static_cast<uint8_t>(year),
        static_cast<uint8_t>(month),
        static_cast<uint8_t>(day),
        static_cast<uint8_t>(hour),
        static_cast<uint8_t>(min),
        static_cast<uint8_t>(sec));
}

int64_t getTime()
{
    /* 46.3.8. Reading the calendar
     * The read order of DR last is important
     *
     * FIXME: In theory we also need to wait for the RSF flag
     */
    RegisterCopy tr = rtc.TR;
    RegisterCopy ssr = rtc.SSR;
    RegisterCopy dr = rtc.DR;

    const uint32_t year = dr.read<RTC_DR::YU>() + 10 * dr.read<RTC_DR::YT>() + 1999;
    const uint32_t month = dr.read<RTC_DR::MU>() + 10 * dr.read<RTC_DR::MT>();
    const uint32_t day = dr.read<RTC_DR::DU>() + 10 * dr.read<RTC_DR::DT>();

    const uint32_t hour = tr.read<RTC_TR::HU>() + 10 * tr.read<RTC_TR::HT>();
    const uint32_t min = tr.read<RTC_TR::MNU>() + 10 * tr.read<RTC_TR::MNT>();
    const uint32_t sec = tr.read<RTC_TR::SU>() + 10 * tr.read<RTC_TR::ST>();

    // FIXME: This assumes the default values for PREDIV_S = 0xFF
    // second fraction = (PREDIV_S - SS) / (PREDIV_S - 1)
    const double second_fraction = (0xFF - ssr.read<RTC_SSR::SS>()) / (0xFF - 1);

    return TimeModel::calendar2LocalTime(static_cast<int32_t>(year),
        static_cast<int32_t>(month),
        static_cast<int32_t>(day),
        static_cast<int32_t>(hour),
        static_cast<int32_t>(min),
        static_cast<double>(sec) + second_fraction);
}

void chargeBackupBattery()
{
    /* Refer to RM0432 Rev 9, Section 5.1.5 Battery backup domain */
    // enable battery charging by setting VBE bit in PWR_CR4 register
    // automatically disable in VBAT mode
    pwr.CR4.set(PWR_CR4::VBE(1));
}

void disableChargeBackupBattery()
{
    pwr.CR4.set(PWR_CR4::VBE(0));
}

int loadRTCBackupRegister(size_t index, uint32_t& reg)
{
    constexpr size_t NUM_REGISTERS = 32;
    if (index >= NUM_REGISTERS) {
        return -1;
    }

    reg = rtc.BKPR[index].read();
    return 0;
}

int storeRTCBackupRegister(size_t index, uint32_t val)
{
    constexpr size_t NUM_REGISTERS = 32;
    if (index >= NUM_REGISTERS) {
        return -1;
    }

    rtc.BKPR[index].write(static_cast<Register::value_t>(val));
    return rtc.BKPR[index].read() == val;
}

}
