#pragma once

#include <cstdint>
#include <cstddef>

namespace RODOS::RTC {

/**
 * @brief Enable the RTC, if Vbat lost power the clock will at J2000
 */
void init();

int64_t getTime();
void setTime(int64_t utcTime);

void chargeBackupBattery();
void disableChargeBackupBattery();

int loadRTCBackupRegister(size_t index, uint32_t& reg);
int storeRTCBackupRegister(size_t index, uint32_t val);

}
