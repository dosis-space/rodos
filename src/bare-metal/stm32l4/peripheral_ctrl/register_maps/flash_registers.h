#pragma once
#include "cortex_m/register_access/register.h"

namespace RODOS {

namespace FLASH_ACR {
    using LATENCY  = RegSubValue<0, 3>;
    using PRFTEN   = RegSubValue<8>;
    using ICEN     = RegSubValue<9>;
    using DCEN     = RegSubValue<10>;
    using ICRST    = RegSubValue<11>;
    using DCRST    = RegSubValue<12>;
    using RUN_PD   = RegSubValue<13>;
    using SLEEP_PD = RegSubValue<14>;
} // namespace FLASH_ACR

namespace FLASH_KEYR {
    using KEYR = Register;
}

namespace FLASH_SR {
    using EOP     = RegSubValue<0>;
    using OPERR   = RegSubValue<1>;
    using PROGERR = RegSubValue<3>;
    using WRPERR  = RegSubValue<4>;
    using PGAERR  = RegSubValue<5>;
    using SIZERR  = RegSubValue<6>;
    using PGSERR  = RegSubValue<7>;
    using MISSERR = RegSubValue<8>;
    using FASTERR = RegSubValue<9>;
    using RDERR   = RegSubValue<14>;
    using OPTVERR = RegSubValue<15>;
    using BSY     = RegSubValue<16>;
    using PREMPTY = RegSubValue<17>;
} // namespace FLASH_SR

namespace FLASH_CR {
    using PG         = RegSubValue<0>;
    using PER        = RegSubValue<1>;
    using MER1       = RegSubValue<2>;
    using PNB        = RegSubValue<3, 8>;
    using BKER       = RegSubValue<11>;
    using MER2       = RegSubValue<15>;
    using STRT       = RegSubValue<16>;
    using OPTSTART   = RegSubValue<17>;
    using FSTPG      = RegSubValue<18>;
    using EOPIE      = RegSubValue<24>;
    using ERRIE      = RegSubValue<25>;
    using RDERRIE    = RegSubValue<26>;
    using OBL_LAUNCH = RegSubValue<27>;
    using OPTLOCK    = RegSubValue<30>;
    using LOCK       = RegSubValue<31>;
} // namespace FLASH_CR

namespace FLASH_OPTR {
    using RDP        = RegSubValue<0, 8>;
    using BOR_LEV    = RegSubValue<8, 3>;
    using nRST_STOP  = RegSubValue<12>;
    using nRST_STDBY = RegSubValue<13>;
    using nRST_SHDW  = RegSubValue<14>;
    using IWDG_SW    = RegSubValue<16>;
    using IWDG_StOP  = RegSubValue<17>;
    using IWDG_STDBY = RegSubValue<18>;
    using WWDG_SW    = RegSubValue<19>;
    using BFB2       = RegSubValue<20>;
    using DB1M       = RegSubValue<21>;
    using DBANK      = RegSubValue<22>;
    using nBOOT1     = RegSubValue<23>;
    using SRAM2_PE   = RegSubValue<24>;
    using SRAM2_RST  = RegSubValue<25>;
    using nSWBoot0   = RegSubValue<26>;
    using nBOOT0     = RegSubValue<27>;
} // namespace FLASH_OPTR

struct FlashStruct {
    Register ACR;
    Register PDKEYR;
    Register KEYR;
    Register OPTKEYR;
    Register SR;
    Register CR;
    Register ECCR;
    uint32_t UNUSED0;
    Register OPTR;
    Register PCROP1SR;
    Register PCROP1ER;
    Register WRP1AR;
#ifdef STM32L4R5xx
    Register WRP2AR;
#else
    Register WRP1BR;
#endif
    uint32_t UNUSED1[4];
    Register PCROP2SR;
    Register PCROP2ER;
#ifdef STM32L4R5xx
    Register WRP1BR;
#else
    Register WRP2AR;
#endif
    Register WRP2BR;
#ifdef STM32L4R5xx
    uint32_t UNUSED2[(0x130 - 0x54) / sizeof(uint32_t)];
    Register CFGR;
#endif
};

#ifdef STM32L4R5xx
static_assert(sizeof(struct FlashStruct) == 0x134, "struct FlashStruct size mismatch!");
#else
static_assert(sizeof(struct FlashStruct) == 0x54, "struct FlashStruct size mismatch!");
#endif

} // namespace RODOS
