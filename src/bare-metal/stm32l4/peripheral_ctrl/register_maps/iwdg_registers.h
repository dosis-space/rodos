#pragma once
#include "cortex_m/register_access/register.h"

namespace RODOS{

namespace IWDG_KR {
    using KEY = RegSubValue<0, 16>;

    constexpr uint32_t KEY_START = 0xCCCC;
    constexpr uint32_t KEY_RELOAD = 0xAAAA;
    constexpr uint32_t KEY_UNLOCK = 0x5555;
}

namespace IWDG_PR {
    using PR = RegSubValue<0, 3>;

    constexpr uint32_t PR_DIV_4   = PR(0);
    constexpr uint32_t PR_DIV_8   = PR(1);
    constexpr uint32_t PR_DIV_16  = PR(2);
    constexpr uint32_t PR_DIV_32  = PR(3);
    constexpr uint32_t PR_DIV_64  = PR(4);
    constexpr uint32_t PR_DIV_128 = PR(5);
    constexpr uint32_t PR_DIV_256 = PR(6);
    // 7 is also /256
}

namespace IWDG_RLR {
    using RL = RegSubValue<0, 12>;

    constexpr uint32_t RL_MAX = 0xFFF;
}

namespace IWDG_SR {
    using PVU = RegSubValue<0>;
    using RVU = RegSubValue<1>;
    using WVU = RegSubValue<2>;
}

namespace IWDG_WINR {
    using WIN = RegSubValue<0, 12>;
}

struct IWDGStruct {
    Register KR;
    Register PR;
    Register RLR;
    Register SR;
    Register WINR;

    uint8_t padding[0x3ec];
};

static_assert(sizeof(IWDGStruct)==0x400);

}
