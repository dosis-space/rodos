#pragma once
#include "cortex_m/register_access/register.h"

namespace RODOS {

namespace DEVICE_ELECTRONIC_SIGNATURE_PACKAGE_DATA {
    using PKG = RegSubValue8<0, 4>;
}

struct DeviceElectronicSignatureStruct {
    Register8 PACKAGE_DATA;
    Register8 PACKAGE_DATA_RESERVED;
    Register8 UNUSED0[142];
    Register8 UID[12];
    Register8 UNUSED1[68];
    Register8 FLASH_SIZE[2];
};

static_assert(sizeof(struct DeviceElectronicSignatureStruct) == 0xe2, "struct DeviceElectronicSignatureStruct size mismatch!");

} // namespace RODOS
