#include <cstdint>

#include "rodos.h"
#include "hw_uart_dbg.h"

namespace RODOS {

int8_t getcharNoWait() {
    if (__builtin_expect(uartStdout == nullptr, false)) {
        // Debug uart ist not yet initialized
        return -1;
    }

    int16_t c = uartStdout->getcharNoWait();
    if(c<0) {
        return -1;
    } else {
        return static_cast<int8_t>(c);
    }
}

bool isCharReady() {
    if (__builtin_expect(uartStdout == nullptr, false)) {
        // Debug uart ist not yet initialized
        return false;
    }

    return uartStdout->isDataReady();
}

char* getsNoWait() {
    constexpr auto IN_BUFFER_SIZE { 122 };
    static constinit char inBuffer[IN_BUFFER_SIZE];
    static constinit int inBufferIndex { 0 };

    while(1) {
        int8_t c = getcharNoWait();
        if(c < 0) return 0;

        if(c == '\r' || c == '\n') {
            inBuffer[inBufferIndex] = 0;
            inBufferIndex = 0;
            return inBuffer;
        }

        inBuffer[inBufferIndex] = static_cast<char>(c);
        if(inBufferIndex < 120) inBufferIndex++;
    }
}

void activateTopicCharInput() { }

}
