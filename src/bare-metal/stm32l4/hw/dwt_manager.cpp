#include "hal/hal_dwt.h"
#include "peripheral_ctrl/peripheral_defs.h"

namespace RODOS {

void DwtManager::DwtNotImplemented() {}

void DwtManager::init() {
    resetAllThreadCycleCounts();
    dbg.DEMCR.set(DEMCR::TRCENA(1));
    dwt.CYCCNT.write(static_cast<uint32_t>(0));
}

void DwtManager::startCycleCount() {
    dwt.CTRL.set(DWT_CR::CYCCNTENA(1));
}

void DwtManager::stopCycleCount() {
    dwt.CTRL.set(DWT_CR::CYCCNTENA(0));
}

uint8_t DwtManager::getThreadPointers(Thread* threads[], uint8_t size) {
    uint8_t i = 0;
    for(auto& elem : GlobalLists::get(GlobalListsEnum::THREAD_LIST)) {
        auto* threadPtr = static_cast<Thread*>(&elem);
        if (i < size) {
            threads[i] = threadPtr;
        }
        i++;
    }
    return i;
}

uint32_t DwtManager::getCycleCount() {
    return dwt.CYCCNT.read();
}

void DwtManager::reset() {
    dwt.CYCCNT.write(static_cast<uint32_t>(0));
}

void DwtManager::resetAllThreadCycleCounts() {
    for(auto& elem : GlobalLists::get(GlobalListsEnum::THREAD_LIST)) {
        auto& threadRef = static_cast<Thread&>(elem);
        threadRef.activeCycles = 0;
    }
}
} // namespace RODOS
