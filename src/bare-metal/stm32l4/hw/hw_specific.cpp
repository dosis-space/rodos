#include "rodos.h"

#include "hal/hal_uart.h"
#include "hw_context.h"
#include "hw_hal/gpio/exti.h"

#include "peripheral_ctrl/clock_management/clock_manager.h"
#include "peripheral_ctrl/register_maps/flash_registers.h"
#include "peripheral_ctrl/rtc/rtc.h"
#include "peripheral_ctrl/peripheral_defs.h"
#include "hw/cpu_manager.h"
#include "hw/flash_manager.h"
#include "cortex_m/nvic.h"
#include "hw_uart_dbg.h"

#include <bit>

namespace RODOS {

void hwDisableInterrupts()
{
    asm volatile("cpsid i");
}

void hwEnableInterrupts()
{
    asm volatile("cpsie i");
}

void hwInit()
{
    EXTI::initEXTInterrupts();
    HWContext::init();
}

void HWContext::init(void)
{
    /* FWRSTF..LPWRSTF */
    uint32_t lastResetReason = rcc.CSR.readBits(0xFF000000);

    /* clear the reset flags */
    rcc.CSR.set(RODOS::RCC_CSR::RMVF(1));

    ResetReason::init(lastResetReason);

    NVIC::setAllIrqsToDefaultPriority();
    FlashManager::initCache();
    ClockManager::initClocks();
    CPUManager::setupDebugging();
    RTC::init();

    VCORERangeManager::init();

    extern int32_t myNodeNr; // independent/globaldata.cpp
    uint32_t       uuidWord = CPUManager::getFirstUUIDWord();
    myNodeNr                = *reinterpret_cast<int32_t*>(&uuidWord);

    static RODOS::HAL_UART debugUart { UART_DEBUG_IDX, UART_DEBUG_TX, UART_DEBUG_RX };
    uartStdout = &debugUart;
    uartStdout->init(115200);

    PRINTF("CPU: %lu MHz\n", HCLK::freq / 1'000'000);
    int32_t year, month, day, hour, min;
    double sec;
    TimeModel::localTime2Calendar(RTC::getTime(), year, month, day, hour, min, sec);
    PRINTF("RTC: %04ld-%02ld-%02ld %02ld:%02ld:%02lf\n", year, month, day, hour, min, sec);
}

void hwResetAndReboot()
{
    NVIC::systemReset();
}

int64_t hwGetRTCTime()
{
    return RTC::getTime();
}

void hwSetRTCTime(int64_t time)
{
    RTC::setTime(time);
}

int hwLoadRTCBackupRegister(size_t index, uint32_t& reg)
{
    return RTC::loadRTCBackupRegister(index, reg);
}

int hwStoreRTCBackupRegister(size_t index, uint32_t val)
{
    return RTC::storeRTCBackupRegister(index, val);
}

void hwChargeRTCBackupBattery()
{
    RTC::chargeBackupBattery();
}

void hwDisableChargeRTCBackupBattery()
{
    RTC::disableChargeBackupBattery();
}

void enterSleepMode()
{
    asm volatile("wfi");
}

void hwInitWatchdog(int32_t intervalMilliseconds)
{
    /* STM32L4 Independent WatchDoG, chapter 36 of the Manual
     * The IWDG uses the 32Khhz LSI clock, dividing it by a prescaler selected
     * by the value in the PR register:
     *
     * 0: /4
     * 1: /8
     * 2: /16
     * 3: /32
     * 4: /64
     * 5: /128
     * 6: /256
     * 7: /256
     *
     * The resulting signal drives a 12bit downcounter, if it reaches 0 a reset is triggered.
     * the window feature is not used.
     *
     * Formular for the watchdog interval:
     * t_ms = (reload_vaue * prescaler / LSI::freq / 1000)
     *
     *
     * The steps necessary to initialise the IWDG are described in the Manual:
     * 36.3.2 Window option, Paragraph "Configuring the IWDG when the window option is disabled"
     */

    /* With the available prescaler and reload values the IWDG has a range of 0.125ms - 32760ms */
    RODOS_ASSERT(intervalMilliseconds >= 1);
    RODOS_ASSERT(intervalMilliseconds <= 32760);

    /* Enabling the IWDG will force the LSI on, to avoid confusion make sure the user is
     * aware of this.
     * This is also required so that LSI::freq returns the correct value
     */
    RODOS_ASSERT(LSI::enable == true);

    /* 1. Enable watchdog:
     * This also starts the watchdog, counting down from 0xFFFF (Manual 36.3.1)
     * At reset the prescaler defaults to /4, so we have 8191.875ms to setup the iwdg after this point
     */
    iwdg.KR.write(IWDG_KR::KEY(IWDG_KR::KEY_START));

    /* 2. Enable registers access: */
    iwdg.KR.write(IWDG_KR::KEY_UNLOCK);

    /* 3. Compute prescaler and reload values:
     * To fit the highest bit of reloadValueAtPrescaler4 into the reload Register, we need to shift until
     * the highest bit is inside the range of IWDG_RLR again, bit_width returns the index of the highest set bit
     *
     * The RODOS_ASSERT above limits the range of intervalMilliseconds so that:
     * 1. the static_cast is always safe
     * 2. prescaler is in the range of IWDG_PR
     */
    const uint32_t reloadValueAtPrescaler4 { static_cast<uint32_t>(intervalMilliseconds) * LSI::freq / 1000 / 4 };
    constexpr size_t RELOAD_VALUE_BITS { std::bit_width(IWDG_RLR::RL_MAX) };
    const uint8_t prescaler { static_cast<uint8_t>(std::bit_width(reloadValueAtPrescaler4 >> RELOAD_VALUE_BITS)) };

    /* 4. Write the computed values */
    iwdg.PR.write(IWDG_PR::PR(prescaler));
    iwdg.RLR.write(IWDG_RLR::RL(reloadValueAtPrescaler4 >> prescaler));

    /* 5. Reload the iwdg with new settings to give the application enough time to enter its application loop */
    hwTriggerWatchdog();

    /* in case the application calls hwInitWatchdog again, ensure that all register writes have finished
     * this can take a maximum of 4ms after the last write occured (at 5 clocks with /256 prescaler)
     */
    while (iwdg.SR.read<IWDG_SR::PVU>()) { ; }
    while (iwdg.SR.read<IWDG_SR::RVU>()) { ; }
    while (iwdg.SR.read<IWDG_SR::WVU>()) { ; }
}

void hwTriggerWatchdog()
{
    /* the reload register is already setup in hwInitWatchdog */
    iwdg.KR.write(IWDG_KR::KEY_RELOAD);
}

static constexpr int64_t FLASH_TIMEOUT_IN_NS {500 * MILLISECONDS};

/**
 * Busy wait until the flash is ready.
 *
 * returns false if we time out, true otherwise
 */
static bool busyWaitUntilFlashReady() {
    int64_t startTime = NOW();

    while (flash.SR.read<FLASH_SR::BSY>()) {
        if ((NOW() - startTime) > FLASH_TIMEOUT_IN_NS) {
            return false;
        }
    }

    return true;
}

/**
 *  returns the size of the flash memory area in bytes
 */
static size_t getFlashSize()
{
    return static_cast<size_t>(deviceElectronicSignature.FLASH_SIZE[0].read() |
                              (deviceElectronicSignature.FLASH_SIZE[1].read() << 8)) * 1024;
}

static bool isInDualBankMode()
{
    if (!mcu_specific::SUPPORTS_DUAL_BANK_MODE) {
        return false;
    }

#ifdef STM32L4R5xx
    // check DB1M bit if flash size is less than 2MiB and DBANK otherwise
    if (getFlashSize() < 0x200000) {
        return flash.OPTR.read<FLASH_OPTR::DB1M>() == 1;
    } else {
        return flash.OPTR.read<FLASH_OPTR::DBANK>() == 1;
    }
#else
    // check DUALBANK bit
    return flash.OPTR.read<FLASH_OPTR::DB1M>() == 1;
#endif
}

int32_t hwFlashPageSize()
{
    if (isInDualBankMode()) {
        // Dual-bank mode
        return mcu_specific::FLASH_PAGE_SIZE_SINGLE_BANK >> 1;
    } else {
        // Single-bank mode
        return mcu_specific::FLASH_PAGE_SIZE_SINGLE_BANK;
    }
}

int32_t hwFlashUnlock(uint32_t key1, uint32_t key2)
{
    // Unlocking twice results in a hard fault, so let's not do that
    if (flash.CR.read<FLASH_CR::LOCK>() == 0) {
        return -2;
    }

    flash.KEYR.write(key1);
    flash.KEYR.write(key2);
    return flash.CR.read<FLASH_CR::LOCK>() == 0 ? 0 : -1;
}

int32_t hwFlashLock()
{
    flash.CR.set(FLASH_CR::LOCK(1));
    return flash.CR.read<FLASH_CR::LOCK>() == 1 ? 0 : -1;
}

static constexpr uint32_t FLASH_START_ADDR { 0x0800'0000 };

int32_t hwFlashRead(size_t offset, size_t length, uint8_t *buffer)
{
    const size_t flashSizeBytes = getFlashSize();

    // check that offset is not too large
    if (offset >= flashSizeBytes) {
        return -1;
    }

    // make sure we don't read further than the flash memory area
    if (offset + length >= flashSizeBytes) {
        return -1;
    }

    if (!busyWaitUntilFlashReady()) {
        return -1;
    }

    size_t readBytes = 0;

    uint8_t* address = reinterpret_cast<uint8_t*>(FLASH_START_ADDR | offset);

    while (readBytes < length) {
        *(buffer++) = *(address++);

        if (flash.SR.read<FLASH_SR::RDERR>() != 0) {
            break;
        }

        ++readBytes;
    }

    return static_cast<int32_t>(readBytes);
}

int32_t hwFlashWrite(size_t offset, size_t length, const uint8_t *buffer)
{
    // TODO: check if the following is actually required:
    // unset PER bit in case it was still on from a failed erase operation
    flash.CR.set(FLASH_CR::PER(0));

    const size_t flashSizeBytes = getFlashSize();

    // check that offset is not too large
    if (offset >= flashSizeBytes) {
        return -1;
    }

    // make sure we don't write further than the flash memory area
    if (offset + length >= flashSizeBytes) {
        return -1;
    }

    // check that offset is double-word aligned and length is multiple of 8
    if ((offset & 7) != 0 || (length & 7) != 0) {
        return -1;
    }

    // 1. Check that no Flash memory operation is ongoing
    if (!busyWaitUntilFlashReady()) {
        return -1;
    }

    // 2. Check and clear all error programming flags due to a previous programming
    flash.SR.set(FLASH_SR::PROGERR(1));
    flash.SR.set(FLASH_SR::SIZERR(1));
    flash.SR.set(FLASH_SR::PGAERR(1));
    flash.SR.set(FLASH_SR::PGSERR(1));
    flash.SR.set(FLASH_SR::WRPERR(1));
    flash.SR.set(FLASH_SR::MISSERR(1));
    flash.SR.set(FLASH_SR::FASTERR(1));

    // 3. Set the PG bit in the Flash control register
    flash.CR.set(FLASH_CR::PG(1));


    volatile uint32_t* address = reinterpret_cast<uint32_t*>(FLASH_START_ADDR | offset);

    size_t writtenBytes = 0;

    // 4. Perform the data write operation at the desired memory address.
    //    Only double word can be programmed
    while (writtenBytes < length) {
        uint32_t toWriteLower{};
        uint32_t toWriteHigher{};
        RODOS::memcpy(&toWriteLower, buffer, 4);
        buffer += 4;
        RODOS::memcpy(&toWriteHigher, buffer, 4);
        buffer += 4;

        hwDisableInterrupts();
        // Write a first word in an address aligned with double word
        *(address++) = toWriteLower;
        // Ensure that memory writes have been observed by the flash before we continue
        asm volatile("dmb st");

        // Write the second word
        *(address++) = toWriteHigher;
        asm volatile("dmb st");
        hwEnableInterrupts();

        // 5. Wait until the BSY bit is cleared in the FLASH_SR register
        if (!busyWaitUntilFlashReady()) {
            return static_cast<int32_t>(writtenBytes);
        }

        // 6. Check that EOP flag is set in the FLASH_SR register, and clear it by software
        // (The EOP bit is set only if the end of operation interrupts are enabled (EOPIE = 1))
        if (flash.CR.read<FLASH_CR::EOPIE>() == 1 && flash.SR.read<FLASH_SR::EOP>() == 1) {
            flash.SR.set(FLASH_SR::EOP(0));
        }

        // check for any errors
        if (flash.SR.read<FLASH_SR::OPERR>()   != 0 ||
            flash.SR.read<FLASH_SR::PGSERR>()  != 0 ||
            flash.SR.read<FLASH_SR::PROGERR>() != 0 ||
            flash.SR.read<FLASH_SR::SIZERR>()  != 0 ||
            flash.SR.read<FLASH_SR::PGAERR>()  != 0 ||
            flash.SR.read<FLASH_SR::WRPERR>()  != 0 ||
            flash.SR.read<FLASH_SR::MISSERR>() != 0 ||
            flash.SR.read<FLASH_SR::FASTERR>() != 0) {
            break;
        }

        writtenBytes += 8;
    }

    // 7. Clear the PG bit in the FLASH_SR register if there no more programming request anymore
    flash.CR.set(FLASH_CR::PG(0));

    return static_cast<int32_t>(writtenBytes);
}

int32_t hwFlashErasePage(size_t offset)
{
    // We allow using both the correct flash address (0x080...) and the offset from the flash start (0x000...)
    offset &= ~FLASH_START_ADDR;

    // unset PG bit in case it was still on from a failed programming operation
    flash.CR.set(FLASH_CR::PG(0));

    size_t flashSizeBytes = getFlashSize();

    size_t pageNum;
    uint32_t bank;
    size_t amountPages;
    int32_t pageSizeWithErr = hwFlashPageSize();

    if (pageSizeWithErr < 0 ) {
        return -1;
    }

    size_t pageSizeBytes = static_cast<size_t>(pageSizeWithErr);

    if (!isInDualBankMode()) {
        // Single-bank mode
        bank = 0;

        amountPages = flashSizeBytes / pageSizeBytes;

        pageNum = offset / pageSizeBytes;
    } else { // Dual-bank mode

        amountPages = (flashSizeBytes / pageSizeBytes) / 2; // pages per bank

        pageNum = offset / pageSizeBytes;

        // determine in which bank the page is
        if (pageNum >= amountPages) {
            bank = 1;
            pageNum -= amountPages;
        } else {
            bank = 0;
        }
    }

    // check if the offset is page alligned
    if ((offset & (pageSizeBytes - 1)) != 0) {
        return -1;
    }

    if (pageNum >= amountPages) {
        return -1;
    }

    hwDisableInterrupts();

    // 1. Check that no Flash memory operation is ongoing
    if (!busyWaitUntilFlashReady()) {
        hwEnableInterrupts();
        return -1;
    }

    // 2. Check and clear all error programming flags due to a previous programming
    flash.SR.set(FLASH_SR::PROGERR(1));
    flash.SR.set(FLASH_SR::SIZERR(1));
    flash.SR.set(FLASH_SR::PGAERR(1));
    flash.SR.set(FLASH_SR::PGSERR(1));
    flash.SR.set(FLASH_SR::WRPERR(1));
    flash.SR.set(FLASH_SR::MISSERR(1));
    flash.SR.set(FLASH_SR::FASTERR(1));

    // 3. Set the PER bit and select the page to erase with the associated bank
    flash.CR.set(FLASH_CR::PER(1));
    flash.CR.set(FLASH_CR::PNB(pageNum));

    if (mcu_specific::SUPPORTS_DUAL_BANK_MODE) {
        flash.CR.set(FLASH_CR::BKER(static_cast<uint32_t>(bank)));
    }

    // 4. Set the STRT bit in the FLASH_CR register
    flash.CR.set(FLASH_CR::STRT(1));

    // 5. Wait for the BSY bit to be cleared in the FLASH_SR register
    if (!busyWaitUntilFlashReady()) {
        hwEnableInterrupts();
        return -1;
    }

    flash.CR.set(FLASH_CR::PER(0));

    hwEnableInterrupts();
    return flash.SR.read<FLASH_SR::OPERR>() == 0 ? 0 : -1;
}

}
