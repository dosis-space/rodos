#include "rodos.h"

#ifndef NO_RODOS_NAMESPACE
namespace RODOS {
#endif

void PWMNotImplemented() {
    RODOS_ERROR("PWM is not implemented for linux_x86!");
}
HAL_PWM::HAL_PWM([[gnu::unused]] PWM_IDX idx) : context(nullptr) { PWMNotImplemented(); }
int32_t HAL_PWM::init([[gnu::unused]] uint32_t freq, [[gnu::unused]] uint32_t increments) { (void)context;return 1; }
int32_t HAL_PWM::config([[gnu::unused]] PWM_PARAMETER_TYPE type, [[gnu::unused]] int32_t paramVal) { return 1; }
void    HAL_PWM::reset() {}
int32_t HAL_PWM::write([[gnu::unused]] uint32_t highPulseWidth) { return 1; }

#ifndef NO_RODOS_NAMESPACE
}
#endif
