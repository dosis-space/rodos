#include "rodos-debug.h"
#include "hal/hal_spi.h"

namespace RODOS {

void SPINotImplemented() {
    RODOS_ERROR("SPI is not implemented for linux_x86!");
}

HAL_SPI::HAL_SPI(SPI_IDX) : context{ nullptr } { SPINotImplemented(); }
HAL_SPI::HAL_SPI(
  SPI_IDX, GPIO_PIN, GPIO_PIN, GPIO_PIN, GPIO_PIN)
  : context{ nullptr } { SPINotImplemented(); }

int32_t HAL_SPI::init(uint32_t, bool, bool) { return -1; }
void    HAL_SPI::reset() {}
int32_t HAL_SPI::config(SPI_PARAMETER_TYPE, int32_t) { return -1; }
int32_t HAL_SPI::status(SPI_STATUS_TYPE) { return -1; }
bool    HAL_SPI::isWriteFinished() { return true; }
bool    HAL_SPI::isReadFinished() { return true; }
int32_t HAL_SPI::write(const void*, size_t) { return -1; }
int32_t HAL_SPI::read(void*, size_t) { return -1; }
int32_t HAL_SPI::writeRead(const void*, size_t, void*, size_t) { return -1; }
int32_t HAL_SPI::writeTrig(const void*, size_t, uint8_t, int64_t, bool) { return -1; }
int32_t HAL_SPI::readTrig(void*, size_t, uint8_t, int64_t, bool) { return -1; }
int32_t HAL_SPI::writeReadTrig(const void*, size_t, void*, size_t, uint8_t, int64_t, bool) { return -1; }

} // namespace RODOS
