#include "rodos.h"

#ifndef NO_RODOS_NAMESPACE
namespace RODOS {
#endif

void I2CNotImplemented() {
    RODOS_ERROR("I2C is not implemented for linux_x86!");
}
HAL_I2C::HAL_I2C([[gnu::unused]] I2C_IDX idx) : context(nullptr) { I2CNotImplemented(); }
HAL_I2C::HAL_I2C([[gnu::unused]] I2C_IDX idx, [[gnu::unused]] GPIO_PIN sclPin, [[gnu::unused]] GPIO_PIN sdaPin) : context(nullptr) { I2CNotImplemented(); }
int32_t HAL_I2C::init([[gnu::unused]] uint32_t speed) { (void)context;return 1; }
void    HAL_I2C::reset() {}
bool    HAL_I2C::isWriteFinished() { return true; }
bool    HAL_I2C::isReadFinished() { return true; }
int32_t HAL_I2C::write([[gnu::unused]] const uint8_t addr, [[gnu::unused]] const uint8_t* txBuf, [[gnu::unused]]uint32_t txBufSize) { return 1; }
int32_t HAL_I2C::read([[gnu::unused]] const uint8_t addr, [[gnu::unused]] uint8_t* rxBuf, [[gnu::unused]] uint32_t rxBufSize) { return 1; }
int32_t HAL_I2C::writeRead([[gnu::unused]] const uint8_t addr, [[gnu::unused]] const uint8_t* txBuf, [[gnu::unused]] uint32_t txBufSize, [[gnu::unused]] uint8_t* rxBuf, [[gnu::unused]] uint32_t rxBufSize) { return 1; }

#ifndef NO_RODOS_NAMESPACE
}
#endif
