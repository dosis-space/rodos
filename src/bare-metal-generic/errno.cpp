#include <cerrno>
#include "rodos.h"

namespace RODOS {

int globalErrno = 0;

extern "C" int* __errno(){
    Thread* currentThread = Thread::getCurrentThread();
    if(currentThread == nullptr){
        return &globalErrno;
    }else{
        return &currentThread->localErrno;
    }
}

}
