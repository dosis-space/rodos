#include "ff.h"     //Important to include before "diskio.h" to fix undefined types in there
#include "diskio.h"
#include "rodos.h"
#include "sdcard/sdcard_wrap.h"


namespace rodos::support::filesystem {

using namespace rodos::support::sdcard;

DRESULT getFatfsResult(int32_t status) {
    switch(status) {
        case status::NO_ERROR:
            return RES_OK;
        case status::STORAGE_ERROR:
            return RES_ERROR;
        case status::SD_ADDRESS_ERROR:
        case status::SD_ILLEGAL_COMMAND:
            return RES_PARERR;
        case status::SD_NO_INIT:
            return RES_NOTRDY;
    }

    return RES_ERROR;
}

extern "C" {

DSTATUS disk_initialize(BYTE pdrv) {
    if(getWrap().sdInitSpi()) {
        return STA_NOINIT;
    }
    return disk_status(pdrv);
}

DSTATUS disk_status([[gnu::unused]] BYTE pdrv) {
    uint8_t returnvalue{ 0 };
    switch(getWrap().sdGetStatus()) {
        case status::NO_ERROR:
            returnvalue = 0;
            break;
        case status::SD_NO_INIT:
            returnvalue = STA_NOINIT;
            break;
        case status::SPI_LINE_HIGH:
        case status::STORAGE_ERROR:
            returnvalue = STA_NODISK;
            break;
    }
    return returnvalue;
}

DRESULT disk_read([[gnu::unused]] BYTE pdrv, BYTE* buff, LBA_t sector, UINT count) {
    for(UINT i{ 0 }; i < count; i++) {
        DRESULT error{ getFatfsResult(getWrap().sdReadBlock((buff + i * data::BLOCKSIZE), sector + i)) };
        if(error) {
            return error;
        }
    }
    return RES_OK;
}
DRESULT disk_write([[gnu::unused]] BYTE pdrv, const BYTE* buff, LBA_t sector, UINT count) {
    for(UINT i{ 0 }; i < count; i++) {
        if(getWrap().sdWriteBlock(sector + i, static_cast<const uint8_t*>(buff + i * data::BLOCKSIZE)) != 0) {
            return RES_ERROR;
        }
    }
    return RES_OK;
}

DRESULT disk_ioctl([[gnu::unused]] BYTE pdrv, BYTE cmd, void* buff) {
    switch(cmd) {
        case CTRL_SYNC: {  //is called, for the fatfs to synchronize with pending write processes
            return RES_OK; //disk_write is blocking, by the time this can be called, it will already have finnished
        }
        case GET_SECTOR_COUNT: {
            int32_t count{ getWrap().sdGetSectorcount() };
            if(count < 0) {
                return RES_NOTRDY;
            }
            *(LBA_t*)buff = count;
            break;
        }
        case GET_SECTOR_SIZE: {
            int32_t size = getWrap().sdGetSectorsize();
            if(size < 0)
                return RES_NOTRDY;
            *(WORD*)buff = size;
            break;
        }
        case GET_BLOCK_SIZE: {
            int32_t size = getWrap().sdGetBlocksize();

            if(size < 0)
                return RES_NOTRDY;
            *(DWORD*)buff = size;
            break;
        }
        default:
            return RES_PARERR;
    }
    return RES_OK;
}

DWORD get_fattime (void) {
	/*
		Current local time shall be returned as bit-fields packed into a DWORD value. The bit fields are as follows:

		bit31:25
			Year origin from the 1980 (0..127, e.g. 37 for 2017)
		bit24:21
			Month (1..12)
		bit20:16
			Day of the month (1..31)
		bit15:11
			Hour (0..23)
		bit10:5
			Minute (0..59)
		bit4:0
			Second / 2 (0..29, e.g. 25 for 50) 
	*/
	//8.8.1988 00:00:00
	return 0x11080000;		//TODO: useful
}

}

} // namespace rodos::support::filesystem
