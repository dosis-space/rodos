#pragma once

#include "hal/hal_spi.h"
#include "timemodel.h"
#include "rodos-debug.h"
#include "string_pico.h"

namespace rodos::support::sdcard {

// Connect SDCard to SPI3 on STM32L496ZG SSBv2
// SDIO PINS for SPI connection on STM32L4 to SDCard
constexpr RODOS::GPIO_PIN SDIO_MOSI = RODOS::GPIO_107; //PG11
constexpr RODOS::GPIO_PIN SDIO_MISO = RODOS::GPIO_106; //PG10
constexpr RODOS::GPIO_PIN SDIO_SCLK = RODOS::GPIO_105; //PPG9
constexpr RODOS::GPIO_PIN SDIO_CS   = RODOS::GPIO_108; //PGP12
constexpr RODOS::SPI_IDX  SDIO_IDX  = RODOS::SPI_IDX2; //SPI_IDX0 is SPI1, then counting upwards


/**
 * @brief   Command Definitions for SD / MMC Cards
 */
namespace cmd {

    /**
     * @brief   Headers for all commands for the SD Card
     */
    namespace header {
        constexpr uint8_t CMD0    = 0;  /* GO_IDLE_STATE */
        constexpr uint8_t CMD1    = 1;  /* SEND_OP_COND */
        constexpr uint8_t CMD8    = 8;  /* SEND_IF_COND */
        constexpr uint8_t CMD9    = 9;  /* SEND_CSD */
        constexpr uint8_t CMD10   = 10; /* SEND_CID */
        constexpr uint8_t CMD12   = 12; /* STOP_TRANSMISSION */
        constexpr uint8_t CMD16   = 16; /* SET_BLOCKLEN */
        constexpr uint8_t CMD17   = 17; /* READ_SINGLE_BLOCK */
        constexpr uint8_t CMD18   = 18; /* READ_MULTIPLE_BLOCK */
        constexpr uint8_t CMD23   = 23; /* SET_BLOCK_COUNT */
        constexpr uint8_t CMD24   = 24; /* WRITE_BLOCK */
        constexpr uint8_t CMD25   = 25; /* WRITE_MULTIPLE_BLOCK */
        constexpr uint8_t CMD32   = 32;
        constexpr uint8_t CMD33   = 33;
        constexpr uint8_t CMD38   = 38;
        constexpr uint8_t CMD41   = 41; /* SEND_OP_COND (ACMD) */
        constexpr uint8_t CMD55   = 55; /* APP_CMD */
        constexpr uint8_t CMD58   = 58; /* READ_OCR */
        constexpr uint8_t DEFAULT = 0x40;
        constexpr uint8_t INVALID = 0x80;
        constexpr size_t  SIZE    = 1;
    } // namespace header


    /**
     * @brief   SPI Default Arguments for Specific Commands
     */
    namespace arg {
        constexpr uint32_t CMD0         = 0x0000;
        constexpr uint32_t CMD8         = 0x01AA;
        constexpr uint32_t CMD9         = 0x0000;
        constexpr uint32_t CMD55        = 0x0000;
        constexpr uint32_t CMD58        = 0x0000;
        constexpr uint8_t  LSB_POSITION = 4;
    } // namespace arg


    /**
     * @brief   SPI Default CRC Checksums for specific commands
     */
    namespace crc {
        constexpr uint8_t DEFAULT = 0x01;
        constexpr uint8_t GENPOL  = 0x85; //generator polynom for CRC checksums
        constexpr uint8_t CMD0    = 0x95;
        constexpr uint8_t CMD8    = 0x87;
        constexpr uint8_t CMD58   = 0xFD;
    } // namespace crc

    constexpr size_t SIZE = 7;

} // namespace cmd


/**
 * @brief   SPI specifications for the SD Card.
 *          Must be a value between 100kHz and 20MHz
 *          For the STM32L4 the Value needs to be 80MHz divided by a power of two
 */
namespace baudrate {
    constexpr uint32_t INIT    = 80'000'000 / 256; //156kHz
    constexpr uint32_t DATA    = 80'000'000 / 8;   //10MHz
    constexpr uint32_t FASTEST = 80'000'000 / 4;   //20MHz
} // namespace baudrate


/**
 * @brief SD Card Data Configuration
 */
namespace data {

    /**
     * @brief SPI data tokens
     */
    namespace token {
        constexpr uint8_t START_BLOCK       = 0xFE; //Data Token for CMD17/18/24
        constexpr uint8_t START_BLOCK_MULTI = 0xFC; //Data Token for CMD25
        constexpr uint8_t STOP_TRANSACTION  = 0xFD; //Stop Transaction Token for CMD 25

    } // namespace token


    constexpr size_t  BLOCKSIZE  = 0x200;
    constexpr size_t  BUFFERSIZE = BLOCKSIZE + 64; //size for the buffer used in all data operations here
    constexpr size_t  PACKETSIZE = BLOCKSIZE + 3;  //actual size used for the packed (must fit in buffer)
    constexpr size_t  CSDSIZE    = 16;             //16Byte Data blocks for CSD AND CID
    constexpr size_t  CIDSIZE    = CSDSIZE;
    constexpr uint8_t CSDMASK    = 0xFF;
    constexpr size_t  HEADERSIZE = cmd::header::SIZE;
} // namespace data

namespace response {

    /**
     * @brief   SD Response header masks and configuration
     */
    namespace header {
        constexpr uint8_t SUCCESSFUL           = 0x00;
        constexpr uint8_t IDLE                 = 0x01;
        constexpr uint8_t ERASE_RESET          = 0x02;
        constexpr uint8_t ILLEGAL_COMMAND      = 0x04;
        constexpr uint8_t CRC_ERROR            = 0x08;
        constexpr uint8_t ERASE_SEQUENCE_ERROR = 0x10;
        constexpr uint8_t ADDRESS_ERROR        = 0x20;
        constexpr uint8_t PARAMETER_ERROR      = 0x40;

        constexpr size_t SIZE = 1;
    } // namespace header

    // SD Card Response Types lengths
    constexpr size_t RESPONSE1_LENGTH = header::SIZE;
    constexpr size_t RESPONSE2_LENGTH = header::SIZE + data::CSDSIZE;
    constexpr size_t RESPONSE3_LENGTH = header::SIZE + 4;
    constexpr size_t RESPONSE7_LENGTH = RESPONSE3_LENGTH;
    constexpr size_t MAXIMUM_SIZE     = RESPONSE2_LENGTH; //size for the buffe;

    constexpr uint32_t VERSIONMASK  = 0xf0000000;
    constexpr uint8_t  VERSIONSHIFT = 28;
} // namespace response


//Config for SDCard
namespace control {
    constexpr int64_t TRIGGER_TIMEOUT       = 200 * RODOS::MILLISECONDS;
    constexpr int64_t READ_DELAY            = 4 * RODOS::MILLISECONDS;
    constexpr int64_t INIT_DELAY            = 2 * RODOS::MILLISECONDS;
    constexpr int64_t ACMD41_TIMEOUT        = 1000 * RODOS::MILLISECONDS;
    constexpr int32_t IDLE_HIGH             = 1;
    constexpr uint8_t SPI_DUMMY             = 0xFF;
    constexpr uint8_t INIT_DUMMY_ITERATIONS = static_cast<uint8_t>((74 / 8) + 1);
    constexpr uint8_t READ_DATABLOCK_TRIES  = 2;
} // namespace control


namespace addressingmode {
    constexpr uint8_t NOT_SET = 0;
    constexpr uint8_t BYTE    = 1;
    constexpr uint8_t BLOCK   = 2;
} // namespace addressingmode


/**
 * @brief   SD Card SPI connection status
 */
namespace status {
    constexpr int32_t NO_ERROR                  = 0;
    constexpr int32_t STORAGE_ERROR             = -1;
    constexpr int32_t SPI_ERROR_OR_NO_RESPONSE  = -2;
    constexpr int32_t SPI_MISMATCH_0X1AA        = -3;
    constexpr int32_t SPI_FORMAT_ERROR          = -4;
    constexpr int32_t SPI_LINE_HIGH             = -5;
    constexpr int32_t SPI_UNKNOWN_CARD          = -6;
    constexpr int32_t SPI_ERROR_CMD55_R1        = -7;
    constexpr int32_t SPI_ERROR_CMD41_R1        = -8;
    constexpr int32_t SPI_ERROR_SET_BLOCKLENGTH = -9;
    constexpr int32_t SPI_ERROR_WRITE_READ      = -10;
    constexpr int32_t SD_NO_INIT                = -11;
    constexpr int32_t SD_ADDRESS_ERROR          = -12;
    constexpr int32_t SD_ILLEGAL_COMMAND        = -13;
} // namespace status


/**
 * @brief   SDCard Erase Types
 */
namespace erasetype {
    constexpr uint8_t ERASE   = 0;
    constexpr uint8_t DISCARD = 1;
    constexpr uint8_t FULE    = 2;
} // namespace erasetype


namespace response {
    struct Response {
        explicit Response(size_t datalength)
          : dataLength{ datalength } {}

        [[nodiscard]] uint8_t getHeader() const {
            return data[0];
        }

        [[nodiscard]] int32_t getError() const {
            uint8_t header{ getHeader() };

            if(header == header::SUCCESSFUL) {
                return status::NO_ERROR;
            }

            if((header & header::IDLE) != 0) {
                return status::SD_NO_INIT;
            }

            if((header & header::ILLEGAL_COMMAND) != 0) {
                return status::SD_ILLEGAL_COMMAND;
            }

            if((header & header::ADDRESS_ERROR) != 0) {
                return status::SD_ADDRESS_ERROR;
            }

            return status::STORAGE_ERROR;
        }

        void getR2Response(uint8_t* dest) const {
            RODOS::memcpy(dest, payload, dataLength);
        }

        [[nodiscard]] uint32_t getR3Response() const {
            uint32_t responseValue{ 0 };
            for(size_t i{ 0 }; i < sizeof(responseValue); i++) {
                responseValue <<= 8;
                responseValue += payload[i]; // NOLINT
            }
            return responseValue;
        }

        [[nodiscard]] uint32_t getR7Response() const {
            return getR3Response();
        }

        void setResponse(uint8_t* src) {
            RODOS::memcpy(data, src, dataLength);
        }

        [[nodiscard]] size_t getLength() const {
            return dataLength;
        }

        size_t   dataLength{ RESPONSE1_LENGTH };
        uint8_t  data[MAXIMUM_SIZE]{};
        uint8_t* payload = data + header::SIZE; // NOLINT
    };
} // namespace response

template <size_t SIZE>
struct SPIBuffer {
    uint8_t data[SIZE];
};

using SDDataBuffer = SPIBuffer<data::BUFFERSIZE>;
using SDCmdBuffer  = SPIBuffer<cmd::SIZE>;
using SDRespBuffer = SPIBuffer<response::MAXIMUM_SIZE>;

struct SDCSDRegister {

    static constexpr size_t getSize() {
        return data::CSDSIZE;
    }

    uint8_t data[data::CSDSIZE];
};


class SDCardHardware {

  public:
    SDCardHardware();

    /**
     *  @brief  prepare the sdcard for usage    
     *          following the power sequence as described in:
     *          http://elm-chan.org/docs/mmc/mmc_e.html#spiinit
     *          http://elm-chan.org/docs/mmc/i/sdinit.png 
     *  @return state of the execution, 0 ^= no errors
     */
    int32_t sdPowerSequence();

    /**
     *  @brief  write a byte array to the data buffer, to be
     *          sent to the sdcard in the next step
     *  @param  src     source array
     *  @param  len     length of the data to be written, maxumum the
     *                  blocksize of the sdcard
     *  @retval 0 upon success
     */
    int32_t writeDataBuffer(const uint8_t* src, size_t length);

    /**
     *  @brief  write the array from the data buffer of the sdcard
     *          interface, to a defined location. 
     *          Used when eg. just demanded some data from the sdcard
     *          This also strips all headers from the received data
     *  @param  dest    the array to write the data to
     *  @param  length  the maximum size of the destination buffer
     *  @retval 0 upon success  
     */
    int32_t readDataBuffer(uint8_t* dest, size_t length);

    /**
     *  @brief raise the spi frequency to data transmission speeds after initialization
     */
    int32_t setSpiToDataFrequency();


    //CMDS 0-58:

    /**
     *  @brief  send a CMD0 from the SDCard Protocol to the card
     *  @param  res: which address to store the response to
     *  @return error values, 0^= successful execution
     */
    int32_t goIdleState(response::Response* res);

    /**
     *  @brief  send a CMD8 from the SDCard Protocol to the card
     *  @param  res: which address to store the response to
     *  @return error values, 0^= successful execution
     */
    int32_t sendIfCond(response::Response* res);

    /**
     *  @brief  build a CMD9 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t sendCsd(response::Response* res, SDCSDRegister* csd);

    /**
     *  @brief  build a CMD16 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @param  blockLength desired Blocklength
     *  @return error values
     */
    int32_t setBlockLen(response::Response* res, uint32_t blockLength);

    /**
     *  @brief  build a CM17 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t readBlock(response::Response* res, uint32_t block);

    /**
     *  @brief  build a CMD24 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t writeBlock(response::Response* res, uint32_t block);

    /**
     *  @brief  build a CMD32 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t eraseWrBlkStart(response::Response* res, uint32_t startBlk);

    /**
     *  @brief  build a CMD33 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t eraseWrBlkEnd(response::Response* res, uint32_t endBlk);

    /**
     *  @brief  build a CMD38 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t erase(response::Response* res, uint32_t eraseFunction = 0);

    /**
     *  @brief  build an ACMD41 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t acmd41(response::Response* res, uint32_t arg);

    /**
     *  @brief  build a CMD41 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t appSendOpCond(response::Response* res, uint32_t arg);

    /**
     *  @brief  build a CMD55 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t appCmd(response::Response* res);

    /**
     *  @brief  build a CMD58 from the SDCard Protocol and send it to the card
     *  @param  res the response value
     *  @return error values
     */
    int32_t readOcr(response::Response* res);

    void reset();

    uint8_t getAddressingMode();

    void setAddressingMode(uint8_t mode);

  private:
    RODOS::HAL_SPI m_sdSpi;
    uint8_t        m_addressingMode;

    /**
     *  @brief  send a given command. Important: this doesn't support QuadSPI/multiple SPI-slaves connected to one SPI port. Only one Slave Select (SS) GPIO is implemented (for now).
     *  @param  cmdID: the command to be send, the length of this array may at most be 6
     *  @param  res: which address to store the response to
     *  @return state about initialization
     */
    int32_t cmdSend(uint8_t cmdID, uint32_t arg, uint8_t crc, response::Response* res);

    /**
     *  @brief  send the array, stored in m_writeBuf to the SDCard.
     *          the data starts at Byte 1, with Byte 0 reserved for the Protocol token,
     *  @param  token   the operation specific token, as defined in the SDCard protocol
     *  @return state of operation
     */
    int32_t dataSend(uint8_t token);

    /**
     *  @brief  wait for data to be received from the SDCard and store them to m_dataBuf.data
     *  @return state of operation
     */
    int32_t dataRead(size_t expectedLength = data::PACKETSIZE);

    /**
     *  @brief  Setup the command buffer, to send a command in the next step
     *  @param  commandID  identification number of the command, see SDDocumentation
     *  @param  arg     command parameters / argument
     *  @param  crc     cyclic redundancy check
     */
    int32_t initCmdBuffer(uint8_t commandID, uint32_t arg, uint8_t crc);

    /**
     *  @brief As part of `initCmdBuffer` prepare the checksum part of the commanding array
     *  @param crc checksum value to be set to
     */
    void setCmdBufCRC(uint8_t crc);

    /**
     *  @brief As part of `initCmdBuffer` prepare the argument part of the commanding array
     *  @param arg argument value to be set to
     */
    void setCmdBufArgument(uint32_t arg);

    /**
     *  @brief As part of `initCmdBuffer` prepare the commanding part of the commanding array
     *  @param cmd command value to be set to
     */
    void setCmdBufCommand(uint8_t cmdID);

    SDDataBuffer m_dataBuf{};
    SDCmdBuffer  m_cmdBuf{};
    SDRespBuffer m_respBuf{};
};

} // namespace rodos::support::sdcard
