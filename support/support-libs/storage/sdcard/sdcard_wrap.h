#pragma once

#include "sdcard_hw.h"

namespace rodos::support::sdcard {

constexpr uint32_t SDVERSION_MASK_CCS{ 0x40000000 };
constexpr uint8_t  CMD0_RESPONSE_KNOWNCARD_HEADER{ 0x01 };
constexpr uint8_t  CMD8_RESPONSE_SDC1_MMC3_HEADER{ 0x05 };
constexpr uint32_t CMD8_RESPONSE_KNOWNCARD{ 0x1AA };
constexpr uint32_t CMD8_RESPONSE_MASK{ 0xFFF };
constexpr uint32_t ACMD41_ARG_INIT{ 0x40000000 };
constexpr uint32_t CMD58_RESPONSE_MASK_VOLTAGE{ 0xF };
constexpr uint8_t  OCR_ISIDLE_MASK{ 0x1 };

class SDCardWrapper {
  public:
    SDCardWrapper(SDCardHardware& hw);

    int32_t sdInitSpi();
    int32_t sdGetStatus();

    /**
     *  @brief  reads a block from the sdcard 
     *  @param  dest    where to store the contents of this block (has to have the length of the transmitted blocksize)
     *  @param  block   the position from where to read
     */
    int32_t sdReadBlock(uint8_t* dest, uint32_t block);

    /**
     *  @brief  Write a block to the SDCard
     *  @param  block:  the number of the block, where the data is to be written to
     *  @param  data:   the data to be written. The array has to have the same length as the currently set blocksize. To check this, use the get_blocksize() function. This condition will not be checked.
     */
    int32_t sdWriteBlock(uint32_t block, const uint8_t* data, size_t length = data::BLOCKSIZE);
    int64_t sdGetBlocksize();
    int32_t sdGetSectorsize();
    int32_t sdGetSectorcount();

    /**
     *  @param  discardMode whether discard or fuel or just erase shall be used
     *  @retval status about success of operation, 0 on success
     */
    int32_t sdEraseBlocks(uint32_t startBlk, uint32_t endBlk, uint8_t discardMode = erasetype::ERASE);

  private:
    SDCardHardware& m_sdhardware;
};


SDCardWrapper& getWrap();

} // namespace rodos::support::sdcard
