#include "sdcard_wrap.h"

namespace rodos::support::sdcard {

SDCardWrapper::SDCardWrapper(SDCardHardware& hw)
  : m_sdhardware{ hw } {
}

/**
 *  @brief  initializes the spi mode on a freshly started sdcard
 *          hereby it follows this flow http://elm-chan.org/docs/mmc/i/sdinit.png
 *          Not all types of sdcards supported yet
 */
int32_t SDCardWrapper::sdInitSpi() {
    m_sdhardware.sdPowerSequence();
    response::Response response{ response::RESPONSE1_LENGTH };

    //CMD0
    int32_t cmd0response{ m_sdhardware.goIdleState(&response) };
    if(cmd0response) {
        return status::SPI_UNKNOWN_CARD;
    }
    if(response.getHeader() != CMD0_RESPONSE_KNOWNCARD_HEADER) {
        return status::SPI_UNKNOWN_CARD;
    }
    //now in idle state

    //CMD8
    int32_t cmd8response{ m_sdhardware.sendIfCond(&response) };
    if(cmd8response || (response.getHeader() == CMD8_RESPONSE_SDC1_MMC3_HEADER)) {
        RODOS::RODOS_ERROR("[SDCardWrapper::sdInitSpi()] SDCv1 and MMCv3 not supported yet");
        return status::SPI_UNKNOWN_CARD;
    }
    if((response.getR7Response() & CMD8_RESPONSE_MASK) != CMD8_RESPONSE_KNOWNCARD) {
        return status::SPI_UNKNOWN_CARD;
    }

    //ACMD41
    int32_t status_acmd41{ m_sdhardware.acmd41(&response, ACMD41_ARG_INIT) };
    if(status_acmd41) {
        return status::SPI_UNKNOWN_CARD;
    }

    //CMD 58: check voltage range
    //if not matching, reject card
    int32_t status_cmd58{ m_sdhardware.readOcr(&response) };
    if(status_cmd58) {
        return status::SPI_UNKNOWN_CARD;
    }

    // is CCS bit set
    if((response.getR3Response() & SDVERSION_MASK_CCS) == 0) {
        m_sdhardware.setAddressingMode(addressingmode::BYTE);

        int32_t status_cmd16{ m_sdhardware.setBlockLen(&response, data::BLOCKSIZE) };
        if(status_cmd16 < 0) {
            return status::SPI_UNKNOWN_CARD;
        }
        if(response.getHeader()) {
            return status::SPI_ERROR_OR_NO_RESPONSE;
        }
    }else{
        m_sdhardware.setAddressingMode(addressingmode::BLOCK);
    }

    RODOS::PRINTF("SDCard: SPI-Initialization successful\n");
    RODOS::PRINTF("- Card Block Addressing: %s\n", (m_sdhardware.getAddressingMode() == addressingmode::BLOCK) ? "Block" : "notBlock");

    m_sdhardware.setSpiToDataFrequency(); //raise clock to allow for fast data transfer

    return status::NO_ERROR;
}

int32_t SDCardWrapper::sdGetStatus() {

    response::Response response{ response::RESPONSE3_LENGTH };
    int        status{ m_sdhardware.readOcr(&response) };

    if(status) {
        RODOS::RODOS_ERROR("[SDCardWrapper::sdGetStatus] error read OCR");
        return status::STORAGE_ERROR;
    }

    /* Evaluate the response from readOCR */
    if(response.getHeader() == control::SPI_DUMMY) {
        return status::SPI_LINE_HIGH;
    }
    if(response.getHeader() & OCR_ISIDLE_MASK) {
        return status::SD_NO_INIT;
    }

    return status::NO_ERROR;
}


int32_t SDCardWrapper::sdReadBlock(uint8_t* dest, const uint32_t block) {
    if(!dest) {
        RODOS::RODOS_ERROR("[SDCardWrapper::sdReadBlock] No destination parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response::Response response{ response::RESPONSE1_LENGTH };
    int32_t    status{ m_sdhardware.readBlock(&response, block) };

    if(status) {
        return status;
    }

    status = m_sdhardware.readDataBuffer(dest, data::BLOCKSIZE);

    return status;
}


int32_t SDCardWrapper::sdWriteBlock(uint32_t block, const uint8_t* data, size_t length) {
    if(!data) {
        RODOS::RODOS_ERROR("[SDCardWrapper::sdWriteBlock] No data parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response::Response response{ response::RESPONSE1_LENGTH };
    m_sdhardware.writeDataBuffer(data, length);

    int32_t status{ m_sdhardware.writeBlock(&response, block) };

    return status;
}


int64_t SDCardWrapper::sdGetBlocksize() {
    //IN SDcard Documentation called "SECTOR_SIZE";
    //Retrieved from the CSD Register bits [45:39]
    RODOS::RODOS_ERROR("[SDCardWrapper::sdGetBlocksize] Function not implemented yet");
    return status::STORAGE_ERROR;
}

int32_t SDCardWrapper::sdGetSectorcount() {

    RODOS::RODOS_ERROR("[SDCardWrapper::sdGetSectorcount] Function not implemented yet");
    return status::STORAGE_ERROR;
}

int32_t SDCardWrapper::sdGetSectorsize() {
    RODOS::RODOS_ERROR("[SDCardWrapper::sdGetSectorsize] Function not implemented yet");
    return status::STORAGE_ERROR;
}

int32_t SDCardWrapper::sdEraseBlocks(uint32_t startBlk, uint32_t endBlk, uint8_t discardMode) {
    response::Response response{ response::RESPONSE1_LENGTH };
    //read status register for fule or
    int32_t status{ m_sdhardware.eraseWrBlkStart(&response, startBlk) };
    status |= m_sdhardware.eraseWrBlkEnd(&response, endBlk);
    status |= m_sdhardware.erase(&response, discardMode);

    return status;
}

SDCardWrapper& getWrap() {
    static SDCardHardware s_hardware{};
    static SDCardWrapper  s_wrapper{ s_hardware };
    return s_wrapper;
}

} // namespace rodos::support::sdcard
