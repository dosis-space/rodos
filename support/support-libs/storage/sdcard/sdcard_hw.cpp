#include "sdcard_hw.h"

namespace rodos::support::sdcard {

SDCardHardware::SDCardHardware()
  : m_sdSpi{ SDIO_IDX, SDIO_SCLK, SDIO_MISO, SDIO_MOSI, SDIO_CS },
    m_addressingMode{ addressingmode::NOT_SET } {
}

int32_t SDCardHardware::sdPowerSequence() {
    int64_t wait{ 0 };
    int32_t status{ status::NO_ERROR };

    wait = RODOS::NOW() + control::INIT_DELAY;
    while(RODOS::NOW() < wait) { /* Do nothing */
    }

    m_sdSpi.init(baudrate::INIT);
    m_sdSpi.config(RODOS::SPI_PARAMETER_MOSI_IDL_HIGH, control::IDLE_HIGH);

    //send at least 74 clock pulses to SCLK while DATA_IN / CMD is high
    for(int i = 0; i < control::INIT_DUMMY_ITERATIONS; i++) {
        status |= m_sdSpi.write(&control::SPI_DUMMY, 1);
    }

    return (status >= 0) ? status::NO_ERROR : status::STORAGE_ERROR;
}

int32_t SDCardHardware::writeDataBuffer(const uint8_t* src, size_t length) {
    if(!src) {
        RODOS::RODOS_ERROR("[SDCardHardware::writeDataBuffer] No source array provided.");
        return status::STORAGE_ERROR;
    }

    if(length == 0) {
        RODOS::RODOS_ERROR("[SDCardHardware::writeDataBuffer] Source array length is too small.");
        return status::STORAGE_ERROR;
    }

    size_t lengthCpy{ length > data::BLOCKSIZE ? data::BLOCKSIZE : length };
    m_dataBuf = {};

    RODOS::memcpy(m_dataBuf.data + data::HEADERSIZE, src, lengthCpy);

    return status::NO_ERROR;
}

int32_t SDCardHardware::initCmdBuffer(const uint8_t cmdID, const uint32_t arg, uint8_t crc) {

    if(cmdID >= cmd::header::INVALID) {
        RODOS::RODOS_ERROR("[SDCardHardware::writeCmdBuffer] Invalid SD card command");
        return status::SPI_FORMAT_ERROR;
    }

    setCmdBufCommand(cmdID);
    setCmdBufArgument(arg);
    setCmdBufCRC(crc);

    return status::NO_ERROR;
}

void SDCardHardware::setCmdBufCommand(uint8_t cmdID) {
    m_cmdBuf.data[0] = (cmdID | cmd::header::DEFAULT);
}

void SDCardHardware::setCmdBufArgument(uint32_t arg) {
    uint32_t arg_copy{ arg };
    for(int i = cmd::arg::LSB_POSITION; i > 0; i--) {
        m_cmdBuf.data[i] = static_cast<uint8_t>(arg_copy & data::CSDMASK);
        arg_copy         = arg_copy >> 8;
    }
}

void SDCardHardware::setCmdBufCRC(uint8_t crc) {
    m_cmdBuf.data[5] = (crc | cmd::crc::DEFAULT);
    m_cmdBuf.data[6] = control::SPI_DUMMY;
}

int32_t SDCardHardware::readDataBuffer(uint8_t* dest, size_t length) {

    if(!dest || length == 0) {
        RODOS::RODOS_ERROR("[SDCardHardware::readDataBuffer] No destination provided or size too small.");
        return status::STORAGE_ERROR;
    }

    size_t lenCpy{ length > data::BLOCKSIZE ? data::BLOCKSIZE : length };

    RODOS::memcpy(dest, m_dataBuf.data + data::HEADERSIZE, lenCpy);

    return status::NO_ERROR;
}


int32_t SDCardHardware::setSpiToDataFrequency() {
    return m_sdSpi.config(RODOS::SPI_PARAMETER_BAUDRATE, baudrate::DATA);
}


int32_t SDCardHardware::cmdSend(const uint8_t cmdID, const uint32_t arg, uint8_t crc, response::Response* response) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::cmdSend] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    int32_t spi_status{ 0 };

    spi_status = initCmdBuffer(cmdID, arg, crc);
    if(spi_status) {
        return spi_status;
    }

    spi_status = m_sdSpi.writeTrig(m_cmdBuf.data, cmd::SIZE, control::SPI_DUMMY, control::TRIGGER_TIMEOUT, false);
    if(spi_status < 0) {
        RODOS::RODOS_ERROR("[SDCardHardware::cmdSend] Could not send command");
        RODOS::PRINTF("    %d\n", static_cast<int>(spi_status));
        return status::SPI_ERROR_OR_NO_RESPONSE;
    }

    spi_status = m_sdSpi.read(response->data, response->getLength());

    if(spi_status < 0) {
        RODOS::RODOS_ERROR("[SDCardHardware::cmdSend] Could not read command response");
        return status::SPI_ERROR_OR_NO_RESPONSE;
    }

    return status::NO_ERROR;
}

int32_t SDCardHardware::dataSend(const uint8_t dataToken) {
    m_dataBuf.data[0]                   = dataToken;
    m_dataBuf.data[data::BLOCKSIZE + 2] = cmd::crc::DEFAULT;
    m_dataBuf.data[data::BLOCKSIZE + 1] = cmd::crc::DEFAULT;

    int32_t status{ m_sdSpi.writeTrig(m_dataBuf.data, data::PACKETSIZE, control::SPI_DUMMY, control::TRIGGER_TIMEOUT) };

    if(status < 0) {
        return status::SPI_ERROR_WRITE_READ;
    }

    return status::NO_ERROR;
}


int32_t SDCardHardware::dataRead(size_t expectedLength) {
    m_dataBuf = {};

    int32_t status{ m_sdSpi.readTrig(m_dataBuf.data, expectedLength, data::token::START_BLOCK, control::READ_DELAY, false) };

    if(status < 0) {
        return status::SPI_ERROR_WRITE_READ;
    }

    return status::NO_ERROR;
}


uint8_t SDCardHardware::getAddressingMode() {
    return m_addressingMode;
}

void SDCardHardware::setAddressingMode(uint8_t mode) {
    m_addressingMode = mode;
}

/***************************************************/
/* SD Card Protocol Commands                       */
/***************************************************/


int32_t SDCardHardware::goIdleState(response::Response* response) {
    return cmdSend(cmd::header::CMD0, cmd::arg::CMD0, cmd::crc::CMD0, response);
}


int32_t SDCardHardware::sendIfCond(response::Response* response) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::sendIfCond] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE7_LENGTH;
    return cmdSend(cmd::header::CMD8, cmd::arg::CMD8, cmd::crc::CMD8, response);
}


int32_t SDCardHardware::sendCsd(response::Response* response, SDCSDRegister* csd) {
    if(!response || !csd) {
        RODOS::RODOS_ERROR("[SDCardHardware::sendCsd] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE2_LENGTH;
    int32_t status{ cmdSend(cmd::header::CMD9, cmd::arg::CMD9, cmd::crc::DEFAULT, response) };

    if(status) {
        return status::SPI_ERROR_WRITE_READ;
    }

    response->getR2Response(csd->data);

    return status::NO_ERROR;
}


[[nodiscard]] int32_t SDCardHardware::setBlockLen(response::Response* response, uint32_t blockLength) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::setBlockLen] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    int32_t status{ cmdSend(cmd::header::CMD16, blockLength, cmd::crc::DEFAULT, response) };

    if(status) {
        return status::SPI_ERROR_SET_BLOCKLENGTH;
    }

    return status;
}


[[nodiscard]] int32_t SDCardHardware::readBlock(response::Response* response, uint32_t block) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::readBlock] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    if(getAddressingMode() == addressingmode::BYTE) {
        block = block << 9;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    int32_t status{ cmdSend(cmd::header::CMD17, block, cmd::crc::DEFAULT, response) };

    if(status) {
        return status;
    }

    status = response->getError();
    if(status) {
        return status;
    }

    int64_t wait{ RODOS::NOW() + control::READ_DELAY };
    while(RODOS::NOW() < wait) { /*DO NOTHING*/
    }

    status = dataRead();
    if(status < 0) {
        return status;
    }

    if(m_dataBuf.data[0] != data::token::START_BLOCK) {
        return status::SPI_FORMAT_ERROR;
    }

    return status;
}


int32_t SDCardHardware::writeBlock(response::Response* response, uint32_t block) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::writeBlock] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    if(getAddressingMode() == addressingmode::BYTE) {
        block = block << 9;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    int32_t status{ cmdSend(cmd::header::CMD24, block, cmd::crc::DEFAULT, response) };

    if(status) {
        return status;
    }

    status = dataSend(data::token::START_BLOCK);
    return status;
}


int32_t SDCardHardware::eraseWrBlkStart(response::Response* response, const uint32_t startBlk) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::eraseWrBlkStart] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    return cmdSend(cmd::header::CMD32, startBlk, cmd::crc::DEFAULT, response);
}


int32_t SDCardHardware::eraseWrBlkEnd(response::Response* response, const uint32_t endBlk) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::eraseWrBlkEnd] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    return cmdSend(cmd::header::CMD33, endBlk, cmd::crc::DEFAULT, response);
}


int32_t SDCardHardware::erase(response::Response* response, const uint32_t eraseFunction) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::erase] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    return cmdSend(cmd::header::CMD38, eraseFunction, cmd::crc::DEFAULT, response);
}


int32_t SDCardHardware::appSendOpCond(response::Response* response, uint32_t arg) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::appSendOpCond] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    return cmdSend(cmd::header::CMD41, arg, cmd::crc::DEFAULT, response);
}


int32_t SDCardHardware::acmd41(response::Response* response, uint32_t arg) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::acmd41] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    int64_t end_loop = RODOS::NOW() + (control::ACMD41_TIMEOUT);
    do {
        int32_t status_cmd55{ appCmd(response) };
        if(status_cmd55) {
            return status_cmd55;
        }
        if(response->getHeader() != response::header::IDLE) {
            return status::SPI_ERROR_CMD55_R1;
        }

        int32_t status_cmd41{ appSendOpCond(response, arg) };
        if(status_cmd41) {
            return status_cmd41; //error, break, UNKNOWN CARD
        }
        if(response->getHeader() == response::header::SUCCESSFUL) {
            return status::NO_ERROR; //correct, break, SDCard V2+
        }
        if(response->getHeader() == response::header::IDLE) {
            continue; //correct, continue
        }
        return status::SPI_ERROR_OR_NO_RESPONSE;
    } while(RODOS::NOW() < end_loop);

    RODOS::RODOS_ERROR("[SDCardHardware::acmd41] Could not send command");
    return status::STORAGE_ERROR;
}

int32_t SDCardHardware::appCmd(response::Response* response) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::appCmd] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE1_LENGTH;
    return cmdSend(cmd::header::CMD55, cmd::arg::CMD55, cmd::crc::DEFAULT, response);
}


int32_t SDCardHardware::readOcr(response::Response* response) {
    if(!response) {
        RODOS::RODOS_ERROR("[SDCardHardware::readOcr] no response parameter provided");
        return status::SPI_FORMAT_ERROR;
    }

    response->dataLength = response::RESPONSE3_LENGTH;
    return cmdSend(cmd::header::CMD58, cmd::arg::CMD58, cmd::crc::CMD58, response);
}


void SDCardHardware::reset() {
    m_sdSpi.reset();
}

} // namespace rodos::support::sdcard
