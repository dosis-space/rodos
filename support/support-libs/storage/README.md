### SDCard Driver

#### Components
![](sdcard_layout.png)

* **sdCardWrapper** Implements semantic functionality for initializing the SDCard and reading/writing blocks of Data.
* **sdCardHardware** Implements syntactic functionality for sending Commands and Data and reading responses from the sdcard

#### Folder Structure

|File|Purpose|
| --- | --- |
|**storage/**|Contains all files regarding all storage devices underneath the used filesystems. The FAT Filesystem was added as an external repository in the submodules.|
|**storage/sdcard/**|Drivers for the SDCard|
|README.md and .png files | Documentation around the SDCard and Fat Filesystem |
|storage_diskio.cpp|Interface between the FAT Filesystem and the SDCard functions. This will contain the interface to more devices, that are added in the future.|


#### Pinout

SDCard:
![](sdcard_pinout.png)

Nucleo STM32L496ZG:
![](nucleol4_pinout.jpeg)


For all SPI connections, there exist multiple possible pins for each line (MOSI, MISO, SCLK, CS). Connect each line to one of the respective pins. Possible Pins on the STM32L4 for SPI 1 and SPI 3 connections:

SPI 1
|Line|Possible Pins|
| --- | --- |
| MOSI | PA7, PA12, PB5, PE15, PG4|
|MISO|PA6, PA11, PB4, PE14, PG3|
|SCLK|PA1, PA5, PB3, PE13, PG2|
|CS|PA4, PA15, PB0, PE12, PG5|

In software set the SPI_IDX value to SPI_IDX0.

SPI3:
|Line|Possible Pins|
| --- | --- |
|MOSI|PB5\*, PC12, PG11\*\*|
|MISO|PB4\*, PC11, PG10\*\*|
|SCLK|PB3\*, PC10, PG9\*\*|
|CS|PA4\*, PA15, PG12\*\*|

\*Used for Nucleo boards

\*\*Used for SSBv2

In software set the SPI_IDX value to SPI_IDX2.


For the operating system define the PINS for the SPI connection in the file [/support/support-libs/storage/sdcard/sdcard_hw.h](https://gitlab.lrz.de/move/software/rodos/-/blob/feature/filesystem/support/support-libs/storage/sdcard/sdcard_hw.h). 

#### Setup of the tutorials

1. Connect the SDcard as shown in the Pinout diagram. Connect all lines from the SDCard with one of the respective PINS on the STM32L4. Don't forget the Pull-Up resistor
2. Set the configuration for the SDCard Connections accordingly. *Currently set to SSBv2 pins, must be changed for nucleo testing*
3. Build all files for the STM32L4 as described in [Doxygen](https://move.pages.gitlab.lrz.de/software/firmware/index.html). For meson use:
```bash
meson build-nucleol4 -Dbuild_all_tutorials=true --cross-file meson/stm32l4/cross.ini
```
4. There are two tutorials in __tutorials/80-target-specific/stm32l4/__, which can be used to test the connection to the sdcard. Flash those to the nucleo:
    * **sdcard_test.cpp**: test writing to specific blocks on the sdcard
    * **fatfs_test.cpp**: test writing to files with the FAT Filesystem

