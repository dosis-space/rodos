#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/sys-hash-checker.h"
#include "migration/implementation/commander/migration-commander-helper.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"
#include "migration/implementation/info/migration-info-subscriber.h"

#include "listelement.h"
#include "initiator.h"
#include "rodos-debug.h"
#include "synccommbuffer.h"
#include "timemodel.h"
#include "topic.h"

namespace RODOS::migration {

/**
 * @brief The MigrationCommander class used to command migrations and request MigrationInfo.
 *
 * @tparam MIG_TOPIC_IDS The ids for all RODOS::Topics used in the internal migration machinery.
 * Topic ids should always be >= 2000.
 *
 * @note Can only be instantiated once per sys_hash, this is checked in the Initiator.
 * @note Can be validly used without an instantiated MigrationSystem.
 */
template <meta::MigrationTopicIds MIG_TOPIC_IDS>
class MigrationCommander
  : public implementation::SysHashChecker,
    public Initiator {
  public:
    /**
     * @brief constructor for MigrationCommander
     *
     * @param[in] sys_name The name of the migration system used. Only used for printing it out in
     * the Initiator. Can also be accessed via getSysName().
     *
     * @param[in] sys_hash The identifier of the migration system this node is part of. Only one
     * MigrationCommander/System object with the same sys_hash can be instantiated on a node.
     * Threads can only be migrated within the same migration system (identified by the same
     * sys_hash). The user has to ensure that this sys_hash is correct between nodes and unique
     * between migration systems.
     *
     * @param[in] instance_id The identifier of the node within the migration system
     * (which is itself identified via the sys_hash). The user has to ensure that this instance_id
     * is unique between other nodes of the same migration system. Different migration systems may
     * use the same instance_ids.
     */
    MigrationCommander(const char* sys_name,
                       uint16_t    sys_hash,
                       uint16_t    instance_id)
      : SysHashChecker(sys_hash),
        m_sys_name{ sys_name },
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_command_ack_sub{
            m_sys_hash,
            m_instance_id,
            m_mig_ack_topic,
            AckType::COMMAND_ACK,
            m_mig_error_forwarder
        },
        m_mig_info_sub{
            m_sys_hash,
            m_instance_id,
            m_mig_info_topic,
            m_mig_commander_notifier
        },
        m_mig_command_helper{
            m_sys_hash,
            m_instance_id,
            m_mig_cmd_topic,
            m_mig_info_request_topic,
            m_mig_error_forwarder,
            m_mig_command_ack_sub,
            m_mig_commander_notifier,
            m_mig_info_sub
        } {}

    virtual ~MigrationCommander() = default;

    void init() final {
        if(!checkSysHashUniqueInList()) {
            RODOS_ERROR("[MigrationCommander::init] sys_hash isn't unique on node!\n"
                        "-> this will lead to undefined behavior!");
        }
        PRINTF("\n"
               "\t[MigrationCommander::init]\n"
               "\t-> MigrationSystem name: '%s'\n"
               "\t-> MigrationSystem hash: 0x%x\n"
               "\t-> local instance id:    0x%x\n\n",
               m_sys_name,
               (unsigned)m_sys_hash,
               (unsigned)m_instance_id);
    }

    /**
     * @brief Request the migration of a MigratableThread from one node to another.
     *
     * @param[in] sys_hash The sys_hash in which the migration will take place (to which the
     * migrate_from/to_ids belong). Does not have to be the same as the requester's sys_hash.
     *
     * @param[in] migrate_from_id The node from which to migrate (which holds the thread that shall
     * be migrated before the request). Does not have to be the same as the requester's instance_id.
     *
     * @param[in] migrate_to_id The node to which to migrate.
     *
     * @param[in] ticket The ticket identifier of the MigratableThread that shall be migrated.
     *
     * @note Call waitForMigrationResponse() afterwards to receive a response indicating whether
     * the migration has succeeded or not.
     * @warning It is guaranteed that you only receive a migration response for the newest request.
     * If you request a new migration, you won't be able to receive a response for the
     * previous request anymore.
     */
    void requestMigration(uint16_t sys_hash,
                          uint16_t migrate_from_id,
                          uint16_t migrate_to_id,
                          uint32_t ticket) {
        m_mig_command_helper.requestMigration(sys_hash, migrate_from_id, migrate_to_id, ticket);
    }

    /**
     * @brief Wait to receive a migration response to the last migration request.
     *
     * @param[out] mig_error_out Output parameter, filled when the response has been received
     * (return value 'true'). MigrationError::OK indicates that the migration has succeeded,
     * something else that is has failed.
     *
     * @param[in] timeout How long to block (wait) before returning with return value 'false'.
     * Should be larger than the MIG_ACK_TIMEOUT (so that you still get the corresponding error).
     *
     * @return Returns 'true' if a response has been received, 'false' if it has timeouted.
     *
     * @note It is guaranteed that you only receive a migration response for the newest request.
     */
    bool waitForMigrationResponse(MigrationError& mig_error_out, int64_t timeout) {
        return m_mig_command_helper.waitForMigrationResponse(mig_error_out, timeout);
    }

    /**
     * @brief Requests a migration and waits for a migration response.
     * Same as calling requestMigration() and waitForMigrationResponse() consecutively.
     *
     * Parameters correspond to the ones in requestMigration() and waitForMigrationResponse().
     */
    bool syncCommandMigration(uint16_t        sys_hash,
                              uint16_t        migrate_from_id,
                              uint16_t        migrate_to_id,
                              uint32_t        ticket,
                              MigrationError& mig_error_out,
                              int64_t         timeout) {
        requestMigration(sys_hash, migrate_from_id, migrate_to_id, ticket);
        return waitForMigrationResponse(mig_error_out, timeout);
    }


    /**
     * @brief Requests MigrationInfo from a specific migration system instance
     * (information about tickets in storage) and waits for it to arrive.
     *
     * @param[in] request_sys_hash The sys_hash the requested instance is within.
     *
     * @param[in] request_instance_id The specific identifier of the instance from which
     * MigrationInfo is requested within its migration system.
     *
     * @param[out] mig_storage_info Main output parameter. It is filled when the MigrationInfo
     * has arrived. It stores information about all slots in the storage of the requested instance
     * (especially the thread tickets stored in it). Note that this is filled best effort, i.e.
     * if the received MigrationInfo has more slots than NUM_SLOTS it will throw away exceeding
     * elements, if it receives less slots it will fill less than NUM_SLOTS elements.
     * Information to this is stored in diff_to_expected_size.
     *
     * @param[out] diff_to_expected_size Stored when MigrationInfo is received.
     * If > 0: number of additional bytes that couldn't be stored anymore in the mig_storage_info
     * because the received MigrationInfo had more slots than NUM_SLOTS.
     * If < 0: number of bytes that couldn't be filled in the mig_storage_info because the received
     * MigrationInfo had less slots than NUM_SLOTS.
     *
     * @param[in] timeout How long to block (wait) before returning with return value 'false'.
     *
     * @return Returns 'true' if the MigrationInfo has been received, 'false' if it has timeouted.
     */
    template <uint8_t NUM_SLOTS>
    bool syncGetMigrationInfo(uint16_t request_sys_hash,
                              uint16_t request_instance_id,
                              MigrationSlotInfo (&mig_storage_info)[NUM_SLOTS],
                              int32_t& diff_to_expected_size,
                              int64_t  timeout) {
        return m_mig_command_helper.syncGetMigrationInfo(
          request_sys_hash, request_instance_id, mig_storage_info, diff_to_expected_size, timeout);
    }


    const char* getSysName() const { return m_sys_name; }
    uint16_t    getSysHash() const { return m_sys_hash; }
    uint16_t    getInstanceId() const { return m_instance_id; }

  protected:
    const char* m_sys_name;
    uint16_t    m_sys_hash;
    uint16_t    m_instance_id;

    Topic<MigrationCmd> m_mig_cmd_topic{
        MIG_TOPIC_IDS.cmd_topic_id,
        "MigrationCmdTopic"
    };
    Topic<MigrationAck> m_mig_ack_topic{
        MIG_TOPIC_IDS.ack_topic_id,
        "MigrationAckTopic"
    };
    Topic<MigrationInfoRequest> m_mig_info_request_topic{
        MIG_TOPIC_IDS.info_request_topic_id,
        "MigrationInfoRequestTopic"
    };
    Topic<MaxMigInfo> m_mig_info_topic{
        MIG_TOPIC_IDS.info_topic_id,
        "MigrationInfoTopic"
    };

  private:
    SyncCommBuffer<MigrationError>              m_mig_error_forwarder{};
    implementation::ack::MigrationAckSubscriber m_mig_command_ack_sub;

    SyncCommBuffer<int32_t>                       m_mig_commander_notifier{};
    implementation::info::MigrationInfoSubscriber m_mig_info_sub;

    implementation::commander::MigrationCommanderHelper m_mig_command_helper;
};

} // namespace RODOS::migration
