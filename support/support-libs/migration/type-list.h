#pragma once

#include "rodos-debug.h"

#include <concepts>
#include <type_traits>
#include <utility>

namespace RODOS::migration {

enum class IndexedTypeFound : bool {
    TRUE  = true,
    FALSE = false,
};

template <typename F, typename T, typename... Args>
concept ValidFunctor =
  requires(Args&&... args) { F::template func<T>(std::forward<Args>(args)...); };

/**
 * @brief A compile-time indexed TypeList.
 *
 * Functors object functions can be called on each type (providing it as a template argument) by a
 * call to callFuncOnIndexedType() and providing only a runtime index of the type in the TypeList.
 */
template <typename... Ts>
class TypeList;

template <>
class TypeList<> {
  public:
    static constexpr int32_t size = 0;

    /// @brief call Functor F with given arguments on the type indexed by given index
    template <typename F, typename... Args>
    [[nodiscard]] static IndexedTypeFound callFuncOnIndexedType(int32_t i, Args&&... args) {
        return callFuncOnIndexedType<F>(0, i, std::forward<Args>(args)...);
    }

  protected:
    template <typename F, typename... Args>
    static IndexedTypeFound callFuncOnIndexedType(int32_t, int32_t, Args&&...) {
        return IndexedTypeFound::FALSE;
    }
};

template <typename Head, typename... Tail>
class TypeList<Head, Tail...> : private TypeList<Tail...> {
  public:
    static constexpr int32_t size = TypeList<Tail...>::size + 1;

    /// @brief returns the index of the first appearance of the given type in the list
    template <typename T>
    [[nodiscard]] static constexpr int32_t index_of() {
        if constexpr(std::is_same<Head, T>::value) {
            return 0;
        } else {
            return 1 + TypeList<Tail...>::template index_of<T>();
        }
    }

    /// @brief call Functor F with given arguments on the type indexed by given index
    template <typename F, typename... Args>
    requires ValidFunctor<F, Head, Args...>
    [[nodiscard]] static IndexedTypeFound callFuncOnIndexedType(int32_t i, Args&&... args) {
        return callFuncOnIndexedType<F>(0, i, std::forward<Args>(args)...);
    }

  protected:
    template <typename F, typename... Args>
    static IndexedTypeFound callFuncOnIndexedType(int32_t count, int32_t i, Args&&... args) {
        if(i != count) {
            return TypeList<Tail...>::
              template callFuncOnIndexedType<F>(++count, i, std::forward<Args>(args)...);
        }
        F::template func<Head>(std::forward<Args>(args)...);
        return IndexedTypeFound::TRUE;
    }
};

} // namespace RODOS::migration
