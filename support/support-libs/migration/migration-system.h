/**
 * @file migration-system.h
 * @date 2020/06/08 22:26
 * @author Roland Konlechner, Minu Föger
 *
 * The migration system handles run-time migration of threads between nodes
 */

#pragma once

#include "migration/migration-types.h"
#include "migration/migration-commander.h"
#include "migration/implementation/storage/migration-storage.h"
#include "migration/implementation/info/migration-info-request-subscriber.h"
#include "migration/implementation/transmitter/migration-transmitter.h"
#include "migration/implementation/receiver/migration-receiver.h"

#include "timemodel.h"
#include "rodos-semaphore.h"
#include "rodos-debug.h"

namespace RODOS::migration {

/**
 * @brief The MigrationSystem class that holds all parts of the migration machinery
 * (including the MigrationCommander) and can hold MigratableThreads.
 *
 * @tparam TList The TypeList of all migratable threads this MigrationSystem can hold.
 * The index of a specific thread type in the list is used to identify the thread type on the new
 * node. Because of this, the TypeList should most likely be the same in all MigrationSystem objects
 * with the same sys_hash.
 *
 * @tparam MIG_TOPIC_IDS The ids for all RODOS::Topics used in the internal migration machinery.
 * Topic ids should always be >= 2000.
 *
 * @tparam MIG_STORAGE_PARAMS Parameters for the internal migration storage in which the
 * MigratableThreads are created.
 *
 * @tparam MIG_ACK_TIMEOUT The time to wait for the Acknowledgment of MigrationData
 * (has the destination received the thread data?).
 *
 * @tparam MIG_QUEUE_PARAMS Parameters for the various internal queues.
 *
 * @note Either a bare MigrationCommander (that can only command) or a MigrationSystem
 * (that can also hold threads) can be instantiated with the same sys_hash on the same node
 * (not both). The MigrationSystem object inherits from MigrationCommander and thus can also
 * be used as and casted to a MigrationCommander.
 */
template <typename TList,
          meta::MigrationTopicIds      MIG_TOPIC_IDS,
          meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          int64_t                      MIG_ACK_TIMEOUT  = 1500 * MILLISECONDS,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS = meta::MigrationQueueParams{}>
class MigrationSystem : public MigrationCommander<MIG_TOPIC_IDS> {
  public:
    using ConcreteMigCommander = MigrationCommander<MIG_TOPIC_IDS>;

    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;
    using ConcreteMigInfo = MigrationInfo<MIG_STORAGE_PARAMS.num_slots>;

    using ConcreteMigStorage =
      implementation::storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;

    using ConcreteMigInfoRequestSub =
      implementation::info::MigrationInfoRequestSubscriber<TList, MIG_STORAGE_PARAMS>;

    using ConcreteMigTransmitter =
      implementation::transmitter::
        MigrationTransmitter<TList, MIG_STORAGE_PARAMS, MIG_ACK_TIMEOUT, MIG_QUEUE_PARAMS>;

    using ConcreteMigReceiver =
      implementation::receiver::MigrationReceiver<TList, MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;

    /**
     * @brief constructor for MigrationSystem (same as in MigrationCommander)
     *
     * @param[in] sys_name The name of the migration system used. Only used for printing it out in
     * the Initiator. Can also be accessed via getSysName().
     *
     * @param[in] sys_hash The identifier of the migration system this node is part of. Only one
     * MigrationCommander/System object with the same sys_hash can be instantiated on a node.
     * Threads can only be migrated within the same migration system (identified by the same
     * sys_hash). The user has to ensure that this sys_hash is correct between nodes and unique
     * between migration systems.
     *
     * @param[in] instance_id The identifier of the node within the migration system
     * (which is itself identified via the sys_hash). The user has to ensure that this instance_id
     * is unique between other nodes of the same migration system. Different migration systems may
     * use the same instance_ids.
     */
    MigrationSystem(const char* sys_name,
                    uint16_t    sys_hash,
                    uint16_t    instance_id)
      : ConcreteMigCommander(sys_name, sys_hash, instance_id),
        m_mig_storage{},
        m_mig_info_request_sub{
            ConcreteMigCommander::m_sys_hash,
            ConcreteMigCommander::m_instance_id,
            m_mig_sema,
            ConcreteMigCommander::m_mig_info_request_topic,
            ConcreteMigCommander::m_mig_info_topic,
            m_mig_storage
        },
        m_mig_transmitter{
            ConcreteMigCommander::m_sys_hash,
            ConcreteMigCommander::m_instance_id,
            m_mig_sema,
            ConcreteMigCommander::m_mig_cmd_topic,
            m_mig_data_topic,
            ConcreteMigCommander::m_mig_ack_topic,
            m_mig_storage,
        },
        m_mig_receiver{
            ConcreteMigCommander::m_sys_hash,
            ConcreteMigCommander::m_instance_id,
            m_mig_sema,
            m_mig_data_topic,
            ConcreteMigCommander::m_mig_ack_topic,
            m_mig_storage
        } {}

    ~MigrationSystem() {
        if(isShuttingDown) {
            return;
        }
        RODOS_ERROR("MigrationSystem deleted!");
    }


    /**
     * @brief Create a migratable user thread in the internal storage of the MigrationSystem object.
     *
     * @tparam T The thread type to be created. Must be derived from MigratableThread.
     *
     * @param[in] ticket The ticket identifier of the migratable thread.
     * @param[in] args The arguments of the user thread's constructor (forwarded to it).
     */
    template <typename T, typename... Args>
    void createThread(uint32_t ticket, Args&&... args) {
        auto* slot_ptr = m_mig_storage.reserveEmptySlotUnprotected();
        if(slot_ptr == nullptr) {
            RODOS_ERROR("[MigrationSystem::createThread] "
                        "no empty slot found - thread instantiation failed!");
            return;
        }
        slot_ptr->template createThread<T>(ticket, std::forward<Args>(args)...);
    }

  private:
    Semaphore m_mig_sema{};

    Topic<ConcreteMigData> m_mig_data_topic{
        MIG_TOPIC_IDS.data_topic_id,
        "MigrationDataTopic"
    };

    ConcreteMigStorage        m_mig_storage;
    ConcreteMigInfoRequestSub m_mig_info_request_sub;

    ConcreteMigTransmitter m_mig_transmitter;
    ConcreteMigReceiver    m_mig_receiver;
};

} // namespace RODOS::migration
