#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"

namespace RODOS::migration::implementation::ack {

/**
 * @brief Small RAII wrapper for safer use of starting and stopping the acceptance of
 * an Ack of a specific format (abstracted as an AckAcceptanceSession).
 */
class ScopedAckAcceptanceSession {
  public:
    ScopedAckAcceptanceSession(MigrationAckSubscriber& mig_ack_sub,
                               uint16_t                expected_seq_num)
      : m_mig_ack_sub{ mig_ack_sub } {
        m_mig_ack_sub.startAcceptingAcks(expected_seq_num);
    }
    ~ScopedAckAcceptanceSession() {
        m_mig_ack_sub.stopAcceptingAcks();
    }

  private:
    MigrationAckSubscriber& m_mig_ack_sub;
};

} // namespace RODOS::migration::implementation::ack
