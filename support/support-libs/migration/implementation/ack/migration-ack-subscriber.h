#pragma once

#include "migration/migration-types.h"

#include "rodos-semaphore.h"
#include "subscriber.h"
#include "topic.h"
#include "synccommbuffer.h"
#include "timemodel.h"

namespace RODOS::migration::implementation::ack {

/**
 * @brief Subscriber to "mig_ack_topic" that the migration system uses to signal that
 * an operation has succeeded or not (via an error code).
 *
 * Acknowledgements are sent in following cases:
 * - the instance receiving a thread signals to the instance transmitting a thread
 *   that the migratable thread was received and was successfully reconstructed or not
 * - the instance transmitting a thread signals to the instance commanding a migration
 *   that the migration has succeeded or not
 *
 * This Subscriber simply forwards any valid MigrationAck to a SyncCommBuffer that is given to it.
 * The class allows to start and stop accepting an Ack of a specific format.
 * Only accepts one Ack per AckAcceptanceSession (any subsequent Acks are dropped silently).
 *
 * @note Should be used only with the provided RAII wrapper classes.
 */
class MigrationAckSubscriber : public SubscriberReceiver<MigrationAck> {
  public:
    MigrationAckSubscriber(uint16_t                        sys_hash,
                           uint16_t                        instance_id,
                           Topic<MigrationAck>&            mig_ack_topic,
                           AckType                         expected_ack_type,
                           SyncCommBuffer<MigrationError>& mig_error_forwarder)
      : SubscriberReceiver<MigrationAck>(mig_ack_topic, "MigrationAckSubscriber"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_expected_ack_type{ expected_ack_type },
        m_mig_error_forwarder{ mig_error_forwarder } {}


    void startAcceptingAcks(uint16_t expected_seq_num) {
        ScopeProtector scope{ &m_ack_sema };
        m_mig_error_forwarder.clearDataState();
        m_current_session_info = {
            .is_valid         = true,
            .expected_seq_num = expected_seq_num
        };
    }

    void stopAcceptingAcks() {
        ScopeProtector scope{ &m_ack_sema };
        m_current_session_info.is_valid = false;
        m_mig_error_forwarder.clearDataState();
    }

    void put(MigrationAck& mig_ack) final {
        ScopeProtector scope{ &m_ack_sema };

        if((!m_current_session_info.is_valid) ||
           (mig_ack.dest_sys_hash != m_sys_hash) ||
           (mig_ack.dest_instance_id != m_instance_id) ||
           (mig_ack.ack_type != m_expected_ack_type) ||
           (mig_ack.seq_num != m_current_session_info.expected_seq_num)) {
            return;
        }
        MigrationError dummy_mig_error{};
        if(!m_mig_error_forwarder.getOnlyIfNewData(dummy_mig_error)) {
            m_mig_error_forwarder.put(mig_ack.mig_error);
        }
    }

  private:
    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    AckType                         m_expected_ack_type;
    SyncCommBuffer<MigrationError>& m_mig_error_forwarder;

    Semaphore m_ack_sema{};
    struct {
        bool     is_valid{ false };
        uint16_t expected_seq_num{};
    } m_current_session_info{};
};

} // namespace RODOS::migration::implementation::ack
