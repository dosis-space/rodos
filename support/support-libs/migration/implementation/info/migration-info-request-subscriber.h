#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/storage/migration-storage.h"

#include "rodos-semaphore.h"
#include "subscriber.h"

namespace RODOS::migration::implementation::info {

/**
 * @brief Subscriber to "mig_info_request_topic" that receives incoming migration info requests
 * from the migration commander instance and responds to them accordingly on the "mig_info_topic".
 *
 * Migration info messages contain information of the state of a specific migration instance
 * (= node) within a migration system. A migration commander object can request such an
 * info message by sending migration info requests over the "mig_info_request_topic".
 *
 * @note Because instances within a migration system can potentially have differently large
 * migration storages, the subscriber only publishes a partial message corresponding to the
 * local number of migration slots to the "mig_info_topic".
 */
template <typename TList, meta::MigrationStorageParams MIG_STORAGE_PARAMS>
class MigrationInfoRequestSubscriber : public SubscriberReceiver<MigrationInfoRequest> {
  public:
    using ConcreteMigInfo    = MigrationInfo<MIG_STORAGE_PARAMS.num_slots>;
    using ConcreteMigStorage = storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;

    MigrationInfoRequestSubscriber(uint16_t                     sys_hash,
                                   uint16_t                     instance_id,
                                   Semaphore&                   mig_sema,
                                   Topic<MigrationInfoRequest>& mig_info_request_topic,
                                   Topic<MaxMigInfo>&           mig_info_topic,
                                   ConcreteMigStorage&          mig_storage)
      : SubscriberReceiver<MigrationInfoRequest>(mig_info_request_topic,
                                                 "MigrationInfoRequestSubscriber"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_sema{ mig_sema },
        m_mig_info_topic{ mig_info_topic },
        m_mig_storage{ mig_storage } {}


    void put(MigrationInfoRequest& mig_info_req) final {
        if((mig_info_req.request_sys_hash != m_sys_hash) ||
           (mig_info_req.request_instance_id != m_instance_id)) {
            return;
        }
        ConcreteMigInfo mig_info{
            .requester_sys_hash    = mig_info_req.sender_sys_hash,
            .requester_instance_id = mig_info_req.sender_instance_id,
            .mig_storage_info{},
        };
        fillTicketArray(mig_info);
        m_mig_info_topic.TopicInterface::publishMsgPart(&mig_info, sizeof(ConcreteMigInfo));
    }

  private:
    void fillTicketArray(ConcreteMigInfo& mig_info) {
        ScopeProtector scope{ &m_mig_sema };
        for(int i = 0; i < MIG_STORAGE_PARAMS.num_slots; i++) {
            auto* slot_ptr = m_mig_storage.getSlotPtrByIndex(i);
            if((slot_ptr == nullptr) || (!slot_ptr->isValidlyOccupied())) {
                mig_info.mig_storage_info[i] = { false, 0 };
            } else {
                mig_info.mig_storage_info[i] = { true, slot_ptr->getMigThreadPtr()->getTicket() };
            }
        }
    }

    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Semaphore& m_mig_sema;

    Topic<MaxMigInfo>& m_mig_info_topic;

    ConcreteMigStorage& m_mig_storage;
};

} // namespace RODOS::migration::implementation::info
