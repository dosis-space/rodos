#pragma once

#include "migration/implementation/info/migration-info-subscriber.h"

namespace RODOS::migration::implementation::info {

/**
 * @brief Small RAII wrapper for safer use of starting and stopping the acceptance of
 * a MigrationInfo message into an output array (abstracted as an MigInfoAcceptanceSession).
 */
class ScopedMigInfoAcceptanceSession {
  public:
    ScopedMigInfoAcceptanceSession(MigrationInfoSubscriber& mig_info_sub,
                                   void*                    mig_slot_info_array,
                                   uint8_t                  num_slots)
      : m_mig_info_sub{ mig_info_sub } {
        m_mig_info_sub.startAcceptingMigInfos(mig_slot_info_array, num_slots);
    }
    ~ScopedMigInfoAcceptanceSession() {
        m_mig_info_sub.stopAcceptingMigInfos();
    }

  private:
    MigrationInfoSubscriber& m_mig_info_sub;
};

} // namespace RODOS::migration::implementation::info
