#pragma once

#include "migration/migration-types.h"

#include "misc-rodos-funcs.h"
#include "rodos-semaphore.h"
#include "string_pico.h"
#include "subscriber.h"
#include "synccommbuffer.h"

namespace RODOS::migration::implementation::info {

/**
 * @brief Subscriber to "mig_info_topic" that receives the migration info messages and forwards
 * them up to higher layers (i.e. the migration commander that requested them).
 *
 * This Subscriber simply forwards the payload of a valid MigrationInfo message to higher layers.
 * The class allows to start and stop accepting MigrationInfo messages directed to the local
 * migration instance. It only forwards the first MigrationInfo message received in the current
 * session (any subsequent MigrationInfo messages are dropped silently).
 */
class MigrationInfoSubscriber : public Subscriber {
  public:
    MigrationInfoSubscriber(uint16_t                 sys_hash,
                            uint16_t                 instance_id,
                            Topic<MaxMigInfo>&       mig_info_topic,
                            SyncCommBuffer<int32_t>& commander_notifier)
      : Subscriber(mig_info_topic, "MigrationInfoSubscriber"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_info_topic{ mig_info_topic },
        m_mig_commander_notifier{ commander_notifier } {}

    /**
     * @brief Start accepting MigrationInfo messages directed to this node.
     *
     * @param[out] mig_slot_info_array The payload of the first MigrationInfo message that is
     * received will be copied to this array.
     * @param[in] num_slots Number of slots the array has (determines maximal amount of bytes
     * that can be copied to the specified array).
     */
    void startAcceptingMigInfos(void* mig_slot_info_array, uint8_t num_slots) {
        ScopeProtector scope{ &m_mig_info_sema };
        m_mig_commander_notifier.clearDataState();
        m_current_session_info = {
            .is_valid            = true,
            .mig_slot_info_array = mig_slot_info_array,
            .num_slots           = num_slots,
        };
    }

    /**
     * @brief Stops accepting MigrationInfo messages directed to this node
     * (subsequent message will be silently dropped).
     */
    void stopAcceptingMigInfos() {
        ScopeProtector scope{ &m_mig_info_sema };
        m_current_session_info.is_valid = false;
        m_mig_commander_notifier.clearDataState();
    }

    /**
     * @brief Actual put() method (gets called when new MigrationInfo messages arrive).
     *
     * @note If a received MigrationInfo message is valid and a valid MigInfoAcceptanceSession
     * is open, then the following is done:
     * - the payload of the message is memcopied to the output array specified in
     * the MigInfoAcceptanceSession.
     * - further, the higher layer is notified of the arrival of a MigrationInfo message via
     * pushing a value to a SyncCommBuffer given to the Subscriber. The value holds the byte-wise
     * difference of: size_of_received_mig_info_payload - size_of_output_mig_info_array.
     */
    uint32_t put(const uint32_t topic_id, const size_t len, void* data, const NetMsgInfo&) final {
        ScopeProtector scope{ &m_mig_info_sema };

        if(!rodosMessageSanityCheck(topic_id, len, data)) {
            return 0;
        }
        auto& mig_info = *static_cast<MaxMigInfo*>(data);
        if(!migInfoSanityCheck(mig_info)) {
            return 0;
        }

        if(!isCurrentSessionValidlyOpen()) {
            return 0;
        }
        int32_t dummy_diff_to_expected_size{};
        if(!m_mig_commander_notifier.getOnlyIfNewData(dummy_diff_to_expected_size)) {
            m_mig_commander_notifier.put(memCopyMigStorageInfo(mig_info));
            return 1;
        }
        return 0;
    }

  private:
    bool rodosMessageSanityCheck(const uint32_t topic_id, const size_t len, void* data) {
        return (m_mig_info_topic.topicId == topic_id) &&
               (len >= sizeof(MinMigInfo)) &&
               (len <= sizeof(MaxMigInfo)) &&
               (data != nullptr);
    }

    bool migInfoSanityCheck(const MaxMigInfo& mig_info) {
        if((mig_info.requester_sys_hash != m_sys_hash) ||
           (mig_info.requester_instance_id != m_instance_id)) {
            return false;
        }
        return true;
    }

    bool isCurrentSessionValidlyOpen() {
        if(!m_current_session_info.is_valid ||
           (m_current_session_info.mig_slot_info_array == nullptr)) {
            return false;
        }
        return true;
    }

    int32_t memCopyMigStorageInfo(const MaxMigInfo& mig_info) {
        int32_t size_incoming_mig_storage_info =
          mig_info.num_slots * sizeof(MigrationSlotInfo);
        int32_t size_outgoing_mig_storage_info =
          m_current_session_info.num_slots * sizeof(MigrationSlotInfo);

        int32_t num_bytes_to_be_written = RODOS::min(size_incoming_mig_storage_info,
                                                     size_outgoing_mig_storage_info);
        RODOS::memcpy(m_current_session_info.mig_slot_info_array,
                      mig_info.mig_storage_info,
                      static_cast<size_t>(num_bytes_to_be_written));

        int32_t diff_to_expected_size =
          (size_incoming_mig_storage_info - size_outgoing_mig_storage_info);
        return diff_to_expected_size;
    }

    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Topic<MaxMigInfo>& m_mig_info_topic;

    Semaphore                m_mig_info_sema{};
    SyncCommBuffer<int32_t>& m_mig_commander_notifier;
    struct {
        bool    is_valid{ false };
        void*   mig_slot_info_array{ nullptr };
        uint8_t num_slots{};
    } m_current_session_info{};
};

} // namespace RODOS::migration::implementation::info
