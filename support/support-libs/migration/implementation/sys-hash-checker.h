#pragma once

#include "listelement.h"

namespace RODOS::migration::implementation {

/**
 * @brief Small helper class that a class can inherit from and pass an integral sys_hash value to
 * during construction. Later (e.g. in an Initiator) the derived class can check whether the given
 * sys_hash was unique among all other globally instantiated objects derived from SysHashChecker.
 *
 * This is done by appending itself to a static RODOS::List during construction.
 *
 * @note Used in the MigrationCommander to determine whether a given sys_hash value (which is used
 * to identify the local migration instance) is unique on the local node.
 */
class SysHashChecker : public ListElement {
  public:
    inline static List s_sys_hash_checker_list{};

    explicit SysHashChecker(uint16_t sys_hash)
      : ListElement(s_sys_hash_checker_list), m_sys_hash{ sys_hash } {}
    virtual ~SysHashChecker() = default;

    bool checkSysHashUniqueInList() {
        for(auto& elem : s_sys_hash_checker_list) {
            auto& sys_hash_checker = static_cast<SysHashChecker&>(elem);
            if((&sys_hash_checker != this) && (sys_hash_checker.m_sys_hash == m_sys_hash)) {
                return false;
            }
        }
        return true;
    }

  private:
    uint16_t m_sys_hash;
};

} // namespace RODOS::migration::implementation
