#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"
#include "migration/implementation/info/migration-info-subscriber.h"
#include "migration/implementation/info/scoped_mig_info_acceptance_session.h"

#include "rodos-semaphore.h"
#include "synccommbuffer.h"
#include "timemodel.h"
#include "topic.h"

namespace RODOS::migration::implementation::commander {

/**
 * @brief Helper class to the MigrationCommander class. Handles the actual process of
 * commanding migrations of migratable threads.
 *
 * @note The encapsulation into this helper class is done to increase readability and
 * to enable better unit testing.
 */
class MigrationCommanderHelper {
  public:
    MigrationCommanderHelper(uint16_t                        sys_hash,
                             uint16_t                        instance_id,
                             Topic<MigrationCmd>&            mig_cmd_topic,
                             Topic<MigrationInfoRequest>&    mig_info_request_topic,
                             SyncCommBuffer<MigrationError>& mig_error_forwarder,
                             ack::MigrationAckSubscriber&    mig_command_ack_sub,
                             SyncCommBuffer<int32_t>&        mig_commander_notifier,
                             info::MigrationInfoSubscriber&  mig_info_sub)
      : m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_cmd_topic{ mig_cmd_topic },
        m_mig_info_request_topic{ mig_info_request_topic },
        m_mig_error_forwarder{ mig_error_forwarder },
        m_mig_command_ack_sub{ mig_command_ack_sub },
        m_mig_commander_notifier{ mig_commander_notifier },
        m_mig_info_sub{ mig_info_sub } {}

    virtual ~MigrationCommanderHelper() = default;

    /**
     * @brief Request the migration of a MigratableThread from one node to another.
     *
     * @note For more see MigrationCommander::requestMigration()
     */
    void requestMigration(uint16_t sys_hash,
                          uint16_t migrate_from_id,
                          uint16_t migrate_to_id,
                          uint32_t ticket) {
        uint16_t tmp_seq_num{};
        {
            ScopeProtector scope{ &m_seq_num_sema };
            tmp_seq_num = ++m_seq_num;
        }
        MigrationCmd cmd{
            .sender_sys_hash    = m_sys_hash,
            .sender_instance_id = m_instance_id,
            .target_sys_hash    = sys_hash,
            .migrate_from_id    = migrate_from_id,
            .migrate_to_id      = migrate_to_id,
            .seq_num            = tmp_seq_num,
            .ticket             = ticket
        };
        m_mig_command_ack_sub.startAcceptingAcks(tmp_seq_num);
        m_mig_cmd_topic.publish(cmd);
    }

    /**
     * @brief Wait to receive a migration response to the last migration request.
     *
     * @note For more see MigrationCommander::waitForMigrationResponse()
     */
    bool waitForMigrationResponse(MigrationError& mig_error_out, int64_t timeout) {
        bool has_retrieved_something = m_mig_error_forwarder.syncGet(mig_error_out, timeout);
        m_mig_command_ack_sub.stopAcceptingAcks();
        return has_retrieved_something;
    }

    /**
     * @brief Requests MigrationInfo from a specific migration system instance
     * (information about tickets in storage) and waits for it to arrive.
     *
     * @note For more see MigrationCommander::syncGetMigrationInfo()
     */
    template <uint8_t NUM_SLOTS>
    bool syncGetMigrationInfo(uint16_t request_sys_hash,
                              uint16_t request_instance_id,
                              MigrationSlotInfo (&mig_storage_info)[NUM_SLOTS],
                              int32_t& diff_to_expected_size,
                              int64_t  timeout) {
        info::ScopedMigInfoAcceptanceSession scoped_mig_info_acceptance_session{
            m_mig_info_sub,
            mig_storage_info,
            NUM_SLOTS
        };
        MigrationInfoRequest info_request{
            .sender_sys_hash     = m_sys_hash,
            .sender_instance_id  = m_instance_id,
            .request_sys_hash    = request_sys_hash,
            .request_instance_id = request_instance_id
        };
        m_mig_info_request_topic.publish(info_request);

        bool has_retrieved_something =
          m_mig_commander_notifier.syncGet(diff_to_expected_size, timeout);
        return has_retrieved_something;
    }

  private:
    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Topic<MigrationCmd>&         m_mig_cmd_topic;
    Topic<MigrationInfoRequest>& m_mig_info_request_topic;

    Semaphore m_seq_num_sema{};
    uint16_t  m_seq_num{};

    SyncCommBuffer<MigrationError>& m_mig_error_forwarder;
    ack::MigrationAckSubscriber&    m_mig_command_ack_sub;

    SyncCommBuffer<int32_t>&       m_mig_commander_notifier;
    info::MigrationInfoSubscriber& m_mig_info_sub;
};

} // namespace RODOS::migration::implementation::commander
