#pragma once

#include "migration/migration-types.h"

#include "rodos-semaphore.h"
#include "string_pico.h"
#include "subscriber.h"
#include "fifo.h"
#include "thread.h"
#include "timemodel.h"

namespace RODOS::migration::implementation::receiver {

/**
 * @brief Subscriber to "mig_data_topic" that receives incoming migration data and forwards them
 * via a BlockFifo to higher layers.
 */
template <meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class MigrationReceiverDataSubscriber : public Subscriber {
  public:
    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;
    using MigRecvQueue    = BlockFifo<ConcreteMigData, MIG_QUEUE_PARAMS.num_elems_mig_recv_queue>;

    static constexpr auto MIN_ALLOWED_MIG_DATA_SIZE = sizeof(MigrationData<1>);
    static constexpr auto MAX_ALLOWED_MIG_DATA_SIZE = sizeof(ConcreteMigData);

    MigrationReceiverDataSubscriber(uint16_t                sys_hash,
                                    uint16_t                instance_id,
                                    Topic<ConcreteMigData>& mig_data_topic,
                                    Semaphore&              mig_recv_queue_sema,
                                    MigRecvQueue&           mig_recv_queue,
                                    Thread&                 mig_transmitter_thread)
      : Subscriber(mig_data_topic, "MigrationReceiverDataSubscriber"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_data_topic{ mig_data_topic },
        m_mig_recv_queue_sema{ mig_recv_queue_sema },
        m_mig_recv_queue{ mig_recv_queue },
        m_mig_receiver_thread{ mig_transmitter_thread } {}


    /**
     * @brief The overridden raw put() method of the RODOS::Subscriber (called when message gets
     * published on the subscribed RODOS::Topic).
     *
     * @note Using raw Subscriber (put() method receives raw byte pointer) to be able to
     * partially publish data (don't have to always transmit the whole maximal thread size).
     */
    uint32_t put(const uint32_t topic_id, const size_t len, void* data, const NetMsgInfo&) final {
        if(!sanityCheck(topic_id, len, data)) {
            return 0;
        }
        auto& mig_data = *static_cast<ConcreteMigData*>(data);
        if((mig_data.sys_hash != m_sys_hash) || (mig_data.dest_instance_id != m_instance_id)) {
            return 0;
        }
        {
            ScopeProtector scope{ &m_mig_recv_queue_sema };

            size_t num_empty_slots_in_queue{};
            auto*  next_queue_entry = m_mig_recv_queue.getBufferToWrite(num_empty_slots_in_queue);
            if(next_queue_entry == nullptr) {
                return 0;
            }

            RODOS::memcpy(next_queue_entry, &mig_data, len);

            constexpr size_t NUM_QUEUE_ENTRIES_WRITTEN = 1;
            m_mig_recv_queue.writeConcluded(NUM_QUEUE_ENTRIES_WRITTEN);
        }
        m_mig_receiver_thread.resume();
        return 1;
    }

  private:
    bool sanityCheck(const uint32_t topic_id, const size_t len, void* data) {
        return ((m_mig_data_topic.topicId == topic_id) &&
                (len >= MIN_ALLOWED_MIG_DATA_SIZE) &&
                (len <= MAX_ALLOWED_MIG_DATA_SIZE) &&
                (data != nullptr));
    }

    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Topic<ConcreteMigData>& m_mig_data_topic;

    Semaphore&    m_mig_recv_queue_sema;
    MigRecvQueue& m_mig_recv_queue;

    Thread& m_mig_receiver_thread;
};

} // namespace RODOS::migration::implementation::receiver
