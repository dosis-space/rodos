#pragma once

#include "migration/migration-types.h"
#include "migration/type-list.h"
#include "migration/implementation/migration-functors.h"
#include "migration/implementation/storage/migration-storage.h"

#include "rodos-debug.h"
#include "timemodel.h"
#include "rodos-semaphore.h"

namespace RODOS::migration::implementation::receiver {

/**
 * @brief Helper class to the MigrationReceiver class. Handles the actual reconstruction process
 * of a migratable thread within the local migration storage.
 *
 * @note The encapsulation into this helper class is done to increase readability and
 * to enable better unit testing.
 */
template <typename TList,
          meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class MigrationReceiverHelper {
  public:
    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;

    using ConcreteMigStorage     = storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;
    using ConcreteMigStorageSlot = typename ConcreteMigStorage::ConcreteMigStorageSlot;

    using ConcreteReconstructFunctor =
      ReconstructFunctor<TList, MIG_STORAGE_PARAMS.max_thread_size, MIG_STORAGE_PARAMS.stack_size>;

    MigrationReceiverHelper(uint16_t             sys_hash,
                            uint16_t             instance_id,
                            Semaphore&           mig_sema,
                            Topic<MigrationAck>& mig_ack_topic,
                            ConcreteMigStorage&  mig_storage)
      : m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_sema{ mig_sema },
        m_mig_ack_topic{ mig_ack_topic },
        m_mig_storage{ mig_storage } {}

    /**
     * @brief Receive a migration data object and reconstruct the corresponding migratable thread
     * from it in the local migration storage.
     *
     * @param[in] mig_data The migration data that was received and carries all necessary
     * information to completely reconstruct the corresponding migratable thread.
     *
     * @note Error handling is done as follows:
     * - problems while checking the incoming migration data for sanity or while reserving a slot
     * in the local migration storage (e.g. storage full):
     * Send back Response Ack with corresponding Error to the migration transmitter (which deals
     * with the error appropriately).
     * - received valid migration data and reserved slot but error during reconstruction:
     * Free reserved slot again and send back Response Ack with corresponding Error to the
     * migration transmitter (which deals with the error appropriately).
     *
     * @return Indicates whether the thread migration succeeded or not.
     */
    bool receiveThread(ConcreteMigData& mig_data) {
        ScopeProtector scope{ &m_mig_sema };

        ConcreteMigStorageSlot* slot_ptr{ nullptr };
        MigrationError          receiver_mig_error =
          checkAndReserveSlot(mig_data.type_index, mig_data.ticket, mig_data.len, slot_ptr);
        if((slot_ptr == nullptr) || (receiver_mig_error != MigrationError::OK)) {
            sendBackResponseAck(mig_data.src_instance_id, receiver_mig_error);
            return false;
        }

        if(!reconstructThreadInSlot(mig_data, *slot_ptr)) {
            RODOS_ERROR("[MigrationReceiverHelper::receiveThread] "
                        "couldn't reconstruct thread because of invalid type index "
                        "in received migration data!");
            slot_ptr->free();
            sendBackResponseAck(mig_data.src_instance_id, MigrationError::THREAD_RECONSTRUCT_ERROR);
            return false;
        }
        sendBackResponseAck(mig_data.src_instance_id, receiver_mig_error);
        return true;
    }

  private:
    MigrationError checkAndReserveSlot(int32_t                  type_index,
                                       uint32_t                 ticket,
                                       uint32_t                 payload_length,
                                       ConcreteMigStorageSlot*& slot_ptr_out) {
        slot_ptr_out = nullptr;
        if(!sanityCheck(type_index, payload_length)) {
            return MigrationError::INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE;
        }
        if(m_mig_storage.findSlotIndexByTicketUnprotected(ticket) >= 0) {
            return MigrationError::THREAD_ALREADY_EXISTENT_ON_DEST_NODE;
        }
        slot_ptr_out = m_mig_storage.reserveEmptySlotUnprotected();
        if(slot_ptr_out == nullptr) {
            return MigrationError::NO_EMPTY_SPACE_ON_DEST_NODE;
        }
        return MigrationError::OK;
    }

    bool sanityCheck(int32_t type_index, uint32_t payload_length) {
        if((type_index < 0) || (type_index >= TList::size)) {
            return false;
        }
        uint32_t expected_size{};
        auto     ret_get_size =
          TList::template callFuncOnIndexedType<GetSizeOfTypeFunctor>(
            type_index,
            expected_size);
        if((ret_get_size != IndexedTypeFound::TRUE) || (payload_length != expected_size)) {
            return false;
        }
        return true;
    }

    void sendBackResponseAck(uint16_t dest_instance_id, MigrationError receiver_mig_error) {
        MigrationAck response_ack{
            .dest_sys_hash    = m_sys_hash,
            .dest_instance_id = dest_instance_id,
            .ack_type         = AckType::RESPONSE_ACK,
            .mig_error        = receiver_mig_error,
            .seq_num          = EXPECTED_SEQ_NUM_FOR_RESPONSE_ACK
        };
        m_mig_ack_topic.publish(response_ack);
    }

    bool reconstructThreadInSlot(ConcreteMigData& mig_data, ConcreteMigStorageSlot& slot) {
        auto ret_reconstr =
          TList::template callFuncOnIndexedType<ConcreteReconstructFunctor>(
            mig_data.type_index,
            slot,
            mig_data.bytes[0]);
        return (ret_reconstr == IndexedTypeFound::TRUE);
    }

    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Semaphore& m_mig_sema;

    Topic<MigrationAck>& m_mig_ack_topic;

    ConcreteMigStorage& m_mig_storage;
};

} // namespace RODOS::migration::implementation::receiver
