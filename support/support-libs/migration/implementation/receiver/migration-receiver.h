#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/storage/migration-storage.h"
#include "migration/implementation/receiver/migration-receiver-helper.h"
#include "migration/implementation/receiver/migration-receiver-data-subscriber.h"
#include "migration/implementation/receiver/scoped_mig_data_read_session.h"

#include "rodos-debug.h"
#include "rodos-semaphore.h"
#include "thread.h"
#include "fifo.h"
#include "timemodel.h"

namespace RODOS::migration::implementation::receiver {

/**
 * @brief Main MigrationReceiver Thread class.
 *
 * Processes incoming migration data and reconstructs migrated threads from that data.
 * Internally forwards the data to the MigrationReceiverHelper class that handles the
 * actual migration reconstruction process.
 */
template <typename TList,
          meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class MigrationReceiver : public StaticThread<> {
  public:
    using ConcreteMigStorage = storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;

    using ConcreteMigReceiverDataSub =
      MigrationReceiverDataSubscriber<MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;
    using ConcreteScopedMigDataReadSession =
      ScopedMigDataReadSession<MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;

    using ConcreteMigReceiverHelper =
      MigrationReceiverHelper<TList, MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;

    using ConcreteMigData = typename ConcreteMigReceiverHelper::ConcreteMigData;
    using MigRecvQueue    = typename ConcreteMigReceiverDataSub::MigRecvQueue;

    /**
     * @brief Construct a new MigrationReceiver object
     *
     * @param sys_hash The system hash of the overall migration system network the receiver is part of.
     * @param instance_id The instance identifier of the MigrationSystem object the receiver is part of.
     * @param mig_sema The semaphore used within the MigrationSystem object.
     * @param mig_data_topic The Topic on which the migration data is transferred.
     * @param mig_ack_topic The Topic on which the migration acknowledgments are transferred.
     * @param mig_storage The local storage object holding the migratable thread objects.
     */
    MigrationReceiver(uint16_t                sys_hash,
                      uint16_t                instance_id,
                      Semaphore&              mig_sema,
                      Topic<ConcreteMigData>& mig_data_topic,
                      Topic<MigrationAck>&    mig_ack_topic,
                      ConcreteMigStorage&     mig_storage)
      : StaticThread<>("MigrationReceiver"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_sema{ mig_sema },
        m_mig_data_topic{ mig_data_topic },
        m_mig_ack_topic{ mig_ack_topic },
        m_mig_storage{ mig_storage },
        m_mig_receiver_data_sub{
            m_sys_hash,
            m_instance_id,
            m_mig_data_topic,
            m_mig_recv_queue_sema,
            m_mig_recv_queue,
            *this
        },
        m_mig_receiver_helper{
            m_sys_hash,
            m_instance_id,
            m_mig_sema,
            m_mig_ack_topic,
            m_mig_storage
        } {}


    void run() final {
        while(true) {
            ConcreteScopedMigDataReadSession scoped_mig_data_read_session{
                m_mig_recv_queue_sema,
                m_mig_recv_queue
            };
            auto& mig_data = scoped_mig_data_read_session.accessMigData();

            if(!m_mig_receiver_helper.receiveThread(mig_data)) {
                continue;
            }
            PRINTF(SCREEN_GREEN
                   "[MigrationReceiver::run] (sys_hash: 0x%x, instance_id: 0x%x):\n"
                   "-> received and successfully reconstructed thread (ticket: 0x%x)!\n\n"
                   "" SCREEN_RESET,
                   (unsigned)m_sys_hash,
                   (unsigned)m_instance_id,
                   (unsigned)mig_data.ticket);
        }
    }

  private:
    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Semaphore& m_mig_sema;

    Topic<ConcreteMigData>& m_mig_data_topic;
    Topic<MigrationAck>&    m_mig_ack_topic;

    ConcreteMigStorage& m_mig_storage;

    Semaphore                  m_mig_recv_queue_sema{};
    MigRecvQueue               m_mig_recv_queue{};
    ConcreteMigReceiverDataSub m_mig_receiver_data_sub;

    ConcreteMigReceiverHelper m_mig_receiver_helper;
};

} // namespace RODOS::migration::implementation::receiver
