#pragma once

#include "migration/migration-types.h"

#include "rodos-semaphore.h"
#include "fifo.h"

namespace RODOS::migration::implementation::receiver {

/**
 * @brief RAII wrapper for for thread-safe (and generally simpler) use of the underlying BlockFifo
 * that is used to forward received migration data to higher layers.
 *
 * A read session is defined as the process of reading one migration data packet
 * from the receive queue in a blocking and thread-safe manner.
 *
 * @note The receive queue uses a BlockFifo to avoid having to copy around the
 * large migration data packets on the stack.
 */
template <meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class ScopedMigDataReadSession {
  public:
    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;
    using MigRecvQueue    = BlockFifo<ConcreteMigData, MIG_QUEUE_PARAMS.num_elems_mig_recv_queue>;

    /**
     * @brief Start a session of reading exactly one migration data object from the given queue.
     *
     * @warning If the queue is empty, blocks until new migration data is inserted into the queue.
     *
     * @param mig_recv_queue_sema The semaphore of the receive queue.
     * @param mig_recv_queue The actual receive queue from which to fetch the data object from.
     */
    ScopedMigDataReadSession(Semaphore&    mig_recv_queue_sema,
                             MigRecvQueue& mig_recv_queue)
      : m_mig_recv_queue_sema{ mig_recv_queue_sema },
        m_mig_recv_queue{ mig_recv_queue },
        m_mig_data{ fetchNextMigDataToReadBlocking() } {}

    /**
     * @brief End the read session and pop the read migration data object from the queue.
     */
    ~ScopedMigDataReadSession() {
        concludeRead();
    }

    /**
     * @brief Access the received migration data object on top of the receive queue.
     */
    auto& accessMigData() { return m_mig_data; }

  private:
    ConcreteMigData& fetchNextMigDataToReadBlocking() {
        ConcreteMigData* mig_data_ptr{ nullptr };
        while((mig_data_ptr = fetchReadBufferFromQueue()) == nullptr) {
            Thread::suspendCallerUntil(RODOS::END_OF_TIME);
        }
        return *mig_data_ptr;
    }

    ConcreteMigData* fetchReadBufferFromQueue() {
        ScopeProtector scope{ &m_mig_recv_queue_sema };
        size_t         num_full_slots_in_queue{};
        return m_mig_recv_queue.getBufferToRead(num_full_slots_in_queue);
    }

    void concludeRead() {
        ScopeProtector scope{ &m_mig_recv_queue_sema };

        constexpr size_t NUM_QUEUE_ENTRIES_READ = 1;
        m_mig_recv_queue.readConcluded(NUM_QUEUE_ENTRIES_READ);
    }

    Semaphore&    m_mig_recv_queue_sema;
    MigRecvQueue& m_mig_recv_queue;

    ConcreteMigData& m_mig_data;
};

} // namespace RODOS::migration::implementation::receiver
