#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/storage/migration-storage.h"

#include "subscriber.h"
#include "topic.h"
#include "rodos-semaphore.h"

namespace RODOS::migration::implementation::transmitter {

/**
 * @brief Subscriber to "mig_cmd_topic" that receives the migration commands the user has issued.
 *
 * Checks sanity of command and marks the thread that shall be migrated (the thread then itself
 * has to signal back via the "mig_cmd_callback_topic" that it can now be migrated).
 *
 * @note In case of an invalid command the class sends back an error Ack to the Commander.
 */
template <typename TList,
          meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class MigrationTransmitterCmdSubscriber : public SubscriberReceiver<MigrationCmd> {
  public:
    using ConcreteMigStorage = storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;

    MigrationTransmitterCmdSubscriber(uint16_t                   sys_hash,
                                      uint16_t                   instance_id,
                                      Semaphore&                 mig_sema,
                                      Topic<MigrationCmd>&       mig_cmd_topic,
                                      Topic<CmdBackChannelData>& mig_cmd_callback_topic,
                                      Topic<MigrationAck>&       mig_ack_topic,
                                      ConcreteMigStorage&        mig_storage)
      : SubscriberReceiver<MigrationCmd>(mig_cmd_topic, "MigrationTransmitterCmdSubscriber"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_sema{ mig_sema },
        m_mig_cmd_callback_topic{ mig_cmd_callback_topic },
        m_mig_ack_topic{ mig_ack_topic },
        m_mig_storage{ mig_storage } {}


    void put(MigrationCmd& mig_cmd) final {
        if((mig_cmd.target_sys_hash != m_sys_hash) || (mig_cmd.migrate_from_id != m_instance_id)) {
            return;
        }
        ScopeProtector scope{ &m_mig_sema };

        MigrationError mig_error = checkStorageAndMarkThread(mig_cmd);
        if(mig_error != MigrationError::OK) {
            MigrationAck mig_command_ack{
                .dest_sys_hash    = mig_cmd.sender_sys_hash,
                .dest_instance_id = mig_cmd.sender_instance_id,
                .ack_type         = AckType::COMMAND_ACK,
                .mig_error        = mig_error,
                .seq_num          = mig_cmd.seq_num
            };
            m_mig_ack_topic.publish(mig_command_ack);
        }
    }

  private:
    MigrationError checkStorageAndMarkThread(MigrationCmd& mig_cmd) {
        int32_t slot_index = m_mig_storage.findSlotIndexByTicketUnprotected(mig_cmd.ticket);
        auto*   slot_ptr   = m_mig_storage.getSlotPtrByIndex(slot_index);
        if(slot_ptr == nullptr) {
            return MigrationError::TICKET_NOT_FOUND_ON_SRC_NODE;
        }

        CmdBackChannelData cmd_back_channel_data{
            .slot_index         = static_cast<uint8_t>(slot_index),
            .sender_sys_hash    = mig_cmd.sender_sys_hash,
            .sender_instance_id = mig_cmd.sender_instance_id,
            .migrate_to_id      = mig_cmd.migrate_to_id,
            .seq_num            = mig_cmd.seq_num,
            .ticket             = mig_cmd.ticket
        };
        if(!slot_ptr->getMigThreadPtr()->markShouldMigrate(cmd_back_channel_data,
                                                           m_mig_cmd_callback_topic)) {
            return MigrationError::THREAD_ALREADY_MIGRATING_ON_SRC_NODE;
        }
        return MigrationError::OK;
    }

    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Semaphore&                 m_mig_sema;
    Topic<CmdBackChannelData>& m_mig_cmd_callback_topic;
    Topic<MigrationAck>&       m_mig_ack_topic;

    ConcreteMigStorage& m_mig_storage;
};

} // namespace RODOS::migration::implementation::transmitter
