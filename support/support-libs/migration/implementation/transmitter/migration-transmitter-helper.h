#pragma once

#include "migration/migratable-thread.h"
#include "migration/migration-types.h"
#include "migration/type-list.h"
#include "migration/implementation/migration-functors.h"
#include "migration/implementation/storage/migration-storage.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"
#include "migration/implementation/ack/scoped_ack_acceptance_session.h"

#include "default-platform-parameter.h"
#include "rodos-assert.h"
#include "rodos-debug.h"
#include "rodos-semaphore.h"
#include "synccommbuffer.h"
#include "timemodel.h"

namespace RODOS::migration::implementation::transmitter {

/**
 * @brief Helper class to the MigrationTransmitter class. Handles the actual migration
 * of a migratable thread from the local node to another.
 *
 * @note The encapsulation into this helper class is done to increase readability and
 * to enable better unit testing.
 */
template <typename TList,
          meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          int64_t                      MIG_ACK_TIMEOUT,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class MigrationTransmitterHelper {
  public:
    using ConcreteMigData = MigrationData<MIG_STORAGE_PARAMS.max_thread_size>;
    static_assert(sizeof(ConcreteMigData) <= MAX_NETWORK_MESSAGE_LENGTH,
                  "MigrationData must not be larger than maximum RODOS message length!");

    using ConcreteMigStorage     = storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;
    using ConcreteMigStorageSlot = typename ConcreteMigStorage::ConcreteMigStorageSlot;

    MigrationTransmitterHelper(uint16_t                        sys_hash,
                               uint16_t                        instance_id,
                               Semaphore&                      mig_sema,
                               Topic<ConcreteMigData>&         mig_data_topic,
                               Topic<MigrationAck>&            mig_ack_topic,
                               ConcreteMigStorage&             mig_storage,
                               SyncCommBuffer<MigrationError>& mig_receiver_error_forwarder,
                               ack::MigrationAckSubscriber&    mig_transmitter_ack_sub)
      : m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_sema{ mig_sema },
        m_mig_data_topic{ mig_data_topic },
        m_mig_ack_topic{ mig_ack_topic },
        m_mig_storage{ mig_storage },
        m_mig_receiver_error_forwarder{ mig_receiver_error_forwarder },
        m_mig_transmitter_ack_sub{ mig_transmitter_ack_sub } {}


    /**
     * @brief Migrate a migratable thread from the local node to another.
     *
     * @param[in] cmd_back_channel_data Information about the thread to be migrated as retrievable
     * from the migratable thread.
     *
     * @note Error handling is done as follows:
     * - problems while fetching the thread or preparing the migration message (invalid command):
     * Send back Command Ack with Error (no thread resumption because interpreted as invalid).
     * - no Ack received within timeout or Data Ack with Error received (error on destination node):
     * Send back Command Ack with Error and resume the thread's execution on the local node.
     * - received valid Data Ack but error in thread destruction:
     * Send back Command Ack with Error (no thread resumption because thread is broken).
     *
     * @return Indicates whether the thread migration succeeded or not.
     */
    bool migrateThread(CmdBackChannelData& cmd_back_channel_data) {
        ScopeProtector scope{ &m_mig_sema };

        auto* slot_ptr = getAndCheckSlot(cmd_back_channel_data);
        if(slot_ptr == nullptr) {
            sendBackCommandAck(cmd_back_channel_data, MigrationError::INVALID_SLOT_ON_SRC_NODE);
            return false;
        }
        auto& slot = *slot_ptr;

        if(!fillTmpMigData(slot, cmd_back_channel_data.migrate_to_id)) {
            sendBackCommandAck(cmd_back_channel_data, MigrationError::THREAD_COPY_ERROR);
            return false;
        }

        auto mig_ack_error = publishDataAndWaitForAck();
        if(mig_ack_error != MigrationError::OK) {
            sendBackCommandAck(cmd_back_channel_data, mig_ack_error);
            auto* thread_ptr = slot.getMigThreadPtr();
            RODOS_ASSERT_IFNOT_RETURN(thread_ptr != nullptr, false);
            thread_ptr->resume();
            return false;
        }

        if(!destructThreadAndFreeSlot(slot)) {
            RODOS_ERROR("[MigrationTransmitterHelper::migrateThread] "
                        "couldn't destruct thread because of invalid type index in thread slot!");
            sendBackCommandAck(cmd_back_channel_data, MigrationError::THREAD_DESTRUCT_ERROR);
            return false;
        }

        sendBackCommandAck(cmd_back_channel_data, MigrationError::OK);
        return true;
    }

  private:
    ConcreteMigStorageSlot* getAndCheckSlot(const CmdBackChannelData& cmd_back_channel_data) {
        auto* slot_ptr = m_mig_storage.getSlotPtrByIndex(cmd_back_channel_data.slot_index);
        if((slot_ptr == nullptr) || (!slot_ptr->isValidlyOccupied())) {
            RODOS_ERROR("[MigrationTransmitterHelper::getAndCheckSlot] "
                        "requested migration on invalid thread slot!");
            return nullptr;
        }
        uint32_t slot_ticket = slot_ptr->getMigThreadPtr()->getTicket();
        if(slot_ticket != cmd_back_channel_data.ticket) {
            RODOS_ERROR("[MigrationTransmitterHelper::getAndCheckSlot] "
                        "thread slot doesn't hold requested thread ticket!");
            return nullptr;
        }
        return slot_ptr;
    }

    bool fillTmpMigData(ConcreteMigStorageSlot& slot, uint16_t migrate_to_id) {
        m_tmp_mig_data = {
            .sys_hash         = m_sys_hash,
            .src_instance_id  = m_instance_id,
            .dest_instance_id = migrate_to_id,
            .type_index       = slot.getTypeIndex(),
            .ticket           = slot.getMigThreadPtr()->getTicket(),
            .len{},
            .bytes{},
        };
        if(!getMemCopyOfThread(slot, m_tmp_mig_data)) {
            RODOS_ERROR("[MigrationTransmitterHelper::fillTmpMigData] "
                        "detected invalid type index in thread slot during memcopy of thread!");
            return false;
        }
        return true;
    }

    MigrationError publishDataAndWaitForAck() {
        ack::ScopedAckAcceptanceSession scoped_response_ack_acceptance_session{
            m_mig_transmitter_ack_sub,
            EXPECTED_SEQ_NUM_FOR_RESPONSE_ACK
        };
        uint32_t actual_len_mig_data =
          MIG_DATA_SIZE_WITHOUT_BYTE_BUFFER + m_tmp_mig_data.len;

        m_mig_data_topic.publishMsgPart(m_tmp_mig_data, actual_len_mig_data);

        MigrationError mig_error{};
        bool           has_retrieved_something =
          m_mig_receiver_error_forwarder.syncGet(mig_error, MIG_ACK_TIMEOUT);
        if(!has_retrieved_something) {
            return MigrationError::TRANSMITTER_ACK_TIMEOUT;
        }
        return mig_error;
    }

    void sendBackCommandAck(const CmdBackChannelData& cmd_back_channel_data, MigrationError mig_error) {
        MigrationAck mig_command_ack{
            .dest_sys_hash    = cmd_back_channel_data.sender_sys_hash,
            .dest_instance_id = cmd_back_channel_data.sender_instance_id,
            .ack_type         = AckType::COMMAND_ACK,
            .mig_error        = mig_error,
            .seq_num          = cmd_back_channel_data.seq_num
        };
        m_mig_ack_topic.publish(mig_command_ack);
    }


    bool getMemCopyOfThread(ConcreteMigStorageSlot& slot,
                            ConcreteMigData&        mig_data_out) {
        uint32_t bytes_written{};
        auto     retCopyFunctor =
          TList::template callFuncOnIndexedType<CopyFunctor>(
            slot.getTypeIndex(),
            *slot.getMigThreadPtr(),
            mig_data_out.bytes[0],
            bytes_written);
        mig_data_out.len = bytes_written;
        return (retCopyFunctor == IndexedTypeFound::TRUE);
    }

    bool destructThreadAndFreeSlot(ConcreteMigStorageSlot& slot) {
        auto retDestructFunctor =
          TList::template callFuncOnIndexedType<DestructFunctor>(
            slot.getTypeIndex(),
            *slot.getMigThreadPtr());
        slot.free();
        return (retDestructFunctor == IndexedTypeFound::TRUE);
    }

    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Semaphore& m_mig_sema;

    Topic<ConcreteMigData>& m_mig_data_topic;
    Topic<MigrationAck>&    m_mig_ack_topic;

    ConcreteMigStorage& m_mig_storage;

    SyncCommBuffer<MigrationError>& m_mig_receiver_error_forwarder;
    ack::MigrationAckSubscriber&    m_mig_transmitter_ack_sub;

    ConcreteMigData m_tmp_mig_data{};
};

} // namespace RODOS::migration::implementation::transmitter
