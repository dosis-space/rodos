#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/transmitter/migration-transmitter-helper.h"
#include "migration/implementation/transmitter/migration-transmitter-cmd-subscriber.h"
#include "migration/implementation/transmitter/migration-transmitter-cmd-callback-subscriber.h"
#include "migration/implementation/ack/migration-ack-subscriber.h"
#include "migration/implementation/storage/migration-storage.h"

#include "thread.h"
#include "subscriber.h"
#include "synccommbuffer.h"
#include "fifo.h"
#include "timemodel.h"
#include "rodos-semaphore.h"
#include "rodos-debug.h"

namespace RODOS::migration::implementation::transmitter {

/**
 * @brief Main MigrationTransmitter Thread class.
 *
 * Processes incoming migration commands signaled to it by the individual migratable threads
 * after a thread yields cooperatively due to a received migration command.
 * The transmitter then forwards the commands to the MigrationTransmitterHelper class that handles
 * the actual migration transmission process.
 */
template <typename TList,
          meta::MigrationStorageParams MIG_STORAGE_PARAMS,
          int64_t                      MIG_ACK_TIMEOUT,
          meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
class MigrationTransmitter : public StaticThread<> {
  public:
    using ConcreteMigStorage = storage::MigrationStorage<TList, MIG_STORAGE_PARAMS>;

    using ConcreteMigTransmitterCmdSub =
      MigrationTransmitterCmdSubscriber<TList, MIG_STORAGE_PARAMS, MIG_QUEUE_PARAMS>;
    using ConcreteMigTransmitterCmdCallbackSub =
      MigrationTransmitterCmdCallbackSubscriber<MIG_QUEUE_PARAMS.num_elems_mig_cmd_queue>;
    using ConcreteMigTransmitterHelper =
      MigrationTransmitterHelper<TList, MIG_STORAGE_PARAMS, MIG_ACK_TIMEOUT, MIG_QUEUE_PARAMS>;

    using ConcreteMigData = typename ConcreteMigTransmitterHelper::ConcreteMigData;

    using MigCmdBackChannelFifo =
      typename ConcreteMigTransmitterCmdCallbackSub::MigCmdBackChannelFifo;

    /**
     * @brief Construct a new MigrationTransmitter object
     *
     * @param sys_hash The system hash of the overall migration system network the transmitter is part of.
     * @param instance_id The instance identifier of the MigrationSystem object the transmitter is part of.
     * @param mig_sema The semaphore used within the MigrationSystem object.
     * @param mig_cmd_topic The Topic on which the migration commands are transferred.
     * @param mig_data_topic The Topic on which the migration data is transferred.
     * @param mig_ack_topic The Topic on which the migration acknowledgments are transferred.
     * @param mig_storage The local storage object holding the migratable thread objects.
     */
    MigrationTransmitter(uint16_t                sys_hash,
                         uint16_t                instance_id,
                         Semaphore&              mig_sema,
                         Topic<MigrationCmd>&    mig_cmd_topic,
                         Topic<ConcreteMigData>& mig_data_topic,
                         Topic<MigrationAck>&    mig_ack_topic,
                         ConcreteMigStorage&     mig_storage)
      : StaticThread<>("MigrationTransmitter"),
        m_sys_hash{ sys_hash },
        m_instance_id{ instance_id },
        m_mig_sema{ mig_sema },
        m_mig_cmd_topic{ mig_cmd_topic },
        m_mig_data_topic{ mig_data_topic },
        m_mig_ack_topic{ mig_ack_topic },
        m_mig_storage{ mig_storage },
        m_mig_transmitter_cmd_sub{
            m_sys_hash,
            m_instance_id,
            m_mig_sema,
            m_mig_cmd_topic,
            m_mig_cmd_callback_topic,
            m_mig_ack_topic,
            m_mig_storage
        },
        m_mig_cmd_callback_sub{
            m_mig_cmd_callback_topic,
            m_mig_cmd_back_channel_fifo
        },
        m_mig_transmitter_ack_sub{
            m_sys_hash,
            m_instance_id,
            m_mig_ack_topic,
            AckType::RESPONSE_ACK,
            m_mig_receiver_error_forwarder,
        },
        m_mig_transmitter_helper{
            m_sys_hash,
            m_instance_id,
            m_mig_sema,
            m_mig_data_topic,
            m_mig_ack_topic,
            m_mig_storage,
            m_mig_receiver_error_forwarder,
            m_mig_transmitter_ack_sub,
        } {}


    void run() override {
        while(true) {
            CmdBackChannelData cmd_back_channel_data{};
            m_mig_cmd_back_channel_fifo.syncGet(cmd_back_channel_data, END_OF_TIME);

            if(!m_mig_transmitter_helper.migrateThread(cmd_back_channel_data)) {
                continue;
            }
            PRINTF(SCREEN_GREEN
                   "[MigrationTransmitter::run] (sys_hash: 0x%x, instance_id: 0x%x):\n"
                   "-> successfully migrated thread (ticket: 0x%x) to instance_id 0x%x!\n\n"
                   "" SCREEN_RESET,
                   (unsigned)m_sys_hash,
                   (unsigned)m_instance_id,
                   (unsigned)cmd_back_channel_data.ticket,
                   (unsigned)cmd_back_channel_data.migrate_to_id);
        }
    }

  private:
    uint16_t m_sys_hash;
    uint16_t m_instance_id;

    Semaphore& m_mig_sema;

    Topic<MigrationCmd>&    m_mig_cmd_topic;
    Topic<ConcreteMigData>& m_mig_data_topic;
    Topic<MigrationAck>&    m_mig_ack_topic;

    ConcreteMigStorage& m_mig_storage;

    static constexpr int64_t  TOPIC_ID_PICK_AUTOMATIC = -1;
    static constexpr bool     TOPIC_ONLY_LOCAL        = true;
    Topic<CmdBackChannelData> m_mig_cmd_callback_topic{ TOPIC_ID_PICK_AUTOMATIC,
                                                        "Local MigrationCmdCallbackTopic",
                                                        TOPIC_ONLY_LOCAL };

    MigCmdBackChannelFifo                m_mig_cmd_back_channel_fifo{};
    ConcreteMigTransmitterCmdSub         m_mig_transmitter_cmd_sub;
    ConcreteMigTransmitterCmdCallbackSub m_mig_cmd_callback_sub;

    SyncCommBuffer<MigrationError> m_mig_receiver_error_forwarder{};
    ack::MigrationAckSubscriber    m_mig_transmitter_ack_sub;
    ConcreteMigTransmitterHelper   m_mig_transmitter_helper;
};

} // namespace RODOS::migration::implementation::transmitter
