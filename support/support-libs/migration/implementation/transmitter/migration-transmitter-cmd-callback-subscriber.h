#pragma once

#include "migration/migration-types.h"

#include "rodos-semaphore.h"
#include "subscriber.h"
#include "fifo.h"

namespace RODOS::migration::implementation::transmitter {

/**
 * @brief Subscriber to "mig_cmd_callback_topic" the migratable thread publishes to
 * when it is ready to migrate (stack is empty).
 *
 * Simply returns the CmdBackChannelData back to the MigrationTransmitter
 * for further processing (the actual migration) via a Fifo.
 *
 * @note This class is necessary because the standard putter doesn't use syncPut but putGeneric.
 */
template <uint32_t NUM_ELEMS_MIG_CMD_QUEUE>
class MigrationTransmitterCmdCallbackSubscriber : public SubscriberReceiver<CmdBackChannelData> {
  public:
    using MigCmdBackChannelFifo = SyncFifo<CmdBackChannelData, NUM_ELEMS_MIG_CMD_QUEUE>;

    MigrationTransmitterCmdCallbackSubscriber(Topic<CmdBackChannelData>& mig_cmd_callback_topic,
                                              MigCmdBackChannelFifo&     mig_cmd_back_channel_fifo)
      : SubscriberReceiver<CmdBackChannelData>(mig_cmd_callback_topic,
                                               "MigrationTransmitterCmdCallbackSubscriber"),
        m_mig_cmd_back_channel_fifo{ mig_cmd_back_channel_fifo } {}


    void put(CmdBackChannelData& cmd_back_channel_data) final {
        ScopeProtector scope{ &m_cmd_back_channel_fifo_sema };
        m_mig_cmd_back_channel_fifo.syncPut(cmd_back_channel_data);
    }

  private:
    Semaphore              m_cmd_back_channel_fifo_sema{};
    MigCmdBackChannelFifo& m_mig_cmd_back_channel_fifo;
};

} // namespace RODOS::migration::implementation::transmitter
