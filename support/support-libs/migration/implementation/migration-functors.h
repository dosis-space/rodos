#pragma once

#include "migration/migratable-thread.h"
#include "migration/type-list.h"
#include "migration/implementation/storage/migration-storage-slot.h"

#include "rodos-assert.h"
#include "string_pico.h"

#include <type_traits>

namespace RODOS::migration::implementation {

/**
 * @brief Functor to get size of an index type in the TypeList.
 */
struct GetSizeOfTypeFunctor {
    /**
     * @param[out] size_out Output parameter filled with the size in byte of the indexed type.
     */
    template <typename T>
    static void func(uint32_t& size_out) {
        size_out = sizeof(T);
    }
};

/**
 * @brief Functor to reconstruct a migratable thread in a MigrationStorageSlot from a thread memcpy.
 */
template <typename TList, uint32_t MAX_THREAD_SIZE, uint32_t STACK_SIZE>
struct ReconstructFunctor {
    using ConcreteMigStorageSlot = storage::MigrationStorageSlot<TList, MAX_THREAD_SIZE, STACK_SIZE>;

    /**
     * @param[in] slot Reference to the MigrationStorageSlot to reconstruct the thread into.
     * @param[in] first_byte_migrated_bytes Reference to the first memcpy byte of the
     * migratable thread to be reconstructed.
     *
     * @warning There is no check whether first_byte_migrated_bytes actually holds a valid object
     * of type T. If it does not, a call to this functor will lead to undefined behavior.
     */
    template <typename T>
    static void func(ConcreteMigStorageSlot& slot, const uint8_t& first_byte_migrated_bytes) {
        static_assert(std::is_base_of<MigratableThread, T>::value);
        static_assert(std::is_copy_constructible_v<T>,
                      "Type in TypeList found that doesn't have a copy constructor!");

        const auto& user_thread = reinterpret_cast<const T&>(first_byte_migrated_bytes);
        slot.template createThread<T>(user_thread);
    }
};

/**
 * @brief Functor for copying a migratable thread (taking a raw memcpy of the whole thread).
 */
struct CopyFunctor {
    /**
     * @param[in] mig_thread Generic polymorphic reference to the thread to copy.
     * @param[out] first_byte_dest_buffer First byte of the destination buffer to copy to.
     * @param[out] bytes_written Output parameter indicating how many bytes were copied.
     */
    template <typename T>
    static void func(const MigratableThread& mig_thread,
                     uint8_t&                first_byte_dest_buffer,
                     uint32_t&               bytes_written) {
        static_assert(std::is_base_of<MigratableThread, T>::value);

        const auto& user_thread = static_cast<const T&>(mig_thread);
        const auto* byte_ptr    = reinterpret_cast<const uint8_t*>(&user_thread);
        RODOS::memcpy(&first_byte_dest_buffer, byte_ptr, sizeof(T));
        bytes_written = sizeof(T);
    }
};

/**
 * @brief Functor for destructing a migratable thread (calls the concrete destructor of the thread).
 */
struct DestructFunctor {
    /**
     * @param[in] mig_thread Generic polymorphic reference to the thread to destruct.
     */
    template <typename T>
    static void func(const MigratableThread& mig_thread) {
        static_assert(std::is_base_of<MigratableThread, T>::value);

        const auto& user_thread = static_cast<const T&>(mig_thread);
        user_thread.~T();
    }
};

} // namespace RODOS::migration::implementation
