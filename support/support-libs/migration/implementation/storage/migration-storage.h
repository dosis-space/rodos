#pragma once

#include "migration/migration-types.h"
#include "migration/implementation/storage/migration-storage-slot.h"

namespace RODOS::migration::implementation::storage {

/**
 * @brief Storage for all migratable threads on the node. Consisting of MigrationStorageSlots.
 */
template <typename TList, meta::MigrationStorageParams MIG_STORAGE_PARAMS>
class MigrationStorage {
  public:
    using ConcreteMigStorageSlot =
      MigrationStorageSlot<TList, MIG_STORAGE_PARAMS.max_thread_size, MIG_STORAGE_PARAMS.stack_size>;

    /// @warning not thread-safe!
    ConcreteMigStorageSlot* reserveEmptySlotUnprotected() {
        for(uint32_t i = 0; i < MIG_STORAGE_PARAMS.num_slots; i++) {
            if(m_slots[i].getStatus() == ConcreteMigStorageSlot::Status::EMPTY) {
                auto* slot_ptr = &m_slots[i];
                slot_ptr->occupy();
                return slot_ptr;
            }
        }
        return nullptr;
    }

    /// @warning not thread-safe!
    int findSlotIndexByTicketUnprotected(uint32_t ticket) {
        for(int i = 0; i < MIG_STORAGE_PARAMS.num_slots; i++) {
            if(m_slots[i].isValidlyOccupied()) {
                if(m_slots[i].getMigThreadPtr()->getTicket() == ticket) {
                    return i;
                }
            }
        }
        return -1;
    }

    ConcreteMigStorageSlot* getSlotPtrByIndex(int index) {
        if(index < 0 || index >= MIG_STORAGE_PARAMS.num_slots) {
            return nullptr;
        }
        return &m_slots[index];
    }


  private:
    ConcreteMigStorageSlot m_slots[MIG_STORAGE_PARAMS.num_slots]{};
};

} // namespace RODOS::migration::implementation::storage
