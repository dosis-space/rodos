#pragma once

#include "migration/migratable-thread.h"

#include "global-lists.h"
#include "misc-rodos-funcs.h"

#include <new>
#include <type_traits>

namespace RODOS::migration::implementation::storage {

/**
 * @brief StorageSlot for one migratable thread.
 *
 * Threads have a fixed maximum size for member variables and a fixed stack size.
 * Stacks of migratable threads are managed here and not in the thread itself to
 * reduce thread-copy overhead during migration.
 *
 * @warning This class and its methods are not thread-safe!
 */
template <typename TList, uint32_t MAX_THREAD_SIZE, uint32_t STACK_SIZE>
class MigrationStorageSlot {
  public:
    enum class Status {
        EMPTY,
        OCCUPIED,
    };

    Status            getStatus() const { return m_status; }
    int32_t           getTypeIndex() const { return m_type_index; }
    MigratableThread* getMigThreadPtr() const { return m_mig_thread_ptr; }

    bool isValidlyOccupied() const {
        return (m_status == Status::OCCUPIED) &&
               (m_type_index >= 0) &&
               (m_mig_thread_ptr != nullptr);
    }

    void occupy() {
        m_status = Status::OCCUPIED;
    }

    void free() {
        m_type_index     = -1;
        m_mig_thread_ptr = nullptr;
        m_status         = Status::EMPTY;
    }

    template <typename T, typename... Args>
    void createThread(Args&&... args) {
        static_assert(std::is_base_of<MigratableThread, T>::value);
        static_assert(sizeof(T) <= MAX_THREAD_SIZE);
        m_type_index = TList::template index_of<T>();

        T* t_ptr         = new(m_thread_buffer) T{ std::forward<Args>(args)... };
        m_mig_thread_ptr = static_cast<MigratableThread*>(t_ptr);
        m_mig_thread_ptr->initializeMigratableThread(m_stack, STACK_SIZE);
    }

  private:
    Status  m_status{ Status::EMPTY };
    int32_t m_type_index{ -1 };

    MigratableThread* m_mig_thread_ptr{ nullptr };
    uint8_t           m_thread_buffer[MAX_THREAD_SIZE];

    alignas(8) uint8_t m_stack[STACK_SIZE];
};

} // namespace RODOS::migration::implementation::storage
