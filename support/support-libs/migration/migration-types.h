#pragma once

#include "default-platform-parameter.h"
#include "timemodel.h"

#include <cstddef>
#include <cstdint>

namespace RODOS::migration {

namespace meta {
    struct MigrationStorageParams {
        uint8_t  num_slots{ 5 };
        uint32_t max_thread_size{ 500 };
        uint32_t stack_size{ DEFAULT_STACKSIZE };
    };

    struct MigrationTimeoutParams {
        int64_t ack_timeout{ 1500 * MILLISECONDS };
    };

    struct MigrationQueueParams {
        uint32_t num_elems_mig_recv_queue{ 3 };
        uint32_t num_elems_mig_cmd_queue{ 5 };
    };

    struct MigrationTopicIds {
        int64_t cmd_topic_id;
        int64_t data_topic_id;
        int64_t ack_topic_id;
        int64_t info_request_topic_id;
        int64_t info_topic_id;
    };
} // namespace meta

struct [[gnu::packed]] MigrationCmd {
    uint16_t sender_sys_hash{};
    uint16_t sender_instance_id{};

    uint16_t target_sys_hash{};
    uint16_t migrate_from_id{};
    uint16_t migrate_to_id{};

    uint16_t seq_num{};
    uint32_t ticket{};
};

struct CmdBackChannelData {
    uint8_t slot_index{};

    uint16_t sender_sys_hash{};
    uint16_t sender_instance_id{};

    uint16_t migrate_to_id{};

    uint16_t seq_num{};
    uint32_t ticket{};
};


template <uint32_t MAX_THREAD_SIZE>
struct [[gnu::packed]] MigrationData {
    uint16_t sys_hash{};
    uint16_t src_instance_id{};
    uint16_t dest_instance_id{};
    uint16_t pad{};

    int32_t  type_index{ -1 };
    uint32_t ticket{};

    uint32_t len{};
    uint8_t  bytes[MAX_THREAD_SIZE]{};
};
constexpr uint32_t MIG_DATA_SIZE_WITHOUT_BYTE_BUFFER = offsetof(MigrationData<1>, bytes);


enum class MigrationError : uint8_t {
    OK,
    TICKET_NOT_FOUND_ON_SRC_NODE,
    THREAD_ALREADY_MIGRATING_ON_SRC_NODE,
    INVALID_SLOT_ON_SRC_NODE,
    THREAD_COPY_ERROR,
    TRANSMITTER_ACK_TIMEOUT,
    THREAD_DESTRUCT_ERROR,
    THREAD_ALREADY_EXISTENT_ON_DEST_NODE,
    NO_EMPTY_SPACE_ON_DEST_NODE,
    INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE,
    THREAD_RECONSTRUCT_ERROR,
};
enum class AckType : uint8_t {
    RESPONSE_ACK,
    COMMAND_ACK
};
constexpr uint16_t EXPECTED_SEQ_NUM_FOR_RESPONSE_ACK = 0;
struct [[gnu::packed]] MigrationAck {
    uint16_t dest_sys_hash{};
    uint16_t dest_instance_id{};

    AckType        ack_type{ AckType::RESPONSE_ACK };
    MigrationError mig_error{};

    uint16_t seq_num{ EXPECTED_SEQ_NUM_FOR_RESPONSE_ACK };
};


struct [[gnu::packed]] MigrationInfoRequest {
    uint16_t sender_sys_hash{};
    uint16_t sender_instance_id{};

    uint16_t request_sys_hash{};
    uint16_t request_instance_id{};
};

struct [[gnu::packed]] MigrationSlotInfo {
    bool     is_valid{};
    uint32_t ticket{};
};

template <uint32_t NUM_SLOTS>
struct [[gnu::packed]] MigrationInfo {
    uint16_t requester_sys_hash{};
    uint16_t requester_instance_id{};

    uint8_t           num_slots{ NUM_SLOTS };
    MigrationSlotInfo mig_storage_info[NUM_SLOTS]{};
};
constexpr uint32_t MIG_INFO_SIZE_WITHOUT_STORAGE_INFO = offsetof(MigrationInfo<1>, mig_storage_info);

constexpr uint32_t MAX_MIG_INFO_NUM_SLOTS =
  (MAX_NETWORK_MESSAGE_LENGTH - MIG_INFO_SIZE_WITHOUT_STORAGE_INFO) / sizeof(MigrationSlotInfo);
using MaxMigInfo = MigrationInfo<MAX_MIG_INFO_NUM_SLOTS>;
using MinMigInfo = MigrationInfo<1>;

} // namespace RODOS::migration
