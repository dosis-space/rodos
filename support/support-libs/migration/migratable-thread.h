#pragma once

#include "migration/migration-types.h"

#include "thread.h"
#include "topic.h"
#include "timemodel.h"
#include "rodos-semaphore.h"

#include <atomic>

namespace RODOS::migration {

namespace implementation::storage {
    template <typename TList, uint32_t MAX_THREAD_SIZE, uint32_t STACK_SIZE>
    class MigrationStorageSlot;
}

namespace implementation::transmitter {
    template <typename TList,
              meta::MigrationStorageParams MIG_STORAGE_PARAMS,
              meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
    class MigrationTransmitterCmdSubscriber;
}

/**
 * @brief MigratableThread is a Thread that can be migrated via a MigrationSystem.
 * Any user-defined thread that wants to be migratable has to inherit from this.
 */
class MigratableThread : public Thread {
    template <typename TList, uint32_t MAX_THREAD_SIZE, uint32_t STACK_SIZE>
    friend class implementation::storage::MigrationStorageSlot;

    template <typename TList,
              meta::MigrationStorageParams MIG_STORAGE_PARAMS,
              meta::MigrationQueueParams   MIG_QUEUE_PARAMS>
    friend class implementation::transmitter::MigrationTransmitterCmdSubscriber;

  public:
    /**
     * @brief Initial creation constructor.
     *
     * @param[in] ticket The identifier of the thread within the migration system.
     * The user has to ensure that the ticket is unique between all threads within the migration
     * system the thread is created in.
     *
     * @param[in] name Name of the thread, same as in the normal Thread class.
     * @param[in] priority Thread priority, same as in the normal Thread class.
     */
    MigratableThread(uint32_t      ticket,
                     const char*   name,
                     const int32_t priority = DEFAULT_THREAD_PRIORITY)
      : Thread{ Thread::DontAppendFlag::DONT_APPEND, name, priority },
        m_ticket{ ticket } {}

    /**
     * @brief Reconstruction copy constructor.
     *
     * The copy constructor of the thread is used to reconstruct the thread on the new node from
     * a memcpy of the thread on the old node that is part of the MigrationData sent.
     *
     * In your user thread (inheriting from this) the trivial implicit copy constructor should
     * normally suffice to validly reconstruct your thread on a new node.
     * If you however need to do something more complicated during reconstruction (e.g. initialize
     * a reference to a global object) you can explicitly define a copy constructor in your
     * user thread and do the reconstruction manually (of course call this parent copy constructor
     * in it).
     *
     * @param[in] rhs A reference to the memcpy of the old thread.
     *
     * @warning Note that the rhs is not in a valid state, especially the vtable pointer will
     * most likely not be valid (so don't do anything to fancy with the rhs). Also note that
     * although the memory layout of an object should be quite similar between compilers,
     * it is not standardized (might be undefined when mixing compilers between nodes).
     */
    MigratableThread(const MigratableThread& rhs)
      : Thread{ Thread::DontAppendFlag::DONT_APPEND, rhs.getName(), rhs.getPriority() },
        m_ticket{ rhs.m_ticket } {}

    virtual ~MigratableThread() {
        this->removeAtRuntime(GlobalLists::get(GlobalListsEnum::THREAD_LIST));
    }

    /**
     * @brief The main thread function that is scheduled.
     *
     * Instead of the run() method of the normal Thread class, MigratableThread has the loop()
     * method that has to be implemented by each user thread. It is called internally in a loop,
     * i.e. when it returns it is called again (infinitely).
     *
     * The reason for this is that this migration system is cooperative, i.e. the thread has to
     * give up control cooperatively before the migration can be performed. This can be done by
     * calling checkAndYieldForMigration() manually or simply by returning from the loop() method.
     * Regardless of how control is yielded, reentry point after reconstruction on the new node is
     * always the beginning of the loop() method.
     */
    virtual void loop() = 0;

    uint32_t getTicket() const { return m_ticket; }


  protected:
    /**
     * @brief Manually yield control over the thread and allow migration to happen
     * if thread has already been marked.
     *
     * @return Returns 'true' if migration of this thread was pending,
     * was tried to perform and has failed (suspended itself and was resumed).
     * Returns 'false' if no migration was pending.
     */
    bool checkAndYieldForMigration() {
        ScopeProtector scope{ &m_mig_cmd_callback_sema };
        if(m_mig_cmd_callback_topic == nullptr) {
            return false;
        }
        m_mig_cmd_callback_topic->publish(m_throw_back_data, false);

        m_mig_cmd_callback_topic = nullptr;
        Thread::suspendCallerUntil();
        return true;
    }

  private:
    void initializeMigratableThread(uint8_t* stack_begin, uint32_t stack_size) {
        this->stackSize  = stack_size;
        this->stackBegin = reinterpret_cast<char*>(stack_begin);
        this->stack      = Thread::getFirstValidStackAddr(this->stackBegin, this->stackSize);
        this->initializeStack();

        if(isSchedulerRunning()) {
            this->appendAtRuntime(GlobalLists::get(GlobalListsEnum::THREAD_LIST));
        } else {
            this->append(GlobalLists::get(GlobalListsEnum::THREAD_LIST));
        }
        this->create();
    }

    bool markShouldMigrate(const CmdBackChannelData&  throw_back_data,
                           Topic<CmdBackChannelData>& mig_cmd_callback_topic) {
        ScopeProtector scope{ &m_mig_cmd_callback_sema };
        if(m_mig_cmd_callback_topic != nullptr) {
            return false;
        }
        m_throw_back_data        = throw_back_data;
        m_mig_cmd_callback_topic = &mig_cmd_callback_topic;
        return true;
    }

    void run() override {
        while(true) {
            loop();
            checkAndYieldForMigration();
        }
    }

    uint32_t m_ticket;

    Semaphore                  m_mig_cmd_callback_sema{};
    CmdBackChannelData         m_throw_back_data{};
    Topic<CmdBackChannelData>* m_mig_cmd_callback_topic{ nullptr };
};

} // namespace RODOS::migration
