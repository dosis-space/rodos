#include <iostream>

#include "clkgen_config.h"

void printXTALInfo(){
    std::cout << "XTAL mode: "
        << [](){
            switch(RODOS::clkgenConfig.xtalMode){
            case RODOS::XTAL_MODE::OFF:
                return "Off";
            case RODOS::XTAL_MODE::XTAL:
                return "Crystal";
            case RODOS::XTAL_MODE::EXT_CLK:
                return "External clock";
            }
         }()
        << "\n";
    if(RODOS::clkgenConfig.xtalMode!=RODOS::XTAL_MODE::OFF){
        std::cout << "XTAL frequency: " <<  RODOS::clkgenConfig.xtalFreq << "\n";
    }
    std::cout << "\n";
}

void printPLLInfo(){
    if(RODOS::clkgenConfig.usePLL){
        constexpr const RODOS::PLLSetup& pllSetup { RODOS::globalClockSetup.getPllSetup() };
        std::cout << "PLL enabled\n";
        std::cout << "PLL input frequency: " << RODOS::clkgenConfig.xtalFreq << "\n";
        std::cout << "PLL refence clock divider(NR): " << pllSetup.m_NR << "\n";
        std::cout << "PLL VCO input frequency: " << pllSetup.m_vcoInputFreq << "\n";
        std::cout << "PLL multiplication factor(NF): " << pllSetup.m_NF << "\n";
        std::cout << "PLL VCO output frequency: " << pllSetup.m_vcoOutputFreq << "\n";
        std::cout << "PLL output divider(OD): " << pllSetup.m_OD << "\n";
        std::cout << "PLL output frequency: " << pllSetup.m_frequency << "\n";
    }else{
        std::cout << "PLL not used\n";
    }
    std::cout << "\n";
}

void printSYSCLKInfo(){
    std::cout << "SYSCLK source: "
        << [](){
            switch(RODOS::clkgenConfig.sysclkSource){
            case RODOS::SYSCLK_SOURCE::HBO:
                return "HBO";
            case RODOS::SYSCLK_SOURCE::XTAL:
                return "XTAL(Crystal)";
            case RODOS::SYSCLK_SOURCE::PLL:
                return "PLL";
            case RODOS::SYSCLK_SOURCE::EXT_CLK:
                return "XTAL(External clock)";
            }
         }()
        << "\n";
    std::cout << "SYSCLK Frequency: " << RODOS::globalClockSetup.getSysclkFrequency() << "\n";
    std::cout << "APB1 Frequency: " << RODOS::globalClockSetup.getAPB1Frequency() << "\n";
    std::cout << "APB2 Frequency: " << RODOS::globalClockSetup.getAPB2Frequency() << "\n";
}

int main(){
    printXTALInfo();
    printPLLInfo();
    printSYSCLKInfo();
    return 0;
}
