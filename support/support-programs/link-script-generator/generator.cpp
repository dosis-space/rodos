#include <iostream>
#include <fstream>
#include <iomanip>

constexpr char FLASH_SIZE_MARKER[] = "%FLASH_SIZE%";
constexpr char RAM_SIZE_MARKER[]   = "%RAM_SIZE%";
constexpr char END_OF_RAM_MARKER[] = "%END_OF_RAM%";

int main(int argc, char** argv) {
    if(argc < 5) {
        std::cerr << "Usage: %s <input file> <output file> <flash size> <ram size>" << std::endl;
        exit(1);
    }
    std::ifstream linkScriptTemplateFile;
    std::ofstream linkScriptFile;
    linkScriptTemplateFile.open(argv[1]);
    linkScriptFile.open(argv[2]);
    std::string flashSize(argv[3]);
    std::string ramSize(argv[4]);

    std::string flashSizeMarker(FLASH_SIZE_MARKER);
    std::string ramSizeMarker(RAM_SIZE_MARKER);
    std::string endOfRamMarker(END_OF_RAM_MARKER);

    int               endOfRam = std::stoi(ramSize)*1024 + 0x20000000;
    std::stringstream stream;
    stream << std::hex;
    stream << "0x" << endOfRam;
    std::string endOfRamStr(stream.str());

    std::string line;
    if(linkScriptTemplateFile.is_open() && linkScriptFile.is_open()) {
        while(getline(linkScriptTemplateFile, line)) {
            std::size_t pos = line.find(FLASH_SIZE_MARKER);
            if(pos != std::string::npos) {
                line.replace(pos, flashSizeMarker.size(), flashSize);
            }

            pos = line.find(RAM_SIZE_MARKER);
            if(pos != std::string::npos) {
                line.replace(pos, ramSizeMarker.size(), ramSize);
            }

            pos = line.find(END_OF_RAM_MARKER);
            if(pos != std::string::npos) {
                line.replace(pos, endOfRamMarker.size(), endOfRamStr);
            }
            linkScriptFile << line << "\n";
        }
    } else {
        std::cerr << "Could not open input/output file" << std::endl;
        exit(2);
    }
    return 0;
}
