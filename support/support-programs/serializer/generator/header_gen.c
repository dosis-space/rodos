#include <string.h>
#include <stdio.h>

/**
 * Takes a list of <bitfield_def>.bf's as argument, outputs a header
 * on stdout containing all corresponding <bitfield_def>.h's as includes
 *
 * Example: header_gen some_bitfield.bf another_bitfield.bf
 * Output: 
 *
 * #pragma once
 *
 * #include "some_bitfield.h"
 * #include "another_bitfield.h"
 */

int main(int argc, char ** argv) {
	printf("\n#pragma once\n\n");
	for (int i = 1; i < argc; i++) {
		int len = strlen(argv[i]);
		if (len > 3) {
			argv[i][len - 2] = 'h';
			argv[i][len - 1] = '\0';
			int start = 0;
			for (int k = 0; k < len; k++)
				if(argv[i][k] == '/')
					start = k + 1;

			printf("#include \"%s\"\n", argv[i] + start);
		}
	}
}
