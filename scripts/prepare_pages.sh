#!/bin/bash

echo "Preparing pages in directory public"
echo "Current content:"
ls public
if [ -f "public/index.html" ]
then
    echo "index.html exists. Not toching anything."
    exit 0
fi

ARTIFACT_COUNT=$(ls -1 public | wc -l)
if  [ "$ARTIFACT_COUNT" -gt "1" ]
then
    cd public
    FILES=./*
    echo "<!DOCTYPE html><html><body><ul>" >index.html
    for f in $FILES
    do
        echo "<li><a href=\"${f}\">${f}</a></li>" >>index.html
    done
    echo "</ul></body></html>" >>index.html
    cd ..
    echo "public/index.html created for multiple subparts"
elif [ "$ARTIFACT_COUNT" -gt "0" ]
then
    FILE="public/$(ls -1 public)"
    if [ -d "${FILE}" ]
    then
        mv "${FILE}"/* public/.
    else
        echo "Only one not directory file found in public"
        echo "This might be valid, but no index.html exists or is generated for your pages!"
    fi
else
    echo "Nothing found that could be used for the pages!"
    exit 1 
fi