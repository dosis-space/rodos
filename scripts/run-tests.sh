#!/bin/bash

declare -a failed_tests

for test in $@; do
    echo "Executing Test: ${test}"
    $test
    if [ $? -ne 0 ];then
        failed_tests+=( "$test" )
    fi
done


num_failed_tests=${#failed_tests[@]}
if [ $num_failed_tests -eq 0 ];then
    echo "All tests successful"
    exit 0
else
    echo "$num_failed_tests failed"
    echo "List of failed Tests:"
    for failed_test in "${failed_tests[@]}";do
        echo "$failed_test"
    done
    exit 1
fi

