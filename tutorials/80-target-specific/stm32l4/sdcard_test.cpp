#include "thread.h"
#include "rodos-debug.h"
#include "storage/sdcard/sdcard_wrap.h"
#include "string_pico.h"

namespace rodos::support::sdcard {

/**
 *  @brief  This test is writing two different messages to specified blocks on an SDcard.
 *          On the console you will find the messages stored in those blocks, before and after writing them.        
 */
class SDWRTestThread : RODOS::StaticThread<> {
  public:
    SDWRTestThread(const char* name)
      : RODOS::StaticThread<>{ name } {}

    void init() override {
    }

    void run() override {
        int32_t err{ 0 };              //Error status during operations
        uint8_t content[data::BLOCKSIZE]; //Buffer for read data
        m_sdwp.sdInitSpi();            //setup SPI on sd card


        //READ OLD BLOCKS FIRST//
        for(auto block : TEST_BLOCK) {
            err = m_sdwp.sdReadBlock(content, block);
            RODOS::PRINTF("Read block %d\n", static_cast<int>(block));
            RODOS::printCharsHex("Read:\n", content, data::BLOCKSIZE);
            if(err != 0) {
                RODOS::PRINTF("Read-Error at block %ld\n", block);
            }
        }

        //WRITE NEW VALUES TO THE BLOCKS//
        RODOS::memcpy(m_buffer, MESSAGE, sizeof(MESSAGE));
        for(auto block : TEST_BLOCK) {
            RODOS::PRINTF("Write message1 to block %d\n", static_cast<int>(block));
            err = m_sdwp.sdWriteBlock(block, m_buffer, sizeof(m_buffer));
            if(err != 0) {
                RODOS::PRINTF("Write-Error at block %ld\n", block);
            }
        }

        //VERIFY, WHETHER THIS NEW VALUES HAVE BEEN CORRECTLY SET//
        for(auto block : TEST_BLOCK) {
            err = m_sdwp.sdReadBlock(content, block);
            RODOS::PRINTF("Read block %d\n", static_cast<int>(block));
            RODOS::printCharsHex("Read:\n", content, data::BLOCKSIZE);
            if(err != 0) {
                RODOS::PRINTF("Read-Error at block %ld\n", block);
            }
        }

        //WRITE COMPLETELY NEW VALUES//
        RODOS::memcpy(m_buffer, MESSAGE2, sizeof(MESSAGE2));
        for(auto block : TEST_BLOCK) {
            RODOS::PRINTF("Write message2 to block %d\n", static_cast<int>(block));
            err = m_sdwp.sdWriteBlock(block, m_buffer, sizeof(m_buffer));
            if(err != 0) {
                RODOS::PRINTF("Write-Error at block %ld\n", block);
            }
        }

        RODOS::PRINTF("\033[0m");
    }

  private:
    SDCardWrapper             m_sdwp{ getWrap() };
    static constexpr uint32_t TEST_BLOCK[] = { 74, 75 }; //Blocks to be read from and written to on the sdcard in this test
    static constexpr char     MESSAGE[]    = "##$$-- RODOS: Testing whether message can be successfully written and read from the sdcard --$$##";
    static constexpr char     MESSAGE2[]   = "Wellwell, whaddaya know. This block should have changed";
    uint8_t                   m_buffer[sizeof(MESSAGE) > sizeof(MESSAGE2) ? sizeof(MESSAGE) : sizeof(MESSAGE2)];

} test1{ "SD Test Thread 1" };

} // namespace rodos::support::sdcard
