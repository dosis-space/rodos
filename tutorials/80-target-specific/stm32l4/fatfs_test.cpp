#include "thread.h"
#include "rodos-debug.h"
#include "ff.h"
#include "storage/sdcard/sdcard_wrap.h"


namespace rodos::support::filesystem {

FATFS            FatFs; /* Work area (filesystem object) for logical drive */
constexpr size_t SIZE_READ{ 96 };
constexpr size_t SIZE_MESSAGE{ 64 }; //How many write iterations

static const char* dialogue{ "C: 'Hey, heyhey AUTO, what's that flashing button?'\nA: 'CaPtAIn, PRObE1 hAs ReTUrnEd POsiTiVE'\nC: 'Positive?'\n(e-va)\nC: 'But ... no probes ... ever came back positive ... before.'\nP: 'GreetingsAndCongratulationsCaptain,IfYouAreSeeingThis thatMeansYourExtraTerrestialVegetationEvaluator or *hehe* <EVE-Probe> hasReturnedFromEarthWithAConfirmedSpecimenOfOngoingPhotosynthesis ThatsRight ItMeansIt'sTimeToGoBackHome.'\nC: 'Home? We're going ... back?' \nP: 'NowThatEarthHasBeenReturnedToALifeSustainingStatus MyGolly WeCanBeginOperationRecolonize SimplyFollowThisManualsInstructionsToPlaceThePlantInYourShipsHoloDetector AndTheAXIOMWillImmediatelyNavigateYourReturnToEarth It'sThatEasy [...] SeeYouBackHomeReal'Soon'\nC:'Operate .. ManuAl. *ahem* <MANUAL, RELAY INSTRUCTIONS. .. MANUA-AL>. *gasp* Wow. Will you look at that!'\n...\n" };
constexpr size_t   DIALOGUE_LENGTH{ 824 };


size_t update_msg(char* buffer, size_t max_length) {
    static size_t pos{ 0 };
    size_t        msg_skip{ max_length };

    msg_skip = (DIALOGUE_LENGTH - pos < msg_skip) ? DIALOGUE_LENGTH - pos : msg_skip;

    RODOS::memcpy(buffer, (dialogue + pos), msg_skip);

    pos = (msg_skip + pos) % DIALOGUE_LENGTH;

    return msg_skip;
}

class FilesystemSDTest : RODOS::StaticThread<> {
  public:
    FilesystemSDTest(const char* name)
      : RODOS::StaticThread<>{ name } { /*do nothing*/
    }

    void init() override {
        rodos::support::sdcard::getWrap(); // To initialize static variables (not ideal)
    }

    void run() override {
        RODOS::PRINTF("Starting ...\n");
        char    readbuf[SIZE_READ + 1]{};
        char    msgbuf[SIZE_MESSAGE + 1]{};
        UINT    bytesWritten{ 0 };
        FRESULT status{ FR_OK };
        UINT    bytesRead{ 0 };
        FIL     file{};

        /****************************/
        /* Initialize               */
        /****************************/
        //f_mount
        RODOS::PRINTF("\nMount\n");
        status = f_mount(&FatFs, "/", 0);
        if(status) {
            RODOS::PRINTF("Error with mounting: %d\n", status);
        }

        //f_opendir
        status = f_mkdir("topic");

        //f_open
        status = f_open(&file, "/topic/hellowor.txt", FA_OPEN_APPEND | FA_WRITE | FA_READ);
        if(status) {
            RODOS::PRINTF("Error with opening: %d\n", status);
        }

        //iterate over message above
        RODOS::PRINTF("Writing\n");
        do {
            bytesWritten = static_cast<UINT>(update_msg(msgbuf, SIZE_MESSAGE));
            UINT written{ 0 };

            RODOS::PRINTF("Writing %s\n", msgbuf);
            status = f_write(&file, msgbuf, bytesWritten, &written);

            if(status) {
                RODOS::PRINTF("Error in writing %d\n", status);
            }
            RODOS::PRINTF("Written %d B.\n", written);
        } while(bytesWritten == SIZE_MESSAGE);

        //flush back the written data
        status = f_close(&file);
        if(status) {
            RODOS::PRINTF("Error on Closing, %d", status);
        }


        /****************************/
        /* Read a whole File        */
        /****************************/
        status = f_open(&file, "/topic/hellowor.txt", FA_READ);
        if(status) {
            RODOS::PRINTF("Error with opening: %d\n", status);
        }

        do {
            status = f_read(&file, readbuf, SIZE_READ, &bytesRead);
            if(status) {
                RODOS::PRINTF("Error in reading %d\n", status);
            }
            RODOS::PRINTF("\033[33m%s\033[0m", readbuf);
        } while(bytesRead == SIZE_READ);

        //flush back the written data
        status = f_close(&file);
        if(status) {
            RODOS::PRINTF("Error on Closing, %d", status);
        }


        RODOS::PRINTF("Finished Test Routine !!\n");
    }

} test1{ "Filesystem SD Test Thread 1" };
} // namespace rodos::support::filesystem
