/* RODOS includes */
#include "thread.h"
#include "rodos-debug.h"
#include "timemodel.h"
#include "misc-rodos-funcs.h"

class StatusRtcThread : RODOS::StaticThread<> {
public:
    StatusRtcThread(const char* name)
        : RODOS::StaticThread<> { name }
    {
        RODOS::Thread::setPeriodicBeat(0, 1 * RODOS::SECONDS);
    }

    void run() final
    {
        while (true) {
            int64_t time = RODOS::hwGetRTCTime();
            RODOS::PRINTF("current RTC time: %lld\n", time);
            printCalendarTime(time);
            RODOS::PRINTF("current RODOS time: %lld\n", RODOS::NOW());
            RODOS::Thread::suspendUntilNextBeat();
        }
    }

    void printCalendarTime(const int64_t time)
    {
        int32_t year{};
        int32_t month{};
        int32_t day{};
        int32_t hour{};
        int32_t min{};
        double  sec{};

        RODOS::TimeModel::localTime2Calendar(time, year, month, day, hour, min, sec);

        RODOS::PRINTF("current RTC calendar time: %04ld-%02ld-%02ldT%02ld:%02ld:%02f+02:00\n", year, month, day, hour, min, sec);
    }
} rtcThread2{ "RTC Status Thread" };
