#include "rodos.h"

// NUCLEO: on CN7, labeled SPI_B
HAL_SPI usedSPI{SPI_IDX0, GPIO_019, GPIO_020, GPIO_021, GPIO_004};

class FRAM_OPCODES {
public:
    static constexpr uint8_t RDID = 0b1001'1111;
    static constexpr uint8_t WRITE = 0b0000'0010;
    static constexpr uint8_t RDSR = 0b0000'0101;
    static constexpr uint8_t READ = 0b0000'0011;
    static constexpr uint8_t WREN = 0b0000'0110;
    static constexpr uint8_t WRSR = 0b0000'0001;
};


//convenience type for array initialization
struct [[gnu::packed]] FirmwareResetHandlerPtr {
    const uint32_t ptr = 195*4+1;
};

// the firmware to flash, hardcoded
struct [[gnu::packed]] Firmware {
    const uint32_t stackptr{0x2000'8000};
    const FirmwareResetHandlerPtr resetHandlerPtr{};
    const FirmwareResetHandlerPtr interruptHandlerPtrs[193]{};
    const uint8_t while1Opcode[2]{0xfe, 0xe7};
};

static_assert(FirmwareResetHandlerPtr{}.ptr == (offsetof(Firmware, while1Opcode)+1));


struct [[gnu::packed]] FirmwareWriteBuffer {
    const uint8_t command = FRAM_OPCODES::WRITE;
    const uint8_t address[3]{0,0,0};
    const Firmware firmware{};
};

struct [[gnu::packed]] FirmwareReadBuffer {
    uint8_t hiZ[4]{};
    uint8_t receivedFirmware[sizeof(Firmware)]{};
};


class VA41620FramFlasher : StaticThread<> {
public:
    VA41620FramFlasher(HAL_SPI& spi)
    : StaticThread<>{}
    , m_spi{spi}
    {}

private:
    void init() final {
        m_spi.init(625'000);
    }

    bool checkId(){
        constexpr uint8_t EXPECTED_ID[9] = {0x7F, 0x7F, 0x7F, 0x7F, 0x7F, 0x7F, 0xC2, 0x25, 0x08};
        uint8_t recBuf[10]{};
        m_spi.writeRead(&FRAM_OPCODES::RDID, sizeof(FRAM_OPCODES::RDID), recBuf, sizeof(recBuf));
        if(memcmp(recBuf+1, EXPECTED_ID, sizeof(EXPECTED_ID))!=0){
            PRINTF("Wrong ID. Expected: ");
            printCharsHex("", EXPECTED_ID, sizeof(EXPECTED_ID));
            PRINTF(" got: ");
            printCharsHex("", recBuf+1, sizeof(EXPECTED_ID));
            return false;
        }else{
            return true;
        }
    }

    uint8_t getStatusRegister(){
        uint8_t recBuf[2]{};
        m_spi.writeRead(&FRAM_OPCODES::RDSR, sizeof(FRAM_OPCODES::RDSR), recBuf, sizeof(recBuf));
        return recBuf[1];
    }

    void enableWriteLatch(){
        m_spi.write(&FRAM_OPCODES::WREN, sizeof(FRAM_OPCODES::WREN));
    }

    void writeStatusRegister(uint8_t value){
        const uint8_t writeBuf[2]{FRAM_OPCODES::WRSR, value};
        m_spi.write(writeBuf, sizeof(writeBuf));
    }

    void disableBlockProtect(){
        constexpr uint8_t writeprotectBits = 0b0000'1100;
        const uint8_t statusRegisterVal = getStatusRegister();
        PRINTF("Disabling block protection. Status Register before: %02x\n", statusRegisterVal);
        enableWriteLatch();
        writeStatusRegister(statusRegisterVal & ~writeprotectBits);
        PRINTF("Status register after: %02x\n", getStatusRegister());
    }


    bool flashFRAM(){
        enableWriteLatch();
        m_spi.write(&s_firmwareWriteBuffer, sizeof(s_firmwareWriteBuffer));

        constexpr uint8_t readCommand[4]{FRAM_OPCODES::READ, 0, 0, 0};
        m_spi.writeRead(readCommand, sizeof(readCommand), &m_firmwareReadBuffer, sizeof(m_firmwareReadBuffer));
        printCharsHex("Written Firmware: ", &m_firmwareReadBuffer.receivedFirmware, sizeof(Firmware));
        if(memcmp(&s_firmwareWriteBuffer.firmware, m_firmwareReadBuffer.receivedFirmware, sizeof(Firmware)) != 0){
            PRINTF("Error while reading back written firmware\n");
            return false;
        }else{
            return true;
        }
    }

    void run() final {
        if(!checkId()){
            return;
        }
        disableBlockProtect();
        constexpr int maxTries = 3;
        int tries = 0;
        do {
            if(flashFRAM()){
                PRINTF("Flashing succeeded\n");
                return;
            }else{
                tries++;
                PRINTF("Flashing failed\n");
            }
        }while(tries<maxTries);
    }

    HAL_SPI& m_spi;
    FirmwareReadBuffer m_firmwareReadBuffer{};
    static constexpr FirmwareWriteBuffer s_firmwareWriteBuffer{};
};

VA41620FramFlasher flasher{usedSPI};
