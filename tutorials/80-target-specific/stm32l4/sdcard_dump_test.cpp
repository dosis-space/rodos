#include "thread.h"
#include "rodos-debug.h"
#include "storage/sdcard/sdcard_wrap.h"
#include "string_pico.h"

namespace rodos::support::sdcard {

constexpr uint32_t START_BLOCK = 0;

/**
 *  @brief  This test is writing two different messages to specified blocks on an SDcard.
 *          On the console you will find the messages stored in those blocks, before and after writing them.        
 */
class SDDumpTest : RODOS::StaticThread<> {
  public:
    SDDumpTest(const char* name)
      : RODOS::StaticThread<>{ name } {}

    void init() override {
    }

    void run() override {
        int32_t err{ 0 };                 //Error status during operations
        uint8_t content[data::BLOCKSIZE]; //Buffer for read data
        m_sdwp.sdInitSpi();               //setup SPI on sd card


        //READ OLD BLOCKS FIRST//
        for(int i = START_BLOCK;; i++) {
            err = m_sdwp.sdReadBlock(content, i << 9);
            if(err != 0) {
                RODOS::PRINTF("Read-Error at block %d\n", static_cast<int>(i));
                RODOS::PRINTF("Error: %x", static_cast<int>(err));
                break;
            }
            RODOS::PRINTF("\033[32m(%d)\033[0m ", static_cast<int>(i));
            RODOS::printCharsHex("", content, data::BLOCKSIZE);

            RODOS::AT(RODOS::NOW() + 100 * RODOS::MILLISECONDS);
        }


        RODOS::PRINTF("\033[0m");
    }

  private:
    SDCardWrapper m_sdwp{ getWrap() };

} dumpTest{ "SD Dump Test Thread 1" };

} // namespace rodos::support::sdcard
