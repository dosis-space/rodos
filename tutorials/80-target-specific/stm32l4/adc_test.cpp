#include "hal/hal_adc.h"
#include "thread.h"
#include "rodos-debug.h"

constexpr size_t NUMBER_OF_ADCS { 3 };
constexpr size_t NUMBER_OF_CHANNELS { 19 };

RODOS::HAL_ADC adcs[NUMBER_OF_ADCS] {RODOS::ADC_IDX0, RODOS::ADC_IDX1, RODOS::ADC_IDX2};



/**
 * @brief contains the names for the ADC channels or nullptr if reserved or not enabled
 */
constexpr const char* CHANNEL_NAMES[NUMBER_OF_CHANNELS][NUMBER_OF_ADCS] { 
    { "V_REFIN", nullptr, nullptr },
    { "PC0", "PC0", "PC0" },
    { "PC1", "PC1", "PC1" },
    { "PC3", "PC3", "PC3" },
    { "PC4", "PC4", "PC4" },
    { "PA0", "PA0", nullptr },
    { "PA1", "PA1", "PF3" },
    { "PA2", "PA2", "PF4" },
    { "PA3", "PA3", "PF5" },
    { "PA4", "PA4", "PF6" },
    { "PA5", "PA5", "PF7" },
    { "PA6", "PA6", "PF8" },
    { "PA7", "PA7", "PF9" },
    { "PC4", "PC4", "PF10" },
    { "PC5", "PC5", "DAC1" },
    { "PB0", "PB0", "DAC2" },
    { "PB1", "PB1", nullptr },
    { "V_Temperature", "DAC1", "V_Temperature" },
    { "VBAT/3", "DAC2", "VBAT/3" },
};


class ADCTestThread : RODOS::StaticThread<>{
    void run() final {
        for (auto& adc : adcs ){
            adc.init(RODOS::ADC_CH_000);
        }
        
        TIME_LOOP(0, 1*RODOS::SECONDS){
            RODOS::PRINTF("conversion at time: %lld\n", static_cast<long long>(RODOS::NOW()));
            for(size_t adcIdx = 0; adcIdx<NUMBER_OF_ADCS; adcIdx++){
                for(size_t adcChannel = 0; adcChannel<NUMBER_OF_CHANNELS; adcChannel++){
                    if(CHANNEL_NAMES[adcChannel][adcIdx]!=nullptr){
                        printAdcChannel(adcIdx, static_cast<RODOS::ADC_CHANNEL>(adcChannel));
                    }
                }
                RODOS::PRINTF("\n");
            }
            uint16_t temperatureSensorValue = adcs[0].read(RODOS::ADC_CH_017);
            float temperature = adcs[0].convertTemperatureSensorVoltageToTemperature(temperatureSensorValue);
            RODOS::PRINTF("Temperature: %fC, raw value: %u\n", temperature, temperatureSensorValue);
            RODOS::PRINTF("\n");

        }
    }

    static void printAdcChannel(size_t adcIdx, RODOS::ADC_CHANNEL channel){
        const uint16_t value { adcs[adcIdx].read(channel) };
        const float volts { convertToVolts(value) };
        RODOS::PRINTF("ADC %d, Ch %2d, %7s: %4uLSB = %1.4fV\n", adcIdx, static_cast<int>(channel), CHANNEL_NAMES[channel][adcIdx], value, volts);
    }

    static constexpr float convertToVolts(uint16_t value){
        constexpr float VREF { 3.3f };
        constexpr float conversionFactor { VREF / static_cast<float>(0xFFF) };
        return value*conversionFactor;
    }
};

ADCTestThread adcTestThread {};
