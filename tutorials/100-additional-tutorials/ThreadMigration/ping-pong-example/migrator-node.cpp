/**
 * @file
 * @brief Example showcasing the basic features of thread migration on simple thread classes.
 *
 * A dummy thread is commanded to migrate between two nodes in a ping-pong manner by a thread on an
 * external third node.
 *
 * This application is the migrator node of the example on which the MigratorThread runs that
 * issues the migration commands. As the node doesn't participate in the migration system
 * (it only issues commands to it), it only has a MigrationCommander object instantiated and not a
 * MigrationSystem object.
 *
 * @note The application has to be run together with the two other nodes (node0 and node1).
 * It should be started last (after starting node0 and node1).
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "threads/migrator-thread.h"

#include "application.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Ping Pong Example MigratorNode", APP_ID };

ConcreteMigCommander glob_mig_commander{
    MIG_MIGRATOR_SYS_NAME,
    MIG_MIGRATOR_SYS_HASH,
    MIGRATOR_NODE_INSTANCE_ID,
};

MigratorThread migrator_thread{};
