/**
 * @file
 * @brief Example showcasing the basic features of thread migration on simple thread classes.
 *
 * A dummy thread is commanded to migrate between two nodes in a ping-pong manner by a thread on an
 * external third node.
 *
 * This application is one of the two auxiliary nodes in the example (node1) between which the
 * dummy thread migrates.
 *
 * @note The application has to be run together with the two other nodes (node0 and migrator node).
 * It should be started before the migrator node.
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "application.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Ping Pong Example Node1", APP_ID };

ConcreteMigSys glob_mig_sys{
    MIG_STANDARD_SYS_NAME,
    MIG_STANDARD_SYS_HASH,
    NORMAL_NODE_INSTANCE_IDS[1]
};
