#pragma once

#include "gateway/gateway.h"
#include "gateway/linkinterfaceudp.h"

constexpr int32_t              UDP_PORT = -50'000;
inline RODOS::UDPInOut         udp{ UDP_PORT };
inline RODOS::LinkinterfaceUDP linkinterface_udp{ &udp };
inline RODOS::Gateway          gateway{ &linkinterface_udp, true };
