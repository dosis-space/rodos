#pragma once

#include <cstdint>

constexpr char     MIG_STANDARD_SYS_NAME[] = "MigrationSystem Standard: Ping Pong Example";
constexpr uint16_t MIG_STANDARD_SYS_HASH   = 0x1234;
constexpr unsigned NUM_NORMAL_NODES        = 2;
constexpr uint16_t NORMAL_NODE_INSTANCE_IDS[NUM_NORMAL_NODES]{ 0xAA, 0xBB };


constexpr char     MIG_MIGRATOR_SYS_NAME[]   = "MigrationSystem Migrator: Ping Pong Example";
constexpr uint16_t MIG_MIGRATOR_SYS_HASH     = 0xABCD;
constexpr uint16_t MIGRATOR_NODE_INSTANCE_ID = 0xFF;


constexpr uint32_t TICKET_USERTHREAD = 42;
