#pragma once

#include "threads/userthread.h"

#include "migration/type-list.h"
#include "migration/migration-types.h"
#include "migration/migration-system.h"


constexpr RODOS::migration::meta::MigrationTopicIds MIG_TOPIC_IDS{
    .cmd_topic_id          = 2000,
    .data_topic_id         = 2001,
    .ack_topic_id          = 2002,
    .info_request_topic_id = 2003,
    .info_topic_id         = 2004
};
constexpr RODOS::migration::meta::MigrationStorageParams MIG_STORAGE_PARAMS{
    .num_slots = 2
};
constexpr int64_t MIG_ACK_TIMEOUT            = 1500 * RODOS::MILLISECONDS;
constexpr int64_t STANDARD_COMMANDER_TIMEOUT = 2 * MIG_ACK_TIMEOUT;

using MyTypeListType = RODOS::migration::TypeList<UserThread>;

using ConcreteMigCommander = RODOS::migration::MigrationCommander<MIG_TOPIC_IDS>;

using ConcreteMigSys = RODOS::migration::MigrationSystem<MyTypeListType,
                                                         MIG_TOPIC_IDS,
                                                         MIG_STORAGE_PARAMS,
                                                         MIG_ACK_TIMEOUT>;
