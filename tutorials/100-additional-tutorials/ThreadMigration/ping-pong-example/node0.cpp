/**
 * @file
 * @brief Example showcasing the basic features of thread migration on simple thread classes.
 *
 * A dummy thread is commanded to migrate between two nodes in a ping-pong manner by a thread on an
 * external third node.
 *
 * This application is one of the two auxiliary nodes in the example (node0) between which the
 * dummy thread migrates. The dummy thread is initially instantiated in this thread.
 *
 * @note The application has to be run together with the two other nodes (node1 and migrator node).
 * It should be started before the migrator node.
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "initiator.h"
#include "application.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Ping Pong Example Node0", APP_ID };

ConcreteMigSys glob_mig_sys{
    MIG_STANDARD_SYS_NAME,
    MIG_STANDARD_SYS_HASH,
    NORMAL_NODE_INSTANCE_IDS[0]
};

class MigInitiator : public RODOS::Initiator {
  public:
    void init() override {
        glob_mig_sys.createThread<UserThread>(TICKET_USERTHREAD);
    }
} mig_initiator{};
