#include "threads/userthread.h"

#include "rodos-debug.h"
#include "timemodel.h"

UserThread::UserThread(uint32_t ticket)
  : MigratableThread(ticket, "UserThread") {}

UserThread::UserThread(const UserThread& rhs)
  : MigratableThread(rhs),
    m_x{ rhs.m_x } {
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[UserThread::UserThread] copy constructor call (ticket: 0x%x)!\n"
                  "" SCREEN_RESET,
                  (unsigned)getTicket());
}

UserThread::~UserThread() {
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[UserThread::~UserThread] destructor call (ticket: 0x%x)\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());
}

void UserThread::init() {
    RODOS::PRINTF(SCREEN_GREEN
                  "\n\t[UserThread::init] (ticket: 0x%x)!\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());
}
void UserThread::loop() {
    auto reactivationTime = RODOS::TimeModel::computeNextBeat(0, INCREMENT_INTERVAL, RODOS::NOW());
    Thread::suspendCallerUntil(reactivationTime);

    RODOS::PRINTF("[UserThread::loop] (ticket: 0x%x) looping - m_x: %d\n",
                  (unsigned)this->getTicket(),
                  m_x++);
}
