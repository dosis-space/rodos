#pragma once

#include "migration/migratable-thread.h"

class UserThread : public RODOS::migration::MigratableThread {
  public:
    static constexpr uint64_t INCREMENT_INTERVAL = 500 * RODOS::MILLISECONDS;

    explicit UserThread(uint32_t ticket);
    UserThread(const UserThread& rhs);
    ~UserThread() override;

    void init() final;
    void loop() final;

  private:
    int m_x{};
};
