#include "threads/migrator-thread.h"

#include "shared/mig-header.h"
#include "shared/mig-ids.h"

#include "migration/migration-types.h"

#include "rodos-debug.h"
#include "timemodel.h"

extern ConcreteMigCommander glob_mig_commander;

void MigratorThread::run() {
    while(true) {
        uint16_t migrate_from_id  = NORMAL_NODE_INSTANCE_IDS[m_current_node_index];
        unsigned other_node_index = (m_current_node_index + 1) % NUM_NORMAL_NODES;
        uint16_t migrate_to_id    = NORMAL_NODE_INSTANCE_IDS[other_node_index];

        getPrintInfo(migrate_from_id);

        executeMigration(migrate_from_id, migrate_to_id);
        m_current_node_index = other_node_index;

        getPrintInfo(migrate_from_id);

        RODOS::PRINTF("\n");

        auto reactivationTime =
          RODOS::TimeModel::computeNextBeat(0, MIGRATION_INCREMENT_INTERVAL, RODOS::NOW());
        Thread::suspendCallerUntil(reactivationTime);
    }
}

void MigratorThread::getPrintInfo(uint16_t request_id) {
    RODOS::PRINTF("requesting migrating info from id %x...\n", (unsigned)request_id);

    RODOS::migration::MigrationSlotInfo mig_storage_info[MIG_STORAGE_PARAMS.num_slots]{};
    int32_t                             diff_to_expected_size{};

    bool has_timeouted =
      glob_mig_commander.syncGetMigrationInfo(MIG_STANDARD_SYS_HASH,
                                              request_id,
                                              mig_storage_info,
                                              diff_to_expected_size,
                                              MIG_ACK_TIMEOUT);
    if(!has_timeouted) {
        RODOS::PRINTF("\tmigration info request has timeouted!\n");
        return;
    }

    RODOS::PRINTF("\tinfo for request_id %x - diff = %d\n",
                  (unsigned)request_id,
                  (int)diff_to_expected_size);
    for(auto& elem : mig_storage_info) {
        RODOS::PRINTF("\tis_valid: %u, ticket: 0x%x\n",
                      (unsigned)elem.is_valid,
                      (unsigned)elem.ticket);
    }
}

void MigratorThread::executeMigration(uint16_t migrate_from_id, uint16_t migrate_to_id) {
    RODOS::PRINTF("commanding migration of 0x%x from id 0x%x to id 0x%x...\n",
                  (unsigned)TICKET_USERTHREAD,
                  (unsigned)migrate_from_id,
                  (unsigned)migrate_to_id);

    using namespace RODOS::migration;
    MigrationError mig_error{};
    bool           has_retrieved_something =
      glob_mig_commander.syncCommandMigration(MIG_STANDARD_SYS_HASH,
                                              migrate_from_id,
                                              migrate_to_id,
                                              TICKET_USERTHREAD,
                                              mig_error,
                                              STANDARD_COMMANDER_TIMEOUT);
    if(!has_retrieved_something) {
        RODOS::PRINTF("\tmigration command timeouted!\n");
        return;
    }
    if(mig_error != MigrationError::OK) {
        RODOS::PRINTF("\tmigration failed with error: %u!\n", (unsigned)mig_error);
        return;
    }

    RODOS::PRINTF("\tmigrated thread (ticket: 0x%x) from id 0x%x to id 0x%x\n",
                  (unsigned)TICKET_USERTHREAD,
                  (unsigned)migrate_from_id,
                  (unsigned)migrate_to_id);
}
