#pragma once

#include "thread.h"

class MigratorThread : RODOS::StaticThread<> {
  public:
    static constexpr uint64_t MIGRATION_INCREMENT_INTERVAL = 5 * RODOS::SECONDS;

    void run() final;

  private:
    void getPrintInfo(uint16_t request_id);
    void executeMigration(uint16_t migrate_from_id, uint16_t migrate_to_id);

    uint16_t m_current_node_index = 0;
};
