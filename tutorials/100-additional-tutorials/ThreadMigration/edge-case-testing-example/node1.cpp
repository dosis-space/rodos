/**
 * @file
 * @brief Example testing edge cases of thread migration (e.g. erroneous migration requests).
 *
 * This application is an auxiliary node in the example to which the main migrator node tries to
 * migrate to (and usually should fail to).
 *
 * @note It has to be run together with the other two example application nodes (node0 and
 * migrator node). It should be started before the migrator node.
 *
 * @note To test the error handling of the MigrationSystem class two migration system objects are
 * instantiated with the same sys_hash (this is invalid). It is therefore expected behavior that a
 * RODOS error is thrown during initialization.
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "application.h"
#include "thread.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Error Testing Example Node1", APP_ID };


using namespace RODOS::migration;

constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{ .num_slots = NUM_SLOTS_NODE_1 };
using ConcreteMigSysNode1 = MigrationSystem<TypeListEmpty,
                                            MIG_TOPIC_IDS,
                                            MIG_STORAGE_PARAMS,
                                            MIG_ACK_TIMEOUT,
                                            MIG_QUEUE_PARAMS>;

ConcreteMigSysNode1 glob_mig_sys_0{ MIG_SYS_NAME, MIG_SYS_HASH, NODE_1_ID };
ConcreteMigSysNode1 glob_mig_sys_1{ MIG_SYS_NAME, MIG_SYS_HASH, NODE_1_ID };

class PrinterThread : public RODOS::StaticThread<> {
  public:
    void run() final {
        RODOS::PRINTF("\n[PrinterThread::run] the screen should be red!\n"
                      "-> two migration systems with the same sys_hash were instantiated!\n");
    }
} printer_thread{};
