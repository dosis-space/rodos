/**
 * @file
 * @brief Example testing edge cases of thread migration (e.g. erroneous migration requests).
 *
 * This application is the main node (migrator node) of the example on which the actual
 * tester thread (the MigratorThread) runs on.
 *
 * @note However it still has to run together with the other two example application nodes
 * (node0 and node1). It should be started last (after starting node0 and node1).
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "threads/migrator-thread.h"

#include "application.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Error Testing Example MigratorNode", APP_ID };


using namespace RODOS::migration;

constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{ .num_slots = NUM_SLOTS_NODE_MIGRATOR };
using ConcreteMigSysMigratorNode = MigrationSystem<TypeListDummyThread,
                                                   MIG_TOPIC_IDS,
                                                   MIG_STORAGE_PARAMS,
                                                   MIG_ACK_TIMEOUT,
                                                   MIG_QUEUE_PARAMS>;

ConcreteMigSysMigratorNode glob_mig_sys{ MIG_SYS_NAME, MIG_SYS_HASH, NODE_MIGRATOR_ID };
ConcreteMigCommander&      glob_mig_commander = glob_mig_sys;

class MigInitiator : public RODOS::Initiator {
  public:
    void init() override {
        glob_mig_sys.createThread<DummyThread>(TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR);
        glob_mig_sys.createThread<DummyThread>(TICKET_UNIQUE_NODE_MIGRATOR);
    }
} mig_initiator{};


MigratorThread migrator_thread{};
