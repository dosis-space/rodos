#pragma once

#include "gateway/gateway.h"
#include "gateway/linkinterfaceudp.h"

constexpr int32_t              UDP_PORT = -50'000;
inline RODOS::UDPInOut         udp{ UDP_PORT };
inline RODOS::LinkinterfaceUDP linkinterfaceUDP{ &udp };
inline RODOS::Gateway          gateway{ &linkinterfaceUDP, true };
