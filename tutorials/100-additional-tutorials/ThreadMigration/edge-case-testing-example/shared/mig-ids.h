#pragma once

#include <cstdint>

constexpr char     MIG_SYS_NAME[]   = "MigrationSystem: Error Testing Example";
constexpr uint16_t MIG_SYS_HASH     = 0x1234;
constexpr uint16_t NODE_MIGRATOR_ID = 0xAA;
constexpr uint16_t NODE_0_ID        = 0xBB;
constexpr uint16_t NODE_1_ID        = 0xCC;

constexpr uint16_t INVALID_SYS_HASH    = 0xFF'FF;
constexpr uint16_t INVALID_INSTANCE_ID = 0xFF'FF;


constexpr uint8_t NUM_SLOTS_NODE_MIGRATOR = 3;
constexpr uint8_t NUM_SLOTS_NODE_0        = 2;
constexpr uint8_t NUM_SLOTS_NODE_1        = 1;


constexpr uint32_t TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR = 0x1000;
constexpr uint32_t TICKET_UNIQUE_NODE_MIGRATOR           = 0x2000;
constexpr uint32_t TICKET_UNIQUE_NODE_0                  = 0x3000;
constexpr uint32_t INVALID_TICKET                        = 0xFF'FF'FF'FF;
