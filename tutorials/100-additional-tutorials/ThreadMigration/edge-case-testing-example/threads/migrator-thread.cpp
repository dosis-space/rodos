#include "threads/migrator-thread.h"

#include "shared/mig-header.h"
#include "shared/mig-ids.h"

#include "migration/migration-types.h"

#include "rodos-assert.h"
#include "rodos-debug.h"

extern ConcreteMigCommander& glob_mig_commander;

using namespace RODOS::migration;

void MigratorThread::run() {
    RODOS::PRINTF("[MigratorThread::run] "
                  "start of migration edge-case testing:\n\n\n");

    RODOS::PRINTF("[MigratorThread::run] "
                  "starting testMigCmdErrors()...\n");
    testMigrationCommandErrors();
    RODOS::PRINTF("[MigratorThread::run] "
                  "leaving testMigCmdErrors()!\n\n\n");

    RODOS::PRINTF("[MigratorThread::run] "
                  "starting testInterleavingAlreadyMigrating()...\n");
    testInterleavingAlreadyMigrating();
    RODOS::PRINTF("[MigratorThread::run] "
                  "leaving testInterleavingAlreadyMigrating()!\n\n\n");

    RODOS::PRINTF("[MigratorThread::run] "
                  "starting testInterleavingNoAckRace()...\n");
    testInterleavingNoAckRace();
    RODOS::PRINTF("[MigratorThread::run] "
                  "leaving testInterleavingNoAckRace()!\n\n\n");

    RODOS::PRINTF("[MigratorThread::run] "
                  "end of migration edge-case testing! "
                  "If no RODOS errors where thrown, the tests were successful!\n"
                  "-> Stopping thread!\n");
}

/**
 * @brief Tries to trigger all possible migration errors by executing all kinds of erroneous
 * migration commands.
 *
 * @note Not included MigrationErrors are:
 * - THREAD_ALREADY_MIGRATING_ON_SRC_NODE: tested in testInterleavingNoAckRace() below
 * - INVALID_SLOT_ON_SRC_NODE: not reasonably testable (without smashing memory)
 * - THREAD_COPY_ERROR: not reasonably testable (without smashing memory)
 * - THREAD_DESTRUCT_ERROR: not reasonably testable (without smashing memory)
 * - THREAD_RECONSTRUCT_ERROR: not reasonably testable (without smashing memory)
 *
 * Additionally tests some other edge-cases like migrating to same node or
 * migrating to an invalid migration system (triggering a timeout on the commander side).
 */
void MigratorThread::testMigrationCommandErrors() {
    MigrationError mig_error{};

    // MigratorThread on nodeMigrator commands:
    // migration (node0 --[INVALID_TICKET]--> nodeMigrator):
    // -> ticket not found on source node
    RODOS::PRINTF("\t-> start subtest (node0 --[INVALID_TICKET]--> nodeMigrator): "
                  "expect TICKET_NOT_FOUND_ON_SRC_NODE...\n");
    RODOS_ASSERT(
      performSyncMigration(MIG_SYS_HASH, NODE_0_ID, NODE_MIGRATOR_ID, INVALID_TICKET, mig_error));
    RODOS_ASSERT(mig_error == MigrationError::TICKET_NOT_FOUND_ON_SRC_NODE);
    RODOS::PRINTF("\t-> end   subtest (node0 --[INVALID_TICKET]--> nodeMigrator): "
                  "expect TICKET_NOT_FOUND_ON_SRC_NODE!\n\n");

    // MigratorThread on nodeMigrator commands:
    // migration (node0 --[TICKET_UNIQUE_NODE_0]--> INVALID):
    // -> Ack timeout
    RODOS::PRINTF("\t-> start subtest (node0 --[TICKET_UNIQUE_NODE_0]--> INVALID): "
                  "expect TRANSMITTER_ACK_TIMEOUT...\n");
    RODOS_ASSERT(
      performSyncMigration(
        MIG_SYS_HASH, NODE_0_ID, INVALID_INSTANCE_ID, TICKET_UNIQUE_NODE_0, mig_error));
    RODOS_ASSERT(mig_error == MigrationError::TRANSMITTER_ACK_TIMEOUT);
    RODOS::PRINTF("\t-> end   subtest (node0 --[TICKET_UNIQUE_NODE_0]--> INVALID): "
                  "expect TRANSMITTER_ACK_TIMEOUT!\n\n");

    // MigratorThread on nodeMigrator commands:
    // migration (node0 --[TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR]--> nodeMigrator):
    // -> thread already existent on destination node
    RODOS::PRINTF("\t-> start subtest (node0 --[TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR]--> nodeMigrator): "
                  "expect THREAD_ALREADY_EXISTENT_ON_DEST_NODE...\n");
    RODOS_ASSERT(
      performSyncMigration(
        MIG_SYS_HASH, NODE_0_ID, NODE_MIGRATOR_ID, TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR, mig_error));
    RODOS_ASSERT(mig_error == MigrationError::THREAD_ALREADY_EXISTENT_ON_DEST_NODE);
    RODOS::PRINTF("\t-> end   subtest (node0 --[TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR]--> nodeMigrator): "
                  "expect THREAD_ALREADY_EXISTENT_ON_DEST_NODE!\n\n");

    // MigratorThread on nodeMigrator commands:
    // migration (nodeMigrator --[TICKET_UNIQUE_NODE_MIGRATOR]--> node0):
    // -> no empty space
    RODOS::PRINTF("\t-> start subtest (nodeMigrator --[TICKET_UNIQUE_NODE_MIGRATOR]--> node0): "
                  "expect NO_EMPTY_SPACE_ON_DEST_NODE...\n");
    RODOS_ASSERT(
      performSyncMigration(
        MIG_SYS_HASH, NODE_MIGRATOR_ID, NODE_0_ID, TICKET_UNIQUE_NODE_MIGRATOR, mig_error));
    RODOS_ASSERT(mig_error == MigrationError::NO_EMPTY_SPACE_ON_DEST_NODE);
    RODOS::PRINTF("\t-> end   subtest (nodeMigrator --[TICKET_UNIQUE_NODE_MIGRATOR]--> node0): "
                  "expect NO_EMPTY_SPACE_ON_DEST_NODE!\n\n");

    // MigratorThread on nodeMigrator commands:
    // migration (node0 --[TICKET_UNIQUE_NODE_0]--> node1):
    // -> wrong type index
    RODOS::PRINTF("\t-> start subtest (node0 --[TICKET_UNIQUE_NODE_0]--> node1): "
                  "expect INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE...\n");
    RODOS_ASSERT(
      performSyncMigration(
        MIG_SYS_HASH, NODE_0_ID, NODE_1_ID, TICKET_UNIQUE_NODE_0, mig_error));
    RODOS_ASSERT(mig_error == MigrationError::INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE);
    RODOS::PRINTF("\t-> end   subtest (node0 --[TICKET_UNIQUE_NODE_0]--> node1): "
                  "expect INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE!\n\n");

    // MigratorThread on nodeMigrator commands:
    // migration (node0 --[TICKET_UNIQUE_NODE_0]--> node0):
    // -> actually: thread already existent on destination node
    // -> however: Because of the transmitter blocking the receiver, the transmitter timeouts while
    // waiting for the Response Ack (this is then propagated to the migration commander).
    // When the Response Ack finally arrives, it is silently dropped (Ack session already closed).
    RODOS::PRINTF("\t-> start subtest (node0 --[TICKET_UNIQUE_NODE_0]--> node0): "
                  "expect TRANSMITTER_ACK_TIMEOUT...\n");
    RODOS_ASSERT(
      performSyncMigration(
        MIG_SYS_HASH, NODE_0_ID, NODE_0_ID, TICKET_UNIQUE_NODE_0, mig_error));
    RODOS_ASSERT(mig_error == MigrationError::TRANSMITTER_ACK_TIMEOUT);
    RODOS::PRINTF("\t-> end   subtest (node0 --[TICKET_UNIQUE_NODE_0]--> node0): "
                  "expect TRANSMITTER_ACK_TIMEOUT!\n\n");

    // MigratorThread on nodeMigrator commands:
    // migration with wrong sys_hash (node0 --[TICKET_UNIQUE_NODE_0]--> nodeMigrator):
    // -> wait timeout on commanding side
    RODOS::PRINTF("\t-> start subtest (node0 --[TICKET_UNIQUE_NODE_0]--> nodeMigrator): "
                  "expect commander-side timeout...\n");
    RODOS_ASSERT(
      !performSyncMigration(
        INVALID_SYS_HASH, NODE_0_ID, NODE_MIGRATOR_ID, TICKET_UNIQUE_NODE_0, mig_error));
    RODOS::PRINTF("\t-> end   subtest (node0 --[TICKET_UNIQUE_NODE_0]--> nodeMigrator): "
                  "expect commander-side timeout!\n");
}

/**
 * @brief Test sending multiple migration requests for same thread ("interleaving" requests).
 *
 * Only the first request should be executed, the second request should fail with a
 * THREAD_ALREADY_MIGRATING_ON_SRC_NODE error.
 * In this case the first request should timeout (so migration takes a while and second request
 * arrives while the first request is still being served) and not lead to any valid migration.
 */
void MigratorThread::testInterleavingAlreadyMigrating() {
    // request1 of MigratorThread on nodeMigrator:
    // migration (node0 --[TICKET_UNIQUE_NODE_0]--> INVALID):
    // -> should be: Ack timeout
    RODOS::PRINTF("\t-> request 1 (node0 --[TICKET_UNIQUE_NODE_0]--> nodeMigrator): "
                  "expect TRANSMITTER_ACK_TIMEOUT\n");
    glob_mig_commander.requestMigration(
      MIG_SYS_HASH, NODE_0_ID, INVALID_INSTANCE_ID, TICKET_UNIQUE_NODE_0);

    // request2 of MigratorThread on nodeMigrator:
    // migration (node0 --[TICKET_UNIQUE_NODE_0]--> node1):
    // -> should be: invalid type index
    RODOS::PRINTF("\t-> request 2 (node0 --[TICKET_UNIQUE_NODE_0]--> nodeMigrator): "
                  "expect INVALID_MIG_DATA_RECEIVED_ON_DEST_NODE\n");
    glob_mig_commander.requestMigration(
      MIG_SYS_HASH, NODE_0_ID, NODE_1_ID, TICKET_UNIQUE_NODE_0);

    RODOS::PRINTF("\n");

    RODOS::PRINTF("\t-> start subtest (wait for THREAD_ALREADY_MIGRATING_ON_SRC_NODE and timeout)...\n");
    MigrationError mig_error{};
    RODOS_ASSERT(
      glob_mig_commander.waitForMigrationResponse(mig_error, STANDARD_COMMANDER_TIMEOUT));
    RODOS_ASSERT(mig_error == MigrationError::THREAD_ALREADY_MIGRATING_ON_SRC_NODE);
    RODOS_ASSERT(
      !glob_mig_commander.waitForMigrationResponse(mig_error, STANDARD_COMMANDER_TIMEOUT));
    RODOS::PRINTF("\t-> end   subtest (wait for THREAD_ALREADY_MIGRATING_ON_SRC_NODE and timeout)!\n\n");

    // check if request1 (should have timeouted due to invalid destination) didn't migrate validly
    RODOS::PRINTF("\t-> start subtest (check request 1 failed as expected)...\n");
    MigrationSlotInfo info_array[NUM_SLOTS_NODE_0]{};
    int32_t           diff_to_expected{};
    RODOS_ASSERT(
      glob_mig_commander.syncGetMigrationInfo(MIG_SYS_HASH,
                                              NODE_0_ID,
                                              info_array,
                                              diff_to_expected,
                                              STANDARD_COMMANDER_TIMEOUT));
    RODOS_ASSERT(diff_to_expected == 0);
    RODOS_ASSERT(info_array[0].is_valid);
    RODOS_ASSERT(info_array[0].ticket == TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR);
    RODOS_ASSERT(info_array[1].is_valid);
    RODOS_ASSERT(info_array[1].ticket == TICKET_UNIQUE_NODE_0);
    RODOS::PRINTF("\t-> end   subtest (check request 1 failed as expected)!\n");
}

/**
 * @brief Test the guarantee of the MigrationCommander that waiting for a migration response will
 * really just wait for a response to the newest request.
 *
 * This is done by creating a queue of command requests (clogging the queue with a request
 * that timeouts) and waiting for a response while the command queue is simultaneously processed.
 * Although responses related to requests not issued last arrive while waiting, only the response
 * related to the latest request should be accepted by the wait command.
 */
void MigratorThread::testInterleavingNoAckRace() {
    // first request of MigratorThread on nodeMigrator to clog command queue:
    // migration (node0 --[TICKET_UNIQUE_NODE_0]--> INVALID):
    // -> should be: Ack timeout
    RODOS::PRINTF("\t-> initial queue clogging request (node0 --[TICKET_UNIQUE_NODE_0]--> INVALID): "
                  "expect TRANSMITTER_ACK_TIMEOUT\n");
    glob_mig_commander.requestMigration(
      MIG_SYS_HASH, NODE_0_ID, INVALID_INSTANCE_ID, TICKET_UNIQUE_NODE_0);

    RODOS::PRINTF("\t-> subsequent requests (node0 --[INVALID_TICKET]--> nodeMigrator): "
                  "expect TICKET_NOT_FOUND_ON_SRC_NODE\n");
    for(unsigned i = 1; i < MIG_QUEUE_PARAMS.num_elems_mig_cmd_queue; i++) {
        // subsequent requests of MigratorThread on nodeMigrator:
        // migration (node0 --[INVALID_TICKET]--> nodeMigrator):
        // -> should be: ticket not found
        glob_mig_commander.requestMigration(
          MIG_SYS_HASH, NODE_0_ID, NODE_MIGRATOR_ID, INVALID_TICKET);
    }

    // newest request of MigratorThread on nodeMigrator:
    // migration (node0 --[TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR]--> nodeMigrator):
    // -> should be: thread already existent
    RODOS::PRINTF("\t-> newest request (node0 --[INVALID_TICKET]--> nodeMigrator): "
                  "expect THREAD_ALREADY_EXISTENT_ON_DEST_NODE\n");
    glob_mig_commander.requestMigration(
      MIG_SYS_HASH, NODE_0_ID, NODE_MIGRATOR_ID, TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR);

    RODOS::PRINTF("\n");

    // only response to newest request should be received (THREAD_ALREADY_EXISTENT_ON_DEST_NODE)
    RODOS::PRINTF("\t-> start subtest (only newest request should be received)...\n");
    MigrationError mig_error{};
    RODOS_ASSERT(
      glob_mig_commander.waitForMigrationResponse(mig_error, STANDARD_COMMANDER_TIMEOUT));
    RODOS_ASSERT(mig_error == MigrationError::THREAD_ALREADY_EXISTENT_ON_DEST_NODE);
    RODOS::PRINTF("\t-> end   subtest (only newest request should be received)!\n");
}

bool MigratorThread::performSyncMigration(uint16_t        sys_hash,
                                          uint16_t        migrate_from_id,
                                          uint16_t        migrate_to_id,
                                          uint32_t        ticket,
                                          MigrationError& mig_error_out) {
    return glob_mig_commander.syncCommandMigration(
      sys_hash, migrate_from_id, migrate_to_id, ticket, mig_error_out, STANDARD_COMMANDER_TIMEOUT);
}
