#include "threads/dummy-thread.h"

#include "rodos-debug.h"
#include "timemodel.h"

DummyThread::DummyThread(uint32_t ticket)
  : MigratableThread(ticket, "DummyThread") {}

DummyThread::DummyThread(const DummyThread& rhs)
  : MigratableThread(rhs),
    m_x{ rhs.m_x } {
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[DummyThread::DummyThread] copy constructor call (ticket: 0x%x) - "
                  "m_x: %d!\n"
                  "" SCREEN_RESET,
                  (unsigned)getTicket(),
                  (int)m_x);
}

DummyThread::~DummyThread() {
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[DummyThread::~DummyThread] destructor call (ticket: 0x%x) - "
                  "m_x: %d!\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket(),
                  (int)m_x);
}

void DummyThread::init() {
    RODOS::PRINTF(SCREEN_GREEN
                  "\n\t[DummyThread::init] (ticket: 0x%x)!\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());
}
void DummyThread::loop() {
    auto reactivationTime = RODOS::TimeModel::computeNextBeat(0, INCREMENT_INTERVAL, RODOS::NOW());
    Thread::suspendCallerUntil(reactivationTime);

    m_x++;
}
