#pragma once

#include "migration/migratable-thread.h"

class DummyThread : public RODOS::migration::MigratableThread {
  public:
    static constexpr uint64_t INCREMENT_INTERVAL = 10 * RODOS::MILLISECONDS;

    explicit DummyThread(uint32_t ticket);
    DummyThread(const DummyThread& rhs);
    ~DummyThread() override;

    void init() final;
    void loop() final;

  private:
    int m_x{};
};
