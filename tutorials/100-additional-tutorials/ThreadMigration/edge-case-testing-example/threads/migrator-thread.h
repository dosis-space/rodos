#pragma once

#include "thread.h"

#include "migration/migration-types.h"

/**
 * @brief Actual tester thread executed on the MigratorNode that runs edge-case tests.
 *
 * @note Tests may take a while because some errors that shall be triggered by the test
 * are only thrown after a certain timeout.
 */
class MigratorThread : RODOS::StaticThread<> {
  public:
    void run() final;

  private:
    void testMigrationCommandErrors();
    void testInterleavingAlreadyMigrating();
    void testInterleavingNoAckRace();

    bool performSyncMigration(uint16_t sys_hash,
                              uint16_t migrate_from_id,
                              uint16_t migrate_to_id,
                              uint32_t ticket,
                              RODOS::migration::
                                MigrationError& mig_error_out);

    uint16_t m_current_node_index = 0;
};
