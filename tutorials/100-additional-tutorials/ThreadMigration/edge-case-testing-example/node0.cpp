/**
 * @file
 * @brief Example testing edge cases of thread migration (e.g. erroneous migration requests).
 *
 * This application is an auxiliary node in the example to which the main migrator node tries to
 * migrate to (and usually should fail to).
 *
 * @note It has to be run together with the other two example application nodes (node1 and
 * migrator node). It should be started before the migrator node.
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "initiator.h"
#include "application.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Error Testing Example Node0", APP_ID };


using namespace RODOS::migration;

constexpr meta::MigrationStorageParams MIG_STORAGE_PARAMS{ .num_slots = NUM_SLOTS_NODE_0 };
using ConcreteMigSysNode0 = MigrationSystem<TypeListDummyThread,
                                            MIG_TOPIC_IDS,
                                            MIG_STORAGE_PARAMS,
                                            MIG_ACK_TIMEOUT,
                                            MIG_QUEUE_PARAMS>;

ConcreteMigSysNode0 glob_mig_sys{ MIG_SYS_NAME, MIG_SYS_HASH, NODE_0_ID };

class MigInitiator : public RODOS::Initiator {
  public:
    void init() override {
        glob_mig_sys.createThread<DummyThread>(TICKET_NON_UNIQUE_NODE_0_AND_MIGRATOR);
        glob_mig_sys.createThread<DummyThread>(TICKET_UNIQUE_NODE_0);
    }
} mig_initiator{};
