#include "threads/publisher-thread.h"

#include "shared/glob-objects.h"
#include "shared/mig-header.h"
#include "shared/mig-ids.h"

#include "rodos-assert.h"
#include "timemodel.h"

extern ConcreteMigCommander& glob_mig_commander;

PublisherThread::PublisherThread(uint32_t ticket)
  : MigratableThread(ticket, "PublisherThread") {
    glob_publisher_count++;
}

PublisherThread::PublisherThread(const PublisherThread& rhs)
  : MigratableThread(rhs),
    m_x{ rhs.m_x } {
    glob_publisher_count++;
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[PublisherThread::PublisherThread] copy constructor call (ticket: 0x%x)!\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());
}

PublisherThread::~PublisherThread() {
    glob_publisher_count--;
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[PublisherThread::~PublisherThread] destructor call (ticket: 0x%x)!\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());
}

void PublisherThread::init() {
    RODOS::PRINTF("\n\t[PublisherThread::init] %s (ticket: 0x%x)!\n",
                  this->getName(),
                  (unsigned)this->getTicket());
}

void PublisherThread::loop() {
    glob_example_topic.publish(m_x);
    RODOS::PRINTF("[PublisherThread::loop] (ticket: 0x%x) - m_count: %u, published m_x: %d\n",
                  (unsigned)this->getTicket(),
                  (unsigned)m_count,
                  (int)m_x);

    m_count++;
    m_x++;

    if(m_count >= MIN_CYCLES_UNTIL_MIGRATION) {
        uint16_t migrate_from_id = glob_mig_commander.getInstanceId();
        uint16_t migrate_to_id{};
        for(unsigned i = 0; i < NUM_NODES; i++) {
            if((migrate_to_id = INSTANCE_IDS[i]) != migrate_from_id) {
                break;
            }
        }
        RODOS::PRINTF("[PublisherThread::loop] "
                      "requesting migration of ticket 0x%x from id 0x%x to id 0x%x...\n",
                      (unsigned)TICKET_PUBLISHER,
                      (unsigned)migrate_from_id,
                      (unsigned)migrate_to_id);
        glob_mig_commander.requestMigration(MIG_SYS_HASH,
                                            migrate_from_id,
                                            migrate_to_id,
                                            TICKET_PUBLISHER);
        RODOS_ASSERT(this->checkAndYieldForMigration());

        RODOS::migration::MigrationError mig_error{};
        RODOS_ASSERT(glob_mig_commander.waitForMigrationResponse(mig_error, 0));
        RODOS::PRINTF("[PublisherThread::loop] migration failed with error: %u!\n",
                      (unsigned)mig_error);
    }

    auto reactivationTime = RODOS::TimeModel::computeNextBeat(0, INCREMENT_INTERVAL, RODOS::NOW());
    Thread::suspendCallerUntil(reactivationTime);
}
