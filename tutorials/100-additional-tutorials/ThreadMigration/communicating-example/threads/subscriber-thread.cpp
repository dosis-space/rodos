#include "threads/subscriber-thread.h"

#include "migration/migration-types.h"
#include "shared/glob-objects.h"
#include "shared/mig-header.h"
#include "shared/mig-ids.h"

#include "rodos-assert.h"
#include "timemodel.h"

extern ConcreteMigCommander& glob_mig_commander;

SubscriberThread::SubscriberThread(uint32_t ticket)
  : MigratableThread(ticket, NAME),
    Subscriber(glob_example_topic, NAME) {
}

SubscriberThread::SubscriberThread(const SubscriberThread& rhs)
  : MigratableThread(rhs),
    Subscriber(Subscriber::DontAppendFlag::DONT_APPEND, glob_example_topic, NAME) {
    this->Subscriber::appendAtRuntime(glob_example_topic.mySubscribers);

    RODOS::PRINTF(SCREEN_MAGENTA
                  "[SubscriberThread::SubscriberThread] copy constructor call (ticket: 0x%x)\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());
}

SubscriberThread::~SubscriberThread() {
    RODOS::PRINTF(SCREEN_MAGENTA
                  "[SubscriberThread::~SubscriberThread] destructor call (ticket: 0x%x)\n"
                  "" SCREEN_RESET,
                  (unsigned)this->getTicket());

    this->Subscriber::removeAtRuntime(glob_example_topic.mySubscribers);
}

void SubscriberThread::init() {
    RODOS::PRINTF("\n\t[SubscriberThread::init] %s (ticket: 0x%x)\n",
                  this->MigratableThread::getName(),
                  (unsigned)this->getTicket());
}

void SubscriberThread::loop() {
    if(glob_publisher_count > 0) {
        uint16_t migrate_from_id = glob_mig_commander.getInstanceId();
        uint16_t migrate_to_id{};
        for(unsigned i = 0; i < NUM_NODES; i++) {
            migrate_to_id = INSTANCE_IDS[i];
            if(migrate_to_id != migrate_from_id) {
                break;
            }
        }
        RODOS::PRINTF("[SubscriberThread::loop] "
                      "requesting migration of ticket 0x%x from id 0x%x to id 0x%x...\n",
                      (unsigned)TICKET_SUBSCRIBER,
                      (unsigned)migrate_from_id,
                      (unsigned)migrate_to_id);
        glob_mig_commander.requestMigration(MIG_SYS_HASH,
                                            migrate_from_id,
                                            migrate_to_id,
                                            TICKET_SUBSCRIBER);
        RODOS_ASSERT(this->checkAndYieldForMigration());

        RODOS::migration::MigrationError mig_error{};
        RODOS_ASSERT(glob_mig_commander.waitForMigrationResponse(mig_error, 0));
        RODOS::PRINTF("[SubscriberThread::loop] migration failed with error: %u!\n",
                      (unsigned)mig_error);
    }
    Thread::suspendCallerUntil(RODOS::END_OF_TIME);
}

uint32_t SubscriberThread::put(const uint32_t topicId,
                               const size_t   len,
                               void*          data,
                               const RODOS::NetMsgInfo&) {
    RODOS_ASSERT_IFNOT_RETURN((topicId == glob_example_topic.topicId) && (len == sizeof(int)), 0);
    auto& x = *static_cast<int*>(data);
    RODOS::PRINTF("[SubscriberThread::put] thread (ticket: 0x%x) received - value: %d\n",
                  (unsigned)this->getTicket(),
                  (int)x);
    this->resume();
    return 1;
}
