#pragma once

#include "migration/migration-types.h"
#include "migration/migratable-thread.h"

#include "rodos-assert.h"
#include "rodos-debug.h"
#include "timemodel.h"
#include "topic.h"

/**
 * @brief The PublisherThread periodically publishes messages to a global Topic and periodically
 * requests to migrate itself to the other node of the example.
 *
 * @note The PublisherThread doesn't need an explicit Copy Constructor to re-access its
 * RODOS::Topic but uses it to notify a potential SubscriberThread on the new node that a
 * new PublisherThread has arrived.
 *
 * @note The PublisherThread uses the both the asynchronous migration request methods and the
 * cooperative yielding feature of the thread migration system to be able to command its
 * own migration (and recognize if the migration failed).
 */
class PublisherThread : public RODOS::migration::MigratableThread {
  public:
    explicit PublisherThread(uint32_t ticket);
    PublisherThread(const PublisherThread& rhs);
    ~PublisherThread() override;

    void init() override;
    void loop() override;

  private:
    static constexpr uint64_t INCREMENT_INTERVAL         = 500 * RODOS::MILLISECONDS;
    static constexpr unsigned MIN_CYCLES_UNTIL_MIGRATION = 8;

    int      m_x{};
    unsigned m_count{ 0 };
};
