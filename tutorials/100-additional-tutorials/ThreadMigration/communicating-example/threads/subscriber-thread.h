#pragma once

#include "migration/migratable-thread.h"

#include "subscriber.h"

/**
 * @brief The SubscriberThread is subscribed to a RODOS::Topic (that is published to by the
 * PublisherThread) and migrates itself away when a PublisherThread is migrated to its node.
 *
 * @note The SubscriberThread needs an explicit Copy Constructor to re-register its
 * RODOS::Subscriber to RODOS and an explicit Destructor to remove its RODOS::Subscriber
 * from the RODOS subscriber list.
 *
 * @note The SubscriberThread uses the both the asynchronous migration request methods and the
 * cooperative yielding feature of the thread migration system to be able to command its
 * own migration (and recognize if the migration failed).
 */
class SubscriberThread : public RODOS::migration::MigratableThread,
                         public RODOS::Subscriber {
  public:
    static constexpr const char* NAME = "SubscriberThread";

    explicit SubscriberThread(uint32_t ticket);
    SubscriberThread(const SubscriberThread& rhs);
    ~SubscriberThread() override;

    void init() override;
    void loop() override;

    uint32_t put(const uint32_t           topicId,
                 const size_t             len,
                 void*                    data,
                 const RODOS::NetMsgInfo& netMsgInfo) override;
};
