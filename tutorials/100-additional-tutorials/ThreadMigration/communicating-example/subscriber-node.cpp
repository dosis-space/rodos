/**
 * @file
 * @brief Example showcasing thread migration of more complicated thread classes that have to
 * re-allocate or re-access RODOS resources (e.g. a RODOS::Subscriber) on reconstruction.
 *
 * This application is the subscriber node of the example on which the SubscriberThread
 * (that is subscribed to a RODOS::Topic on which the PublisherThread periodically publishes)
 * is initially instantiated.
 * The SubscriberThread is periodically migrated between the to nodes of the example without loosing
 * its ability to receive messages on its subscribed RODOS::Topic.
 *
 * @note The application has to be run together with the publisher node and should be started
 * before the publisher node.
 */

#include "shared/mig-header.h"
#include "shared/mig-ids.h"
#include "shared/mig-gateway-object.h"

#include "initiator.h"
#include "application.h"

constexpr int      APP_ID = 2'000;
RODOS::Application app{ "Migration Communicating Example Subscriber Node", APP_ID };

ConcreteMigSys glob_mig_sys{
    MIG_SYS_NAME,
    MIG_SYS_HASH,
    INSTANCE_IDS[1]
};
ConcreteMigCommander& glob_mig_commander = glob_mig_sys;

class MigInitiator : public RODOS::Initiator {
  public:
    void init() override {
        glob_mig_sys.createThread<SubscriberThread>(TICKET_SUBSCRIBER);
    }
} mig_initiator{};
