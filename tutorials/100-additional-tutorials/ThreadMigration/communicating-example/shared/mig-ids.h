#pragma once

#include <cstdint>

constexpr char     MIG_SYS_NAME[] = "MigrationSystem Communicating Example";
constexpr uint16_t MIG_SYS_HASH   = 0x1234;

constexpr uint32_t TICKET_PUBLISHER  = 0x1111;
constexpr uint32_t TICKET_SUBSCRIBER = 0x2222;

constexpr unsigned NUM_NODES = 2;
constexpr uint16_t INSTANCE_IDS[NUM_NODES]{ 0xAA, 0xBB };
