#pragma once

#include "topic.h"

#include <atomic>

inline RODOS::Topic<int> glob_example_topic{ -1, "Global Example Topic" };
inline std::atomic<int>  glob_publisher_count{ 0 };
