#include "list-protected.h"

#include "application.h"
#include "thread.h"
#include "rodos-debug.h"

namespace {

class SpinOnBranchCtrExampleThread : public RODOS::StaticThread<> {
  public:
    SpinOnBranchCtrExampleThread()
      : RODOS::StaticThread<>{ "SpinOnBranchCtrExampleThread" } {}

    void run() override {
        RODOS::ListElement elem0{ "elem0" };
        elem0.appendAtRuntime(m_protectedList);
        RODOS::PRINTF("appended ListElement \"%s\" to m_protectedList\n",
                      elem0.getName());

        RODOS::ListElement elem1{ "elem1" };
        elem1.appendAtRuntime(m_protectedList);
        RODOS::PRINTF("appended ListElement \"%s\" to m_protectedList\n",
                      elem1.getName());

        RODOS::PRINTF("\n");
        {
            auto  iter      = m_protectedList.begin();
            auto& elemFirst = *iter;
            RODOS::PRINTF("removing ListElement \"%s\" from m_protectedList "
                          "while iterating on (unrelated) element \"%s\"\n"
                          "\t-> this should succeed\n...\n",
                          elem0.getName(),
                          elemFirst.getName());

            elem0.removeAtRuntime(m_protectedList);

            RODOS::PRINTF("done!\n");
        }

        RODOS::PRINTF("\n");
        {
            auto  iter      = m_protectedList.begin();
            auto& elemFirst = *iter;
            RODOS::PRINTF(SCREEN_YELLOW
                          "removing ListElement \"%s\" from m_protectedList "
                          "while iterating on (same) element \"%s\"\n"
                          "\t-> this should spinlock on branchCtr forever!\n"
                          "...\n" SCREEN_RESET,
                          elemFirst.getName(),
                          elemFirst.getName());

            elemFirst.removeAtRuntime(m_protectedList);
        }

        TIME_LOOP(RODOS::NOW(), 1 * RODOS::SECONDS) {
            RODOS::PRINTF(SCREEN_RED
                          "you should not see this "
                          "- operation didn't block!\n" SCREEN_RESET);
        }
    }

  private:
    RODOS::ProtectedList m_protectedList{};
};

RODOS::Application app{ "Application Example spin-on-branch_ctr" };

SpinOnBranchCtrExampleThread exampleThread{};

} // namespace
