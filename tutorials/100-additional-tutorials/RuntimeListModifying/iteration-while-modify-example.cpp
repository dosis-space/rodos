#include <cstdlib>
#include <time.h>

#include "list-protected.h"

#include "application.h"
#include "thread.h"
#include "rodos-debug.h"

namespace {

RODOS::ProtectedList globProtectedList{};

class ModifierThread : public RODOS::StaticThread<> {
  public:
    explicit ModifierThread(int64_t waitTime)
      : RODOS::StaticThread<>{ "ModifierThread" },
        m_waitTime{ waitTime } {}

    void run() override {
        RODOS::ListElement elemArray[]{
            { "elem0" },
            { "elem1" },
            { "elem2" }
        };
        constexpr int NUM_ELEMS = sizeof(elemArray) / sizeof(elemArray[0]);

        while(true) {
            for(int i = 0; i < NUM_ELEMS; i++) {
                RODOS::PRINTF(
                  SCREEN_BRIGHT_GREEN
                  "\nModifierThread %d: "
                  "begin appending elemArray[%d]!\n" SCREEN_RESET,
                  (int)this->listElementID, i);
                elemArray[i].appendAtRuntime(globProtectedList);
                RODOS::PRINTF(
                  SCREEN_BRIGHT_GREEN
                  "ModifierThread %d: "
                  "end appending elemArray[%d]!\n" SCREEN_RESET,
                  (int)this->listElementID, i);

                RODOS::AT(RODOS::NOW() + m_waitTime);
            }

            for(int i = 0; i < NUM_ELEMS; i++) {
                RODOS::PRINTF(
                  SCREEN_YELLOW
                  "\nModifierThread %d: "
                  "begin removing elemArray[%d]!\n" SCREEN_RESET,
                  (int)this->listElementID, i);
                elemArray[i].removeAtRuntime(globProtectedList);
                RODOS::PRINTF(
                  SCREEN_YELLOW
                  "ModifierThread %d: "
                  "end removing elemArray[%d]!\n" SCREEN_RESET,
                  (int)this->listElementID, i);

                RODOS::AT(RODOS::NOW() + m_waitTime);
            }
        }
    }

  private:
    int64_t m_waitTime;
};

class IteratingThread : public RODOS::StaticThread<> {
  public:
    IteratingThread()
      : RODOS::StaticThread<>{ "IteratingThread" } {}

    void run() override {
        while(true) {
            unsigned count = 0;
            for([[maybe_unused]] auto& elem : globProtectedList) {
                count++;
            }

            if(count != m_numElemsInList) {
                RODOS::PRINTF(
                  "IteratingThread %2d: new number of elements in list noticed"
                  " (%u -> %u)\n",
                  (int)this->listElementID, m_numElemsInList, count);
                m_numElemsInList = count;
            }
        }
    }

  private:
    unsigned m_numElemsInList{ 0 };
};


RODOS::Application app{ "Application Example Runtime List Modifying" };

constexpr int64_t MODIFIER_WAIT_TIME = 300 * RODOS::MILLISECONDS;
ModifierThread    modifierThread{ MODIFIER_WAIT_TIME };

constexpr unsigned NUM_ITER_THREADS = 5;
IteratingThread    iterThreads[NUM_ITER_THREADS]{};

} // namespace
