#include "application.h"
#include "default-platform-parameter.h"
#include "listelement.h"
#include "thread.h"

#include "rodos-assert.h"
#include "rodos-debug.h"

#include <new>

namespace RODOS::migration {
class MigratableThread : public Thread {
  public:
    MigratableThread(uint8_t* stack_begin, uint32_t stack_size)
      : Thread(Thread::DontAppendFlag::DONT_APPEND) {
        this->stackSize  = DEFAULT_STACKSIZE;
        this->stackBegin = reinterpret_cast<char*>(stack_begin);
        this->stack      = Thread::getFirstValidStackAddr(this->stackBegin, this->stackSize);
        this->initializeStack();

        this->appendAtRuntime(GlobalLists::get(GlobalListsEnum::THREAD_LIST));
        this->create();
    }
};
} // namespace RODOS::migration

namespace {

class UserThread : public RODOS::migration::MigratableThread {
  public:
    UserThread(int x, const char* name)
      : MigratableThread(m_stack, sizeof(m_stack)),
        m_x{ x } {
        RODOS::PRINTF(
          SCREEN_MAGENTA
          "constructor %s (id = %ld)"
          " - appending %s to threadList!\n" SCREEN_RESET,
          this->getName(), (long)this->listElementID, this->getName());
    }

    ~UserThread() override {
        RODOS::PRINTF(
          SCREEN_MAGENTA
          "destructor %s (id = %ld)"
          " - removing %s from threadList!\n" SCREEN_RESET,
          this->getName(), (long)this->listElementID, this->getName());

        this->removeAtRuntime(RODOS::GlobalLists::get(RODOS::GlobalListsEnum::THREAD_LIST));
    }

    void init() override {
        RODOS::PRINTF(SCREEN_GREEN "init() - %s (id = %ld)!\n" SCREEN_RESET,
                      this->getName(), (long)this->listElementID);
    }
    void run() override {
        TIME_LOOP(RODOS::NOW(), 300 * RODOS::MILLISECONDS) {
            RODOS::PRINTF("%s (id = %ld) - looping - m_x: %d\n",
                          this->getName(), (long)this->listElementID, m_x++);
        }
    }

  private:
    alignas(8) uint8_t m_stack[DEFAULT_STACKSIZE];
    int m_x;
};

class InjectorThread : public RODOS::StaticThread<> {
  public:
    InjectorThread(int64_t wait_time, int val0, int val1)
      : RODOS::StaticThread<>{ "InjectorThread" },
        m_wait_time{ wait_time },
        m_val0{ val0 },
        m_val1{ val1 } {}

    void init() {
        RODOS::PRINTF(SCREEN_YELLOW
                      "init() - %s (id = %ld)\n" SCREEN_RESET,
                      this->getName(), (long)this->listElementID);
    }

    void constructObject(void* addr, int val, const char* name) {
        new(addr) UserThread{ val, name };
        static_cast<UserThread*>(addr)->init();
    }

    void deconstructObject(void* addr) {
        static_cast<UserThread*>(addr)->~UserThread();
    }

    void run() {
        // construct all objects
        RODOS::PRINTF(SCREEN_YELLOW
                      "%s (id = %ld): "
                      "injecting \"%s\" in buffer0!\n" SCREEN_RESET,
                      this->getName(),
                      (long)this->listElementID,
                      NAME_FIRST_THREAD);
        constructObject(buffer0, m_val0, NAME_FIRST_THREAD);

        RODOS::AT(RODOS::NOW() + m_wait_time);

        RODOS::PRINTF(SCREEN_YELLOW
                      "%s (id = %ld): "
                      "injecting \"%s\" in buffer1!\n" SCREEN_RESET,
                      this->getName(),
                      (long)this->listElementID,
                      NAME_SECOND_THREAD);
        constructObject(buffer1, m_val1, NAME_SECOND_THREAD);


        // wait a while
        RODOS::AT(RODOS::NOW() + m_wait_time);


        // deconstruct all objects
        RODOS::PRINTF(SCREEN_YELLOW
                      "%s (id = %ld): "
                      "removing \"%s\" from buffer0!\n" SCREEN_RESET,
                      this->getName(),
                      (long)this->listElementID,
                      NAME_FIRST_THREAD);
        deconstructObject(buffer0);

        RODOS::AT(RODOS::NOW() + m_wait_time);

        RODOS::PRINTF(SCREEN_YELLOW
                      "%s (id = %ld): "
                      "removing \"%s\" from buffer1!\n" SCREEN_RESET,
                      this->getName(),
                      (long)this->listElementID,
                      NAME_SECOND_THREAD);
        deconstructObject(buffer1);


        // then just print something alone
        TIME_LOOP(RODOS::NOW(), 1 * RODOS::SECONDS) {
            RODOS::PRINTF(SCREEN_YELLOW
                          "%s (id = %ld): "
                          "looping alone!\n" SCREEN_RESET,
                          this->getName(), (long)this->listElementID);
        }
    }

  private:
    int64_t m_wait_time;
    int     m_val0, m_val1;

    static constexpr size_t BUFFER_SIZE = DEFAULT_STACKSIZE * 2;

    inline static const char* NAME_FIRST_THREAD  = "UserThread0";
    inline static const char* NAME_SECOND_THREAD = "UserThread1";

    uint8_t buffer0[BUFFER_SIZE]{};
    uint8_t buffer1[BUFFER_SIZE]{};
};

RODOS::Application app{ "Application Example Runtime Thread Injecting" };

constexpr auto INJECT_WAIT_TIME = 2 * RODOS::SECONDS;
constexpr int  START_VAL0       = 100;
constexpr int  START_VAL1       = 20000000;
InjectorThread injector{ INJECT_WAIT_TIME, START_VAL0, START_VAL1 };

} // namespace
