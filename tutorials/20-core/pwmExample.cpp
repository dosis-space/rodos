/*!
 * \file    pwmExample.cpp
 *
 * \date    30.10.2012
 * \author  Michael Ruffer
 *
 * \brief   To run this example you must connect e.g. an oscilloscope
 *          to the pwm pins according the hal_pwm.cpp of your target
 *          -> e.g. STM32F4 pin PB0 (Timer 3 Chan 3)
 */

#include "rodos.h"
#include "hal/hal_pwm.h"

constexpr int NUM_OF_PWM = 16;
HAL_PWM pwms[] = {
    HAL_PWM(PWM_IDX00), HAL_PWM(PWM_IDX01), HAL_PWM(PWM_IDX02), HAL_PWM(PWM_IDX03), 
    HAL_PWM(PWM_IDX04), HAL_PWM(PWM_IDX05), HAL_PWM(PWM_IDX06), HAL_PWM(PWM_IDX07), 
    HAL_PWM(PWM_IDX08), HAL_PWM(PWM_IDX09), HAL_PWM(PWM_IDX10), HAL_PWM(PWM_IDX11), 
    HAL_PWM(PWM_IDX12), HAL_PWM(PWM_IDX13), HAL_PWM(PWM_IDX14), HAL_PWM(PWM_IDX15)
};

class PWMExample: public Thread {
    uint64_t periode;
    HAL_PWM *pwms;
public:
    PWMExample(const char* name, HAL_PWM pwms[], uint64_t periode) :
            Thread(name) {
        this->periode = periode;
        this->pwms = pwms;
    }

    void init() {
        for (int i = 0; i < NUM_OF_PWM; i++){
            pwms[i].init(1000, 100);
        }
    }

    void run() {
        uint32_t pulseWidth = 0;
        bool toggleIncrements = false;

        TIME_LOOP(0,periode){

            if (pulseWidth == 0){
                pwms[2].config(PWM_PARAMETER_INCREMENTS, toggleIncrements ? 200 : 100);
                toggleIncrements = !toggleIncrements;
            }

            for(int i = 0; i < NUM_OF_PWM; i++) {
                pwms[i].write(pulseWidth);
            }

            pulseWidth += 10;
            if (pulseWidth >= 100) {
                pulseWidth = 0;
            }

        }
    }
};

PWMExample pwmThread("PWMExample", pwms, 500 * MILLISECONDS);
/***********************************************************************/
