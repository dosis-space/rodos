#include "hal/hal_resetreason.h"
#include "misc-rodos-funcs.h"
#include "rodos-debug.h"
#include "thread.h"

class ResetReasonTest : public RODOS::StaticThread<> {
    void run() override {
        RODOS::PRINTF("\nThe last reset was caused by one of:\n");

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::LOWPOWER>() != 0) {
            RODOS::PRINTF("low power\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::WWDG>() != 0) {
            RODOS::PRINTF("window watch dog\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::IWDG>() != 0) {
            RODOS::PRINTF("independent watchdog\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::SOFTWARE>() != 0) {
            RODOS::PRINTF("software\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::BROWNOUT>() != 0) {
            RODOS::PRINTF("brown-out\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::EXTERNAL>() != 0) {
            RODOS::PRINTF("external source\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::OBL>() != 0) {
            RODOS::PRINTF("obl\n");
        }

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::FIREWALL>() != 0) {
            RODOS::PRINTF("firewall\n");
        }

        RODOS::PRINTF("\n");

        RODOS::PRINTF("Setting up the Independent Watchdog with a timeout=3s\n");

        RODOS::hwInitWatchdog(3000);

        RODOS::PRINTF("Done, waiting 2.5s before triggering the watchdog\n");

        RODOS::AT(RODOS::NOW() + 2.5 * RODOS::SECONDS);

        RODOS::PRINTF("TriggerWatchdog, sleeping 2s\n");
        RODOS::hwTriggerWatchdog();

        RODOS::AT(RODOS::NOW() + 2 * RODOS::SECONDS);

        RODOS::PRINTF("Still alive.\n");

        if (RODOS::ResetReason::getReason().read<RODOS::RESET_REASON::SOFTWARE>() != 0) {
            RODOS::PRINTF("Last reset was caused by software, causing a reset by letting the iwdg expire:\n");

            RODOS::AT(RODOS::NOW() + 2 * RODOS::SECONDS);
        } else {
            RODOS::PRINTF("Last reset was not caused by software, causing a software reset:\n");

            RODOS::AT(RODOS::NOW() + 1 * RODOS::SECONDS);

            RODOS::hwResetAndReboot();
        }

        RODOS::PRINTF("We should not have reached this stage, something is wrong.\n");
        __builtin_unreachable();
    }
} resetReasonTest;
