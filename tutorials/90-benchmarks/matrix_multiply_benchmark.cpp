#include "thread.h"
#include "rodos-debug.h"
#include "timemodel.h"
#include <cstddef>


constexpr size_t TEST_MATRIX_SIZE { 50 };

struct TestMatrix {
    float data[TEST_MATRIX_SIZE][TEST_MATRIX_SIZE];
};

void multiplyMatrix(const TestMatrix& a, const TestMatrix& b, TestMatrix& result){
    for(size_t i=0; i<TEST_MATRIX_SIZE; i++){
        for(size_t j=0; j<TEST_MATRIX_SIZE; j++){
            result.data[i][j] = 0;
            for(size_t k=0;k<TEST_MATRIX_SIZE;k++){
                result.data[i][j] += a.data[i][k]*b.data[k][j];
            }
        }
    }
}

void ensureMatrixDoesntGetOptimizedOut(TestMatrix& matrix){
    asm volatile("" : "=m"(matrix) : "m"(matrix));
}

class MatrixMultiplyThread : RODOS::StaticThread<> {
public:
    MatrixMultiplyThread()
    : RODOS::StaticThread<>{}
    , a {}
    , b {}
    , result {}
    {
        initializeMatrices();
    }

private:
    TestMatrix a;
    TestMatrix b;
    TestMatrix result;
    void initializeMatrices(){
        for(size_t i=0; i<TEST_MATRIX_SIZE; i++){
            for(size_t j=0; j<TEST_MATRIX_SIZE; j++){
                a.data[i][j] = static_cast<float>(i)+7.0f*static_cast<float>(j);
                b.data[i][j] = 13.0f*static_cast<float>(i)-5.0f*static_cast<float>(j)+300.0f;
            }
        }
    }

    void run() override {
        while(true){
            int64_t startTime { RODOS::NOW() };
            constexpr size_t MULTIPLICATIONS_PER_MEASUREMENT { 100 };
            for(size_t i=0; i<MULTIPLICATIONS_PER_MEASUREMENT; i++){
                ensureMatrixDoesntGetOptimizedOut(a);
                ensureMatrixDoesntGetOptimizedOut(b);
                multiplyMatrix(a, b, result);
                ensureMatrixDoesntGetOptimizedOut(result);
            }
            int64_t endTime { RODOS::NOW() };
            RODOS::PRINTF("Multiplication time: %lldus\n", static_cast<long long>((endTime-startTime)/RODOS::MICROSECONDS));
            RODOS::AT(RODOS::NOW()+2*RODOS::SECONDS);
        }
    }
} matrixMultiplyThread;
